<?php

global $_LANGPDF;
$_LANGPDF = array();
$_LANGPDF['PDF03ab340b3f99e03cff9e84314ead38c0'] = 'Qtà';
$_LANGPDF['PDF065ab3a28ca4f16f55f103adc7d0226f'] = 'Spedizionef';
$_LANGPDF['PDF068f80c7519d0528fb08e82137a72131'] = 'Prodotti';
$_LANGPDF['PDF28a59051cd90053f87bacd5f1ffbc0b8'] = 'Indirizzo di Fatturazione';
$_LANGPDF['PDF2f2f0f119a907c6c67a3c6fcde0193ab'] = 'Indirizzo di Consegna';
$_LANGPDF['PDF440ac606343b8a66acad0d2978786d2a'] = 'Data Fattura';
$_LANGPDF['PDF466eadd40b3c10580e3ab4e8061161ce'] = 'Fattura';
$_LANGPDF['PDF4e065ba1bec1d62b2d5450256612fa96'] = 'Numero Fattura';
$_LANGPDF['PDF559e7ca805230fc80e3644f87bb3994d'] = 'Data Ordine';
$_LANGPDF['PDF707436a5aa13b82a4d777f64c717a625'] = 'Metodo di Pagamento';
$_LANGPDF['PDFdeb10517653c255364175796ace3553f'] = 'Prodotto';
$_LANGPDF['PDFea9cf7e47ff33b2be14e6dd07cbcefc6'] = 'Spedizione';

?>