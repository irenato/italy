<div class="content_sortPagiBar clearfix">
	<div class="sortPagiBar clearfix row">					
			<div class="col-md-8 col-sm-8 col-xs-6">				
				<div class="sort">
				{include file="$tpl_dir./product-sort.tpl"}
				{include file="$tpl_dir./nbr-product-page.tpl"}									
				</div>
			</div>
			<div class="product-compare col-md-4 col-sm-4 col-xs-6">
				{include file="$tpl_dir./product-compare.tpl"}
				<div class="display hidden-xs pull-right">
					<!--<span class="display-title">{l s='View:'}</span>-->
					<div id="grid"><a rel="nofollow" href="#" title="{l s='Grid'}"><i class="fa fa-th-large"></i></a></div>
					<div id="list"><a rel="nofollow" href="#" title="{l s='List'}"><i class="fa fa-th-list"></i></a></div>
				</div>
				
			</div>
    </div>
</div>

{include file="$tpl_dir./product-list.tpl" products=$products}

<div class="content_sortPagiBar">
	<div class="bottom-pagination-content clearfix row">
		<div class="col-md-12 col-sm-12 col-xs-12 custPag">
			{include file="$tpl_dir./pagination.tpl" no_follow=1 paginationId='bottom'}
		</div>
		{*<div class="product-compare col-md-2 col-sm-4 col-xs-4">
			{include file="$tpl_dir./product-compare.tpl" paginationId='bottom'}
		</div>*}
	</div>
</div>