{$count = 0}
{foreach from=$products item=product}
 <div class="this_month {if ($count++)%2 != 0}product_detail{/if} ">
    <div class="row">        
      <div class="col-md-8 flot_right col-sm-8 col-xs-12"> 
        <div class="product_section">
          <div class="col-md-6 product_right col-sm-6 col-xs-6">
            <div class="leather_product">
              {if Context::getContext()->language->id==1}
                {$my_param='en'}
              {/if}
              {if Context::getContext()->language->id==2}
                {$my_param='it'}
              {/if}
              <a href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" hreflang={$my_param}><img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default_fashion')|escape:'html':'UTF-8'}" alt="{$product.name|escape:'html':'UTF-8'}"/></a>
            </div>
          </div>
          <div class="col-md-6 product_left col-sm-6 col-xs-6">
            <div class="product_content product-block">
              <h3>{$product.name}</h3>
      			  {capture name='displayProductListReviews'}{hook h='displayProductListReviews' product=$product}{/capture}
              {*
      				{if $smarty.capture.displayProductListReviews}
      					<div class="hook-reviews">
      					{hook h='displayProductListReviews' product=$product}
      					</div>
      				{/if}
              *}
              <div class="price">
              <p>
                {if ($product['id_category_default'] == 26)||($product['id_category_default'] == 27)||($product['id_category_default'] == 28)}
                {convertPrice price=$product['price']*$product['minimal_quantity']}
                {else}
              {if !$priceDisplay}
              {convertPrice price=$product.price}
              {else}{convertPrice price=$product.price_tax_exc}
              {/if}
              {/if}
              </p>
              </div>
            </div>
            <div class="shop_now">
              <a href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" class="shopnow_btn" hreflang={$my_param}>Shop Now</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 flot_left col-sm-4 col-xs-12">
        <div class="discription">
          <h3>{$product.manufacturer_name|regex_replace:"/(<[\w\s-=\"']+>|<\/\w+>)/" : ""}</h3>
          <p>{$product.description|regex_replace:"/(<[\w\s-=\"']+>|<\/\w+>)/" : ""}</p>
        </div>
      </div>
    </div>
  </div>
  {/foreach}