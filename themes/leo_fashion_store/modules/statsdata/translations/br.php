<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsdata}leo_fashion_store>statsdata_a51950bf91ba55cd93a33ce3f8d448c2'] = 'A mineração de dados para as estatísticas';
$_MODULE['<{statsdata}leo_fashion_store>statsdata_c77dfd683d0d76940e5e04cb24e8bce1'] = 'Este módulo deve ser ativado se você quiser usar as estatísticas.';
$_MODULE['<{statsdata}leo_fashion_store>statsdata_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'Configuração atualizada';
$_MODULE['<{statsdata}leo_fashion_store>statsdata_f4f70727dc34561dfde1a3c529b6205c'] = 'Configurações';
$_MODULE['<{statsdata}leo_fashion_store>statsdata_1a5b75c4be3c0100c99764b21e844cc8'] = 'Salve visualizações de páginas para cada cliente';
$_MODULE['<{statsdata}leo_fashion_store>statsdata_73d55f0a158656cbfabab45a3e151d73'] = 'Armazenando página cliente vistas usa um monte de recursos de CPU e espaço de banco de dados. Apenas permitir que se o servidor pode lidar com isso.';
$_MODULE['<{statsdata}leo_fashion_store>statsdata_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Ativado';
$_MODULE['<{statsdata}leo_fashion_store>statsdata_b9f5c797ebbf55adccdd8539a65a0241'] = 'Inválido';
$_MODULE['<{statsdata}leo_fashion_store>statsdata_dd8289e220ec0de9a8b99adc7f9014b1'] = 'Salve vistas globais página';
$_MODULE['<{statsdata}leo_fashion_store>statsdata_339acfd90b82e91ce9141ec75e4bff24'] = 'Vista global página usa menos recursos do que clientes da, mas usa recursos, no entanto.';
$_MODULE['<{statsdata}leo_fashion_store>statsdata_c0e4406117ba4c29c4d66e3069ebf3d3'] = 'Detecção de Plugins';
$_MODULE['<{statsdata}leo_fashion_store>statsdata_e4af29282b3a403c2b23c2a516bba889'] = 'Detecção Plugins carrega um extra de 20 kb JavaScript arquivo uma vez para os novos visitantes.';
$_MODULE['<{statsdata}leo_fashion_store>statsdata_c9cc8cce247e49bae79f15173ce97354'] = 'Salvar';
