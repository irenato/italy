<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statssales}leo_fashion_store>statssales_45c4b3e103155326596d6ccd2fea0f25'] = 'Ventas y pedidos';
$_MODULE['<{statssales}leo_fashion_store>statssales_d2fb07753354576172a2b144c373a610'] = 'Añade gráficos que presentan la evolución de las ventas y las órdenes para el salpicadero Estadísticas.';
$_MODULE['<{statssales}leo_fashion_store>statssales_6602bbeb2956c035fb4cb5e844a4861b'] = 'Guía';
$_MODULE['<{statssales}leo_fashion_store>statssales_bdaa0cab56c2880f8f60e6a2cef40e63'] = 'Acerca de estados de pedido';
$_MODULE['<{statssales}leo_fashion_store>statssales_4b75384caa4e6830c22f15e06e0bfac0'] = 'Estos estados de pedido no se pueden quitar de la Oficina Virtual; Sin embargo usted tiene la opción de añadir más.';
$_MODULE['<{statssales}leo_fashion_store>statssales_5cc6f5194e3ef633bcab4869d79eeefa'] = 'Sólo órdenes válidas se representan gráficamente.';
$_MODULE['<{statssales}leo_fashion_store>statssales_c3987e4cac14a8456515f0d200da04ee'] = 'Todos los países';
$_MODULE['<{statssales}leo_fashion_store>statssales_d7778d0c64b6ba21494c97f77a66885a'] = 'Filtro';
$_MODULE['<{statssales}leo_fashion_store>statssales_9ccb8353e945f1389a9585e7f21b5a0d'] = 'Los pedidos realizados:';
$_MODULE['<{statssales}leo_fashion_store>statssales_156e5c5872c9af24a5c982da07a883c2'] = 'Productos compraron:';
$_MODULE['<{statssales}leo_fashion_store>statssales_998e4c5c80f27dec552e99dfed34889a'] = 'CSV Exportación';
$_MODULE['<{statssales}leo_fashion_store>statssales_ec3e48bb9aa902ba2ad608547fdcbfdc'] = 'Ventas:';
$_MODULE['<{statssales}leo_fashion_store>statssales_f6825178a5fef0a97dacf963409829f0'] = 'Usted puede ver la distribución de estados de pedido de abajo.';
$_MODULE['<{statssales}leo_fashion_store>statssales_da80af4de99df74dd59e665adf1fac8f'] = 'No hay pedidos para este período.';
$_MODULE['<{statssales}leo_fashion_store>statssales_b52b44c9d23e141b067d7e83b44bb556'] = 'Productos:';
$_MODULE['<{statssales}leo_fashion_store>statssales_17833fb3783b26e0a9bc8b21ee85302a'] = 'Porcentaje de pedidos por el estado.';
