<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockmanufacturer}leo_fashion_store>blockmanufacturer_bc3e73cfa718a3237fb1d7e1da491395'] = 'Hersteller Block';
$_MODULE['<{blockmanufacturer}leo_fashion_store>blockmanufacturer_71087c7035e626bd33c72ae9a7f042af'] = 'Zeigt ein Block Listing Produkthersteller und / oder Marken.';
$_MODULE['<{blockmanufacturer}leo_fashion_store>blockmanufacturer_f8c922e47935b3b76a749334045d61cf'] = 'Es ist eine ungültige Anzahl von Elementen.';
$_MODULE['<{blockmanufacturer}leo_fashion_store>blockmanufacturer_5b2e13ff6fa0da895d14bd56f2cb2d2d'] = 'Bitte aktivieren Sie mindestens eine Systemliste.';
$_MODULE['<{blockmanufacturer}leo_fashion_store>blockmanufacturer_f38f5974cdc23279ffe6d203641a8bdf'] = 'Einstellungen aktualisiert.';
$_MODULE['<{blockmanufacturer}leo_fashion_store>blockmanufacturer_f4f70727dc34561dfde1a3c529b6205c'] = 'Einstellungen';
$_MODULE['<{blockmanufacturer}leo_fashion_store>blockmanufacturer_bfdff752293014f11f17122c92909ad5'] = 'Verwenden Sie einen Klartext-Liste';
$_MODULE['<{blockmanufacturer}leo_fashion_store>blockmanufacturer_a7c6946ecc7f4ed19c2691a1e7a28f97'] = 'Display-Hersteller in einem Klartext-Liste.';
$_MODULE['<{blockmanufacturer}leo_fashion_store>blockmanufacturer_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Aktiviert';
$_MODULE['<{blockmanufacturer}leo_fashion_store>blockmanufacturer_b9f5c797ebbf55adccdd8539a65a0241'] = 'Behindert';
$_MODULE['<{blockmanufacturer}leo_fashion_store>blockmanufacturer_2eef734f174a02ae3d7aaafefeeedb42'] = 'Anzahl Elemente zum Anzeigen';
$_MODULE['<{blockmanufacturer}leo_fashion_store>blockmanufacturer_b0fa976774d2acf72f9c62e9ab73de38'] = 'Verwenden Sie eine Dropdown-Liste';
$_MODULE['<{blockmanufacturer}leo_fashion_store>blockmanufacturer_56353167778d1520bfecc70c470e6d8a'] = 'Display-Hersteller in einer Dropdown-Liste.';
$_MODULE['<{blockmanufacturer}leo_fashion_store>blockmanufacturer_c9cc8cce247e49bae79f15173ce97354'] = 'Speichern';
$_MODULE['<{blockmanufacturer}leo_fashion_store>blockmanufacturer_2377be3c2ad9b435ba277a73f0f1ca76'] = 'Hersteller';
$_MODULE['<{blockmanufacturer}leo_fashion_store>blockmanufacturer_bf24faeb13210b5a703f3ccef792b000'] = 'Alle Hersteller';
$_MODULE['<{blockmanufacturer}leo_fashion_store>blockmanufacturer_1c407c118b89fa6feaae6b0af5fc0970'] = 'Kein Hersteller';
