<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_2e6774abc54cb13cef2c5bfd5a2cb463'] = 'distribution de transporteur';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_b56f2e8e5f8694e8d09cbd8ec27c4e57'] = 'Ajoute un graphique affichant la répartition de chaque porte de la planche de bord Statistiques.';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_b1c94ca2fbc3e78fc30069c8d0f01680'] = 'Tous';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_d7778d0c64b6ba21494c97f77a66885a'] = 'Filtre';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_ff61af405aa570a9000e6ba2da39857a'] = 'Ce graphique représente la répartition de support pour vos commandes. Vous pouvez également affiner la mise au point du graphique pour afficher la distribution d\'un statut d\'ordre particulier.';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_998e4c5c80f27dec552e99dfed34889a'] = 'CSV Export';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_ae916988f1944283efa2968808a71287'] = 'Pas de commandes valides ont été reçues pour cette période.';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_d5b9d0daaf017332f1f8188ab2a3f802'] = 'Pourcentage de commandes énumérés par transporteur.';
