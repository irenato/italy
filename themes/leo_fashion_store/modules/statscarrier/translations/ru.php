<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_2e6774abc54cb13cef2c5bfd5a2cb463'] = 'Распределение Carrier';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_b56f2e8e5f8694e8d09cbd8ec27c4e57'] = 'Добавляет график, отображающий распределение друг перевозчиков к Stats приборной панели.';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_b1c94ca2fbc3e78fc30069c8d0f01680'] = 'Все';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_d7778d0c64b6ba21494c97f77a66885a'] = 'Фильтр';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_ff61af405aa570a9000e6ba2da39857a'] = 'Этот график представляет собой распределение носителей для ваших заказов. Вы также можете сузить фокус на графике для отображения распределения для конкретного статуса заказа.';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_998e4c5c80f27dec552e99dfed34889a'] = 'CSV экспорт';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_ae916988f1944283efa2968808a71287'] = 'Нет действительные заказы не были получены в течение этого периода.';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_d5b9d0daaf017332f1f8188ab2a3f802'] = 'Процент заказов, перечисленных перевозчиком.';
