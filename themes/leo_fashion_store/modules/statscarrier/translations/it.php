<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_2e6774abc54cb13cef2c5bfd5a2cb463'] = 'Distribuzione Carrier';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_b56f2e8e5f8694e8d09cbd8ec27c4e57'] = 'Aggiunge un grafico che visualizza la distribuzione reciproche vettori al cruscotto Statistiche.';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_b1c94ca2fbc3e78fc30069c8d0f01680'] = 'Tutto';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_d7778d0c64b6ba21494c97f77a66885a'] = 'Filtro';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_ff61af405aa570a9000e6ba2da39857a'] = 'Questo grafico rappresenta la distribuzione del vettore per i vostri ordini. È anche possibile restringere la messa a fuoco del grafico per visualizzare la distribuzione di un particolare stato dell\'ordine.';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_998e4c5c80f27dec552e99dfed34889a'] = 'CSV Export';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_ae916988f1944283efa2968808a71287'] = 'Non ci sono ordini validi sono stati ricevuti per questo periodo.';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_d5b9d0daaf017332f1f8188ab2a3f802'] = 'Percentuale degli ordini elencati dal vettore.';
