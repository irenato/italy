<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_2e6774abc54cb13cef2c5bfd5a2cb463'] = 'Distribución Carrier';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_b56f2e8e5f8694e8d09cbd8ec27c4e57'] = 'Añade un gráfico que muestra la distribución de cada uno de los portadores del salpicadero Estadísticas.';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_b1c94ca2fbc3e78fc30069c8d0f01680'] = 'Todo';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_d7778d0c64b6ba21494c97f77a66885a'] = 'Filtro';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_ff61af405aa570a9000e6ba2da39857a'] = 'Este gráfico representa la distribución de soporte para sus órdenes. También puede reducir el enfoque de la gráfica para mostrar la distribución de un estado de la orden en particular.';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_998e4c5c80f27dec552e99dfed34889a'] = 'CSV Exportación';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_ae916988f1944283efa2968808a71287'] = 'No hay órdenes válidas se han recibido para este período.';
$_MODULE['<{statscarrier}leo_fashion_store>statscarrier_d5b9d0daaf017332f1f8188ab2a3f802'] = 'Porcentaje de órdenes directas del portador.';
