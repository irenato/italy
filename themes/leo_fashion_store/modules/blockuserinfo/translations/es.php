<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockuserinfo}leo_fashion_store>blockuserinfo_a2e9cd952cda8ba167e62b25a496c6c1'] = 'Info Bloquear usuario';
$_MODULE['<{blockuserinfo}leo_fashion_store>blockuserinfo_970a31aa19d205f92ccfd1913ca04dc0'] = 'Añade un bloque que muestra información sobre el cliente.';
$_MODULE['<{blockuserinfo}leo_fashion_store>blockuserinfo_0c3bf3014aafb90201805e45b5e62881'] = 'Ver mi carrito de la compra';
$_MODULE['<{blockuserinfo}leo_fashion_store>blockuserinfo_a85eba4c6c699122b2bb1387ea4813ad'] = 'Carro';
$_MODULE['<{blockuserinfo}leo_fashion_store>blockuserinfo_deb10517653c255364175796ace3553f'] = 'Producto';
$_MODULE['<{blockuserinfo}leo_fashion_store>blockuserinfo_068f80c7519d0528fb08e82137a72131'] = 'Productos';
$_MODULE['<{blockuserinfo}leo_fashion_store>blockuserinfo_9e65b51e82f2a9b9f72ebe3e083582bb'] = '(Vacío)';
$_MODULE['<{blockuserinfo}leo_fashion_store>blockuserinfo_2cbfb6731610056e1d0aaacde07096c1'] = 'Ver mi cuenta de cliente';
$_MODULE['<{blockuserinfo}leo_fashion_store>blockuserinfo_a0623b78a5f2cfe415d9dbbd4428ea40'] = 'Su cuenta';
$_MODULE['<{blockuserinfo}leo_fashion_store>blockuserinfo_83218ac34c1834c26781fe4bde918ee4'] = 'Bienvenida';
$_MODULE['<{blockuserinfo}leo_fashion_store>blockuserinfo_4b877ba8588b19f1b278510bf2b57ebb'] = 'Me Salir';
$_MODULE['<{blockuserinfo}leo_fashion_store>blockuserinfo_c87aacf5673fada1108c9f809d354311'] = 'Desconectar';
$_MODULE['<{blockuserinfo}leo_fashion_store>blockuserinfo_d4151a9a3959bdd43690735737034f27'] = 'Ingrese a su cuenta de cliente';
$_MODULE['<{blockuserinfo}leo_fashion_store>blockuserinfo_b6d4223e60986fa4c9af77ee5f7149c5'] = 'Registrarse';
$_MODULE['<{blockuserinfo}leo_fashion_store>nav_2cbfb6731610056e1d0aaacde07096c1'] = 'Ver mi cuenta de cliente';
$_MODULE['<{blockuserinfo}leo_fashion_store>nav_4b877ba8588b19f1b278510bf2b57ebb'] = 'Me Salir';
$_MODULE['<{blockuserinfo}leo_fashion_store>nav_c87aacf5673fada1108c9f809d354311'] = 'Desconectar';
$_MODULE['<{blockuserinfo}leo_fashion_store>nav_d4151a9a3959bdd43690735737034f27'] = 'Ingrese a su cuenta de cliente';
$_MODULE['<{blockuserinfo}leo_fashion_store>nav_b6d4223e60986fa4c9af77ee5f7149c5'] = 'Registrarse';
$_MODULE['<{blockuserinfo}leo_fashion_store>nav_50201c8fdd58cd7e69fe955450afceb7'] = 'Top enlaces';
$_MODULE['<{blockuserinfo}leo_fashion_store>nav_7ec9cceb94985909c6994e95c31c1aa8'] = 'Mis listas de regalo';
$_MODULE['<{blockuserinfo}leo_fashion_store>nav_9f7b857dbd2834e6dbae6d8de2611789'] = 'Lista de deseos';
$_MODULE['<{blockuserinfo}leo_fashion_store>nav_b145abfd6b2f88971d725cbd94a5879f'] = 'Ingrese a su cuenta de cliente';
$_MODULE['<{blockuserinfo}leo_fashion_store>nav_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'Mi cuenta';
$_MODULE['<{blockuserinfo}leo_fashion_store>nav_bea8d9e6a0d57c4a264756b4f9822ed9'] = 'Mi cuenta';
$_MODULE['<{blockuserinfo}leo_fashion_store>nav_6ff063fbc860a79759a7369ac32cee22'] = 'Caja';
$_MODULE['<{blockuserinfo}leo_fashion_store>nav_8b1a9953c4611296a827abf8c47804d7'] = '¡Hola';
