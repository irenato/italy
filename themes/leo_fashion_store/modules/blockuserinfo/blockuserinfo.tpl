<script type="text/javascript">
/* Blockusreinfo */

$(document).ready( function(){
	if( $(window).width() < 991 ){
			 $(".header_user_info").addClass('popup-over');
			 $(".header_user_info .links").addClass('popup-content');
		}
		else{
			$(".header_user_info").removeClass('popup-over');
			 $(".header_user_info .links").removeClass('popup-content');
		}
	$(window).resize(function() {
		if( $(window).width() < 991 ){
			 $(".header_user_info").addClass('popup-over');
			 $(".header_user_info .links").addClass('popup-content');
		}
		else{
			$(".header_user_info").removeClass('popup-over');
			 $(".header_user_info .links").removeClass('popup-content');
		}
	});
});
</script>
<!-- Block user information module NAV  -->


<div class="header_user_info topbar-box popup-over  pull-right ">
	<div class=" popup-title"><a href="#"><span>{l s='Top links' mod='blockuserinfo'} </span></a></div>
		<ul class="links">
			{if Context::getContext()->language->id==1}
				{$my_param='en'}
			{/if}
			{if Context::getContext()->language->id==2}
				{$my_param='it'}
			{/if}

		{if $is_logged}
                {*<li>
			<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='My Charly' mod='blockuserinfo'}">{l s='My Charly' mod='blockuserinfo'}</a>
		</li>*}
		{/if}
		{if $is_logged}<li>
			<a id="wishlist-total" href="{$link->getModuleLink('blockwishlist', 'mywishlist', array(), true)|addslashes}" title="{l s='My wishlists' mod='blockuserinfo'}" hreflang={$my_param}>
				{l s='Wish List' mod='blockuserinfo'}
			</a>
		</li>
		<li>
				<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account" rel="nofollow" hreflang={$my_param}>

					<span>{l s='Hello' mod='blockuserinfo'}, {$cookie->customer_firstname} {$cookie->customer_lastname}</span></a>
			</li>
			<li><a class="logout" href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}" hreflang={$my_param}>
				{l s='Sign out' mod='blockuserinfo'}
			</a></li>
		{else}
        
			<li><a class="login" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Login to your customer account' mod='blockuserinfo'}" hreflang={$my_param}>
				{l s='Sign In' mod='blockuserinfo'}
			</a></li>
		{/if}
		{if $is_logged}

		{*<li>
			<a href="{$link->getPageLink(order, true)|escape:'html':'UTF-8'}" title="{l s='Checkout' mod='blockuserinfo'}">{l s='Checkout' mod='blockuserinfo'}</a>
		</li>*}

		{/if}
		</ul>
	
</div>
