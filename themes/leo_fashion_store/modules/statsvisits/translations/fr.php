<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_504c16c26a96283f91fb46a69b7c8153'] = 'Visites et de visiteurs';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_432c3ab90b3af30ad318201ba09aa824'] = 'Ajoute des statistiques sur vos visites et les visiteurs de la planche de bord Statistiques.';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_6602bbeb2956c035fb4cb5e844a4861b'] = 'Guide';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_ffbee337f031c2282b311bac40bc8bb9'] = 'Déterminer l\'intérêt d\'une visite';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_e90d50ca1e68dc66c97bd62929dcbaf1'] = 'Les visiteurs de l\'évolution du graphique ressemble fortement les visites de la graphique, mais fournit des informations supplémentaires:';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_e9849ece0b2ecf1eea74d92d492a47f2'] = 'Si tel est le cas, félicitations, votre site est bien planifié et agréable. Content de voir que vous avez été attentif.';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_c745121a98cf1d5b26bc5299d9880d5c'] = 'Sinon, la conclusion est pas si simple. Le problème peut être esthétique ou ergonomique. Il est également possible que de nombreux visiteurs ont tort visité votre URL sans avoir un intérêt particulier dans votre boutique. Ce phénomène étrange et toujours confus est cause la plus probable par les moteurs de recherche. Si tel est le cas, vous devriez envisager de revoir la structure de votre référencement.';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_9bf5a493522a65d550f096505874873b'] = 'Cette information est surtout qualitative. Il est à vous de déterminer l\'intérêt d\'une visite décousu.';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_b8901fb7bbfaf9b0c4724343c7cd1f90'] = 'Une visite correspond à un utilisateur d\'Internet à venir à votre boutique, et jusqu\'à la fin de leur session, une seule visite est prise en compte.';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_f43a4cf6dcc4ec617d2296d03d26c90f'] = 'Un visiteur est une personne inconnue qui n\'a pas enregistré ou identifié dans votre magasin. Un visiteur peut également être considéré comme une personne qui a visité votre boutique plusieurs fois.';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_54067074d24489ddb5654bf46642cb85'] = 'Total des visites:';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_23e640d55e56db79971918936e95bf9d'] = 'Total des visiteurs:';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_998e4c5c80f27dec552e99dfed34889a'] = 'CSV Export';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_39b960b0a5e2ebaaa638d946f1892050'] = 'Nombre de visites et de visiteurs uniques';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_d7e637a6e9ff116de2fa89551240a94d'] = 'Visites';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_ae5d01b6efa819cc7a7c05a8c57fcc2c'] = 'Visiteurs';
