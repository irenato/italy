<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_504c16c26a96283f91fb46a69b7c8153'] = 'Wizyty i odwiedzający';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_432c3ab90b3af30ad318201ba09aa824'] = 'Dodaje statystyki dotyczące wizyt i odwiedzających Statystyki desce rozdzielczej.';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_6602bbeb2956c035fb4cb5e844a4861b'] = 'Przewodnik';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_ffbee337f031c2282b311bac40bc8bb9'] = 'Określić zainteresowanie wizytą';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_e90d50ca1e68dc66c97bd62929dcbaf1'] = 'Zawodnik gości, mocno przypomina wykres ewolucja \"wykres wizyty, ale zapewnia dodatkowe informacje:';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_e9849ece0b2ecf1eea74d92d492a47f2'] = 'Jeśli jest to przypadek, gratulacje, witryna jest dobrze zaplanowane i miłe. Cieszę się, że już zwracać uwagę.';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_c745121a98cf1d5b26bc5299d9880d5c'] = 'W przeciwnym razie wniosek nie jest takie proste. Problemem może być estetyczne i ergonomiczne. Możliwe jest także, że wielu turystów błędnie odwiedził URL bez posiadania szczególnego zainteresowania sklepu. To dziwne i mylące zjawisko coraz Najbardziej prawdopodobną przyczyną jest przez wyszukiwarki. Jeśli tak jest, należy rozważyć zmiany strukturę SEO.';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_9bf5a493522a65d550f096505874873b'] = 'Informacja ta jest głównie ilościowy. To do Ciebie, aby określić zainteresowanie wybitą wizyty.';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_b8901fb7bbfaf9b0c4724343c7cd1f90'] = 'Wizyta odpowiada przybywających do sklepu użytkownikiem Internetu, a do końca ich sesji, tylko jedna wizyta jest liczony.';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_f43a4cf6dcc4ec617d2296d03d26c90f'] = 'Użytkownik jest nieznany człowiek, który nie został zarejestrowany lub zalogowany do swojego sklepu. Użytkownik może również uznać, kto odwiedził Twój sklep wiele razy.';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_54067074d24489ddb5654bf46642cb85'] = 'Ilość wizyt:';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_23e640d55e56db79971918936e95bf9d'] = 'Wszystkich odwiedzających:';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_998e4c5c80f27dec552e99dfed34889a'] = 'Eksport CSV';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_39b960b0a5e2ebaaa638d946f1892050'] = 'Liczba odwiedzin oraz unikalnych użytkowników';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_d7e637a6e9ff116de2fa89551240a94d'] = 'Wizyty';
$_MODULE['<{statsvisits}leo_fashion_store>statsvisits_ae5d01b6efa819cc7a7c05a8c57fcc2c'] = 'Gości';
