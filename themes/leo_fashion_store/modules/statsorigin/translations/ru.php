<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_f0b1507c6bdcdefb60a0e6f9b89d4ae8'] = 'Посетители происхождения';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_a69c2a3091fe48c7f4f391595aa3ac19'] = 'Добавляет график, отображающий сайты ваши посетители пришли из к Stats приборной панели.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_14542f5997c4a02d4276da364657f501'] = 'Прямая ссылка';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_3edf8ca26a1ec14dd6e91dd277ae1de6'] = 'Происхождение';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_4b69c1f7f555aa19fd90ee01e4aa63cd'] = 'На вкладке, мы сломать 10 самых популярных справочные сайты, которые приносят клиентов на ваш интернет-магазине.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_6602bbeb2956c035fb4cb5e844a4861b'] = 'Руководство';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_cec998cc46cd200fa97490137de2cc7f'] = 'Что такое веб-сайт для приглашений?';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_54f00c2c9a36e2b300b5bacc1bb7912c'] = 'Ссылающаяся это адрес предыдущего странице, с которой ссылка последовал посетителя.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_7231fe46fc79eb2b3a269f790b79e01f'] = 'Ссылающаяся также позволяет узнать, какие ключевые слова используют посетители сайта в поисковых системах при просмотре для вашего интернет-магазина.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_af19c8da1c414055c960a73d86471119'] = 'Ссылающаяся может быть:';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_c227be237c874ba6b2f8771d7b66b90e'] = 'Кто-то, кто размещает ссылку на ваш магазин.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_ea87a2280d5cdb638a2727147a3dd85c'] = 'Партнер, который согласился на обмен ссылками с целью привлечения новых клиентов.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_998e4c5c80f27dec552e99dfed34889a'] = 'CSV экспорт';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_96b0141273eabab320119c467cdcaf17'] = 'Общий';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_0bebf95ee829c33f34fde535ed4ed100'] = 'Только прямые ссылки';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_450a7e38e636dd49f5dfb356f96d3996'] = 'Десять справочные сайты';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_52ef9633d88a7480b3a938ff9eaa2a25'] = 'Другие';
