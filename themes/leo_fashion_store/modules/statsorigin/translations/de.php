<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_f0b1507c6bdcdefb60a0e6f9b89d4ae8'] = 'Besucher Herkunft';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_a69c2a3091fe48c7f4f391595aa3ac19'] = 'Fügt eine Grafik die Anzeige der Webseiten Ihrer Besucher aus auf die Stats Armaturenbrett kam.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_14542f5997c4a02d4276da364657f501'] = 'Direktlink';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_3edf8ca26a1ec14dd6e91dd277ae1de6'] = 'Herkunft';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_4b69c1f7f555aa19fd90ee01e4aa63cd'] = 'In der Registerkarte, wir brechen die 10 beliebtesten Befassung Websites, die Kunden zu bringen, um Ihren Online-Shop.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_6602bbeb2956c035fb4cb5e844a4861b'] = 'Führung';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_cec998cc46cd200fa97490137de2cc7f'] = 'Was ist ein Referral-Website?';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_54f00c2c9a36e2b300b5bacc1bb7912c'] = 'Der Referrer ist die URL der vorherigen Webseite, von der ein Link wurde vom Besucher gefolgt.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_7231fe46fc79eb2b3a269f790b79e01f'] = 'Ein Referrer können Sie auch wissen, welche Besucher nutzen in Suchmaschinen beim Surfen für Ihren Online-Shop Keywords.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_af19c8da1c414055c960a73d86471119'] = 'Ein Referrer können sein:';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_c227be237c874ba6b2f8771d7b66b90e'] = 'Jemand, der einen Link postet, um Ihren Shop.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_ea87a2280d5cdb638a2727147a3dd85c'] = 'Ein Partner, um neue Kunden zu gewinnen, um eine Link-Tausch vereinbart hat.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_998e4c5c80f27dec552e99dfed34889a'] = 'CSV-Export';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_96b0141273eabab320119c467cdcaf17'] = 'Gesamt';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_0bebf95ee829c33f34fde535ed4ed100'] = 'Nur direkte Links';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_450a7e38e636dd49f5dfb356f96d3996'] = 'Top-Ten-Referral-Websites';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_52ef9633d88a7480b3a938ff9eaa2a25'] = 'Andere';
