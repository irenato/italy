<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_f0b1507c6bdcdefb60a0e6f9b89d4ae8'] = 'Pochodzenie odwiedzających';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_a69c2a3091fe48c7f4f391595aa3ac19'] = 'Dodaje wykres wyświetlający strony internetowe odwiedzający przybyli z do Statystyki desce rozdzielczej.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_14542f5997c4a02d4276da364657f501'] = 'Link bezpośredni';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_3edf8ca26a1ec14dd6e91dd277ae1de6'] = 'Pochodzenie';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_4b69c1f7f555aa19fd90ee01e4aa63cd'] = 'W zakładce, możemy rozbić 10 najbardziej popularnych stron internetowych, które przynoszą skierowania klientów do sklepu internetowego.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_6602bbeb2956c035fb4cb5e844a4861b'] = 'Przewodnik';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_cec998cc46cd200fa97490137de2cc7f'] = 'Co to jest strona skierowanie?';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_54f00c2c9a36e2b300b5bacc1bb7912c'] = 'Polecający jest adres URL poprzedniej strony internetowej, z której link został następnie przez odwiedzającego.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_7231fe46fc79eb2b3a269f790b79e01f'] = 'Polecający umożliwia również wiedzieć, jakie słowa kluczowe użytkownicy korzystają z wyszukiwarek, podczas przeglądania w sklepie internetowym.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_af19c8da1c414055c960a73d86471119'] = 'Polecający może być:';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_c227be237c874ba6b2f8771d7b66b90e'] = 'Ktoś, kto księguje link do swojego sklepu.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_ea87a2280d5cdb638a2727147a3dd85c'] = 'Partner, który zgodził się na wymianę linków w celu przyciągnięcia nowych klientów.';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_998e4c5c80f27dec552e99dfed34889a'] = 'Eksport CSV';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_96b0141273eabab320119c467cdcaf17'] = 'Całkowity';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_0bebf95ee829c33f34fde535ed4ed100'] = 'Tylko bezpośrednie linki';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_450a7e38e636dd49f5dfb356f96d3996'] = 'Dziesięć stron skierowania';
$_MODULE['<{statsorigin}leo_fashion_store>statsorigin_52ef9633d88a7480b3a938ff9eaa2a25'] = 'Inne';
