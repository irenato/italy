<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_9862f1949f776f69155b6e6b330c7ee1'] = 'Top-Seller Block';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_ed6476843a865d9daf92e409082b76e1'] = 'Fügt einen Block Anzeigen Top-Produkte Ihres Shops.';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_c888438d14855d7d96a2724ee9c306bd'] = 'Einstellungen aktualisiert';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_f4f70727dc34561dfde1a3c529b6205c'] = 'Einstellungen';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_24ff4e4d39bb7811f6bdf0c189462272'] = 'Immer diesen Block anzuzeigen';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_84b0c5fdef19ab8ef61cd809f9250d85'] = 'Zeigen Sie den Block, auch wenn keine Verkaufshits vorhanden sind.';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Aktiviert';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_b9f5c797ebbf55adccdd8539a65a0241'] = 'Behindert';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_c9cc8cce247e49bae79f15173ce97354'] = 'Speichern';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers-home_09a5fe24fe0fc9ce90efc4aa507c66e7'] = 'Momentan keine Verkaufshits zu diesem Zeitpunkt.';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_1d0a2e1f62ccf460d604ccbc9e09da95'] = 'Sehen Sie sich eine Top-Seller Produkte';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_3cb29f0ccc5fd220a97df89dafe46290'] = 'Verkaufshits';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_eae99cd6a931f3553123420b16383812'] = 'Alle Verkäufer';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_f7be84d6809317a6eb0ff3823a936800'] = 'Momentan keine Verkaufshits dieser Zeit';
$_MODULE['<{blockbestsellers}leo_fashion_store>tab_d7b2933ba512ada478c97fa43dd7ebe6'] = 'Bestseller';
