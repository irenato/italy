<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_9862f1949f776f69155b6e6b330c7ee1'] = 'Bloque de los mas vendidos';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_ed6476843a865d9daf92e409082b76e1'] = 'Añade un bloque de visualización de mayor venta los productos de su tienda.';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_c888438d14855d7d96a2724ee9c306bd'] = 'Ajustes actualizan';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_f4f70727dc34561dfde1a3c529b6205c'] = 'Ajustes';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_24ff4e4d39bb7811f6bdf0c189462272'] = 'Siempre mostrar este bloque';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_84b0c5fdef19ab8ef61cd809f9250d85'] = 'Mostrar el bloque, incluso si no hay productos más vendidos están disponibles.';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activado';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_b9f5c797ebbf55adccdd8539a65a0241'] = 'Discapacitado';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers-home_09a5fe24fe0fc9ce90efc4aa507c66e7'] = 'No hay productos más vendidos en este momento.';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_1d0a2e1f62ccf460d604ccbc9e09da95'] = 'Ver a los mejores productos de los vendedores';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_3cb29f0ccc5fd220a97df89dafe46290'] = 'Los más vendidos';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_eae99cd6a931f3553123420b16383812'] = 'Los productos más vendidos';
$_MODULE['<{blockbestsellers}leo_fashion_store>blockbestsellers_f7be84d6809317a6eb0ff3823a936800'] = 'No hay productos más vendidos en este momento';
$_MODULE['<{blockbestsellers}leo_fashion_store>tab_d7b2933ba512ada478c97fa43dd7ebe6'] = 'Top Ventas';
