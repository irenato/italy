<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsbestsuppliers}leo_fashion_store>statsbestsuppliers_b5f5c19c8729b639d4d2a256fcb01a10'] = 'Set di record vuoto restituito';
$_MODULE['<{statsbestsuppliers}leo_fashion_store>statsbestsuppliers_f5c493141bb4b2508c5938fd9353291a'] = 'Mostra %1$s di %2$s';
$_MODULE['<{statsbestsuppliers}leo_fashion_store>statsbestsuppliers_49ee3087348e8d44e1feda1917443987'] = 'Nome';
$_MODULE['<{statsbestsuppliers}leo_fashion_store>statsbestsuppliers_2a0440eec72540c5b30d9199c01f348c'] = 'Quantità venduta';
$_MODULE['<{statsbestsuppliers}leo_fashion_store>statsbestsuppliers_ea067eb37801c5aab1a1c685eb97d601'] = 'Totale pagato';
$_MODULE['<{statsbestsuppliers}leo_fashion_store>statsbestsuppliers_cc3eb9ba7d0e236f33023a4744d0693a'] = 'Le migliori fornitori';
$_MODULE['<{statsbestsuppliers}leo_fashion_store>statsbestsuppliers_37607fc64452028f4d484aa014071934'] = 'Aggiunge una lista dei migliori fornitori del cruscotto Statistiche.';
$_MODULE['<{statsbestsuppliers}leo_fashion_store>statsbestsuppliers_998e4c5c80f27dec552e99dfed34889a'] = 'CSV Export';
