<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockcurrencies}leo_fashion_store>blockcurrencies_f7a31ae8f776597d4282bd3b1013f08b'] = 'Währungsblock';
$_MODULE['<{blockcurrencies}leo_fashion_store>blockcurrencies_80ed40ee905b534ee85ce49a54380107'] = 'Fügt einen Block so dass Kunden ihre bevorzugte Einkaufswährung zu wählen.';
$_MODULE['<{blockcurrencies}leo_fashion_store>blockcurrencies_386c339d37e737a436499d423a77df0c'] = 'Währung';
$_MODULE['<{blockcurrencies}leo_fashion_store>blockcurrencies_df19f5428d7bc5d7026c5d8a847e53af'] = 'Währung: ';
