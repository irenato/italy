<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_f7c34fc4a48bc683445c1e7bbc245508'] = 'Neue Produkte Block';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_d3ee346c7f6560faa13622b6fef26f96'] = 'Zeigt einen Block mit neuesten Produkte Ihres Shops.';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_1cd777247f2a6ed79534d4ace72d78ce'] = 'Bitte vervollständigen Sie die \"Produkte, um\" Feld.';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_73293a024e644165e9bf48f270af63a0'] = 'Ungültige Nummer.';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_c888438d14855d7d96a2724ee9c306bd'] = 'Einstellungen aktualisiert';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_f4f70727dc34561dfde1a3c529b6205c'] = 'Einstellungen';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_26986c3388870d4148b1b5375368a83d'] = 'Produkte angezeigt';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_3ea7689283770958661c27c37275b89c'] = 'Definieren Sie die Anzahl der Produkte in dieser Block angezeigt.';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_85dd6b2059e1ff8fbefcc9cf6e240933'] = 'Anzahl der Tage, für die das Produkt wird als \"neue\"';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_24ff4e4d39bb7811f6bdf0c189462272'] = 'Immer diesen Block anzuzeigen';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_d68e7b860a7dba819fa1c75225c284b5'] = 'Zeigen Sie den Block, auch wenn keine neuen Produkte verfügbar sind.';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Aktiviert';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_b9f5c797ebbf55adccdd8539a65a0241'] = 'Behindert';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_c9cc8cce247e49bae79f15173ce97354'] = 'Speichern';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_9ff0635f5737513b1a6f559ac2bff745'] = 'Neue Artikel';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_43340e6cc4e88197d57f8d6d5ea50a46'] = 'Weiterlesen';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_60efcc704ef1456678f77eb9ee20847b'] = 'Alle neuen Produkte';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_18cc24fb12f89c839ab890f8188febe8'] = 'Neue Produkte Lassen Sie zu diesem Zeitpunkt nicht.';
$_MODULE['<{blocknewproducts}leo_fashion_store>blocknewproducts_home_0af0aac2e9f6bd1d5283eed39fe265cc'] = 'Keine neuen Produkte zu diesem Zeitpunkt.';
$_MODULE['<{blocknewproducts}leo_fashion_store>tab_a0d0ebc37673b9ea77dd7c1a02160e2d'] = 'Neu eingetroffen';
