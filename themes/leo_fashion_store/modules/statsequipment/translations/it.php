<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_247270d410e2b9de01814b82111becda'] = 'Browser e sistemi operativi';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_2876718a648dea03aaafd4b5a63b1efe'] = 'Aggiunge una scheda contenente i grafici su browser web e l\'utilizzo del sistema operativo per il cruscotto Statistiche.';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_6602bbeb2956c035fb4cb5e844a4861b'] = 'Guida';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_854c8e126f839cc861cde822b641230e'] = 'Fare in modo che il vostro sito sia accessibile al maggior numero possibile di persone';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_0d5f13106dec10bb8a9301541052278c'] = 'Quando si amministra un sito, è importante tenere traccia del software utilizzato dai visitatori, per essere sicuri che il sito sia visibile a tutti allo stesso modo. PrestaShop è fatto per essere compatibile con i più recenti browser e sistemi operativi (OS). In ogni modo, poiché tu potresti aggiungere funzionalità avanzate al tuo sito web o persino modificare il codice di base di PrestaShop, queste aggiunte potrebbero non essere accessibili a tutti. Perciò è una buona idea tenere traccia della percentuale di utenti per ciascun tipo di software prima di aggiungere o cambiare qualcosa cui solo un limitato numero di utenti potrebbe poter accedere.';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_11db1362a88c5e3e74c8f699c14d6798'] = 'Indica la percentuale di ciascun browser web utilizzato dai clienti.';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_998e4c5c80f27dec552e99dfed34889a'] = 'CSV Export';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_90c58bfe4872fc9ca7bf6a181c3e5edd'] = 'Indica la percentuale di ciascun sistema operativo utilizzato dai clienti.';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_bb38096ab39160dc20d44f3ea6b44507'] = 'Plugin';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_9ffafc9e090c8e1c06f928ef2817efd6'] = 'Web browser utilizzato';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_0241b7aaaa5f76afd585bb6cdae314d1'] = 'Sistema operativo utilizzato';
