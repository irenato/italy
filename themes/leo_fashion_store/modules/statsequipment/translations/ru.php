<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_247270d410e2b9de01814b82111becda'] = 'Браузеры и операционные системы';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_2876718a648dea03aaafd4b5a63b1efe'] = 'Добавляет вкладку, содержащую графики о веб-браузер и использования операционной системы на приборной панели Stats.';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_6602bbeb2956c035fb4cb5e844a4861b'] = 'Руководство';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_854c8e126f839cc861cde822b641230e'] = 'Убедившись, что ваш сайт доступен для многих людей, насколько это возможно';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_11db1362a88c5e3e74c8f699c14d6798'] = 'Показывает процент от каждой веб-браузера, используемого клиентами.';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_998e4c5c80f27dec552e99dfed34889a'] = 'CSV экспорт';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_90c58bfe4872fc9ca7bf6a181c3e5edd'] = 'Показывает процент каждой операционной системы, используемой клиентами.';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_bb38096ab39160dc20d44f3ea6b44507'] = 'Плагины';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_9ffafc9e090c8e1c06f928ef2817efd6'] = 'Веб-браузер используется';
$_MODULE['<{statsequipment}leo_fashion_store>statsequipment_0241b7aaaa5f76afd585bb6cdae314d1'] = 'Операционная система, используемая';
