<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statslive}leo_fashion_store>statslive_fa55230e9791f2b71322869318a5f00f'] = 'Visitatori on line';
$_MODULE['<{statslive}leo_fashion_store>statslive_abf306fd198ab007d480ed610a6690fb'] = 'Aggiunge un elenco di clienti e visitatori che sono attualmente in linea al cruscotto Statistiche.';
$_MODULE['<{statslive}leo_fashion_store>statslive_85f955e33756b8f40ce35e5b277de5bc'] = 'È necessario attivare la funzione \"Salva pagine viste per ogni cliente\" nella \"data mining per le statistiche\" modulo (StatsData) per vedere le pagine che i visitatori stanno visualizzando.';
$_MODULE['<{statslive}leo_fashion_store>statslive_f5ee3b50dba1fb98f1342a584e46cd30'] = 'Gli attuali clienti on-line';
$_MODULE['<{statslive}leo_fashion_store>statslive_66c4c5112f455a19afde47829df363fa'] = 'Totale:';
$_MODULE['<{statslive}leo_fashion_store>statslive_d37c2bf1bd3143847fca087b354f920e'] = 'ID cliente';
$_MODULE['<{statslive}leo_fashion_store>statslive_49ee3087348e8d44e1feda1917443987'] = 'Nome';
$_MODULE['<{statslive}leo_fashion_store>statslive_13aa8652e950bb7c4b9b213e6d8d0dc5'] = 'La pagina corrente';
$_MODULE['<{statslive}leo_fashion_store>statslive_9dd3bc54879dc9425d44de81f3d7dfdc'] = 'Vedi profilo cliente';
$_MODULE['<{statslive}leo_fashion_store>statslive_08448d43d8e4b39ad5126d4b06e2f3cc'] = 'Non ci sono clienti attivi online in questo momento.';
$_MODULE['<{statslive}leo_fashion_store>statslive_cf6377279146be659952cea754c558b1'] = 'I visitatori on-line in corso';
$_MODULE['<{statslive}leo_fashion_store>statslive_f8cf0a1a6b03d2b01602992ea273134c'] = 'ID Guest';
$_MODULE['<{statslive}leo_fashion_store>statslive_a12a3079e14ced46e69ba52b8a90b21a'] = 'IP';
$_MODULE['<{statslive}leo_fashion_store>statslive_bc5188ca43d423ba3730e4c030609d6e'] = 'Ultima attività';
$_MODULE['<{statslive}leo_fashion_store>statslive_b6f05e5ddde1ec63d992d61144452dfa'] = 'Referrer';
$_MODULE['<{statslive}leo_fashion_store>statslive_ec0fc0100c4fc1ce4eea230c3dc10360'] = 'Indefinito';
$_MODULE['<{statslive}leo_fashion_store>statslive_6adf97f83acf6453d4a6a4b1070f3754'] = 'Nessuno';
$_MODULE['<{statslive}leo_fashion_store>statslive_a55533db46597bee3cd16899c007257e'] = 'Non ci sono visitatori on-line.';
$_MODULE['<{statslive}leo_fashion_store>statslive_24efa7ee4511563b16144f39706d594f'] = 'Avviso';
$_MODULE['<{statslive}leo_fashion_store>statslive_e5900cd9ae26ca607f7cd497f114b9f9'] = 'IP alimentari sono escluse dai visitatori on-line.';
$_MODULE['<{statslive}leo_fashion_store>statslive_05b564d49dbd9049f0df80a45cfe7d1c'] = 'Aggiungere o rimuovere un indirizzo IP.';
