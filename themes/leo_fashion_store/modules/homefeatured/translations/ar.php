<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{homefeatured}leo_fashion_store>homefeatured_5d17bf499a1b9b2e816c99eebf0153a9'] = 'منتجات مميزة على الصفحة الرئيسية';
$_MODULE['<{homefeatured}leo_fashion_store>homefeatured_6d37ec35b5b6820f90394e5ee49e8cec'] = 'ظهرت يعرض المنتجات في العمود الأوسط من صفحتك الرئيسية.';
$_MODULE['<{homefeatured}leo_fashion_store>homefeatured_6af91e35dff67a43ace060d1d57d5d1a'] = 'تم تحديث الإعدادات الخاصة بك.';
$_MODULE['<{homefeatured}leo_fashion_store>homefeatured_f4f70727dc34561dfde1a3c529b6205c'] = 'الإعدادات';
$_MODULE['<{homefeatured}leo_fashion_store>homefeatured_d44168e17d91bac89aab3f38d8a4da8e'] = 'عدد المنتجات التي سيتم عرضها';
$_MODULE['<{homefeatured}leo_fashion_store>homefeatured_1b73f6b70a0fcd38bbc6a6e4b67e3010'] = 'ضبط عدد من المنتجات التي ترغب في عرضه على الصفحة الرئيسية (الافتراضي: 8).';
$_MODULE['<{homefeatured}leo_fashion_store>homefeatured_c9cc8cce247e49bae79f15173ce97354'] = 'حفظ';
$_MODULE['<{homefeatured}leo_fashion_store>homefeatured_ca7d973c26c57b69e0857e7a0332d545'] = 'منتجات مميزة';
$_MODULE['<{homefeatured}leo_fashion_store>homefeatured_03c2e7e41ffc181a4e84080b4710e81e'] = 'جديد';
$_MODULE['<{homefeatured}leo_fashion_store>homefeatured_d3da97e2d9aee5c8fbe03156ad051c99'] = 'أكثر';
$_MODULE['<{homefeatured}leo_fashion_store>homefeatured_4351cfebe4b61d8aa5efa1d020710005'] = 'رأي';
$_MODULE['<{homefeatured}leo_fashion_store>homefeatured_2d0f6b8300be19cf35e89e66f0677f95'] = 'إضافة إلى السلة';
$_MODULE['<{homefeatured}leo_fashion_store>homefeatured_e0e572ae0d8489f8bf969e93d469e89c'] = 'لا توجد منتجات مميزة';
$_MODULE['<{homefeatured}leo_fashion_store>tab_2cc1943d4c0b46bfcf503a75c44f988b'] = 'شعبي';
$_MODULE['<{homefeatured}leo_fashion_store>homefeatured_d505d41279039b9a68b0427af27705c6'] = 'لا يوجد منتجات مميزة في هذا الوقت.';
