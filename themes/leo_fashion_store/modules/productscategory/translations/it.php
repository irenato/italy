<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{productscategory}leo_fashion_store>productscategory_8a4f5a66d0fcc9d13614516db6e3d47a'] = 'Prodotti della stessa categoria';
$_MODULE['<{productscategory}leo_fashion_store>productscategory_1d269d7f013c3d9d891a146f4379eb02'] = 'Aggiunge un blocco nella pagina del prodotto che visualizza i prodotti della stessa categoria.';
$_MODULE['<{productscategory}leo_fashion_store>productscategory_8dd2f915acf4ec98006d11c9a4b0945b'] = 'Impostazioni aggiornati correttamente.';
$_MODULE['<{productscategory}leo_fashion_store>productscategory_f4f70727dc34561dfde1a3c529b6205c'] = 'Impostazioni';
$_MODULE['<{productscategory}leo_fashion_store>productscategory_e06ba84b50810a88438ae0537405f65a'] = 'Prezzi Visualizzare i prodotti \"';
$_MODULE['<{productscategory}leo_fashion_store>productscategory_1d986024f548d57b1d743ec7ea9b09d9'] = 'Visualizzare i prezzi dei prodotti esposti nel blocco.';
$_MODULE['<{productscategory}leo_fashion_store>productscategory_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Attivato';
$_MODULE['<{productscategory}leo_fashion_store>productscategory_b9f5c797ebbf55adccdd8539a65a0241'] = 'Disabile';
$_MODULE['<{productscategory}leo_fashion_store>productscategory_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{productscategory}leo_fashion_store>productscategory_f55e0a28b86c2ab66ac632ab9ddf1833'] = '%s altri prodotti nella stessa categoria';
$_MODULE['<{productscategory}leo_fashion_store>productscategory_bebb44f38b03407098d48198c1d0aaa5'] = '%s altri prodotti nella stessa categoria';
$_MODULE['<{productscategory}leo_fashion_store>productscategory_dd1f775e443ff3b9a89270713580a51b'] = 'Precedente';
$_MODULE['<{productscategory}leo_fashion_store>productscategory_10ac3d04253ef7e1ddc73e6091c0cd55'] = 'Prossimo';
$_MODULE['<{productscategory}leo_fashion_store>productscategory_4bc7ec7ed34d495ddc4b42079569f684'] = 'altri prodotti nella stessa categoria';
