<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{pagesnotfound}leo_fashion_store>pagesnotfound_251295238bdf7693252f2804c8d3707e'] = 'Páginas que no se encuentra';
$_MODULE['<{pagesnotfound}leo_fashion_store>pagesnotfound_607cc8b8993662a37cac86032fb071d2'] = 'Agrega una pestaña en el salpicadero Estadísticas, que muestra las páginas solicitadas por los visitantes que no han sido encontrados.';
$_MODULE['<{pagesnotfound}leo_fashion_store>pagesnotfound_dc3a3db6b98723bf91f924537a630600'] = 'El caché de \"páginas no encontrado\" se ha vaciado.';
$_MODULE['<{pagesnotfound}leo_fashion_store>pagesnotfound_b323790d8ee3c43d317d19aea5012626'] = 'El caché de \"páginas no encontrado\" ha sido eliminado.';
$_MODULE['<{pagesnotfound}leo_fashion_store>pagesnotfound_6602bbeb2956c035fb4cb5e844a4861b'] = 'Guía';
$_MODULE['<{pagesnotfound}leo_fashion_store>pagesnotfound_3604249130acf7fda296e16edc996e5b'] = '404 errores';
$_MODULE['<{pagesnotfound}leo_fashion_store>pagesnotfound_a90083861c168ef985bf70763980aa60'] = 'Cómo atrapar a estos errores?';
$_MODULE['<{pagesnotfound}leo_fashion_store>pagesnotfound_01bd0bf7c5a68ad6ee4423118be3f7b6'] = 'Debe utilizar un archivo .htaccess para redireccionar los errores 404 a la página \"404.php\".';
$_MODULE['<{pagesnotfound}leo_fashion_store>pagesnotfound_193cfc9be3b995831c6af2fea6650e60'] = 'Página';
$_MODULE['<{pagesnotfound}leo_fashion_store>pagesnotfound_b6f05e5ddde1ec63d992d61144452dfa'] = 'Referente';
$_MODULE['<{pagesnotfound}leo_fashion_store>pagesnotfound_64d129224a5377b63e9727479ec987d9'] = 'Contador';
$_MODULE['<{pagesnotfound}leo_fashion_store>pagesnotfound_4a7a7e7cda40454cee7ec247660f8017'] = 'No hay problema \"página no encontrada\", registrada por ahora.';
$_MODULE['<{pagesnotfound}leo_fashion_store>pagesnotfound_d8847bc418fc4f5a3e37c2e8390bb9ed'] = 'Base de datos vacía';
$_MODULE['<{pagesnotfound}leo_fashion_store>pagesnotfound_b9ae3636d6e672413a163f7cb34beb84'] = 'Avisos vacío todo \"páginas no encontradas\" para este período';
$_MODULE['<{pagesnotfound}leo_fashion_store>pagesnotfound_0cf5c3a279c0e8c57b232d8c6bc3f06a'] = 'Avisos ALL \"páginas no encuentran\" vacíos';
