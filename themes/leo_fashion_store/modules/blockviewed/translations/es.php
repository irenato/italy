<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_859e85774d372c6084d62d02324a1cc3'] = 'Visto bloque de productos';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_eaa362292272519b786c2046ab4b68d2'] = 'Añade un bloque donde se presentan productos vistos.';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_2e57399079951d84b435700493b8a8c1'] = 'Debe rellenar el campo \"Productos visualiza \'.';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_73293a024e644165e9bf48f270af63a0'] = 'Número no válido.';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_f38f5974cdc23279ffe6d203641a8bdf'] = 'Ajustes actualizados.';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_f4f70727dc34561dfde1a3c529b6205c'] = 'Ajustes';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_26986c3388870d4148b1b5375368a83d'] = 'Productos para mostrar';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_d36bbb6066e3744039d38e580f17a2cc'] = 'Definir el número de productos que aparecen en este bloque.';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_43560641f91e63dc83682bc598892fa1'] = 'Productos más vistos';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_8f7f4c1ce7a4f933663d10543562b096'] = 'Acerca de';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_9d28294550803dd7ed0e5f4c704d492d'] = 'Productos al azar';
