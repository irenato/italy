<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_859e85774d372c6084d62d02324a1cc3'] = 'Vu bloc des produits';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_eaa362292272519b786c2046ab4b68d2'] = 'Ajoute un bloc affichant produits récemment consultés.';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_2e57399079951d84b435700493b8a8c1'] = 'Vous devez remplir le champ \"produits exposés.';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_73293a024e644165e9bf48f270af63a0'] = 'Nombre non valide.';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_f38f5974cdc23279ffe6d203641a8bdf'] = 'Paramètres mis à jour.';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_f4f70727dc34561dfde1a3c529b6205c'] = 'Paramètres';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_26986c3388870d4148b1b5375368a83d'] = 'Produits à afficher';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_d36bbb6066e3744039d38e580f17a2cc'] = 'Définir le nombre de produits présentés dans ce bloc.';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_43560641f91e63dc83682bc598892fa1'] = 'Produits consultés';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_8f7f4c1ce7a4f933663d10543562b096'] = 'Environ';
$_MODULE['<{blockviewed}leo_fashion_store>blockviewed_9d28294550803dd7ed0e5f4c704d492d'] = 'Produits au hasard';
