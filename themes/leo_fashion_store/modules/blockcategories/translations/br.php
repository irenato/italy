<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_8f0ed7c57fca428f7e3f8e64d2f00918'] = 'Categorias bloco';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_15a6f5841d9e4d7e62bec3319b4b7036'] = 'Adiciona um bloco com categorias de produtos.';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_23e0d4ecc25de9b2777fdaca3e2f3193'] = 'Profundidade máxima: número inválido.';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_0cf328636f0d607ac24a5c435866b94b'] = 'HTML Dinâmico: escolha inválida.';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_f4f70727dc34561dfde1a3c529b6205c'] = 'Configurações';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_1379a6b19242372c1f23cc9adedfcdd6'] = 'Raiz Categoria';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_89b278a71f2be5f620307502326587a0'] = 'Categoria Início';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_62381fc27e62649a16182a616de3f7ea'] = 'Categoria atual';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_19561e33450d1d3dfe6af08df5710dd0'] = 'A profundidade máxima';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_584d4e251b6f778eda9cfc2fc756b0b0'] = 'Defina a profundidade máxima de subníveis categoria exibidas neste bloco (0 = infinitas).';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_971fd8cc345d8bd9f92e9f7d88fdf20c'] = 'Dinâmico';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_c10efcaa2a8ff4eedaa3538fff78eb53'] = 'Ativar o modo dinâmico (animado) para categoria subníveis.';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Ativado';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_b9f5c797ebbf55adccdd8539a65a0241'] = 'Inválido';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_6b46ae48421828d9973deec5fa9aa0c3'] = 'Tipo';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_54e4f98fb34254a6678f0795476811ed'] = 'Por nome';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_883f0bd41a4fcee55680446ce7bec0d9'] = 'Por posição';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_06f1ac65b0a6a548339a38b348e64d79'] = 'Ordem de classificação';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_e3cf5ac19407b1a62c6fccaff675a53b'] = 'Descendente';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_cf3fb1ff52ea1eed3347ac5401ee7f0c'] = 'Crescente';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_5f73e737cedf8f4ccf880473a7823005'] = 'Quantas colunas de rodapé que você gostaria?';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_c9cc8cce247e49bae79f15173ce97354'] = 'Salvar';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_af1b98adf7f686b84cd0b443e022b7a0'] = 'Categorias';
$_MODULE['<{blockcategories}leo_fashion_store>blockcategories_footer_af1b98adf7f686b84cd0b443e022b7a0'] = 'Categorias';
