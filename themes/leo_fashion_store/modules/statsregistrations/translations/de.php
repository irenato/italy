<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsregistrations}leo_fashion_store>statsregistrations_8b15fc6468c919d299f9a601b61b95fc'] = 'Kundenkonten';
$_MODULE['<{statsregistrations}leo_fashion_store>statsregistrations_f14056d6fef225c8aafd5a99d4c70fa8'] = 'Fügt eine Registrierung Fortschritte Registerkarte auf die Stats Armaturenbrett.';
$_MODULE['<{statsregistrations}leo_fashion_store>statsregistrations_247b3bdef50a59d5a83f23c4f1c8fa47'] = 'Anzahl der Besucher, an der Registrierschritt gestoppt:';
$_MODULE['<{statsregistrations}leo_fashion_store>statsregistrations_479c1246d97709e234574e1d2921994d'] = 'Anzahl der Besucher, eine Bestellung direkt nach der Anmeldung gestellt:';
$_MODULE['<{statsregistrations}leo_fashion_store>statsregistrations_a751e9cc4ed4c7585ecc0d97781cb48a'] = 'Absolute Kundenkonten:';
$_MODULE['<{statsregistrations}leo_fashion_store>statsregistrations_6602bbeb2956c035fb4cb5e844a4861b'] = 'Führung';
$_MODULE['<{statsregistrations}leo_fashion_store>statsregistrations_fba0e64541196123bbf8e3737bf9287b'] = 'Anzahl Kundenkonten erstellt';
$_MODULE['<{statsregistrations}leo_fashion_store>statsregistrations_76dcf557776c2b40d47b72ebcd9ac6b7'] = 'Die Gesamtzahl der Konten erstellt ist nicht an sich wichtige Informationen. Jedoch ist es vorteilhaft, die Anzahl über die Zeit erstellt analysieren. Dies zeigt an, ob oder nicht die Dinge sind auf dem richtigen Weg. Sie fühlen sich zu mir?';
$_MODULE['<{statsregistrations}leo_fashion_store>statsregistrations_57a6f41a27c9baa5b402d30e97d4c1e8'] = 'Wie man auf die Evolution der Anmeldungen \"zu handeln?';
$_MODULE['<{statsregistrations}leo_fashion_store>statsregistrations_271ef73d55b8e3cc30963ca9413d4a52'] = 'Wenn Sie Ihren Shop laufen ohne etwas zu ändern zu lassen, sollte die Anzahl der Kundenregistrierungen stabil bleiben oder zeigen einen leichten Rückgang.';
$_MODULE['<{statsregistrations}leo_fashion_store>statsregistrations_bcabec23c8f36cecde037bd35ca4c709'] = 'Eine signifikante Zunahme oder Abnahme der Kundenregistrierung zeigt, dass es wahrscheinlich eine Änderung für Ihren Shop. Mit dem im Verstand, empfehlen wir Ihnen, die Ursache zu identifizieren, das Problem zu beheben und sich wieder in das Geschäft, Geld zu verdienen!';
$_MODULE['<{statsregistrations}leo_fashion_store>statsregistrations_ded9c8756dc14fd26e3150c4718cd9d0'] = 'Hier ist eine Zusammenfassung dessen, was die Schaffung von Kundenkonten auswirken:';
$_MODULE['<{statsregistrations}leo_fashion_store>statsregistrations_87c365f80449f43460a0567d3b24f29f'] = 'Eine Werbekampagne kann eine erhöhte Anzahl von Besuchern auf Ihren Online-Shop zu gewinnen. Dies wird wahrscheinlich durch einen Anstieg der Kundenkonten und Gewinnmargen, die auf Kunden angewiesen folgen werden \"Qualität.\" Gezielte Werbung ist in der Regel effektiver als groß angelegte Werbung ... und es ist auch billiger!';
$_MODULE['<{statsregistrations}leo_fashion_store>statsregistrations_aa09be43df78c214e64ac3c3b255708e'] = 'Specials, Vertrieb, Promotion und / oder Wettbewerbe verlangen in der Regel ein Einkaufs Aufmerksamkeiten. Mit solchen Dingen wird nicht nur Ihr Geschäft lebhaft, es erhöht auch Verkehr, Kundenbindung und wirklich ändern Sie Ihre aktuelle E-Commerce-Philosophie.';
$_MODULE['<{statsregistrations}leo_fashion_store>statsregistrations_8cb5605d77d1d2f9eab6191c0e027747'] = 'Design und Benutzerfreundlichkeit sind heute wichtiger denn je in der Welt der Online-Verkäufe. Ein schlecht gewählt oder schwer zu folgen grafische Thema können Käufer in Schach zu halten. Dies bedeutet, dass Sie sollten danach streben, die richtige Balance zwischen Schönheit und Funktionalität für Ihren Online-Shop zu finden.';
$_MODULE['<{statsregistrations}leo_fashion_store>statsregistrations_998e4c5c80f27dec552e99dfed34889a'] = 'CSV-Export';
