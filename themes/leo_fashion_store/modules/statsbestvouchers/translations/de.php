<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsbestvouchers}leo_fashion_store>statsbestvouchers_58ef962a87e6fbbea6027c17a954a18d'] = 'Leere Datensatzgruppe zurück.';
$_MODULE['<{statsbestvouchers}leo_fashion_store>statsbestvouchers_ca0dbad92a874b2f69b549293387925e'] = 'Code';
$_MODULE['<{statsbestvouchers}leo_fashion_store>statsbestvouchers_49ee3087348e8d44e1feda1917443987'] = 'Name';
$_MODULE['<{statsbestvouchers}leo_fashion_store>statsbestvouchers_11ff9f68afb6b8b5b8eda218d7c83a65'] = 'Vertrieb';
$_MODULE['<{statsbestvouchers}leo_fashion_store>statsbestvouchers_df25596dc94d556af2f1823725118572'] = 'Insgesamt gebrauchte';
$_MODULE['<{statsbestvouchers}leo_fashion_store>statsbestvouchers_b769cee333527b8dc6f3f67882e35a0b'] = 'Beste Gutscheine';
$_MODULE['<{statsbestvouchers}leo_fashion_store>statsbestvouchers_d32edaf4608c91c5795eceaa1948aea7'] = 'Fügt eine Liste der besten Gutscheine im Stats Armaturenbrett.';
$_MODULE['<{statsbestvouchers}leo_fashion_store>statsbestvouchers_998e4c5c80f27dec552e99dfed34889a'] = 'CSV-Export';
