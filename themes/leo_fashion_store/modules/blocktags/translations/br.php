<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blocktags}leo_fashion_store>blocktags_f2568a62d4ac8d1d5b532556379772ba'] = 'Etiquetas bloco';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_b2de1a21b938fcae9955206a4ca11a12'] = 'Adiciona um bloco contendo suas marcas de produtos.';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_8d731d453cacf8cff061df22a269b82b'] = 'Por favor, preencha o campo \"marcas exibidos\".';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_73293a024e644165e9bf48f270af63a0'] = 'Número inválido.';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_c888438d14855d7d96a2724ee9c306bd'] = 'Configurações atualizado';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_f4f70727dc34561dfde1a3c529b6205c'] = 'Configurações';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_726cefc6088fc537bc5b18f333357724'] = 'Marcas exibidos';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_c9cc8cce247e49bae79f15173ce97354'] = 'Salvar';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_189f63f277cd73395561651753563065'] = 'Etiquetas';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_49fa2426b7903b3d4c89e2c1874d9346'] = 'Mais sobre';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_4e6307cfde762f042d0de430e82ba854'] = 'Sem etiquetas foram especificados ainda.';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_70d5e9f2bb7bcb17339709134ba3a2c6'] = 'Nenhuma tag ainda especificado';
