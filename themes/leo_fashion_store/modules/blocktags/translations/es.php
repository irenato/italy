<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blocktags}leo_fashion_store>blocktags_f2568a62d4ac8d1d5b532556379772ba'] = 'Etiquetas bloque';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_b2de1a21b938fcae9955206a4ca11a12'] = 'Añade un bloque que contiene las etiquetas de productos.';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_8d731d453cacf8cff061df22a269b82b'] = 'Por favor, complete el campo \"etiquetas que se muestran\".';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_73293a024e644165e9bf48f270af63a0'] = 'Número no válido.';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_c888438d14855d7d96a2724ee9c306bd'] = 'Ajustes actualizan';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_f4f70727dc34561dfde1a3c529b6205c'] = 'Ajustes';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_726cefc6088fc537bc5b18f333357724'] = 'Las etiquetas que se muestran';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_189f63f277cd73395561651753563065'] = 'Etiquetas';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_49fa2426b7903b3d4c89e2c1874d9346'] = 'Más sobre';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_4e6307cfde762f042d0de430e82ba854'] = 'No hay etiquetas se han especificado aún.';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_70d5e9f2bb7bcb17339709134ba3a2c6'] = 'Sin etiquetas especifican';
