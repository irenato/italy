<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blocktags}leo_fashion_store>blocktags_f2568a62d4ac8d1d5b532556379772ba'] = 'Balises block';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_b2de1a21b938fcae9955206a4ca11a12'] = 'Ajoute un bloc contenant vos étiquettes de produit.';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_8d731d453cacf8cff061df22a269b82b'] = 'Se il vous plaît remplir le champ \"tags affichés\".';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_73293a024e644165e9bf48f270af63a0'] = 'Nombre non valide.';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_c888438d14855d7d96a2724ee9c306bd'] = 'Paramètres mis à jour';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_f4f70727dc34561dfde1a3c529b6205c'] = 'Paramètres';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_726cefc6088fc537bc5b18f333357724'] = 'Variables affichées';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_189f63f277cd73395561651753563065'] = 'Mots clés';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_49fa2426b7903b3d4c89e2c1874d9346'] = 'En savoir plus sur';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_4e6307cfde762f042d0de430e82ba854'] = 'Pas de tags ont encore été précisées.';
$_MODULE['<{blocktags}leo_fashion_store>blocktags_70d5e9f2bb7bcb17339709134ba3a2c6'] = 'Aucuns Tags';
