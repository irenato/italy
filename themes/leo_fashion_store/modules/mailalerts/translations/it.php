<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{mailalerts}leo_fashion_store>mailalerts-account_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'Il mio account';
$_MODULE['<{mailalerts}leo_fashion_store>mailalerts-account_4edfd10d0bb5f51e0fd2327df608b5a8'] = 'I miei avvisi';
$_MODULE['<{mailalerts}leo_fashion_store>mailalerts-account_8bb23c2ae698681ebb650f43acb54dab'] = 'Non ci sono ancora mail.';
$_MODULE['<{mailalerts}leo_fashion_store>mailalerts-account_0b3db27bc15f682e92ff250ebb167d4b'] = 'Torna al tuo account';
$_MODULE['<{mailalerts}leo_fashion_store>product_61172eb93737ebf095d3fa02119ce1df'] = 'Richiesta di notifica registrata';
$_MODULE['<{mailalerts}leo_fashion_store>product_546e02eaa9a986c83cc347e273269f2c'] = 'Avvisami quando disponibile';
$_MODULE['<{mailalerts}leo_fashion_store>mailalerts-account_4642251c6daab2d9fcc03be352ea5339'] = 'Il mio Charly';
