<section id="scrollme" class=" not-fullscreen background parallax section section_intro scrollme" style="background: url({$base_dir}img/about-us/{$cms->hero_image})" data-image-src= "{$base_dir}img/about-us/{$cms->hero_image}" data-diff="100"  data-img-width="1600" data-img-height="600">
			    <div class="hero-content hero-content-for-pages">
			      <h6 class=" wow fadeIn fittext animateme" data-wow-duration="1.2s" data-wow-delay="0.4s" data-when="exit" data-from="0" data-to="0.75" data-opacity="0" data-translatey="-250" style="opacity: 1; transform: translate3d(0px, 0px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scale3d(1, 1, 1); "> {$cms->caption_1}</h6>
			      <h6 class=" wow fadeIn animateme"  data-wow-duration="1.2s" data-wow-delay="0.4s" data-when="exit" data-from="0" data-to="0.75" data-opacity="0" data-translatey="-150" style="opacity: 1; transform: translate3d(0px, 0px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scale3d(1, 1, 1);">{$cms->caption_2}</h6>
			    </div>
			    <div class="arrow-down wow pulse infinite " data-wow-duration="1s" data-wow-delay=".2s"> <img src="img/arrow-down.png"> </div>
		    </section>
  <!----/section-1--->
  <!--section-2-->
  <section id="sec-02" class="section section-story">
    <div class="container">
      <div class="row">
              <div class="colum-left">
                     <div class=" col-xs-12 col-sm-7 ">
                  <p class="titles-story wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0.4s">Esattamente a Casette d'Ete, piccola frazione di Sant'Elpidio a Mare, nel cuore delle Marche, dove sorge il distretto calzaturiero del Fermano – Maceratese, sede di famose griffes che rappresentano il Made in Italy nel mondo. </p>
                </div>
               </div>
               <div class="colum-right">
       				 <div class=" col-xs-12 col-sm-5 pull-right padding-top">
                              <ul class="effects plain clearfix scrollme">
                                <li class="effect">
                                  <div class="  wow fadeIn effect_box effect_box_rotate animateme" data-wow-duration="1.2s" data-wow-delay="0.4s" data-when="view" data-from="0.6" data-to="0.1" data-rotatex="-90" data-rotatey="90" data-rotatez="-90" style="opacity: 1; transform: translate3d(0px, 0px, 0px) rotateX(-0deg) rotateY(0deg) rotateZ(-0deg) scale3d(1, 1, 1);">
                                   <img src="img/project-mg.png"></div>
                                </li>
                              </ul>
        			</div>
        </div>
      </div>
    </div>
  </section>
  <!----/section2--->
  <!----section-3---->
  <section  class=" sectoion-03 bg-03 section section-story ">
  			<div class="container ">
            		<div class="row">
                    	<div class="scrollme">
                            <div class="col-sm-3 wow fadeIn width-100px custom-widthpx" data-wow-duration="1.2s" data-wow-delay="0.4s">
                                    <img class="slide-img animateme art-sign" src="img/side-logo.png" data-when="enter" data-from="0.75" data-to="0" data-opacity="0" data-translatex="-400" style="opacity: 1; transform: translate3d(0px, 0px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scale3d(1, 1, 1);">
                            </div>
                            <div class="col-sm-9">

                            <p class=" wow fadeIn titles-story text22 width-text-smaller  text-color-white animateme" data-wow-duration="1.2s" data-wow-delay="0.4s" data-when="enter" data-from="0.75" data-to="0" data-opacity="0" data-translatex="400" style="opacity: 1; transform: translate3d(0px, 0px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scale3d(1, 1, 1);">
                    Primo Morlacco, negli anni susseguenti la prima guerra mondiale avviò un piccolo commercio ambulante di calzature che partì dalle Marche e lo portò in Veneto.
                </p>

                            </div>
                        </div>
                    </div>
            </div>
  </section>
  <!----/section-3---->
  <!----section-4---->
  <div class="paralax-story sec-04 section triangle-img"  data-parallax="scroll">
        <section class="section-story scrollme">
        		<div class="container">
                		<div class="row">
                        		<div class="col-sm-6">
                                		<div class="content-a">
                                        		<div class="content-b">
                                						<div class="titles-story  animateme " data-when="enter" data-from="0.5" data-to="0" data-crop="false" data-opacity="0" data-scale="1.5" style="opacity: 1; transform: translate3d(0px, 0px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scale3d(1, 1, 1);">
                                              <p class="worldcore text22 wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0.4s">
                                                Negli anni immediatamente seguenti al secondo conflitto venne affiancato dai figli Ferruccio e Virginio.
                                              </p>
                										</div>
                                        		</div>
                                        </div>
                                </div>
                        </div>
                </div>
        </section>
  </div>
  <!-----/section-4---->
  <!---section-05---->
  <section class="section-story section-options">
  		<div class="scrollme">
        		<div class="col-md-4 ss s1 border-left-none animateme" data-when="view" data-from="0.55" data-to="0.05" data-scale="0" style="opacity: 1; transform: translate3d(0px, 0px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scale3d(1, 1, 1);">
                <div class="shadows">
                    <div class="option-center-text">
                        <p class="text19 wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0.4s">Nel 2008 entra in azienda e nel mondo delle calzature Luca, nipote di Ferruccio, il quale negli anni affina il gusto e matura un esperienza unica.</p>
                    </div>
                </div>
            </div>
           		 <div class="col-md-4 ss s2 animateme" data-when="view" data-from="0.55" data-to="0.05" data-scale="0" style="opacity: 1; transform: translate3d(0px, 0px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scale3d(1, 1, 1);">
                <div class="shadows">
                    <div class="option-center-text">
                        <p class="text19 wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0.4s">Nel 2015 sulla base dell'esperienza maturata, grazie ad uno spiccato spirito imprenditoriale, fonda la  Mosca SaS e rileva Charly Calzature, riferimento storico per le calzature di moda in Friuli.</p>
                    </div>
                </div>
            </div>
            	<div class="col-md-4 ss s3 border-right-none animateme" data-when="view" data-from="0.55" data-to="0.05" data-scale="0" style="opacity: 1; transform: translate3d(0px, 0px, 0px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) scale3d(1, 1, 1);">
                <div class="shadows">
                    <div class="option-center-text">
                        <p class="text19 wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0.4s">Con uno sguardo sempre proiettato al futuro con lo scopo di ampliare i propri orizzonti commerciali, decide di affiancare il punto vendita con lo shop-online: </p>
                    			<a href="#" class="wow fadeIn " data-wow-duration="1.2s" data-wow-delay="0.4s">www.charlyselection.com</a>
                    </div>
                </div>
            </div>
        </div>
  </section>