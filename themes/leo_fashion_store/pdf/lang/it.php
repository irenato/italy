<?php

global $_LANGPDF;
$_LANGPDF = array();
$_LANGPDF['PDF023f66b09075fa1cc29e74bed9f5ac40'] = 'Dettaglio IVA';
$_LANGPDF['PDF03ab340b3f99e03cff9e84314ead38c0'] = 'Qtà';
$_LANGPDF['PDF11ef70f0c7f9fed90c43dffb9a0e813e'] = 'Totale (IVA incl.)';
$_LANGPDF['PDF197101c4a1b1fc503dcd6ebee127aa10'] = 'Prezzo Unitario';
$_LANGPDF['PDF1e860e56970a81a1ba3e1fcb7fccc846'] = 'Numero d\'ordine';
$_LANGPDF['PDF20a34c4e30c5bbe1d3f870ac55f0d831'] = 'IVA';
$_LANGPDF['PDF28a59051cd90053f87bacd5f1ffbc0b8'] = 'Indirizzo di Fatturazione';
$_LANGPDF['PDF2f2f0f119a907c6c67a3c6fcde0193ab'] = 'Indirizzo di Consegna';
$_LANGPDF['PDF4118e1a6606ae9bff5134ba3cf50db67'] = 'Totale (IVA escl.)';
$_LANGPDF['PDF440ac606343b8a66acad0d2978786d2a'] = 'Data Fattura';
$_LANGPDF['PDF466eadd40b3c10580e3ab4e8061161ce'] = 'Fattura';
$_LANGPDF['PDF4e065ba1bec1d62b2d5450256612fa96'] = 'Numero Fattura';
$_LANGPDF['PDF559e7ca805230fc80e3644f87bb3994d'] = 'Data Ordine';
$_LANGPDF['PDF63d5049791d9d79d86e9a108b0a999ca'] = 'Riferimento';
$_LANGPDF['PDF6fbc355a02359656f1e4540d58b784e1'] = 'Data Ordine';
$_LANGPDF['PDF707436a5aa13b82a4d777f64c717a625'] = 'Metodo di Pagamento';
$_LANGPDF['PDF8a5a12ba783d8cbb761974430d0a4d6a'] = 'Data Ordine';
$_LANGPDF['PDF8fe77c2601e54f1aaef28cfde997bbad'] = 'IVA';
$_LANGPDF['PDF96b0141273eabab320119c467cdcaf17'] = 'Totale';
$_LANGPDF['PDF9fec2102dd0b5b29d6cdf9d5614fabf7'] = '(IVA escl.)';
$_LANGPDF['PDFa5e35abc0c9e2d2784d0ef619b36448b'] = 'Costo Spedizione';
$_LANGPDF['PDFad0d28cdd9113d3ce911bc064b137cde'] = 'Prezzo base';
$_LANGPDF['PDFb00b85425e74ed2c85dc3119b78ff2c3'] = 'Spedizione Gratuita';
$_LANGPDF['PDFb0f79042ac83c64f3ccc42268c8ade26'] = 'Totale Prodotti';
$_LANGPDF['PDFb602e0d0c6a72053d0a5be60cb2f8126'] = 'Totale IVA';
$_LANGPDF['PDFb94cb106eaa958b2ab473da305e57977'] = 'Totale (IVA escl.)';
$_LANGPDF['PDFdeb10517653c255364175796ace3553f'] = 'Prodotto';

?>