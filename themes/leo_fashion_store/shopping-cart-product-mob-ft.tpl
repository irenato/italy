

<div class="mainTotal">

{assign var='rowspan_total' value=2+$total_discounts_num+$total_wrapping_taxes_num}

{if $use_taxes && $show_taxes && $total_tax != 0}
{assign var='rowspan_total' value=$rowspan_total+1}
{/if}

{if $priceDisplay != 0}
{assign var='rowspan_total' value=$rowspan_total+1}
{/if}

{if $total_shipping_tax_exc <= 0 && (!isset($isVirtualCart) || !$isVirtualCart) && $free_ship}
{assign var='rowspan_total' value=$rowspan_total+1}
{else}
{if $use_taxes && $total_shipping_tax_exc != $total_shipping}
{if $priceDisplay && $total_shipping_tax_exc > 0}
{assign var='rowspan_total' value=$rowspan_total+1}
{elseif $total_shipping > 0}
{assign var='rowspan_total' value=$rowspan_total+1}
{/if}
{elseif $total_shipping_tax_exc > 0}
{assign var='rowspan_total' value=$rowspan_total+1}
{/if}
{/if}


{if $use_taxes}

{if $priceDisplay}


<div class="lastForm">

	<div class="form" id="cart_voucher">

		{if $voucherAllowed}
		<form action="{if $opc}{$link->getPageLink('order-opc', true)}{else}{$link->getPageLink('order', true)}{/if}" method="post" id="voucher">
			<fieldset>
				<h4>{l s='Vouchers'}</h4>
				<input type="text" class="discount_name form-control" id="discount_name" name="discount_name" value="{if isset($discount_name) && $discount_name}{$discount_name}{/if}" />
				<input type="hidden" name="submitDiscount" />
				<button type="submit" name="submitAddDiscount" class="button btn btn-outline button-small btn-sm"><span>{l s='OK'}</span></button>
			</fieldset>
		</form>
		{if $displayVouchers}
		<p id="title" class="title-offers">{l s='Take advantage of our exclusive offers:'}</p>
		<div id="display_cart_vouchers">
			{foreach $displayVouchers as $voucher}
			{if $voucher.code != ''}<span class="voucher_name" data-code="{$voucher.code|escape:'html':'UTF-8'}">{$voucher.code|escape:'html':'UTF-8'}</span> - {/if}{$voucher.name}<br />
			{/foreach}
		</div>
		{/if}
		{/if}
	</div>
</div>


<div class="totalPrice">

	<div class="totalWrap"><span>{if $display_tax_label}{l s='Total price (tax incl.)'}{else}{l s='Total price'}{/if}</span> <span id="total_product"> {displayPrice price=$total_products_wt}</span></div>


</div>

{else}


{if $voucherAllowed}
<div class="lastForm">
	<div class="form" id="cart_voucher">
		<form action="{if $opc}{$link->getPageLink('order-opc', true)}{else}{$link->getPageLink('order', true)}{/if}" method="post" id="voucher">
			<fieldset>
				<h4>{l s='Vouchers'}</h4>
				<input type="text" class="discount_name form-control" id="discount_name" name="discount_name" value="{if isset($discount_name) && $discount_name}{$discount_name}{/if}" />
				<input type="hidden" name="submitDiscount" />
				<button type="submit" name="submitAddDiscount" class="button btn btn-outline button-small btn-sm"><span>{l s='OK'}</span></button>
			</fieldset>
		</form>
		{if $displayVouchers}
		<p id="title" class="title-offers">{l s='Take advantage of our exclusive offers:'}</p>
		<div id="display_cart_vouchers">
			{foreach $displayVouchers as $voucher}
			{if $voucher.code != ''}<span class="voucher_name" data-code="{$voucher.code|escape:'html':'UTF-8'}">{$voucher.code|escape:'html':'UTF-8'}</span> - {/if}{$voucher.name}<br />
			{/foreach}
		</div>
		{/if}
		{/if}
	</div>
</div>

<div class="totalPrice">
	<div class="totalWrap"><span>{if $display_tax_label}{l s='Total price (tax incl.)'}{else}{l s='Total price'}{/if}</span> <span id="total_product">{displayPrice price=$total_products_wt}</span></div>
</div>

{/if}
{else}

<div class="lastForm">
	<div class="form" id="cart_voucher">
		{if $voucherAllowed}
		<form action="{if $opc}{$link->getPageLink('order-opc', true)}{else}{$link->getPageLink('order', true)}{/if}" method="post" id="voucher">
			<fieldset>
				<h4>{l s='Vouchers'}</h4>
				<input type="text" class="discount_name form-control" id="discount_name" name="discount_name" value="{if isset($discount_name) && $discount_name}{$discount_name}{/if}" />
				<input type="hidden" name="submitDiscount" />
				<button type="submit" name="submitAddDiscount" class="button btn btn-outline button-small btn-sm">
					<span>{l s='OK'}</span>
				</button>
			</fieldset>
		</form>
		{if $displayVouchers}
		<p id="title" class="title-offers">{l s='Take advantage of our exclusive offers:'}</p>
		<div id="display_cart_vouchers">
			{foreach $displayVouchers as $voucher}
			{if $voucher.code != ''}<span class="voucher_name" data-code="{$voucher.code|escape:'html':'UTF-8'}">{$voucher.code|escape:'html':'UTF-8'}</span> - {/if}{$voucher.name}<br />
			{/foreach}
		</div>
		{/if}
		{/if}



	</div>
</div>

<div class="totalPrice">

	<div class="totalWrap"><span>{l s='Total price'}</span> <span id="total_product">{displayPrice price=$total_products}</span></div>

</div>


{/if}

<div class="totalPrice"  {if $total_wrapping == 0} style="display: none;"{/if}>

	<div class="totalWrap">
		<span>
			{if $use_taxes}
			{if $display_tax_label}{l s='Total gift wrapping (tax incl.)'}{else}{l s='Total gift-wrapping cost'}{/if}
			{else}
			{l s='Total gift-wrapping cost'}
			{/if}
		</span> 
		<span id="total_wrapping" >
			{if $use_taxes}
			{if $priceDisplay}
			{displayPrice price=$total_wrapping_tax_exc}
			{else}
			{displayPrice price=$total_wrapping}
			{/if}
			{else}
			{displayPrice price=$total_wrapping_tax_exc}
			{/if}
		</span>
	</div>

</div>

{if $total_shipping_tax_exc <= 0 && (!isset($isVirtualCart) || !$isVirtualCart) && $free_ship}


<div class="totalPrice cart_total_delivery{if !$opc && (!isset($cart->id_address_delivery) || !$cart->id_address_delivery)} unvisible{/if}">

	<div class="totalWrap"><span>{l s='Total shipping'}</span> <span id="total_shipping">{l s='Free shipping!'}</span></div>

</div>

{else}
{if $use_taxes && $total_shipping_tax_exc != $total_shipping}
{if $priceDisplay}

<div class="totalPrice cart_total_delivery{if $total_shipping_tax_exc <= 0} unvisible{/if}">

	<div class="totalWrap"><span>{if $display_tax_label}{l s='Total shipping (tax excl.)'}{else}{l s='Total shipping'}{/if}</span> <span id="total_shipping" >{displayPrice price=$total_shipping_tax_exc}</span></div>

</div>

{else}

<div class="totalPrice cart_total_delivery{if $total_shipping <= 0} unvisible{/if}">

	<div class="totalWrap"><span>{if $display_tax_label}{l s='Total shipping (tax incl.)'}{else}{l s='Total shipping'}{/if}</span> <span id="total_shipping">{displayPrice price=$total_shipping}</span></div>

</div>
{/if}
{else}

<div class="totalPrice cart_total_delivery{if $total_shipping_tax_exc <= 0} unvisible{/if}">

	<div class="totalWrap"><span>{l s='Total shipping'}</span> <span id="total_shipping" >{displayPrice price=$total_shipping_tax_exc}</span></div>

</div>
{/if}
{/if}

<div class="totalPrice cart_total_voucher{if $total_discounts == 0} unvisible{/if}">

	<div class="totalWrap">
		<span>{if $display_tax_label}
			{if $use_taxes && $priceDisplay == 0}
			{l s='Total vouchers (tax incl.)'}
			{else}
			{l s='Total vouchers (tax excl.)'}
			{/if}
			{else}
			{l s='Total vouchers'}
			{/if}
		</span> 
		<span id="total_discount">
			{if $use_taxes && $priceDisplay == 0}
			{assign var='total_discounts_negative' value=$total_discounts * -1}
			{else}
			{assign var='total_discounts_negative' value=$total_discounts_tax_exc * -1}
			{/if}
			{displayPrice price=$total_discounts_negative}
		</span>
	</div>

</div>
{if $use_taxes && $show_taxes && $total_tax != 0 }
{if $priceDisplay != 0}

<div class="totalPrice">

	<div class="totalWrap"><span>{if $display_tax_label}{l s='Total (tax excl.)'}{else}{l s='Total'}{/if}</span> <span id="total_price_without_tax" >{displayPrice price=$total_price_without_tax}</span></div>

</div>

{/if}
<div class="totalPrice">

	<div class="totalWrap"><span>{l s='Tax'}</span> <span id="total_tax" >{displayPrice price=$total_tax}</span></div>

</div>
{/if}


<div class="totalPrice ">

	<div class="totalWrap"><span>{l s='Total'}</span>  
		<div class="hookDisplayProductPriceBlock-price">
			{hook h="displayCartTotalPriceLabel"}
		</div>
{if $use_taxes}

<span id="total_price">{displayPrice price=$total_price}</span>


{else}

<span id="total_price">{displayPrice price=$total_price_without_tax}</span>

{/if}



	</div>

</div>

</div>