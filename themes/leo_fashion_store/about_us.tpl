{if count($contents)}
{$count=0}
	<div class="row">
      <div class="col-md-12">
      <!-- slider ---->
        <ul class="bxslider clearfix">
	{foreach from=$contents item=content}
          <li class="row {if $count++ > 0}hide{/if}">
            <div class="left-sec col-md-6 col-sm-6 col-xs-12"> <img src="{$base_dir}img/about-img/{$content.image}" />
            <div class="overlay white"></div>
            </div>
            <div class="right-sec col-md-6 col-sm-6 col-xs-12">
              <div class="title">
                <h2 class="page-heading"> {$content.title}  </h2>
              </div>
              <div class="desc">
              <p>{$content.description}</p>
              </div>
            </div>
          </li>
{/foreach}
          <!--/slider-->
        </ul>
      </div>
    </div>
<script type="text/javascript">
$(document).ready(function(){ldelim}
  $('.bxslider').find('li').removeClass('hide');
	$('.bxslider').bxSlider({ldelim}auto: true{rdelim});
{rdelim});	
</script>
{/if}