<?php
/**
* This source file is subject to the Open Software License (OSL 3.0)
*
*  @author    Vipcom <info@vipcom.vn>
*  @copyright 2015 Vipcom
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

class Tools extends ToolsCore
{
    public static function str2url($str)
    {
        if (version_compare(_PS_VERSION_, '1.5.2.0', '>')) {
            return parent::str2url($str);
        }

        // For Prestashop 1.5.2.0 or older
        if (function_exists('mb_strtolower')) {
            $str = mb_strtolower($str, 'utf-8');
        }

        $str = trim($str);
        if (!function_exists('mb_strtolower')) {
            $str = Tools::replaceAccentedChars($str);
        }

        // Remove all non-whitelist chars.
        //$str = preg_replace('/[^a-zA-Z0-9\s\'\:\/\[\]-\pL]/u', '', $str);
        $str = preg_replace('/[^a-zA-Z0-9\s\'\:\/\[\]-]/', '', $str);
        $str = preg_replace('/[\s\'\:\/\[\]-]+/', ' ', $str);
        $str = str_replace(array(' ', '/'), '-', $str);

        // If it was not possible to lowercase the string with mb_strtolower, we do it after the transformations.
        // This way we lose fewer special chars.
        if (!function_exists('mb_strtolower')) {
            $str = Tools::strtolower($str);
        }

        return $str;
    }
}
