<?php
/**
* This source file is subject to the Open Software License (OSL 3.0)
*
*  @author    Vipcom <info@vipcom.vn>
*  @copyright 2015 Vipcom
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

class VipAdvancedUrl extends Module
{
    public function __construct()
    {
        $this->name = 'vipadvancedurl';
        $this->displayName = 'Advanced URL';
        $this->tab = 'seo';
        $this->version = '1.3.3';
        $this->author = 'Vipcom';
        $this->description = $this->l('Remove id from friendly url.');
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.6');
        $this->module_key = 'bb168ebc6fb6a66fdc8eecf0a1c3c4c6';
        $this->bootstrap = true;
        $this->_html = '';
        parent::__construct();
    }

    public function install()
    {
        return parent::install()
            && Configuration::updateValue('VIP_ADVANCED_URL_CATEGORIES', '0')
            && Configuration::updateValue('VIP_ADVANCED_URL_CHECK', '0')
            && Configuration::updateValue('VIP_ADVANCED_URL_REDIRECT', '0')
            && Configuration::updateValue('VIP_ADVANCED_URL_DISPATCHER', '0')
            && $this->registerHook('actionDispatcher')
            && $this->registerHook('moduleRoutes');
    }

    public function uninstall()
    {
        return parent::uninstall()
            && Configuration::deleteByName('VIP_ADVANCED_URL_CATEGORIES')
            && Configuration::deleteByName('VIP_ADVANCED_URL_CHECK')
            && Configuration::deleteByName('VIP_ADVANCED_URL_REDIRECT')
            && Configuration::deleteByName('VIP_ADVANCED_URL_DISPATCHER');
    }

    public function getContent()
    {
        if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('VIP_ADVANCED_URL_CHECK', Tools::getValue('VIP_ADVANCED_URL_CHECK'));
            Configuration::updateValue('VIP_ADVANCED_URL_REDIRECT', Tools::getValue('VIP_ADVANCED_URL_REDIRECT'));
            Configuration::updateValue('VIP_ADVANCED_URL_CATEGORIES', Tools::getValue('VIP_ADVANCED_URL_CATEGORIES'));
            Configuration::updateValue('VIP_ADVANCED_URL_DISPATCHER', Tools::getValue('VIP_ADVANCED_URL_DISPATCHER'));
            $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
        }

        $newbo = version_compare(_PS_VERSION_, '1.6.0.0', '>=');
        $u = $this->context->shop->getBaseURL();
        $fields = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Setting'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => $newbo ? 'switch' : 'radio',
                        'label' => $this->l('Parent product\'s categories in URL'),
                        'desc' => $this->l('Support url like').' '.$u
                            .'parent-category/child-category/.../rewrite/.<br />'
                            .$this->l('This option overrides')
                            .' <span style="color:#f00;font-weight:bold">Link.php</span>',
                        'name' => 'VIP_ADVANCED_URL_CATEGORIES',
                        'class' => 't',
                        'values' => array(
                            array(
                                'id' => 'parent1',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'parent0',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => $newbo ? 'switch' : 'radio',
                        'label' => $this->l('Check for dupplicated url'),
                        'desc' => $this->l('Display error message for dupplicated friendly url
                            when you edit categories,products,...'),
                        'name' => 'VIP_ADVANCED_URL_CHECK',
                        'class' => 't',
                        'values' => array(
                            array(
                                'id' => 'dup1',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'dup0',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'radio',
                        'label' => $this->l('Redirect'),
                        'desc' => $this->l('Redirect old url with id to new url without id,
                            auto add ending slash to category url,...'),
                        'name' => 'VIP_ADVANCED_URL_REDIRECT',
                        'class' => 't',
                        'values' => array(
                            array(
                                'id' => 'ret0',
                                'value' => 0,
                                'label' => $this->l('None')
                            ),
                            array(
                                'id' => 'ret1',
                                'value' => 1,
                                'label' => $this->l('301 - Permanent')
                            ),
                            array(
                                'id' => 'ret2',
                                'value' => 2,
                                'label' => $this->l('302 - Temporary')
                            )
                        )
                    ),
                    array(
                        'type' => $newbo ? 'switch' : 'radio',
                        'label' => $this->l('Advanced Dispatcher'),
                        'desc' => $this->l('Support url without ending slash or .html, support dupplicated
                            url schema.').'<br />'.$this->l('This option overrides')
                            .' <span style="color:#f00;font-weight:bold">Dispatcher.php</span>',
                        'name' => 'VIP_ADVANCED_URL_DISPATCHER',
                        'class' => 't',
                        'values' => array(
                            array(
                                'id' => 'disp1',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'disp0',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );
        $h = new HelperForm();
        $h->show_toolbar = false;
        $h->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $h->default_form_language = $lang->id;
        $h->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?
            Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $h->identifier = $this->identifier;
        $h->submit_action = 'btnSubmit';
        $h->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.
            '&tab_module='.$this->tab.'&module_name='.$this->name;
        $h->token = Tools::getAdminTokenLite('AdminModules');
        $h->tpl_vars = array(
            'fields_value' => array(
                'VIP_ADVANCED_URL_CATEGORIES' => Tools::getValue(
                    'VIP_ADVANCED_URL_CATEGORIES',
                    Configuration::get('VIP_ADVANCED_URL_CATEGORIES')
                ),
                'VIP_ADVANCED_URL_CHECK' => Tools::getValue(
                    'VIP_ADVANCED_URL_CHECK',
                    Configuration::get('VIP_ADVANCED_URL_CHECK')
                ),
                'VIP_ADVANCED_URL_REDIRECT' => Tools::getValue(
                    'VIP_ADVANCED_URL_REDIRECT',
                    Configuration::get('VIP_ADVANCED_URL_REDIRECT')
                ),
                'VIP_ADVANCED_URL_DISPATCHER' => Tools::getValue(
                    'VIP_ADVANCED_URL_DISPATCHER',
                    Configuration::get('VIP_ADVANCED_URL_DISPATCHER')
                )
            ),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        $this->_html .= $h->generateForm(array($fields));

        if ($newbo) {
            $tags = array('b1' => '', 'b2' => '',
                'p1' => '<div class="panel"><div class="panel-heading"><i class="icon-cogs"></i> ',
                'p2' => '</div>', 'p3' => '</div>'
            );
        } else {
            $tags = array('b1' => '<button>', 'b2' => '</button>',
                'p1' => '<fieldset id="fieldset_0"><legend>',
                'p2' => '</legend>', 'p3' => '</fieldset>'
            );
        }

        $s = Tools::getValue('detail');
        $c = array(
            'product' => array(2, $this->l('Products'), 'Products'),
            'category' => array(2, $this->l('Categories'), 'Categories'),
            'supplier' => array(0, $this->l('Suppliers'), 'Suppliers'),
            'manufacturer' => array(0, $this->l('Manufacturers'), 'Manufacturers'),
            'cms_category' => array(1, $this->l('CMS Categories'), 'CmsContent'),
            'cms' => array(1, $this->l('CMS'), 'CmsContent', 'meta_title'),
        );
        $this->_html .= $tags['p1'].$this->l('Checking for duplicated friendly url...').$tags['p2']
            .'<p>Please keeps all your friendly\'s url differently.</p><table class="table">';
        $db = Db::getInstance(_PS_USE_SQL_SLAVE_);
        $l = $this->context->language->countActiveLanguages();
        foreach ($c as $k => $v) {
            $n = $db->getValue('SELECT count(`id_'.$k.'`) as counter FROM `'._DB_PREFIX_.$k.($v[0] ? '_lang' : '').'`
                GROUP BY '.($v[0] ? '`link_rewrite`, `id_lang`'.($v[0] == 2 ? ', `id_shop`' : '') : '`name`').'
                HAVING count(`'.($v[0] ? 'link_rewrite' : 'name').'`) > 1');
            if ($n && $s == $k) {
                $sql = 'SELECT `id_'.$k.'` as id, `'.(isset($v[3]) ? $v[3].'` as `' : '').'name`, '
                    .($v[0] ? '`id_lang`, `link_rewrite`' : '`name`').' as link_rewrite FROM `'
                    ._DB_PREFIX_.$k.($v[0] ? '_lang' : '').'`
                    WHERE `'.($v[0] ? 'link_rewrite' : 'name').'`
                    IN (SELECT `'.($v[0] ? 'link_rewrite' : 'name').'` FROM `'._DB_PREFIX_.$k.($v[0] ? '_lang' : '').'`
                    GROUP BY '.($v[0] ? '`link_rewrite`, `id_lang`'.($v[0] == 2 ? ', `id_shop`' : '') : '`name`').'
                    HAVING count(`'.($v[0] ? 'link_rewrite' : 'name').'`) > 1)
                    GROUP BY `id`';
                $d = $db->executeS($sql);
                $n = count($d);
                for ($i = 0; $i < $n; $i++) {
                    if (!$v[0]) {
                        $d[$i]['link_rewrite'] = str_replace(
                            ' ',
                            '-',
                            trim(Tools::strtolower($d[$i]['link_rewrite']))
                        );
                    }
                    $this->_html .= '<tr class="odd">'.($i == 0 ? '<td rowspan="'.$n.'">'.$v[1].'</td>' : '').'<td>'
                        .$d[$i]['id'].'</td><td>'.$d[$i]['name'].'</td><td>'
                        .(($l && isset($d[$i]['id_lang'])) ? $this->context->language->getIsoById($d[$i]['id_lang']) :
                        '').'</td><td>/'.$d[$i]['link_rewrite'].'</td><td><a href="'
                        .$this->context->link->getAdminLink('Admin'.$v[2], true).'&id_'.$k.'='.$d[$i]['id']
                        .'&update'.$k.'" target="_blank" class="btn btn-default">'.$tags['b1'].'Edit'.$tags['b2']
                        .'</a></td></tr>';
                }
            } else {
                $this->_html .= '<tr><td>'.$v[1].'</td><td colspan="4"><span style="color:#'.($n ? 'f00' : '090')
                    .';"><strong>'.($n ? $n : 0).'</strong> '.$this->l('duplicated').'</span></td><td>'
                    .($n ? '<a href="'.$this->context->link->getAdminLink('AdminModules', true)
                    .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&detail='.$k
                    .'" class="btn btn-default">'.$tags['b1'].'Detail...'.$tags['b2'].'</a>' : '').'</td></tr>';
            }
        }
        $this->_html .= '</table>'.$tags['p3'].$tags['p1'].$this->l('Help').$tags['p2'].'<div>
<p>Click here goto <a href="'.$this->context->link->getAdminLink('AdminMeta', true).'">SEO & URLs</a>
 that let you customize your shop\'s url.</p>
<p>Default route: this is the best choice for SEO & SEF. Your shop\'s url will be more search engine friendly.</p>
<table class="table">
<tr><td>MODULE</td><td>ROUTE</td><td>URL</td></tr>
<tr><td>Category</td><td>{rewrite}/</td><td>'.$u.'women/</td></tr>
<tr><td>Product</td><td>{category:/}{rewrite}.html</td><td>'.$u.'women/printed-dress.html</td></tr>
<tr><td>Manufacturer</td><td>manufacturers/{rewrite}.html</td><td>'.$u.'manufacturers/adidas.html</td></tr>
<tr><td>Supplier</td><td>supplier/{rewrite}.html</td><td>'.$u.'supplier/prestashop.html</td></tr>
<tr><td>CMS category</td><td>content/{rewrite}/</td><td>'.$u.'content/news/</td></tr>
<tr><td>CMS page</td><td>content/{rewrite}.html</td><td>'.$u.'content/policy.html</td></tr>
</table>
<p>Other routes: for one, who do not like default route.</p>
<table class="table">
<tr><td>MODULE</td><td>ROUTE</td><td>URL</td></tr>
<tr><td>Category</td><td>{rewrite}.html</td><td>'.$u.'women.html</td></tr>
<tr><td>Category</td><td>c-{rewrite}.html</td><td>'.$u.'c-women.html</td></tr>
<tr><td>Category</td><td>{rewrite}</td><td>'.$u.'women</td></tr>
<tr><td>Category</td><td>{categories:/}{rewrite}/</td><td>'.$u.'women/tops/tshirts/</td></tr>
<tr><td>Product</td><td>{rewrite}.html</td><td>'.$u.'printed-dress.html</td></tr>
<tr><td>Product</td><td>{rewrite}</td><td>'.$u.'printed-dress</td></tr>
<tr><td>Product</td><td>{categories:/}{rewrite}.html</td><td>'.$u.'women/tops/tshirts/printed-dress.html</td></tr>
<tr><td>Product</td><td>{category}/{rewrite}</td><td>'.$u.'women/printed-dress</td></tr>
<tr><td>Manufacturer</td><td>{rewrite}.html</td><td>'.$u.'adidas.html</td></tr>
<tr><td>Supplier</td><td>{rewrite}.html</td><td>'.$u.'prestashop.html</td></tr>
<tr><td>CMS category</td><td>{rewrite}/</td><td>'.$u.'news/</td></tr>
<tr><td>CMS category</td><td>{rewrite}.html</td><td>'.$u.'news.html</td></tr>
<tr><td>CMS category</td><td>{rewrite}</td><td>'.$u.'news</td></tr>
<tr><td>CMS page</td><td>{rewrite}.html</td><td>'.$u.'policy.html</td></tr>
<tr><td>CMS page</td><td>{rewrite}</td><td>'.$u.'policy</td></tr>
</table>
<p><span style="color:#f00;font-weight:bold">WARNING</span>: Please carefully keep all your "friendly url" difference
 when you turn on "Advanced Dispacher" and use same url schema for 2 or more controller. This option maybe slow down
 your site, please use at your own risk.</p>
</div>'.$tags['p3'];

        return $this->_html;
    }

    public function hookActionDispatcher($params)
    {
        $db = Db::getInstance(_PS_USE_SQL_SLAVE_);
        // Front office
        if ($params['controller_type'] == 1 && !$params['is_module']) {
            $id = 0;
            $d = Dispatcher::getInstance();
            $c = $d->getController();
            $o = Configuration::get('VIP_ADVANCED_URL_REDIRECT');
            $od = Configuration::get('VIP_ADVANCED_URL_DISPATCHER');
            $uri = preg_replace(
                '#^'.preg_quote(Context::getContext()->shop->getBaseURI(), '#').'#i',
                '/',
                strtok(rawurldecode($_SERVER['REQUEST_URI']), '?')
            );
            $l = $this->context->language->countActiveLanguages();

            // Homepage
            if ($c == 'index') {
                return;
            } elseif ($od && $c != 'index' && !Tools::getValue('ajax') && (!$uri || $uri == '/'
                || ($l > 1 && $uri == '/'.$this->context->language->iso_code.'/'))) {
                $_GET = $l > 1 ? array('isolang' => $this->context->language->iso_code,
                    'id_lang' => $this->context->language->id) : array();
                $d->setController('index');
                return;
            }

            // Controller
            $ctrl = array(
                'pagenotfound' => 1,
                'product' => array(
                    'sql' => 'SELECT `id_product` FROM `'._DB_PREFIX_
                        .'product_lang` WHERE `link_rewrite` = \'%s\' AND `id_lang` = '
                        .(int)$this->context->language->id.' AND `id_shop` = '.(int)$this->context->shop->id,
                    'func' => 'getProductLink',
                ),
                'category' => array(
                    'sql' => 'SELECT `id_category` FROM `'._DB_PREFIX_
                        .'category_lang` WHERE `link_rewrite` = \'%s\' AND `id_lang` = '
                        .(int)$this->context->language->id.' AND `id_shop` = '.(int)$this->context->shop->id,
                    'func' => 'getCategoryLink',
                ),
                'manufacturer' => array(
                    'sql' => 'SELECT `id_manufacturer` FROM `'._DB_PREFIX_.'manufacturer`
                        WHERE REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LOWER(`name`), \' \', \'-\'),
                        \'/\', \'-\'), \'\\\'\', \'-\'), \'&\', \'\'), \'.\', \'\'), \',\', \'\'), \'--\', \'-\')
                        = \'%s\'',
                    'func' => 'getManufacturerLink',
                ),
                'supplier' => array(
                    'sql' => 'SELECT `id_supplier` FROM `'._DB_PREFIX_.'supplier`
                        WHERE REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LOWER(`name`), \' \', \'-\'),
                        \'/\', \'-\'), \'\\\'\', \'-\'), \'&\', \'\'), \'.\', \'\'), \',\', \'\'), \'--\', \'-\')
                        = \'%s\'',
                    'func' => 'getSupplierLink',
                ),
                'cms' => array(
                    array(
                        'sql' => 'SELECT `id_cms` FROM `'._DB_PREFIX_.'cms_lang`
                            WHERE `link_rewrite` = \'%s\' AND `id_lang` = '.(int)$this->context->language->id
                            .(version_compare(_PS_VERSION_, '1.6.0.11', '>') ? ' AND `id_shop` = '
                                .(int)$this->context->shop->id : ''),
                        'func' => 'getCMSLink',
                    ),
                    array(
                        'id' => 'cms_category',
                        'sql' => 'SELECT `id_cms_category` FROM `'._DB_PREFIX_.'cms_category_lang`
                            WHERE `link_rewrite` = \'%s\' AND `id_lang` = '.(int)$this->context->language->id
                            .(version_compare(_PS_VERSION_, '1.6.0.11', '>') ? ' AND `id_shop` = '
                                .(int)$this->context->shop->id : ''),
                        'func' => 'getCMSCategoryLink',
                    ),
                ),
            );

            // TODO: pass this process when schema has {id}

            $remove = 0;
            while (isset($ctrl[$c]) && (Tools::getValue('fc') == 'controller' || Tools::getValue('fc') == '')) {
                if ($c != 'pagenotfound') {
                    $id = -1;
                    if (!isset($ctrl[$c][0])) {
                        $ctrl[$c] = array($ctrl[$c]);
                    }

                    foreach ($ctrl[$c] as $v) {
                        $f = isset($v['id']) ? $v['id'] : $c;
                        if ($rewrite = Tools::getValue('rewrite_'.$f)) {
                            unset($_GET['rewrite_'.$f]);
                            if (!$id = $db->getValue(sprintf($v['sql'], $db->escape($rewrite)))) {
                                if ($o && ($i = $this->getId($rewrite)) && isset($i['id']) && $i['id']
                                    && $id = $db->getValue(sprintf($v['sql'], $db->escape($i['rewrite'])))
                                    && ($url = $this->context->link->{$v['func']}($i['id'], $i['rewrite']))) {
                                    Tools::redirect($url, '', null, $o == 1 ? 'HTTP/1.1 301 Moved Permanently' : '');
                                } else {
                                    break;
                                }
                            } else {
                                $e = 'id_'.$f;
                                $_GET[$e] = $id;
                                break;
                            }
                        }
                    }


                    if ($od && !$id) {
                        $d->removeRoute($f);
                        $remove++;
                        $d->setController();
                        $c = $d->getController();
                    } else {
                        break;
                    }
                } else {
                    // Page not found
                    if ($o) {
                        $s = Meta::getMetaByPage(
                            version_compare(_PS_VERSION_, '1.6.0.11', '>=') ? 'pagenotfound' : '404',
                            $this->context->cookie->id_lang
                        );
                        $s = $s['url_rewrite'];
                        $r = Configuration::get('PS_ROUTE_category_rule');
                        if (Tools::substr($uri, -1) !== '/' && (!$r || Tools::substr($r, -1) == '/')
                            && Tools::substr($uri, -5) !== '.html'
                            && Tools::substr($uri, -Tools::strlen($s)) !== $s
                            && $uri !== '/index.php?controller=404') {
                            Tools::redirect(
                                Tools::getShopDomainSsl(true).Context::getContext()->shop->getBaseURI()
                                .Tools::substr($uri, 1).'/',
                                '',
                                null,
                                $o == 1 ? 'HTTP/1.1 301 Moved Permanently' : ''
                            );
                        }
                    }
                    break;
                }
            }
//p($c);p($id);die;

            unset($_GET['fc']);
            if ($remove) {
                $d->restoreRoute();
            }
        } elseif ($params['controller_type'] == 2 && ($o = Configuration::get('VIP_ADVANCED_URL_CHECK'))) {
            // Back office
            $hook = array(
                'product' => 1,
                'category' => 1,
                'cms' => 1,
                'cms_category' => 1,
                'supplier' => 0,
                'manufacturer' => 0
            );
            foreach ($hook as $k => $v) {
                if (Tools::getIsset('submitAdd'.$k) || Tools::getIsset('submitAdd'.$k.'AndStay')) {
                    $field = 'id_'.$k;
                    $val = Tools::getIsset($field) ? (int)Tools::getValue($field) : 0;
                    if ($v) {
                        foreach (Language::getLanguages(false) as $l) {
                            $id = $db->getValue(
                                'SELECT `'.$field.'` FROM `'._DB_PREFIX_.$k.'_lang`
                                WHERE `link_rewrite` = \''.$db->escape(Tools::getValue('link_rewrite_'.$l['id_lang']))
                                .'\''.($val ? ' AND `'.$field.'` != '.(int)$val : '')
                            );
                            // TODO: can not send error to CMS controller !!!
                            if ($id) {
                                $this->context->controller->errors[] = $this->l('Duplicate "Friendly URL":').' '
                                    .$l['name'].'.';
                            }
                        }
                    } else {
                        $id = $db->getValue(
                            'SELECT `'.$field.'` FROM `'._DB_PREFIX_.$k.'`
                            WHERE `name` = \''.$db->escape(Tools::getValue('name')).'\''
                            .($val ? ' AND `'.$field.'` != '.(int)$val : '')
                        );
                        if ($id) {
                            $this->context->controller->errors[] = $this->l('Duplicate "Name".');
                        }
                    }
                    break;
                }
            }
        }
    }

    public function hookModuleRoutes($params)
    {
        if ($params) {
            $a = array(
                'supplier_rule' => array(
                    'controller' => 'supplier',
                    'rule' => 'supplier/{rewrite}.html',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_supplier'),
                        'id' => array('regexp' => '[0-9]+'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                    'params' => array(
                        'fc' => 'controller',
                        'controller' => 'supplier',
                    ),
                ),
                'manufacturer_rule' => array(
                    'controller' => 'manufacturer',
                    'rule' => 'manufacturers/{rewrite}.html',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_manufacturer'),
                        'id' => array('regexp' => '[0-9]+'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                    'params' => array(
                        'fc' => 'controller',
                        'controller' => 'manufacturer',
                    ),
                ),
                'cms_rule' => array(
                    'controller' => 'cms',
                    'rule' => 'content/{rewrite}.html',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_cms'),
                        'id' => array('regexp' => '[0-9]+'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                    'params' => array(
                        'fc' => 'controller',
                        'controller' => 'cms',
                    ),
                ),
                'cms_category_rule' => array(
                    'controller' => 'cms',
                    'rule' => 'content/{rewrite}/',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_cms_category'),
                        'id' => array('regexp' => '[0-9]+'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                    'params' => array(
                        'fc' => 'controller',
                        'controller' => 'cms',
                    ),
                ),
                'category_rule' => array(
                    'controller' => 'category',
                    'rule' => '{rewrite}/',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_category'),
                        'id' => array('regexp' => '[0-9]+'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                    'params' => array(
                        'fc' => 'controller',
                        'controller' => 'category',
                    ),
                ),
                'product_rule' => array(
                    'controller' => 'product',
                    'rule' => '{category:/}{rewrite}.html',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_product'),
                        'category' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'id' => array('regexp' => '[0-9]+'),
                        'ean13' => array('regexp' => '[0-9\pL]*'),
                        'reference' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'manufacturer' =>   array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'supplier' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'price' => array('regexp' => '[0-9\.,]*'),
                        'tags' => array('regexp' => '[a-zA-Z0-9-\pL]*'),
                    ),
                    'params' => array(
                        'fc' => 'controller',
                        'controller' => 'product',
                    ),
                ),
            );
            if (Configuration::get('VIP_ADVANCED_URL_CATEGORIES')) {
                $a['category_rule']['rule'] = '{categories:/}{rewrite}/';
                $a['category_rule']['keywords']['categories'] = array('regexp' => '[/_a-zA-Z0-9-\pL]*');
                $a['product_rule']['rule'] = '{categories:/}{rewrite}.html';
                $a['product_rule']['keywords']['categories'] = array('regexp' => '[/_a-zA-Z0-9-\pL]*');
            }
            return $a;
        }
    }

    private function getId($s)
    {
        $a = '';
        if (preg_match('/^(\d+)([-_]{0,2})(.*)/', $s, $m) && $m[1] && $m[3]) {
            $a['id'] = $m[1];
            $a['rewrite'] = $m[3];
        } elseif (preg_match('/(.*)([-_])(\d+)$/', $s, $m) && $m[1] && $m[3]) {
            $a['id'] = $m[3];
            $i = Tools::strlen($m[1]) - 1;
            if ($m[1]{$i} == '_' || $m[1]{$i} == '-') {
                $m[1] = Tools::substr($m[1], 0, -1);
            }
            $a['rewrite'] = $m[1];
        }
        return $a;
    }
}
