Advanced URL Version 1.3.1
Copyright by Vipcom 2015

Increase your SEO by remove id from your shop's url.

Search engine alway like website with simple, meaningful and clean url without any number, tags, session id,... Unfortunately, default Prestashop SEO & Friendly URL feature contain IDs.

This module will help you increase your search engine ranking by remove id from your shop's url and prevent duplicate friendly url when you input data at back office.


FEATURES

Remove id from all url: category, product, manufacturer, supplier, cms
Your shop's url will be more search engine friendly.

Support parent categories in category url (optional), this is a missing feature of default prestashop. Please go to Advanced URL configuration page and turn on "Parent categories in URL" option.

Support url without ending slash or .html. You can remove all ending slash or .html from shop's url. Please go to Advanced URL configuration page and turn on "Advanced Dispatcher" option.

Support dupplicated url schema. You can use same url schema for all controller. Please go to Advanced URL configuration page and turn on "Advanced Dispatcher" option.

Auto detect and redirect all your old urls (include id) to the new urls (without id). This feature help your shop keep all current search engine ranking.

Prevent duplicate friendly url at back office: category, product, manufacturer, supplier, cms. When you submit any record, back office warning tools will notify you any duplicate friendly url immediately.

Detect and show you all dupplicated friendly url records in database.

Work with Prestashop cloud and downloaded version.


INSTALLATION
After install this module, when you use default seo route, every thing automatically update and your shop's url will be clean without any id. You must do nothing.

Please goto Back Office -> Preferences -> SEO & URL and use any folowing route as your demand.

Default route: This is the best choice for SEO & SEF. Your shop'us url will bee more search engine friendly like following:

Module       Route                         Example URL
----------------------------------------------------------------
Category     {rewrite}/                    /women/
Product      {category:/}{rewrite}.html    /women/printed-dress.html
Manufacturer manufacturers/{rewrite}.html  /manufacturers/adidas.html
Supplier     supplier/{rewrite}.html       /supplier/prestashop.html
CMS category content/{rewrite}/            /content/news/
CMS page     content/{rewrite}.html        /content/policy.html

Other routes: for one, who do not like default route. You can use any of following route with default route.

Module       Route                         Example URL
---------------------------------------------------------------
Category     {rewrite}.html                /women.html
Category     c-{rewrite}.html              c-women.html
Category     {rewrite}                     /women
Category     {categories:/}{rewrite}/      /women/tops/tshirts/
Product      {rewrite}.html                /printed-dress.html
Product      {rewrite}                     /printed-dress
Product      {categories:/}{rewrite}.html  /women/tops/tshirts/printed-dress.html
Product      {category}/{rewrite}          /women/printed-dress
Manufacturer {rewrite}.html                /adidas.html
Manufacturer {rewrite}                     /adidas
Supplier     {rewrite}.html                /prestashop.html
Supplier     {rewrite}                     /prestashop
CMS category {rewrite}/                    /news/
CMS category {rewrite}.html                /news.html
CMS category {rewrite}                     /news
CMS page     {rewrite}.html                /policy.html
CMS page     {rewrite}                     /policy


WARNING
Please carefully keep all you "friendly url" difference when you turn on "Advanced Dispacher" and use same url schema for 2 or more controller. This option maybe slow down your site, please use at your own risk.


CHANGE
1.0
  [+] Remove id from url

1.1
  [-] Fix some errors.
  [-] Remove all override.
  [+] Checking for current dupplicated friendly url

1.2
  [-] Fix multi-shop wrong duplicated url notice
  [+] Support parent-categories in url (optional)
  [+] Redirect old url to the new url prevent loosing your current search engine ranking.

1.2.2
  [-] Fix supplier, manufacturers, uninstall

1.2.3 & 1.2.4
  [-] Fix some error
  [+] Enable/disable duplicated url warning
  [+] Auto add missing slash to the end of categories url

1.2.5
  [-] Fix 404 page loop redirect
	[-] Fix manufacturer & supplier url
  [+] Update compatible for Prestashop Cloud

1.2.6 & 1.2.7 & 1.2.8
	[-] Fix some bugs
	[+] 301 redirect

1.3.0
	[+] Support url without ending slash or .html
	[+] Support dupplicated url schema
	[+] Add 301 or 302 redirect option
	[-] Fix some bugs

1.3.1
	[-] Fix some bugs