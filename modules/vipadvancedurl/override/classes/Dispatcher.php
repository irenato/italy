<?php
/**
* This source file is subject to the Open Software License (OSL 3.0)
*
*  @author    Vipcom <info@vipcom.vn>
*  @copyright 2015 Vipcom
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

class Dispatcher extends DispatcherCore
{
    protected $backup = array();
    protected $advanced_dispatcher = 0;

    /*
    protected function __construct()
    {
        // Hack for Prestashop 1.5.2.0 and below version
        if (version_compare(_PS_VERSION_, '1.5.2.0', '<=')) {
            $this->default_routes = array(
                'supplier_rule' => array(
                    'controller' => 'supplier',
                    'rule' => 'supplier/{rewrite}.html',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_supplier'),
                        'id' => array('regexp' => '[0-9]+'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                ),
                'manufacturer_rule' => array(
                    'controller' => 'manufacturer',
                    'rule' => 'manufacturers/{rewrite}.html',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_manufacturer'),
                        'id' => array('regexp' => '[0-9]+'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                ),
                'cms_rule' => array(
                    'controller' => 'cms',
                    'rule' => 'content/{rewrite}.html',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_cms'),
                        'id' => array('regexp' => '[0-9]+'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                ),
                'cms_category_rule' => array(
                    'controller' => 'cms',
                    'rule' => 'content/{rewrite}/',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_cms_category'),
                        'id' => array('regexp' => '[0-9]+'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                ),
                'module' => array(
                    'controller' => null,
                    'rule' => 'module/{module}{/:controller}',
                    'keywords' => array(
                        'module' => array('regexp' => '[_a-zA-Z0-9_-]+', 'param' => 'module'),
                        'controller' => array('regexp' => '[_a-zA-Z0-9_-]+', 'param' => 'controller'),
                    ),
                    'params' => array(
                        'fc' => 'module',
                    ),
                ),
                'category_rule' => array(
                    'controller' => 'category',
                    'rule' => '{rewrite}/',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_category'),
                        'categories' => array('regexp' => '[/_a-zA-Z0-9-\pL]*'),
                        'id' => array('regexp' => '[0-9]+'),
                        'meta_keywords' =>  array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' =>     array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                ),
                'product_rule' => array(
                    'controller' => 'product',
                    'rule' => '{category:/}{rewrite}.html',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_product'),
                        'category' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'categories' => array('regexp' => '[/_a-zA-Z0-9-\pL]*'),
                        'id' => array('regexp' => '[0-9]+'),
                        'ean13' => array('regexp' => '[0-9\pL]*'),
                        'reference' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'manufacturer' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'supplier' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'price' => array('regexp' => '[0-9\.,]*'),
                        'tags' => array('regexp' => '[a-zA-Z0-9-\pL]*'),
                    ),
                ),
                'layered_rule' => array(
                    'controller' => 'category',
                    'rule' => '{id}-{rewrite}{/:selected_filters}',
                    'keywords' => array(
                        'id' => array('regexp' => '[0-9]+', 'param' => 'id_category'),
                        'selected_filters' => array('regexp' => '.*', 'param' => 'selected_filters'),
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                ),
            );
        }

        parent::__construct();
    }
    */

    /**
     * Hack to change controller on the fly
     */
    public function setController($controller = '')
    {
        $this->advanced_dispatcher = $this->controller != $controller ? 1 : 0;
        $this->controller = $controller;
        $_GET['controller'] = $this->controller;
    }

    /**
     * Hack to remove route on the fly
     */
    public function removeRoute($route_id, $id_lang = null, $id_shop = null)
    {
        if ($id_lang === null && isset(Context::getContext()->language)) {
            $id_lang = (int)Context::getContext()->language->id;
        }
        if ($id_shop === null && isset(Context::getContext()->shop)) {
            $id_shop = (int)Context::getContext()->shop->id;
        }

        if (isset($this->routes[$id_shop][$id_lang][$route_id.'_rule'])) {
            $this->backup[$route_id.'_rule'] = $this->routes[$id_shop][$id_lang][$route_id.'_rule'];
            unset($this->routes[$id_shop][$id_lang][$route_id.'_rule']);
        } elseif (isset($this->routes[$id_lang][$route_id.'_rule'])) {
            $this->backup[$route_id.'_rule'] = $this->routes[$id_lang][$route_id.'_rule'];
            unset($this->routes[$id_lang][$route_id.'_rule']);
        }
    }

    public function restoreRoute()
    {
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        if (isset($this->routes[$id_shop][$id_lang])) {
            $this->routes[$id_shop][$id_lang] = array_merge($this->routes[$id_shop][$id_lang], $this->backup);
        } else {
            $this->routes[$id_lang] = array_merge($this->routes[$id_lang], $this->backup);
        }
        $this->backup = array();
    }

    /**
     * Rewrite dispatch() to support dupplicated url schema
     */
    public function dispatch()
    {
        $o = Configuration::get('VIP_ADVANCED_URL_DISPATCHER');
        if (!$o) {
            parent::dispatch();
            return;
        }

        $controller_class = '';

        // Get current controller
        $this->getController();
        if (!$this->controller) {
            $this->controller = method_exists($this, 'useDefaultController') ? $this->useDefaultController() :
                $this->default_controller;
        }

        // Dispatch with right front controller
        switch ($this->front_controller) {
            // Dispatch front office controller
            case self::FC_FRONT:
                $controllers = Dispatcher::getControllers(array(_PS_FRONT_CONTROLLER_DIR_,
                    _PS_OVERRIDE_DIR_.'controllers/front/'));
                $controllers['index'] = 'IndexController';
                if (isset($controllers['auth'])) {
                    $controllers['authentication'] = $controllers['auth'];
                }
                if (isset($controllers['compare'])) {
                    $controllers['productscomparison'] = $controllers['compare'];
                }
                if (isset($controllers['contact'])) {
                    $controllers['contactform'] = $controllers['contact'];
                }

                if (!isset($controllers[Tools::strtolower($this->controller)])) {
                    $this->controller = $this->controller_not_found;
                }
                $controller_class = $controllers[Tools::strtolower($this->controller)];
                $params_hook_action_dispatcher = array('controller_type' => self::FC_FRONT,
                    'controller_class' => $controller_class, 'is_module' => 0);
                break;

            // Dispatch module controller for front office
            case self::FC_MODULE:
                $module_name = Validate::isModuleName(Tools::getValue('module')) ? Tools::getValue('module') : '';
                $module = Module::getInstanceByName($module_name);
                $controller_class = 'PageNotFoundController';
                if (Validate::isLoadedObject($module) && $module->active) {
                    $controllers = Dispatcher::getControllers(_PS_MODULE_DIR_.$module_name.'/controllers/front/');
                    if (isset($controllers[Tools::strtolower($this->controller)])) {
                        include_once(_PS_MODULE_DIR_.$module_name.'/controllers/front/'.$this->controller.'.php');
                        $controller_class = $module_name.$this->controller.'ModuleFrontController';
                    }
                }
                $params_hook_action_dispatcher = array('controller_type' => self::FC_FRONT,
                    'controller_class' => $controller_class, 'is_module' => 1);
                break;

            // Dispatch back office controller + module back office controller
            case self::FC_ADMIN:
                if (isset($this->use_default_controller) && $this->use_default_controller && !Tools::getValue('token')
                    && Validate::isLoadedObject(Context::getContext()->employee)
                    && Context::getContext()->employee->isLoggedBack()) {
                    Tools::redirectAdmin(
                        'index.php?controller='.$this->controller.'&token='.Tools::getAdminTokenLite($this->controller)
                    );
                }
                $tab = Tab::getInstanceFromClassName($this->controller, Configuration::get('PS_LANG_DEFAULT'));
                $retrocompatibility_admin_tab = null;

                if ($tab->module) {
                    if (file_exists(_PS_MODULE_DIR_.$tab->module.'/'.$tab->class_name.'.php')) {
                        $retrocompatibility_admin_tab = _PS_MODULE_DIR_.$tab->module.'/'.$tab->class_name.'.php';
                    } else {
                        $controllers = Dispatcher::getControllers(_PS_MODULE_DIR_.$tab->module.'/controllers/admin/');
                        if (!isset($controllers[Tools::strtolower($this->controller)])) {
                            $this->controller = $this->controller_not_found;
                            $controller_class = 'AdminNotFoundController';
                        } else {
                            // Controllers in modules can be named AdminXXX.php or AdminXXXController.php
                            include_once(_PS_MODULE_DIR_.$tab->module.'/controllers/admin/'.
                                $controllers[Tools::strtolower($this->controller)].'.php');
                            $controller_class = $controllers[Tools::strtolower($this->controller)]
                                .(strpos($controllers[Tools::strtolower($this->controller)], 'Controller') ? '' :
                                'Controller');
                        }
                    }
                    $params_hook_action_dispatcher = array('controller_type' => self::FC_ADMIN,
                        'controller_class' => $controller_class, 'is_module' => 1);
                } else {
                    $controllers = Dispatcher::getControllers(array(_PS_ADMIN_DIR_.'/tabs/', _PS_ADMIN_CONTROLLER_DIR_,
                        _PS_OVERRIDE_DIR_.'controllers/admin/'));
                    if (!isset($controllers[Tools::strtolower($this->controller)])) {
                        // If this is a parent tab, load the first child
                        if (version_compare(_PS_VERSION_, '1.6.0.1', '>=') && Validate::isLoadedObject($tab)
                            && $tab->id_parent == 0
                            && ($tabs = Tab::getTabs(Context::getContext()->language->id, $tab->id))
                            && isset($tabs[0])) {
                            Tools::redirectAdmin(Context::getContext()->link->getAdminLink($tabs[0]['class_name']));
                        }
                        $this->controller = $this->controller_not_found;
                    }

                    $controller_class = $controllers[Tools::strtolower($this->controller)];
                    $params_hook_action_dispatcher = array('controller_type' => self::FC_ADMIN,
                        'controller_class' => $controller_class, 'is_module' => 0);

                    if (file_exists(_PS_ADMIN_DIR_.'/tabs/'.$controller_class.'.php')) {
                        $retrocompatibility_admin_tab = _PS_ADMIN_DIR_.'/tabs/'.$controller_class.'.php';
                    }
                }

                // @retrocompatibility with admin/tabs/ old system
                if ($retrocompatibility_admin_tab) {
                    include_once($retrocompatibility_admin_tab);
                    include_once(_PS_ADMIN_DIR_.'/functions.php');
                    runAdminTab($this->controller, !empty($_REQUEST['ajaxMode']));
                    return;
                }
                break;

            default:
                throw new PrestaShopException('Bad front controller chosen');
        }

        // Instantiate controller
        try {
            // Execute hook dispatcher
            if (isset($params_hook_action_dispatcher)) {
                Hook::exec('actionDispatcher', $params_hook_action_dispatcher);
            }

            if ($this->advanced_dispatcher) {
                if ($this->front_controller == self::FC_FRONT) {
                    if (!isset($controllers[Tools::strtolower($this->controller)])) {
                        $this->controller = $this->controller_not_found;
                    }
                    $controller_class = $controllers[Tools::strtolower($this->controller)];
                } elseif ($this->front_controller == self::FC_MODULE) {
                    $module_name = Validate::isModuleName(Tools::getValue('module')) ? Tools::getValue('module') : '';
                    $module = Module::getInstanceByName($module_name);
                    $controller_class = 'PageNotFoundController';
                    if (Validate::isLoadedObject($module) && $module->active) {
                        $controllers = Dispatcher::getControllers(_PS_MODULE_DIR_.$module_name.'/controllers/front/');
                        if (isset($controllers[Tools::strtolower($this->controller)])) {
                            include_once(_PS_MODULE_DIR_.$module_name.'/controllers/front/'.$this->controller.'.php');
                            $controller_class = $module_name.$this->controller.'ModuleFrontController';
                        }
                    }
                }
            }

            // Loading controller
            $controller = Controller::getController($controller_class);
            $controller->run();
        } catch (PrestaShopException $e) {
            $e->displayMessage();
        }
    }
}
