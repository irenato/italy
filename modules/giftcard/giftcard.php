<?php
/*
* 2016 Pushpendra Singh
*
* NOTICE OF LICENSE
*
*
*  @author          Ha!*!*y <pushpendra@singsys.com>
*  @copyright       2016 Ha!*!*y
*/

if (!defined('_PS_VERSION_'))
  exit;

class Giftcard extends Module
{
	const IMG_URL = 'http://103.15.232.35/singsys-stg3/ecommerce/img/';

	public $currency_id;

	public $order_id;


	public function __construct()
	{
		$this->name = 'giftcard';
		$this->tab = 'front_office_features';
		$this->author = 'Pushpendra Singh';
		$this->version = '1.00';

		$this->need_instance = 0;
		$this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.6');

		parent::__construct();

		$this->displayName = $this->l('Gift Card');
		$this->description = $this->l('This modules send mail for Gift Card.');

		$this->dbUpdate_output = '';
	}

	
    public function install()
	{
		if (!parent::install() || !$this->registerHook('actionPaymentConfirmation') )
			return false;
		return true;
	}

	public function uninstall()
	{
		if (!parent::uninstall())
			return false;
		return true;
	}

	public function hookActionPaymentConfirmation($params)
	{
		$this->cart_id = $params['cart']->id;
		$id_shop = $params['cart']->id_shop;
		$this->currency_id = $params['cart']->id_currency;
		$this->order_id = $params['id_order'];
        $this->context->customer = new Customer((int)$this->context->cart->id_customer);
        $order = new Order($this->order_id);
        $this->cur_order = $order;
        $products = $order->getProducts();

		if(count($products)) 
		{
			foreach($products as $product) 
			{
				if($product['product_id'])
				{					
					$sql = 'SELECT count(id_category) AS type FROM `'._DB_PREFIX_.'category_product` WHERE `id_product` = '.(int) $product['product_id'].' AND id_category = '. (int) E_GIFT_CARD_ID;
					$is_egift_card = Db::getInstance()->executeS($sql)[0]['type'];

					$sql = 'SELECT count(id_category) AS type  FROM `'._DB_PREFIX_.'category_product` WHERE `id_product` = '.(int) $product['product_id'].' AND id_category = '. (int) PHYSICAL_GIFT_CARD_ID;

					$physical_gift_card = Db::getInstance()->executeS($sql)[0]['type'];
					
					if($is_egift_card)
					{
						$this->createCartRule($product, true);
					}

					else if($physical_gift_card)
					{
						$this->createCartRule($product);
					}
				}
			}
		}
	}

	private function createCartRule($product, $e_gift = false)
	{
		$product_price = (float) $product['product_price'];
		$cartRule = new CartRule();		
		$cartRule->name = $e_gift? 'E-Gift Card' : 'Physical Gift Card';
		$cartRule->description = $cartRule->name . " Created";
		$cartRule->reduction_amount = $product_price;
		$cartRule->reduction_tax = 1;
		$cartRule->cart_rule_restriction = 0;
		$cartRule->quantity = 1;
		$cartRule->quantity_per_user = 1;
		$cartRule->date_from = date('Y-m-d H:i:s', time());
		$cartRule->date_to = date('Y-m-d H:i:s', time() + 86400); // + 1 year
		$expires = date('d F, Y', time() + 86400); // + 1 year
		$cartRule->code = strtoupper(Tools::passwdGen(8));
		$cartRule->reduction_currency = (int)$this->context->cart->id_currency;
	
		$currency_sql = "SELECT sign FROM currency WHERE id_currency = $this->currency_id";
		$currency_sign = Db::getInstance()->executeS($currency_sql)[0]['sign'];

		if($e_gift)
		{
			$sql = 'SELECT gift_recipient AS email FROM `'._DB_PREFIX_.'cart_product` WHERE `id_cart` = '.(int) $this->cart_id.' AND id_product = '. (int) $product['product_id'];
			$attachment = null;
			$template = 'giftcard';
			$order_reference = Db::getInstance()->executeS("SELECT reference FROM orders WHERE id_order = " . $this->order_id)[0]['reference'];
			$temp_vars =  array('{img_dir}' => self::IMG_URL, '{price}' => $product['product_price'], '{expiry}' => $expires, '{code}' => $cartRule->code, '{currency}' => $currency_sign, '{order_reference}' => $order_reference);
		}

		else
		{
			$sql = 'SELECT email FROM `'._DB_PREFIX_.'employee` WHERE active = 1';
			$attachment = '';
			$order_id = $this->cur_order->id_address_delivery; 
			$physical_gift_card_receipient_query = "SELECT firstname, lastname, address1, address2, postcode, city, phone, phone_mobile FROM address WHERE id_address = $order_id AND active = 1 AND deleted = 0 LIMIT 1";

			$physical_data = Db::getInstance()->executeS($physical_gift_card_receipient_query)[0];
			$mail_content = "Delivery Information for Physical Gift Card:";
			$mail_content .= "<br>Gift-Card Amount: " . $currency_sign . " " . $product_price;
			$mail_content .= "<br>Code:<b>" . $cartRule->code . "</b>";
			$mail_content .= "<br><br>First Name: " . $physical_data['firstname'];
			$mail_content .= "<br>Last Name: " . $physical_data['lastname'];
			$mail_content .= "<br>Address: " . $physical_data['address1'];
			$mail_content .= "<br><br>". $physical_data['address2'];
			$mail_content .= "<br><br>Postcode: " . $physical_data['postcode'];
			$mail_content .= "<br><br>City: " . $physical_data['city'];
			$mail_content .= "<br>Phone 1: " . $physical_data['phone'];
			$mail_content .= "<br>Phone 2: " . $physical_data['phone_mobile'];
			
			$template = 'pgiftcard';
			$temp_vars =  array('{content}' => $mail_content);
		}

		//Check
		$mail_result = Db::getInstance()->executeS($sql);

		if($cartRule->add())
		{
			foreach($mail_result as $mail_receipient)
			{
				$from_table = !isset($attachment)?  'customer' : 'employee';
				$name_query = "SELECT CONCAT(firstname, ' ', lastname) AS name FROM $from_table WHERE email = '" . $mail_receipient['email'] . "' LIMIT 1";
				$receipient_name = Db::getInstance()->executeS($name_query);
				if(count($receipient_name)){
					$receipient_name = $receipient_name[0]['name'];
				}else{
					$receipient_name = 'Anonymous';
				}

				Mail::Send(
		                1,
		                $template,
		                Mail::l($cartRule->name . ' Code', 1),
		               	$temp_vars,
		                $mail_receipient['email'],
		                $receipient_name,
		                'info@charlyselection.com',
		                "Info - Charly Selection"
		        );
			}
		}
	}
}