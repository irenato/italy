<?php
/* SSL Management */
$useSSL = true;

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');
include(dirname(__FILE__).'/bitcoin.php');

if (!$cookie->isLogged(true))
    Tools::redirect('authentication.php?back=order.php');

$bitcoin = new Bitcoin();
echo $bitcoin->showPayValidationPage($cart);

include_once(dirname(__FILE__).'/../../footer.php');

?>
