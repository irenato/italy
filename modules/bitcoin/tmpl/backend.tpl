<h2>{$name}</h2>
<form action="{$request_uri}" method="post">
<div class="clear"></div>
<br>
<h3>{l s='Duration of payments :' mod='bitcoin'}</h3> 
<input name="duration" type="text" value="{$duration}"> {l s='seconds' mod='bitcoin'}
<button name="updDuration" value="Upd" type="submit"><img src="{$this_path_ssl}img/send.png"></button>
<br>
<div class="clear"></div>
<br>
<h3>{l s='Bitcoin Address' mod='bitcoin'}</h3>
	<table cellspacing="0" cellpadding="0" class="table" style="width: 930px;">
		<tbody>
						<tr>
							<th style="width: 10px;"><input type="checkbox" name="checkme" class="noborder" onclick="checkDelBoxes(this.form, 'categoryBox[]', this.checked)"></th>
							<th style="width: 10px;">{l s='Id' mod='bitcoin'}</th>
							<th>{l s='Address' mod='bitcoin'}</th>
							<th style="width: 140px;">{l s='Name' mod='bitcoin'}</th>
                                                        <th style="width: 140px;">{l s='Last used' mod='bitcoin'}</th>
                                                        <th style="width: 140px;">{l s='Added' mod='bitcoin'}</th>
                                                        <th style="width: 60px;">{l s='State' mod='bitcoin'}</th>
                                                        <th style="width: 60px;">{l s='Action' mod='bitcoin'}</th>
						</tr>
{foreach from=$adresses key=k  item=addr}
	<tr class="">
		<td><input type="checkbox"></td>
		<td>{$addr.id}</td>
                <td>{$addr.address}</td>
                <td>{$addr.name}</td>
                <td>{$addr.date_lastused}</td>
                <td>{$addr.date_added}</td>
                <td>{$addr.state}</td>
                <td><button name="btnDelAddress" value="{$addr.id}" type="submit"><img src="{$this_path_ssl}img/delete.png"></button></td>
       </tr>
{/foreach}
            <tr style="display:none;" id="add_address">
					<td></td>
					<td></td>
                                        <td><input maxlength="34" style="width:95%;" name="address" type="text"></td>
                                        <td><input style="width:95%;" name="name" type="text"></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><button name="btnAddAddress" value="Add" type="submit"><img src="{$this_path_ssl}img/add.png"></button></td>
                                        
           </tr></tbody></table> 
           <br><a onclick="javascript:$('#add_address').show();"> {l s='Add new address' mod='bitcoin'} <img src="{$this_path_ssl}img/add.png"></a>
<div class="clear"></div><br>
<h3>{l s='Bitcoin Order' mod='bitcoin'}</h3>	
           <table cellspacing="0" cellpadding="0" class="table" style="width: 930px;">
						<tbody><tr>
							<th style="width: 10px;"><input type="checkbox" name="checkme" class="noborder" onclick="checkDelBoxes(this.form, 'categoryBox[]', this.checked)"></th>
							<th style="width: 10px;">{l s='Id' mod='bitcoin'}</th>
                                                        <th style="width: 50px;">{l s='Id Odrer' mod='bitcoin'}</th>
							<th>{l s='Bitcoin Address' mod='bitcoin'}</th>
                                                        <th style="width: 100px;">{l s='Bitcoin Waiting' mod='bitcoin'}</th>
                                                        <th style="width: 100px;">{l s='Bitcoin Validate' mod='bitcoin'}</th>
                                                        <th style="width: 140px;">{l s='Start' mod='bitcoin'}</th>
                                                        <th style="width: 140px;">{l s='End' mod='bitcoin'}</th>
                                                        <th style="width: 40px;">{l s='Action' mod='bitcoin'}</th>
						</tr>
{foreach from=$orders key=k  item=order}						
<tr class="">
	<td><input type="checkbox"></td>
	<td>{$order.id}</td>
        <td><a target="_blank" href="index.php?tab=AdminOrders&id_order={$order.id_order}&vieworder&token={$token}">{$order.id_order}</a></td>
        <td>{$order.bitcoin_address}</td>
        <td>{$order.waiting}</td>
        <td>{$order.validate}</td>
        <td>{$order.date_start}</td>
        <td>{$order.date_end}</td>
        <td>
        <button name="btnDelOrder" value="{$order.id}" type="submit"><img src="{$this_path_ssl}img/delete.png"></button>
        <button name="btnValOrder" value="{$order.id}" type="submit"><img src="{$this_path_ssl}img/validate.png"></button>
        </td>
</tr>
{/foreach}
</tbody></table> <br>
<div class="clear"></div><br>
<h3>{l s='Bitcoin Payment (last 30)' mod='bitcoin'}</h3>
<table cellspacing="0" cellpadding="0" class="table" style="width: 930px;">
						<tbody><tr>
							<th style="width: 10px;"><input type="checkbox" name="checkme" class="noborder" onclick="checkDelBoxes(this.form, 'categoryBox[]', this.checked)"></th>
							<th style="width: 10px;">{l s='Id' mod='bitcoin'}</th>
                                                        <th>{l s='From Bitcoin Address' mod='bitcoin'}</th>
                                                        <th>{l s='To Bitcoin Address' mod='bitcoin'}</th>
                                                        <th style="width: 60px;">{l s='Value' mod='bitcoin'}</th>
                                                        <th style="width: 60px;">{l s='State' mod='bitcoin'}</th>
                                                        <th style="width: 150px;">{l s='Date' mod='bitcoin'}</th>
                                                        <th style="width: 60px;">{l s='Action' mod='bitcoin'}</th>
						</tr>
{foreach from=$payments key=k  item=payment}	
<tr class="">
	<td><input type="checkbox"></td>
	<td>{$payment.id}</td>
        <td>{$payment.from_bitcoin_address}</td>
        <td>{$payment.to_bitcoin_address}</td>
        <td>{$payment.value}</td>
        <td>{$payment.state}</td>
        <td>{$payment.date}</td>
        <td><button name="btnDelPayment" value="{$payment.id}" type="submit"><img src="{$this_path_ssl}img/delete.png"></button></td>
</tr>
{/foreach}
            <tr style="display:none;" id="add_payment">
					<td></td>
					<td></td>
                                        <td><input maxlength="34" style="width:95%;" name="from_bitcoin_address" type="text"></td>
                                        <td><input maxlength="34" style="width:95%;" name="to_bitcoin_address" type="text"></td>
                                        <td><input style="width:95%;" name="value" type="text"></td>
                                        <td>
<select name="state">
  <option value="waiting">Waiting</option>
  <option value="validate">Validate</option>
</select>
                                        </td>
                                        <td><input style="width:56%;" name="date" value="{$cur_date}" type="date"><input style="width:35%;" name="time" type="time" value="{$cur_time}"></td>
                                        <td><button name="btnAddPayment" value="Add" type="submit"><img src="{$this_path_ssl}img/add.png"></button></td>
           </tr></tbody></table> <br>
           <a onclick="javascript:$('#add_payment').show();"> {l s='Add new payment' mod='bitcoin'} <img src="{$this_path_ssl}img/add.png"></a>
           </form>
           
