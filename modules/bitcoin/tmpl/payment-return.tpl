{if $status == 'waiting'}
	<p>{l s='Your order on' mod='bitcoin'} <span class="bold">{$shop_name}</span> {l s='is complete.' mod='bitcoin'}
		<br /><br /><br />
		{l s='Please send us :' mod='bitcoin'} <span class="price">{$total_to_pay}</span>
		<br /><br />{l s='payable to the bitcoin address :' mod='bitcoin'} <span class="bold">{if $BitcoinAddress}{$BitcoinAddress}{else}___________{/if}</span><br />
<br />
                {l s='From :' mod='bitcoin'} <span class="bold">{$From}</span> {l s='to :' mod='bitcoin'} <span class="bold">{$To}</span><br />
<br />
                {l s='Validate :' mod='bitcoin'} <span class="bold">{$BitcoinValidate} BTC</span> {l s='not yet validate :' mod='bitcoin'} <span class="bold">{$BitcoinWaiting} BTC</span> </span><br />
		<br /><br />{l s='An e-mail has been sent to you with this information.' mod='bitcoin'}
		<br /><br /><span class="bold">{l s='Your order will be activate soon as we receive your payment.' mod='bitcoin'}</span>
		<br /><br />{l s='For any questions or for further information, please contact our' mod='bitcoin'} <a href="{$link->getPageLink('contact-form.php', true)}">{l s='customer support' mod='bitcoin'}</a>.
	</p>
		<br /><br />{l s='This page will refresh every 5 minutes. The link to this page is in the email if you want to check later' mod='bitcoin'}.
	</p>
{elseif $status == 'validate'}
	<p class="warning">
		{l s='This order has been validate' mod='bitcoin'} 
	</p>
{else}
	<p class="warning">
		{l s='We noticed a problem with your order. If you think this is an error, you can contact our' mod='bitcoin'} 
		<a href="{$link->getPageLink('contact-form.php', true)}">{l s='customer support' mod='bitcoin'}</a>.
	</p>
{/if}

<script type="text/javascript">setTimeout("window.location.reload()",5*60*1000);</script>
