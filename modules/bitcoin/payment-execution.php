<?php

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');
include(dirname(__FILE__).'/bitcoin.php');

$bitcoin = new Bitcoin();
if (!isset($cart->id) OR $cart->id_customer == 0 OR $cart->id_address_delivery == 0 OR $cart->id_address_invoice == 0 OR !$bitcoin->active)
	Tools::redirectLink(__PS_BASE_URI__.'order.php?step=1');

$customer = new Customer((int)$cart->id_customer);

if (!Validate::isLoadedObject($customer))
	Tools::redirectLink(__PS_BASE_URI__.'order.php?step=1');

$verif = Db::getInstance()->ExecuteS("SELECT * FROM "._DB_PREFIX_."bitcoin_orders WHERE `id_order` = ".$cart->id." LIMIT 1");

if (isset ($verif[0]["id_order"])){
    echo 'panier déja enregistrer';
    die();
}

$currency = new Currency((int)(isset($_POST['currency_payement']) ? $_POST['currency_payement'] : $cookie->id_currency));
$total = (float)($cart->getOrderTotal(true, Cart::BOTH));

$addr = Db::getInstance()->ExecuteS('SELECT * FROM ' . _DB_PREFIX_ . 'bitcoin_address WHERE `state` = \'available\' LIMIT 1');


//print_r($addr);
//echo "<br/><br/><br/><br/>";
//print_r($cart);
if (!isset($addr[0]['id'])){
    echo 'Accune adresse bitcoin disponible a ce moment';
    die();
}

Db::getInstance()->Execute("UPDATE `"._DB_PREFIX_."bitcoin_address` SET `state` = 'unavailable' WHERE `id` = ".$addr[0]['id'].";");
Db::getInstance()->Execute("INSERT INTO `". _DB_PREFIX_ ."bitcoin_orders` (`id`, `id_order`, `bitcoin_address`, `waiting`, `validate`, `date_start`, `date_end`) VALUES (NULL, '".$cart->id."', '".$addr[0]['address']."', 0, 0, NOW(), FROM_UNIXTIME(".(time()+(int) Configuration::get('BITCOIN_DURATION'))."));");

$mes = "Adr BTC : " .mysql_real_escape_string($addr[0]['address']);
$mailVars =	array(
	'{bitcoin_address}' => $addr[0]['address'],
	'{track_url}' => __PS_BASE_URI__.'order-confirmation.php?id_cart='.(int)($cart->id).'&id_module='.(int)($bitcoin->id).'&id_order='.$bitcoin->currentOrder.'&key='.$customer->secure_key
        );

//print_r($mailVars);
$bitcoin->validateOrder((int)($cart->id), Configuration::get('BITCOIN_ORDER_STATE_WAIT'), $total, $bitcoin->displayName, $mes, $mailVars, (int)($currency->id), false, $customer->secure_key);
Tools::redirectLink(__PS_BASE_URI__.'order-confirmation.php?id_cart='.(int)($cart->id).'&id_module='.(int)($bitcoin->id).'&id_order='.$bitcoin->currentOrder.'&key='.$customer->secure_key);

?>
