<?php   
if (!defined('_PS_VERSION_'))
  exit;

class DynamicPhrase extends Module
{
		public function __construct()
		{
			$this->name = 'dynamicphrase';
		    $this->tab = 'dashboard';
		    $this->version = '1.0.0';
		    $this->author = 'Singsys';
		    $this->need_instance = 0;
		    $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_); 
		    $this->bootstrap = true;
		 
		    parent::__construct();
		 
		    $this->displayName = $this->l('Dynamic Phrase for navbar');
		    $this->description = $this->l('Dynamic Phrase for navbar');
		 
		    $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
		 
		    if (!Configuration::get('PS_DYNAMIC_PHRASE'))      
		      $this->warning = $this->l('No name provided');
	  }

	  public function install()
	  {
		if (!parent::install() || !$this->registerHook('displayNav'))
			return false;

		$success = Configuration::updateValue('PS_DYNAMIC_PHRASE', 'Your Text Here');

		if (!$success)
		{
			parent::uninstall();
			return false;
		}

		return true;
	  }

	  public function uninstall()
	  {
	  	return (parent::uninstall() && 	Configuration::deleteByName('PS_DYNAMIC_PHRASE'));
	  }

	public function hookDisplayNav($params)
	{
		$html = '';
	    $html .='<div class="topleft-content">';
		$html .='<span class="top-phrase-txt">'.Configuration::get('PS_DYNAMIC_PHRASE').'</span>';
		$html .='<span class="top-phrase-phone"><a href="mailto:'.Configuration::get('PS_DYNAMIC_PHONE').'">'.Configuration::get('PS_DYNAMIC_PHONE').'</a></span>';
		$html .='</div>';
		
		return $html;
	}
	
	public function getContent()
	{
		$html = '';
		
		
		
		if (Tools::isSubmit('submit'.$this->name))
		{
			$error = "";
				$PS_DYNAMIC_PHRASE = trim(Tools::getValue('PS_DYNAMIC_PHRASE'));
				$PS_DYNAMIC_PHONE = trim(Tools::getValue('PS_DYNAMIC_PHONE'));
				if(empty($PS_DYNAMIC_PHRASE)){
						$error .= $this->displayError($this->l('Configuration value is required.'));	
				}
				// if(empty($PS_DYNAMIC_PHONE)){
				// 		$error .= $this->displayError($this->l('Text2  field is required.'));	
				// }
				
			if($PS_DYNAMIC_PHRASE && strlen($PS_DYNAMIC_PHRASE) > 60){
				$error .= $this->displayError($this->l('Configuration value should be max 60 character.'));	
			}
			if($PS_DYNAMIC_PHONE && strlen($PS_DYNAMIC_PHONE) > 50){
				$error .= $this->displayError($this->l('Text2 field should be max 50 character.'));	
			}
			if(empty($error)){
				Configuration::updateValue('PS_DYNAMIC_PHRASE', $PS_DYNAMIC_PHRASE);
				Configuration::updateValue('PS_DYNAMIC_PHONE', $PS_DYNAMIC_PHONE);
				$html = $this->displayConfirmation($this->l('Configuration updated'));
			}else{
				$html = $error;
			}
		}
		

		return $html.$this->renderForm();
	}
	

	public function renderForm()
	{
	    // Get default language
	    $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
	     
	    // Init Fields form array
	    $fields_form[0]['form'] = array(
	        'legend' => array(
	            'title' => $this->l('Dynamic phrase for dashboard'),
	        ),
	        'input' => array(
	            array(
	                'type' => 'text',
	                'label' => $this->l('Text 1'),
	                'name' => 'PS_DYNAMIC_PHRASE',
	                'size' => 20,
	                'required' => true
	            ),
				array(
	                'type' => 'text',
	                'label' => $this->l('Text 2'),
	                'name' => 'PS_DYNAMIC_PHONE',
	                'size' => 35,
	                'required' => true
	            )
	        ),
			
	        'submit' => array(
	            'title' => $this->l('Save'),
	            'class' => 'button'
	        )
	    );
	     
	    $helper = new HelperForm();
	     
	    // Module, token and currentIndex
	    $helper->module = $this;
	    $helper->name_controller = $this->name;
	    $helper->token = Tools::getAdminTokenLite('AdminModules');
	    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
	     
	    // Language
	    $helper->default_form_language = $default_lang;
	    $helper->allow_employee_form_lang = $default_lang;
	     
	    // Title and toolbar
	    $helper->title = $this->displayName;
	    $helper->show_toolbar = true;        // false -> remove toolbar
	    $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
	    $helper->submit_action = 'submit'.$this->name;
	    $helper->toolbar_btn = array(
	        'save' =>
	        array(
	            'desc' => $this->l('Save'),
	            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
	            '&token='.Tools::getAdminTokenLite('AdminModules'),
	        ),
	        'back' => array(
	            'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
	            'desc' => $this->l('Back to list')
	        )
	    );
	     
	    // Load current value
	    $helper->fields_value['PS_DYNAMIC_PHRASE'] = Configuration::get('PS_DYNAMIC_PHRASE');
		$helper->fields_value['PS_DYNAMIC_PHONE'] = Configuration::get('PS_DYNAMIC_PHONE');
	     
	    return $helper->generateForm($fields_form);
	}


}