# Prestashop Export Pro Module

This module was built to fill a gap in Prestashop's current code base. It offers a full export which matches Prestashop's native product import exactly.

## Installation
To install this module either upload the module as a zip file using the Prestashop module page. Or upload the module folder straight to your server within the Prestashop module directory.

Once on the server navigate to the Prestashop admin module page and find Export Pro and click install.

## Usage
Using the module is very simple. The module will add a new menu item called "Export Pro" under "Advanced Parameters" within the Prestashop Admin menu.

To begin exporting just choose what you wish to export and click the Export button.

### Export Options
- Export Type (Products, Combinations, Categories, Customers & Addresses)
- Language Select
- Category select (when exporting products)
- Delimiter (choose the character which separates the fields in the CSV output)
- Export only active products


