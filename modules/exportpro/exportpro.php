<?php
/**
 * You are allowed to use this API in your web application.
 *
 *  @author    Oavea <support@oavea.com>
 *  @copyright (C) 2014 by Oavea
 *  @license   oavea software license
 *
 * This program is licenced under the oavea software license. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * oavea software licence which can be found under
 * http://oavea.com/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences.
 *
 * See the oavea software licence agreement for more details.
 */

if (!defined('_PS_VERSION_'))
	exit;

class ExportPro extends Module
{
	public function __construct()
	{
		$this->name = 'exportpro';
		$this->tab = 'administration';
		$this->version = '1.0.0';
		$this->displayName = 'Export Pro';
		$this->author = 'Oavea - oavea.com';
		$this->description = $this->l ('A module to export everything to csv matching the Prestashop import templates.');
		$this->module_key = '2e087b792cab9ccbab87e643376de3ed';

		parent::__construct ();
	}

	public function install()
	{
		$this->installController ('AdminExportPro', 'Export Pro');
		return parent::install ();

	}

	private function installController($controller_name, $name)
	{
		$tab_admin_order_id = Tab::getIdFromClassName ('AdminTools');
		$tab = new Tab();
		$tab->class_name = $controller_name;
		$tab->id_parent = $tab_admin_order_id;
		$tab->module = $this->name;
		$languages = Language::getLanguages (false);
		foreach ($languages as $lang)
			$tab->name[$lang['id_lang']] = $name;

		$tab->save ();
	}

	public function uninstall()
	{
		$this->uninstallController ('AdminExportPro');
		return parent::uninstall ();
	}

	public function uninstallController($controller_name)
	{
		$tab_controller_main_id = TabCore::getIdFromClassName ($controller_name);
		$tab_controller_main = new Tab($tab_controller_main_id);
		$tab_controller_main->delete ();
	}

}
