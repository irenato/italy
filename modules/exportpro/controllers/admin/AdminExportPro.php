<?php
/**
 * You are allowed to use this API in your web application.
 *
 *  @author    Oavea <support@oavea.com>
 *  @copyright (C) 2014 by Oavea
 *  @license   oavea software license
 *
 * This program is licenced under the oavea software license. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * oavea software licence which can be found under
 * http://oavea.com/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences.
 *
 * See the oavea software licence agreement for more details.
 */

if (!defined('_PS_VERSION_'))
	exit;

class AdminExportProController extends ModuleAdminController
{
	public $available_fields;

	public function __construct()
	{
		$this->bootstrap = true;

		$this->meta_title = $this->l ('Export Pro');
		parent::__construct ();
		if (!$this->module->active)
			Tools::redirectAdmin ($this->context->link->getAdminLink ('AdminHome'));

		$this->available_fields['products'] = array(
			'id' => array('label' => 'Product ID'),
			'active' => array('label' => 'Active (0/1)'),
			'name' => array('label' => 'Name'),
			'categories' => array('label' => 'Categories (x,y,z...)'),
			'price_tex' => array('label' => 'Price tax excluded'),
			'price_tin' => array('label' => 'Price tax included'),
			'id_tax_rules_group' => array('label' => 'Tax rules ID'),
			'wholesale_price' => array('label' => 'Wholesale price'),
			'on_sale' => array('label' => 'On sale (0/1)'),
			'reduction_price' => array('label' => 'Discount amount'),
			'reduction_percent' => array('label' => 'Discount percent'),
			'reduction_from' => array('label' => 'Discount from (yyyy-mm-dd)'),
			'reduction_to' => array('label' => 'Discount to (yyyy-mm-dd)'),
			'reference' => array('label' => 'Reference #'),
			'supplier_reference' => array('label' => 'Supplier reference #'),
			'supplier_name' => array('label' => 'Supplier'),
			'manufacturer_name' => array('label' => 'Manufacturer'),
			'ean13' => array('label' => 'EAN13'),
			'upc' => array('label' => 'UPC'),
			'ecotax' => array('label' => 'Ecotax'),
			'width' => array('label' => 'Width'),
			'height' => array('label' => 'Height'),
			'depth' => array('label' => 'Depth'),
			'weight' => array('label' => 'Weight'),
			'quantity' => array('label' => 'Quantity'),
			'minimal_quantity' => array('label' => 'Minimal quantity'),
			'visibility' => array('label' => 'Visibility'),
			'additional_shipping_cost' => array('label' => 'Additional shipping cost'),
			'unity' => array('label' => 'Unit for the unit price'),
			'unit_price' => array('label' => 'Unit price'),
			'description_short' => array('label' => 'Short description'),
			'description' => array('label' => 'Description'),
			'tags' => array('label' => 'Tags (x,y,z...)'),
			'meta_title' => array('label' => 'Meta title'),
			'meta_keywords' => array('label' => 'Meta keywords'),
			'meta_description' => array('label' => 'Meta description'),
			'link_rewrite' => array('label' => 'URL rewritten'),
			'available_now' => array('label' => 'Text when in stock'),
			'available_later' => array('label' => 'Text when backorder allowed'),
			'available_for_order' => array('label' => 'Available for order (0 = No, 1 = Yes)'),
			'available_date' => array('label' => 'Product available date'),
			'date_add' => array('label' => 'Product creation date'),
			'show_price' => array('label' => 'Show price (0 = No, 1 = Yes)'),
			'image' => array('label' => 'Image URLs (x,y,z...)'),
			'delete_existing_images' => array(
				'label' => 'Delete existing images (0 = No, 1 = Yes)'
			),
			'features' => array('label' => 'Feature (Name:Value:Position:Customized)'),
			'online_only' => array('label' => 'Available online only (0 = No, 1 = Yes)'),
			'condition' => array('label' => 'Condition'),
			'customizable' => array('label' => 'Customizable (0 = No, 1 = Yes)'),
			'uploadable_files' => array('label' => 'Uploadable files (0 = No, 1 = Yes)'),
			'text_fields' => array('label' => 'Text fields (0 = No, 1 = Yes)'),
			'out_of_stock' => array('label' => 'Action when out of stock'),
			'shop' => array(
				'label' => 'ID / Name of shop',
				'help' => 'Ignore this field if you don\'t use the Multistore tool. If you leave this field empty, the default shop will be used.',
			),
			'advanced_stock_management' => array(
				'label' => 'Advanced Stock Management',
				'help' => 'Enable Advanced Stock Management on product (0 = No, 1 = Yes).',
			),
			'depends_on_stock' => array(
				'label' => 'Depends on stock',
				'help' => '0 = Use quantity set in product, 1 = Use quantity from warehouse.',
			),
			'warehouse' => array(
				'label' => 'Warehouse',
				'help' => 'ID of the warehouse to set as storage.'
			),
		);

		$this->available_fields['categories'] = array(
			'id' => array('label' => 'Category ID'),
			'active' => array('label' => 'Active (0/1)'),
			'name' => array('label' => 'Name'),
			'id_parent' => array('label' => 'Parent category'),
			'is_root_category' => array('label' => 'Root category (0/1)'),
			'description' => array('label' => 'Description'),
			'meta_title' => array('label' => 'Meta title'),
			'meta_keywords' => array('label' => 'Meta keywords'),
			'meta_description' => array('label' => 'Meta description'),
			'link_rewrite' => array('label' => 'URL rewritten'),
			'image' => array('label' => 'Image URL'),
			'id_shop_default' => array('label' => 'ID / Name of shop'),
		);

		$this->available_fields['combinations'] = array(
			'id' => array('label' => 'Product ID'),
			'group' => array(
				'label' => 'Attribute (Name:Type:Position)'
			),
			'attribute' => array(
				'label' => 'Value (Value:Position)'
			),
			'supplier_reference' => array('label' => 'Supplier reference'),
			'reference' => array('label' => 'Reference'),
			'ean13' => array('label' => 'EAN13'),
			'upc' => array('label' => 'UPC'),
			'wholesale_price' => array('label' => 'Wholesale price'),
			'impact_price' => array('label' => 'Impact on price'),
			'ecotax' => array('label' => 'Ecotax'),
			'quantity' => array('label' => 'Quantity'),
			'minimal_quantity' => array('label' => 'Minimal quantity'),
			'weight' => array('label' => 'Impact on weight'),
			'default_on' => array('label' => 'Default (0 = No, 1 = Yes)'),
			'image_position' => array(
				'label' => 'Image position'
			),
			'image_url' => array('label' => 'Image URL'),
			'delete_existing_images' => array(
				'label' => 'Delete existing images (0 = No, 1 = Yes).'
			),
			'shop' => array(
				'label' => 'ID / Name of shop',
				'help' => 'Ignore this field if you don\'t use the Multistore tool. If you leave this field empty, the default shop will be used.',
			),
			'advanced_stock_management' => array(
				'label' => 'Advanced Stock Management',
				'help' => 'Enable Advanced Stock Management on product (0 = No, 1 = Yes)'
			),
			'depends_on_stock' => array(
				'label' => 'Depends on stock',
				'help' => '0 = Use quantity set in product, 1 = Use quantity from warehouse.'
			),
			'warehouse' => array(
				'label' => 'Warehouse',
				'help' => 'ID of the warehouse to set as storage.'
			),
		);

		$this->available_fields['customers'] = array(

			'id_customer' => array('label' => 'Customer ID'),
			'active' => array('label' => 'Active  (0/1)'),
			'id_gender' => array('label' => 'Titles ID (Mr = 1, Ms = 2, else 0)'),
			'email' => array('label' => 'Email *'),
			'passwd' => array('label' => 'Password *'),
			'birthday' => array('label' => 'Birthday (yyyy-mm-dd)'),
			'lastname' => array('label' => 'Last Name *'),
			'firstname' => array('label' => 'First Name *'),
			'newsletter' => array('label' => 'Newsletter (0/1)'),
			'optin' => array('label' => 'Opt-in (0/1)'),
			'group' => array('label' => 'Groups (x,y,z...)'),
			'id_default_group' => array('label' => 'Default group ID'),
			'id_shop' => array(
				'label' => 'ID / Name of shop',
				'help' => 'Ignore this field if you don\'t use the Multistore tool. If you leave this field empty, the default shop will be used.',
			),
		);
		$this->available_fields['addresses'] = array(
			'id_address' => array('label' => 'Address ID'),
			'alias' => array('label' => 'Alias *'),
			'active' => array('label' => 'Active  (0/1)'),
			'customer_email' => array('label' => 'Customer email *'),
			'id_customer' => array('label' => 'Customer ID'),
			'manufacturer' => array('label' => 'Manufacturer'),
			'supplier' => array('label' => 'Supplier'),
			'company' => array('label' => 'Company'),
			'lastname' => array('label' => 'Last Name *'),
			'firstname' => array('label' => 'First Name *'),
			'address1' => array('label' => 'Address 1 *'),
			'address2' => array('label' => 'Address 2'),
			'postcode' => array('label' => 'Zip/postal code *'),
			'city' => array('label' => 'City *'),
			'country' => array('label' => 'Country *'),
			'state' => array('label' => 'State'),
			'other' => array('label' => 'Other'),
			'phone' => array('label' => 'Phone'),
			'phone_mobile' => array('label' => 'Mobile Phone'),
			'vat_number' => array('label' => 'VAT number'),
		);
	}

	public function getWarehouses($id_warehouses)
	{
		return $id_warehouses['id_warehouse'];
	}

	public function postProcess()
	{
		if (Tools::isSubmit ('submitExport'))
		{
			$export_type = Tools::getValue ('export_type');
			$delimiter = Tools::getValue ('export_delimiter');
			$id_lang = Tools::getValue ('export_language');
			$export_active = (Tools::getValue ('export_active') == 0 ? false : true);
			$export_category = (Tools::getValue ('export_category') == 99999 ? false : Tools::getValue ('export_category'));
			$titles = array();

			set_time_limit (0);
			$fileName = $export_type.'_'.date ('Y_m_d_H_i_s').'.csv';
			header ('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header ('Content-Description: File Transfer');
			header ('Content-type: text/csv');
			header ("Content-Disposition: attachment; filename={$fileName}");
			header ('Expires: 0');
			header ('Pragma: public');

			$f = fopen ('php://output', 'w');

			foreach ($this->available_fields[$export_type] as $field => $array)
				$titles[] = $array['label'];

			fputcsv ($f, $titles, $delimiter, '"');

			switch ($export_type)
			{
				case 'products':
					$products = Product::getProducts ($id_lang, 0, 0, 'id_product', 'ASC', $export_category, $export_active);

					foreach ($products as $product)
					{
						$line = array();
						$p = new Product($product['id_product'], true, $id_lang, 1);

						foreach ($this->available_fields['products'] as $field => $array)
						{
							if (isset($p->$field) && !is_array ($p->$field))
								$line[$field] = $p->$field;
							else
							{
								switch ($field)
								{
									case 'categories':
										$cats = $p->getProductCategoriesFull ($p->id, 1);
										$cat_array = array();
										foreach ($cats as $cat)
											$cat_array[] = $cat['name'];

										$line['categories'] = implode (',', $cat_array);
										break;
									case 'price_tex':
										$line['price_tex'] = $p->getPrice (false);
										$line['price_tin'] = $p->getPrice (true);

										break;
									case 'upc':
										$line['upc'] = $p->upc ? $p->upc : ' ';

										break;
									case 'features':
										$line['features'] = '';
										$features = $p->getFrontFeatures ($id_lang);
										$position = 1;

										foreach ($features as $feature)
										{
											$line['features'] .= $feature['name'].':'.$feature['value'].':'.$position;
											$position++;
										}

										break;
									case 'reduction_price':
										$specificPrice = SpecificPrice::getSpecificPrice($p->id, 1, 0, 0, 0, 0);

										$line['reduction_price'] = '';
										$line['reduction_percent'] = '';
										$line['reduction_from'] = '';
										$line['reduction_to'] = '';

										if ($specificPrice['reduction_type'] == "amount") {
											$line['reduction_price'] = $specificPrice['reduction'];
										} elseif ($specificPrice['reduction_type'] == "percentage") {
											$line['reduction_percent'] = $specificPrice['reduction'] * 100;
										}

										if ($line['reduction_price'] != '' || $line['reduction_percent'] != '') {
											if($specificPrice['from'] != "0000-00-00 00:00:00")
											{
												$line['reduction_from'] = date_format (date_create ($specificPrice['from']), "Y-m-d");
											}
											if($specificPrice['to'] != "0000-00-00 00:00:00")
											{
												$line['reduction_to'] = date_format (date_create ($specificPrice['to']), "Y-m-d");
											}
										}
										break;
									case 'tags':
										$tags = $p->getTags ($id_lang);
										$line['tags'] = $tags;

										break;
									case 'image':
										$link = new Link();
										$imagelinks = array();
										$images = $p->getImages ($id_lang);
										foreach ($images as $image)
											$imagelinks[] = Tools::getShopProtocol ().$link->getImageLink ($p->link_rewrite, $p->id.'-'.$image['id_image']);

										$line['image'] = implode (',', $imagelinks);

										break;
									case 'delete_existing_images':
										$line['delete_existing_images'] = 0;

										break;
									case 'shop':
										$line['shop'] = 1;

										break;
									case 'warehouse':
										$warehouses = Warehouse::getWarehousesByProductId ($p->id);
										$line['warehouse'] = '';
										if (!empty($warehouses))
											$line['warehouse'] = implode (',', array_map ("$this->getWarehouses", $warehouses));

										break;
								}
							}
							if (! $line[$field] && ! in_array($field, array('reduction_price', 'reduction_to', 'reduction_from', 'reduction_percent'))) {
								$line[$field] = '';
							}
						}

						fputcsv ($f, $line, $delimiter, '"');
					}
					break;
				case 'categories':
					$categories = $this->getCategories ($id_lang, $export_active);

					foreach ($categories as $cat)
					{
						$line = array();
						$c = new Category($cat['id_category'], $id_lang, 1);

						foreach ($this->available_fields['categories'] as $field => $array)
						{
							if (isset($c->$field) && !is_array ($c->$field))
								$line[$field] = $c->$field;
							else
							{
								switch ($field)
								{
									case 'image':
										$line['image'] = Link::getCatImageLink ($c->name, $c->id_category);
										break;
								}
							}

							if (!$line[$field])
								$line[$field] = '';

						}
						fputcsv ($f, $line, $delimiter, '"');
					}
					break;
				case 'customers':
					$sql = 'SELECT *
							FROM `'._DB_PREFIX_.'customer`
							'.($export_active ? 'WHERE active=1' : '').'
							ORDER BY `id_customer` ASC';
					$customers = Db::getInstance (_PS_USE_SQL_SLAVE_)->executeS ($sql);

					foreach ($customers as $customer)
					{
						foreach ($this->available_fields['customers'] as $field => $array)
						{
							if (isset($customer[$field]) && !is_array ($customer[$field]))
								$line[$field] = $customer[$field];
							else
							{
								if ($field == 'group')
								{
									$result = Db::getInstance ()->executeS ('
											SELECT cg.`id_group`, gl.`name`
												FROM '._DB_PREFIX_.'customer_group cg
												LEFT JOIN '._DB_PREFIX_.'group_lang gl ON gl.`id_group` = cg.`id_group`
												WHERE cg.`id_customer` = '.(int)$customer['id_customer'].' AND gl.`id_lang` = '.$id_lang);

									$groups = array();
									foreach ($result as $group)
										$groups[] = $group['name'];

									$line[$field] = implode (',', $groups);
								}

							}
							if (!$line[$field])
								$line[$field] = '';

						}
						fputcsv ($f, $line, $delimiter, '"');
					}
					break;
				case 'combinations':
					if (!Combination::isFeatureActive ())
						return false;

					$products = Product::getProducts ($id_lang, 0, 0, 'id_product', 'ASC', $export_category, $export_active);

					foreach ($products as $product)
					{
						$line = array();
						$p = new Product($product['id_product'], true, $id_lang, 1);
						$p->loadStockData ();
						$sql = 'SELECT ag.`id_attribute_group`, ag.`is_color_group`, agl.`name` AS group_name, agl.`public_name` AS public_group_name,
					a.`id_attribute`, a.`position` AS attribute_position, ag.`position` AS group_position, al.`name` AS attribute_name, a.`color` AS attribute_color,
					 product_attribute_shop.`id_product_attribute`,
					IFNULL(stock.quantity, 0) as quantity, pa.`price`, product_attribute_shop.`ecotax`, pa.`weight`,
					pa.`default_on`, pa.`reference`, pa.`unit_price_impact`,
					pa.`minimal_quantity`, product_attribute_shop.`available_date`, ag.`group_type`
				FROM `'._DB_PREFIX_.'product_attribute` pa
				'.Shop::addSqlAssociation ('product_attribute', 'pa').'
				'.Product::sqlStock ('pa', 'pa').'
				LEFT JOIN `'._DB_PREFIX_.'product_attribute_combination` pac ON (pac.`id_product_attribute` = pa.`id_product_attribute`)
				LEFT JOIN `'._DB_PREFIX_.'attribute` a ON (a.`id_attribute` = pac.`id_attribute`)
				LEFT JOIN `'._DB_PREFIX_.'attribute_group` ag ON (ag.`id_attribute_group` = a.`id_attribute_group`)
				LEFT JOIN `'._DB_PREFIX_.'attribute_lang` al ON (a.`id_attribute` = al.`id_attribute`)
				LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl ON (ag.`id_attribute_group` = agl.`id_attribute_group`)
				'.Shop::addSqlAssociation ('attribute', 'a').'
				WHERE pa.`id_product` = '.(int)$p->id.'
				GROUP BY pa.`id_product_attribute`, ag.`id_attribute_group`
				ORDER BY pa.`id_product_attribute`';

						$attributes = Db::getInstance ()->executeS ($sql);

						if ($attributes)
						{
							foreach ($attributes as $value)
							{
								foreach ($this->available_fields['combinations'] as $field => $array)
								{
									if (isset($p->$field) && !is_array ($p->$field))
										$line[$field] = $p->$field;
									else
									{
										switch ($field)
										{
											case 'group':
												$line[$field] = $value['public_group_name'].':'.$value['group_type'].':'.$value['group_position'];
												break;
											case 'attribute':
												$line[$field] = $value['attribute_name'].':'.$value['attribute_position'];
												break;
											case 'impact_price':
												$line[$field] = $value['price'];
												break;
											case 'default_on':
												$line[$field] = $value['default_on'] == 1 ? 1 : 0;
												break;
											case 'image_url':
											case 'image_position':
												$link = new Link();

												$sql = 'SELECT id_image FROM `'._DB_PREFIX_.'product_attribute_image` WHERE `id_product_attribute`='.$value['id_product_attribute'];
												$image = Db::getInstance ()->executeS ($sql);

												if ((int)$image['id_image'] > 0)
												{
													$sql = 'SELECT position FROM `'._DB_PREFIX_.'image` WHERE `id_image`='.$image['id_image'];
													$q = Db::getInstance ()->executeS ($sql);
													$line['image_position'] = $q['position'];
												}
												else
													$line['image_position'] = '';

												if ((int)$image['id_image'] > 0)
													$line['image_url'] = Tools::getShopProtocol ().$link->getImageLink ($p->link_rewrite, $p->id.'-'.$image['id_image']);
												else
													$line['image_url'] = '';

												break;
											case 'delete_existing_images':
												$line['delete_existing_images'] = 0;

												break;
											case 'shop':
												$line[$field] = '';
												break;
											case 'warehouse':
												$warehouses = Warehouse::getWarehousesByProductId ($p->id);
												$line['warehouse'] = '';
												if (!empty($warehouses))
													$line['warehouse'] = implode (',', array_map ("$this->getWarehouses", $warehouses));

												break;
										}
									}
								}

								if (!$line[$field])
									$line[$field] = '';
								fputcsv ($f, $line, $delimiter, '"');
							}

						}
					}

					break;
				case 'addresses':
					$sql = 'SELECT a.*, c.`email` as `customer_email`, s.`name` as `state`, cl.`name` as `country`
							FROM `'._DB_PREFIX_.'address` a
							LEFT JOIN `'._DB_PREFIX_.'customer` c ON (c.`id_customer` = a.`id_customer`)
							LEFT JOIN `'._DB_PREFIX_.'state` s ON (s.`id_state` = a.`id_state`)
							LEFT JOIN `'._DB_PREFIX_.'country_lang` cl ON (cl.`id_country` = a.`id_country`)
							WHERE cl.`id_lang` = '.(int)$id_lang.'
							'.($export_active ? ' AND a.`active`=1' : '');

					$customers = Db::getInstance (_PS_USE_SQL_SLAVE_)->executeS ($sql);

					foreach ($customers as $customer)
					{
						foreach ($this->available_fields['addresses'] as $field => $array)
						{
							if (isset($customer[$field]) && !is_array ($customer[$field]))
								$line[$field] = $customer[$field];
							else
							{

								if($field == 'supplier') {
									$line['supplier'] = '';
									if($customer['id_supplier'] > 0) {
										$supplier = new Supplier($customer['id_supplier']);
										$line['supplier'] = $supplier->name;
									}
								}


								if($field == 'manufacturer') {
									$line['manufacturer'] = '';
									if($customer['id_manufacturer'] > 0) {
										$manufacturer = new Manufacturer($customer['id_manufacturer']);
										$line['manufacturer'] = $manufacturer->name;
									}
								}

								if ($field == 'group')
								{
									$result = Db::getInstance ()->executeS ('
											SELECT cg.`id_group`, gl.`name`
												FROM '._DB_PREFIX_.'customer_group cg
												LEFT JOIN '._DB_PREFIX_.'group_lang gl ON gl.`id_group` = cg.`id_group`
												WHERE cg.`id_customer` = '.(int)$customer['id_customer'].' AND gl.`id_lang` = '.$id_lang);

									$groups = array();
									foreach ($result as $group)
										$groups[] = $group['name'];

									$line[$field] = implode (',', $groups);
								}

							}
							if (!$line[$field])
								$line[$field] = '';
						}
						fputcsv ($f, $line, $delimiter, '"');
					}
					break;
				case 'manufacturers':

					break;
				case 'suppliers':

					break;
				case 'alias':

					break;
			}

			fclose ($f);
			die();
		}
	}

	public function getCategories($id_lang, $active)
	{
		$result = Db::getInstance (_PS_USE_SQL_SLAVE_)->executeS ('
			SELECT *
			FROM `'._DB_PREFIX_.'category` c
			LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON c.`id_category` = cl.`id_category`
			WHERE '.($id_lang ? '`id_lang` = '.(int)$id_lang : '').'
			'.($active ? 'AND `active` = 1' : '').'
			'.(!$id_lang ? 'GROUP BY c.id_category' : '').'
			ORDER BY c.`level_depth` ASC, c.`position` ASC');

		return $result;
	}

	public function initContent()
	{
		$this->content = $this->renderView ();
		parent::initContent ();
	}

	public function renderView()
	{
		return $this->renderConfigurationForm ();
	}

	public function renderConfigurationForm()
	{
		$lang = new Language((int)Configuration::get ('PS_LANG_DEFAULT'));
		$langs = Language::getLanguages ();
		$categories = array();
		$options = array();

		foreach ($langs as $language)
			$options[] = array('id_option' => $language['id_lang'], 'name' => $language['name']);

		$cats = $this->getCategories ($lang->id, true);

		$categories[] = array('id_option' => 99999, 'name' => 'All');

		foreach ($cats as $cat)
			$categories[] = array('id_option' => $cat['id_category'], 'name' => $cat['name']);

		$export_types = array(
			array('id_option' => 'products', 'name' => 'Products'),
			array('id_option' => 'categories', 'name' => 'Categories'),
			array('id_option' => 'combinations', 'name' => 'Combinations'),
			array('id_option' => 'customers', 'name' => 'Customers'),
			array('id_option' => 'addresses', 'name' => 'Addresses'));
		//array('id_option' => 'manufacturers', 'name' => 'Manufacturers'),
		//array('id_option' => 'suppliers', 'name' => 'Suppliers'),
		//array('id_option' => 'alias', 'name' => 'Alias'));

		$inputs = array(
			array(
				'type' => 'select',
				'label' => $this->l ('What kind of entity would you like to export?'),
				'name' => 'export_type',
				'class' => 't export_type',
				'value' => 'products',
				'options' => array(
					'query' => $export_types,
					'id' => 'id_option',
					'name' => 'name'
				),
			),
			array(
				'type' => 'select',
				'label' => $this->l ('Language'),
				'desc' => $this->l ('Choose a language you wish to export'),
				'name' => 'export_language',
				'class' => 't',
				'options' => array(
					'query' => $options,
					'id' => 'id_option',
					'name' => 'name'
				),
			),
			array(
				'type' => 'text',
				'label' => $this->l ('Delimiter'),
				'name' => 'export_delimiter',
				'value' => ',',
				'desc' => $this->l ('The character to separate the fields')
			),
			array(
				'type' => 'radio',
				'label' => $this->l ('Export active items?'),
				'name' => 'export_active',
				'values' => array(
					array('id' => 'active_off', 'value' => 0, 'label' => 'no, export all.'),
					array('id' => 'active_on', 'value' => 1, 'label' => 'yes, export only active items'),
				),
				'is_bool' => true,
			),
			array(
				'type' => 'select',
				'label' => $this->l ('Product Category'),
				'desc' => $this->l ('Choose a product category you wish to export'),
				'name' => 'export_category',
				'class' => 't export_category',
				'options' => array(
					'query' => $categories,
					'id' => 'id_option',
					'name' => 'name'
				),
			),
		);

		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l ('Export Options'),
					'icon' => 'icon-cogs'
				),
				'input' => $inputs,
				'submit' => array(
					'title' => $this->l ('Export'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;

		$helper->default_form_language = $lang->id;
		$emp_lang = Configuration::get ('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get ('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->allow_employee_form_lang = $emp_lang;
		$this->fields_form = array();
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitExport';
		$helper->currentIndex = self::$currentIndex;
		$helper->token = Tools::getAdminTokenLite ('AdminExportPro');
		$helper->tpl_vars = array(
			'export_active' => 0,
			'fields_value' => $this->getConfigFieldsValues (),
			'languages' => $this->context->controller->getLanguages (),
			'id_language' => $this->context->language->id
		);

		$js = '
		<script type="text/javascript">
			$(document).ready(function() {

			$(".export_type").on("change", function(){
				if($(this).val() == "products")
				{
					$(".export_category").closest(".form-group").show();
				} else
				{
					$(".export_category").closest(".form-group").hide();
				}
			});
			});
		</script>
		';

		return $js.$helper->generateForm (array($fields_form));
	}

	public function getConfigFieldsValues()
	{
		return array(
			'export_type' => 'products',
			'export_active' => false,
			'export_category' => 'all',
			'export_delimiter' => ',',
			'export_language' => (int)Configuration::get ('PS_LANG_DEFAULT')
		);
	}

}
