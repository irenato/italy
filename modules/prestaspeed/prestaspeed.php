<?php
/**
 * 2007-2014 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * No redistribute in other sites, or copy.
 *
 * @author    RSI
 * @copyright 2007-2014 RSI
 * @license   http://localhost
 */

ini_set('memory_limit', '256M');
if (!defined('_PS_VERSION_'))
	exit;
if (ini_get('safe_mode'))
{
	/* safe mode */
	/*is on */
}
else
	@set_time_limit(0);

class PrestaSpeed extends Module
{
	private $output = '';
	private $_images;
	private $savelog;
	private $_postErrors = array();

	public function __construct()
	{
		$this->module_key = '9ed5d291ca4bbc39a25aba178a90363a';
		$this->name       = 'prestaspeed';

		if (_PS_VERSION_ < '1.4.0.0')
			$this->tab = 'Tools';
		if (_PS_VERSION_ > '1.4.0.0')
		{
			$this->tab           = 'administration';
			$this->author        = 'RSI';
			$this->need_instance = 0;
		}
		if (_PS_VERSION_ > '1.6.0.0')
			$this->bootstrap = true;
		$this->version = '1.6.1';
		parent::__construct();
		$this->displayName = $this->l('Presta Speed');
		$this->description = $this->l('Optimize database speed and Prestashop performance - RSI');
		if (_PS_VERSION_ < '1.5')
			require(_PS_MODULE_DIR_.$this->name.'/backward_compatibility/backward.php');
	}

	public function install()
	{
		if (!Configuration::updateValue('PRESTASPEED_CO', 1) || !parent::install() || !$this->registerHook('header') || !$this->registerHook('watermark') || !$this->registerHook('footer'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_GU', '1'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_PA', '1'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_DI', '1'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_CA', '0'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_CUSI', ''))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_FUNC', '0'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_CAV', '0'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_DA', '1'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_CAC', Configuration::get('PS_SMARTY_CACHE')))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_TE', Configuration::get('PS_CSS_THEME_CACHE')))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_GZ', Configuration::get('PS_HTACCESS_CACHE_CONTROL')))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_JA', Configuration::get('PS_JS_THEME_CACHE')))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_FR', ''))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_TO', ''))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_FR2', ''))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_TO2', ''))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_ORD', '0'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_HTA', '0'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_COMPRESS', '1'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_BATCHT', '30'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_SPEED', '1'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_BO', '0'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_SMUSH', '0'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_SMUSH2', '0'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_CLEANI', '0'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_TOTFIL', '0'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_TOTSMUSH', '0'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_TOTCOMP', '0'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_TOTCOMPF', '0'))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_DBS', ''))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_IDB', ''))
			return false;
		if (!Configuration::updateValue('PRESTASPEED_TYPE', ''))
			return false;

		if (!Configuration::updateValue('PRESTASPEED_STATS', '0'))
			return false;
		if (_PS_VERSION_ > '1.5.0.0' || _PS_VERSION_ < '1.6.0.0')
			$this->registerHook('displayAdminHomeQuickLinks') == false;
		if (_PS_VERSION_ > '1.5.0.0')
			$this->registerHook('displayBackOfficeHeader') == false;
		if (_PS_VERSION_ > '1.5.0.0')
			$this->registerHook('displayBackOfficeFooter') == false;
		if (_PS_VERSION_ < '1.5.0.0')
			$this->registerHook('backOfficeHome') == false;
		if (_PS_VERSION_ > '1.6.0.0')
		{
			$this->registerHook('dashboardZoneOne') == false;
			$this->registerHook('actionAdminControllerSetMedia') == false;
		}
		$query = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'smush (`id_smush` int(2) NOT NULL, `url` varchar(255) NULL, `smushed` TINYINT(1) NOT NULL,`saved` varchar(255) NULL, PRIMARY KEY(`url`)) ENGINE=MyISAM default CHARSET=utf8';
		Db::getInstance()->Execute($query);
		$sql    = 'SELECT table_schema \''._DB_NAME_.'\', SUM( data_length + index_length) / 1024 / 1024 \'db_size_in_mb\' FROM information_schema.TABLES WHERE table_schema=\''._DB_NAME_.'\' GROUP BY table_schema ;';
		$query2 = mysqli_query($GLOBALS['___mysqli_ston'], $sql);
		$data   = mysqli_fetch_array($query2);
		Configuration::updateValue('PRESTASPEED_IDB', $data['db_size_in_mb']);
		return true;
	}

	public function clearTmpDir()
	{
		$i = 0;
		foreach (scandir(_PS_TMP_IMG_DIR_) as $d)
		{

			if (preg_match('/(.*)\.jpg$/', $d))
			{
				$i++;
				unlink(_PS_TMP_IMG_DIR_.$d);
			}
		}
		Configuration::updateValue('PRESTASPEED_TMPI', $i);
	}

	public function hookHeader()
	{
		$speed = Configuration::get('PRESTASPEED_SPEED');

		/*	if (_PS_VERSION_ > '1.4.0.0' && _PS_VERSION_ < '1.5.0.0')
		
			Tools::addJS(__PS_BASE_URI__.'modules/prestaspeed/js/instantclick.min.js');
	*/
		/*	if (_PS_VERSION_ > '1.5.0.0')*/

		//$this->context->controller->addJS(($this->_path).'js/instantclick.min.js');
		/*
				$this->context->controller->addJS(($this->_path).'js/turbo.min.js');
				header("X-XHR-Current-Location: ".$this->current_url());*/

		if ($speed == 1)
		{
			ini_set('zlib.output_compression_level', 9);
			if (extension_loaded('zlib') && !ini_get('zlib.output_compression'))
				ob_start('ob_gzhandler');
		}

	}

	public function current_url()
	{
		$s        = empty($_SERVER['HTTPS']) ? '' : ($_SERVER['HTTPS'] == 'on') ? 's' : '';
		$protocol = 'http'.$s;
		$port     = ($_SERVER['SERVER_PORT'] == '80') ? '' : (':'.$_SERVER['SERVER_PORT']);
		return $protocol.'://'.$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI'];
	}

	public function hookDisplayBackOfficeFooter()
	{
		/*
		$this->context->smarty->assign(array(
		'psversion'      => _PS_VERSION_,
		));
		return $this->display(__FILE__, 'views/templates/front/insta.tpl');*/
	}

	public function hookDisplayBackOfficeHeader()
	{
		$speed = Configuration::get('PRESTASPEED_SPEED');
		/*
				if (_PS_VERSION_ > '1.4.0.0' && _PS_VERSION_ < '1.5.0.0')

				Tools::addJS(__PS_BASE_URI__.'modules/prestaspeed/js/instantclick.min.js');

			if (_PS_VERSION_ > '1.5.0.0')

				$this->context->controller->addJS(($this->_path).'js/instantclick.min.js');
	*/
	}

	public function hookFooter()
	{
		$this->context->smarty->assign(array(
			'psversion' => _PS_VERSION_,

		));
		/*
	return $this->display(__FILE__, 'views/templates/front/insta.tpl');
	*/
	}

	public function rglob($pattern = '*', $path = '', $flags = 0)
	{
		$files = array();
		$paths = glob($path.'*', GLOB_MARK | GLOB_ONLYDIR | GLOB_NOSORT);

		$files = glob($path.$pattern, $flags);
		foreach ($paths as $path)

			@$files = array_merge($files, $this->rglob($pattern, $path, $flags));

		return $files;
	}

	/*one image*/
	public function smushcustom($cusi)
	{
		if (!is_callable('curl_init'))
			$output = $this->displayError($this->l('cURL not loaded'));

		$this->registerHook('watermark');
		require_once _PS_MODULE_DIR_.$this->name.'/smushit.inc.php';
		$query = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'smush (`id_smush` int(2) NOT NULL, `url` varchar(255) NULL, `smushed` TINYINT(1) NOT NULL,`saved` varchar(255) NULL, PRIMARY KEY(`url`)) ENGINE=MyISAM default CHARSET=utf8';
		Db::getInstance()->Execute($query);
		$total   = '';
		$smushit = new SmushIt();
		$url     = 'http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__;
		$fil     = $cusi;
//var_dump($files2);
		Configuration::updateValue('PRESTASPEED_TOTFIL', 1);

// Make batches of 3 images
//$files = array_chunk($files, 3);
		$start_time         = 0;
		$max_execution_time = 3337200;
		/* $process =
					array(
						array('type' => 'categories', 'dir' => _PS_CAT_IMG_DIR_),
						array('type' => 'manufacturers', 'dir' => _PS_MANU_IMG_DIR_),
						array('type' => 'suppliers', 'dir' => _PS_SUPP_IMG_DIR_),
						array('type' => 'scenes', 'dir' => _PS_SCENE_IMG_DIR_),
						array('type' => 'products', 'dir' => _PS_PROD_IMG_DIR_),
						array('type' => 'stores', 'dir' => _PS_STORE_IMG_DIR_)
					);*/
// Take a batch of three files
		$start_time = time();
		ini_set('max_execution_time', $max_execution_time); // ini_set may be disabled, we need the real value
		$max_execution_time = (int)ini_get('max_execution_time');
		@set_time_limit(0);
		$i = 0;

		/**/
		//$cdir = scandir('img/');
		//print_r( $cdir);

		/**/

		//flush();
		$i++;
		//while (list($key, $value) = each($fil))
		//{
		$query = 'INSERT IGNORE INTO '._DB_PREFIX_.'smush (`id_smush`, `url`, `smushed`, `saved`) VALUES (\''.pSQL((int)$i).'\', \''.str_replace('../../', '', pSQL((string)$fil)).'\', \'0\', \'0\')';
		if (!Db::getInstance()->Execute($query));
		//echo $fil.'<br/>';
		$query2 = 'SELECT * FROM '._DB_PREFIX_.'smush WHERE `url` = \''.str_replace('../../', '', pSQL((string)$fil)).'\' AND `smushed` = 0';
		$quer   = Db::getInstance()->ExecuteS($query2);
		if ($quer != null)
		{
			$remote_result = $smushit->compress($fil);
			//print_r($remote_result).'<br/>';
			$a = 0;
			if (!empty($remote_result->error) || !empty($remote_result->dest_size) == -1)
			{
				$upd = 'UPDATE '._DB_PREFIX_.'smush SET `smushed` = \'-1\' WHERE  '._DB_PREFIX_.'smush.url =  \''.str_replace('../../', '', pSQL((string)$fil)).'\'';
				Db::getInstance()->Execute($upd);
			}
			if (!empty($remote_result->dest))
			{
				if ($remote_result->dest != null && $remote_result->dest_size != -1)
				{
					//$url=$remote_result->dest;
					//$contents=file_get_contents($url);
					//$save_path=$_SERVER['DOCUMENT_ROOT'].'/tmp/'.basename($value);
					//file_put_contents($save_path,$contents);
					$file    = $remote_result->dest;
					$newfile = _PS_ROOT_DIR_.'/'.str_replace('../', '', $fil);
					if (copy($file, $newfile))
					{
						//echo "Copy success!";
						$total = $remote_result->src_size - $remote_result->dest_size;
						$upd   = 'UPDATE '._DB_PREFIX_.'smush SET `smushed` = \'1\', `saved` = \''.pSQL((float)$total).'\' WHERE  '._DB_PREFIX_.'smush.url =  \''.str_replace('../../', '', pSQL((string)$fil)).'\'';
						Db::getInstance()->Execute($upd);
						//
						//print_r($upd);
					}
					//echo     '<br/>';
					$a++;
					//}
					//echo 'Total files smushed'.$a.'<br/>';
				}
			}
		}
		Configuration::updateValue('PRESTASPEED_TOTSMUSH', $i);
		if (time() - $start_time > $max_execution_time - 4) // stop 4 seconds before the timeout, just enough time to process the end of the page on a slow server
			return 'timeout';



		/*	include('../modules/prestaspeed/SimpleImage.php');
				//if (!is_callable('curl_init'))
				//$output = $this->displayError($this->l('cURL not loaded'));

				$this->registerHook('watermark');
				//require_once _PS_MODULE_DIR_.$this->name.'/smushit.inc.php';
				$query = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'smush (`id_smush` int(2) NOT NULL, `url` varchar(255) NULL, `smushed` TINYINT(1) NOT NULL,`saved` varchar(255) NULL, PRIMARY KEY(`url`)) ENGINE=MyISAM default CHARSET=utf8';
				Db::getInstance()->Execute($query);
				$total = '';
				//$smushit = new SmushIt();
				//$url    = 'http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__;
				//$files2 = $this->rglob('{*.jpg,*.png,*.gif}', $type, GLOB_BRACE);
				//$co = count($files2);

		//var_dump($files2);
				Configuration::updateValue('PRESTASPEED_TOTFIL', 1);
				//$fileschunk = array_chunk($files2, 40, false);
		// Make batches of 3 images
		//$files = array_chunk($files, 3);
				$start_time         = 0;
				$max_execution_time = 3337200;
				/* $process =
							array(
								array('type' => 'categories', 'dir' => _PS_CAT_IMG_DIR_),
								array('type' => 'manufacturers', 'dir' => _PS_MANU_IMG_DIR_),
								array('type' => 'suppliers', 'dir' => _PS_SUPP_IMG_DIR_),
								array('type' => 'scenes', 'dir' => _PS_SCENE_IMG_DIR_),
								array('type' => 'products', 'dir' => _PS_PROD_IMG_DIR_),
								array('type' => 'stores', 'dir' => _PS_STORE_IMG_DIR_)
							);*/
// Take a batch of three files
		/*	$start_time = time();
			ini_set('max_execution_time', $max_execution_time); // ini_set may be disabled, we need the real value
			$max_execution_time = (int)ini_get('max_execution_time');
			@set_time_limit(0);
			$i = 0;

			/**/
		/*





				$fil = $cusi;

					$fil2 = $fil;
					var_dump($cusi);

$query = 'INSERT IGNORE INTO '._DB_PREFIX_.'smush (`id_smush`, `url`, `smushed`, `saved`) VALUES (\''.pSQL((int)$i).'\', \''.str_replace('../../', '', pSQL((string)$fil)).'\', \'0\', \'0\')';
if (!Db::getInstance()->Execute($query));

$query2 = 'SELECT * FROM '._DB_PREFIX_.'smush WHERE `url` = \''.str_replace('../../', '', pSQL((string)$fil)).'\' AND `smushed` = 0';
$quer   = Db::getInstance()->ExecuteS($query2);
					if ($quer != null)
					{
						$imasize = filesize($fil2);



						$pathex = $fil;
						$ext  = pathinfo($pathex, PATHINFO_EXTENSION);

						if ($ext == 'jpg' || $ext == 'jpeg')
						{
						$img = new abeautifulsite\SimpleImage($fil);
						$img->save($i, 100);
						if($i)
						$imasize2 = filesize($i);
						else
						$imasize2 = 	filesize($fil);
						if ($imasize2 > $imasize)
						{
						unlink($i);
						$total = 0;

						}
						else{
							if($i)
						copy($i,$fil);
						$total = $imasize - $imasize2;
						$img->save($fil, 100);

						}


						}
						if ($ext == 'png')
						{
								$img = new abeautifulsite\SimpleImage($fil);
						$img->save($i, 10);
						if($i)
						$imasize2 = filesize($i);
						else
						$imasize2 = 	filesize($fil);
						if ($imasize2 > $imasize)
						{
						unlink($i);
						$total = 0;

						}
						else{
							if($i)
						copy($i,$fil);
						$total = $imasize - $imasize2;
						$img->save($fil, 100);

						}


						}
						if ($ext == 'gif')
						{

						}




						$a = 0;

						$upd = 'UPDATE '._DB_PREFIX_.'smush SET `smushed` = \'-1\' WHERE  '._DB_PREFIX_.'smush.url =  \''.str_replace('../../', '', pSQL((string)$fil)).'\'';
						Db::getInstance()->Execute($upd);


						$upd   = 'UPDATE '._DB_PREFIX_.'smush SET `smushed` = \'1\', `saved` = \''.pSQL((float)$total).'\' WHERE  '._DB_PREFIX_.'smush.url =  \''.str_replace('../../', '', pSQL((string)$fil)).'\'';
						Db::getInstance()->Execute($upd);



						$a++;


				$i++;

					}
					Configuration::updateValue('PRESTASPEED_TOTSMUSH', $i);
					if (time() - $start_time > $max_execution_time - 4) // stop 4 seconds before the timeout, just enough time to process the end of the page on a slow server
						return 'timeout';

				*/
	}

	/*gd*/
	public function smushallgd($type, $output)
	{
	/*	include('../modules/prestaspeed/SimpleImage.php');
		//if (!is_callable('curl_init'))
		//$output = $this->displayError($this->l('cURL not loaded'));

		$this->registerHook('watermark');
		//require_once _PS_MODULE_DIR_.$this->name.'/smushit.inc.php';
		$query = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'smush (`id_smush` int(2) NOT NULL, `url` varchar(255) NULL, `smushed` TINYINT(1) NOT NULL,`saved` varchar(255) NULL, PRIMARY KEY(`url`)) ENGINE=MyISAM default CHARSET=utf8';
		Db::getInstance()->Execute($query);
		$total = '';
		//$smushit = new SmushIt();
		$url    = 'http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__;
		$files2 = $this->rglob('{*.jpg,*.png,*.gif}', $type, GLOB_BRACE);
		$co     = count($files2);

//var_dump($files2);
		Configuration::updateValue('PRESTASPEED_TOTFIL', count($files2));
		$fileschunk = array_chunk($files2, 40, false);
// Make batches of 3 images
//$files = array_chunk($files, 3);
		$start_time         = 0;
		$max_execution_time = 3337200;
		/* $process =
					array(
						array('type' => 'categories', 'dir' => _PS_CAT_IMG_DIR_),
						array('type' => 'manufacturers', 'dir' => _PS_MANU_IMG_DIR_),
						array('type' => 'suppliers', 'dir' => _PS_SUPP_IMG_DIR_),
						array('type' => 'scenes', 'dir' => _PS_SCENE_IMG_DIR_),
						array('type' => 'products', 'dir' => _PS_PROD_IMG_DIR_),
						array('type' => 'stores', 'dir' => _PS_STORE_IMG_DIR_)
					);*/
/*
		$start_time = time();
		ini_set('max_execution_time', $max_execution_time); // ini_set may be disabled, we need the real value
		$max_execution_time = (int)ini_get('max_execution_time');
		@set_time_limit(0);
		$i = 0;
*/
		/**/
		//$cdir = scandir('img/');
		//print_r( $cdir);

		/**/
	/*	foreach ($files2 as $fil)
		{

			//var_dump('final');

			//	flush();
			$fil2 = $fil;

			//while (list($key, $value) = each($fil))
			//{
			$query = 'INSERT IGNORE INTO '._DB_PREFIX_.'smush (`id_smush`, `url`, `smushed`, `saved`) VALUES (\''.pSQL((int)$i).'\', \''.str_replace('../../', '', pSQL((string)$fil)).'\', \'0\', \'0\')';
			if (!Db::getInstance()->Execute($query))
				;
			//echo $fil.'<br/>';
			$fi     = $url.str_replace('../', '', $fil);
$query2 = 'SELECT * FROM '._DB_PREFIX_.'smush WHERE `url` = \''.str_replace('../../', '', pSQL((string)$fil)).'\' AND `smushed` = 0';
$quer   = Db::getInstance()->ExecuteS($query2);
			if ($quer != null)
			{
				$imasize = filesize($fil2);


				//$remote_result = $smushit->compress($url.str_replace('../', '', $fil));
				$pathex = $fil;
				$ext    = pathinfo($pathex, PATHINFO_EXTENSION);

				if ($ext == 'jpg' || $ext == 'jpeg')
				{

					$img = new abeautifulsite\SimpleImage($fil);
					$img->save($i.'-optimized.jpg', 90);
					var_dump($img->get_original_info());
					if ($i)
						$imasize2 = filesize($i.'-optimized.jpg');
					else
						$imasize2 = filesize($fil);
					if ($imasize2 > $imasize)
					{
						unlink($i.'-optimized.jpg');
						$total = 0;
						var_dump('mas grande');
					}
					else
					{
						if ($i)
							copy($i.'-optimized.jpg', $fil);
						$total = $imasize - $imasize2;
						unlink($i.'-optimized.jpg');
						var_dump('mas chico');
					}
					var_dump('original:'.$fil.'-size:'.$imasize.' - copy:'.$i.'a.jpg-size:'.$imasize2);

				}
				if ($ext == 'png')
				{
					$img = new abeautifulsite\SimpleImage($fil);
					$img->save($i, 10);
					if ($i)
						$imasize2 = filesize($i);
					else
						$imasize2 = filesize($fil);
					if ($imasize2 > $imasize)
					{
						unlink($i);
						$total = 0;
						//	var_dump('mas grande');
					}
					else
					{
						if ($i)
							copy($i, $fil);
						$total = $imasize - $imasize2;
						$img->save($fil, 100);
						//var_dump('mas chico');
					}
					//var_dump('original:'.$fil.'-size:'.$imasize.' - copy:'.$i.'a.jpg-size:'.$imasize2);

				}
				if ($ext == 'gif')
				{
					/*		$img = new abeautifulsite\SimpleImage($fil);
				$img->save($i);
				if($i)
				$imasize2 = filesize($i);
				else
				$imasize2 = 	filesize($fil);
				if ($imasize2 > $imasize)
				{
				unlink($i);
				$total = 0;
				var_dump('mas grande');
				}
				else{
					if($i)
				copy($i,$fil);
				$total = $imasize - $imasize2;
				$img->save($fil);
				//var_dump('mas chico');
				}*/
					//var_dump('original:'.$fil.'-size:'.$imasize.' - copy:'.$i.'a.jpg-size:'.$imasize2);

			/*	}*/




				//print_r($remote_result).'<br/>';
			/*	$a = 0;*/
				//if (!empty($remote_result->error) || !empty($remote_result->dest_size) == -1)
				//{
			/*	$upd = 'UPDATE '._DB_PREFIX_.'smush SET `smushed` = \'-1\' WHERE  '._DB_PREFIX_.'smush.url =  \''.str_replace('../../', '', pSQL((string)$fil)).'\'';
				Db::getInstance()->Execute($upd);*/
				//}
				//if (!empty($remote_result->dest))
				//	{
				//	if ($remote_result->dest != null && $remote_result->dest_size != -1)
				//	{
				//$url=$remote_result->dest;
				//$contents=file_get_contents($url);
				//$save_path=$_SERVER['DOCUMENT_ROOT'].'/tmp/'.basename($value);
				//file_put_contents($save_path,$contents);
				//	$file    = $remote_result->dest;
				//	$newfile = _PS_ROOT_DIR_.'/'.str_replace('../', '', $fil);
				//	if (copy($file, $newfile))
				//	{
				//echo "Copy success!";

			/*	$upd = 'UPDATE '._DB_PREFIX_.'smush SET `smushed` = \'1\', `saved` = \''.pSQL((float)$total).'\' WHERE  '._DB_PREFIX_.'smush.url =  \''.str_replace('../../', '', pSQL((string)$fil)).'\'';
				Db::getInstance()->Execute($upd);*/
				//
				//print_r($upd);

				//echo     '<br/>';
			/*	$a++;*/
				//}
				//echo 'Total files smushed'.$a.'<br/>';

			/*	$i++;*/
				//var_dump($i.'-'.$co);
/*
			}
			Configuration::updateValue('PRESTASPEED_TOTSMUSH', $i);
			if (time() - $start_time > $max_execution_time - 4) // stop 4 seconds before the timeout, just enough time to process the end of the page on a slow server
				return 'timeout';

		}*/
	}

	/**/
	public function smushall($type, $output, $batcht)
	{
		if (!is_callable('curl_init'))
			$output = $this->displayError($this->l('cURL not loaded'));

		$this->registerHook('watermark');
		require_once _PS_MODULE_DIR_.$this->name.'/smushit.inc.php';
		$query = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'smush (`id_smush` int(2) NOT NULL, `url` varchar(255) NULL, `smushed` TINYINT(1) NOT NULL,`saved` varchar(255) NULL, PRIMARY KEY(`url`)) ENGINE=MyISAM default CHARSET=utf8';
		Db::getInstance()->Execute($query);
		$total   = '';
		$smushit = new SmushIt();
		$url     = 'http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__;
		$files2  = $this->rglob('{*.jpg,*.png,*.gif}', $type, GLOB_BRACE);
//var_dump($files2);
		Configuration::updateValue('PRESTASPEED_TOTFIL', count($files2));
		@$fileschunk = array_chunk($files2, $batcht, false);
// Make batches of 3 images
//$files = array_chunk($files, 3);
		$start_time         = 0;
		$max_execution_time = 3337200;
		/* $process =
					array(
						array('type' => 'categories', 'dir' => _PS_CAT_IMG_DIR_),
						array('type' => 'manufacturers', 'dir' => _PS_MANU_IMG_DIR_),
						array('type' => 'suppliers', 'dir' => _PS_SUPP_IMG_DIR_),
						array('type' => 'scenes', 'dir' => _PS_SCENE_IMG_DIR_),
						array('type' => 'products', 'dir' => _PS_PROD_IMG_DIR_),
						array('type' => 'stores', 'dir' => _PS_STORE_IMG_DIR_)
					);*/
// Take a batch of three files
		$start_time = time();
		ini_set('max_execution_time', $max_execution_time); // ini_set may be disabled, we need the real value
		$max_execution_time = (int)ini_get('max_execution_time');
		@set_time_limit(0);
		$i = 0;

		/**/
		//$cdir = scandir('img/');
		//print_r( $cdir);

		/**/
		foreach ($files2 as $fil)
		{
			//flush();
			$i++;
			//while (list($key, $value) = each($fil))
			//{
			$query = 'INSERT IGNORE INTO '._DB_PREFIX_.'smush (`id_smush`, `url`, `smushed`, `saved`) VALUES (\''.pSQL((int)$i).'\', \''.str_replace('../../', '', pSQL((string)$fil)).'\', \'0\', \'0\')';
			if (!Db::getInstance()->Execute($query));
//echo $fil.'<br/>';
			$query2 = 'SELECT * FROM '._DB_PREFIX_.'smush WHERE `url` = \''.str_replace('../../', '', pSQL((string)$fil)).'\' AND `smushed` = 0';
			$quer   = Db::getInstance()->ExecuteS($query2);
			if ($quer != null)
			{
				$remote_result = $smushit->compress($fil);
				//print_r($remote_result).'<br/>';
				$a = 0;
				if (!empty($remote_result->error) || !empty($remote_result->dest_size) == -1)
				{
					$upd = 'UPDATE '._DB_PREFIX_.'smush SET `smushed` = \'-1\' WHERE  '._DB_PREFIX_.'smush.url =  \''.str_replace('../../', '', pSQL((string)$fil)).'\'';
					Db::getInstance()->Execute($upd);
				}
				if (!empty($remote_result->dest))
				{
					if ($remote_result->dest != null && $remote_result->dest_size != -1)
					{
						//$url=$remote_result->dest;
						//$contents=file_get_contents($url);
						//$save_path=$_SERVER['DOCUMENT_ROOT'].'/tmp/'.basename($value);
						//file_put_contents($save_path,$contents);
						$file    = $remote_result->dest;
						$newfile = _PS_ROOT_DIR_.'/'.str_replace('../', '', $fil);
						if (copy($file, $newfile))
						{
							//echo "Copy success!";
							$total = $remote_result->src_size - $remote_result->dest_size;
							$upd   = 'UPDATE '._DB_PREFIX_.'smush SET `smushed` = \'1\', `saved` = \''.pSQL((float)$total).'\' WHERE  '._DB_PREFIX_.'smush.url =  \''.str_replace('../../', '', pSQL((string)$fil)).'\'';
							Db::getInstance()->Execute($upd);
							//
							//print_r($upd);
						}
						//echo     '<br/>';
						$a++;
						//}
						//echo 'Total files smushed'.$a.'<br/>';
					}
				}
			}
			Configuration::updateValue('PRESTASPEED_TOTSMUSH', $i);
			if (time() - $start_time > $max_execution_time - 4) // stop 4 seconds before the timeout, just enough time to process the end of the page on a slow server
				return 'timeout';
		}
	}

	public function postProcess()
	{
		$errors = '';
		$output = '';
		if (Tools::isSubmit('submitDelete2'))
		{
			if ($smush = Tools::getValue('smush'))
				Configuration::updateValue('PRESTASPEED_SMUSH', $smush);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_SMUSH');

			if ($cleani = Tools::getValue('cleani'))
				Configuration::updateValue('PRESTASPEED_CLEANI', $cleani);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_CLEANI');

			if ($cusi = Tools::getValue('cusi'))
				Configuration::updateValue('PRESTASPEED_CUSI', $cusi);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_CUSI');

			if ($smush2 = Tools::getValue('smush2'))
				Configuration::updateValue('PRESTASPEED_SMUSH2', $smush2);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_SMUSH2');

			if ($batcht = Tools::getValue('batcht'))
				Configuration::updateValue('PRESTASPEED_BATCHT', $batcht);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_BATCHT');

			if ($type = Tools::getValue('type'))
				Configuration::updateValue('PRESTASPEED_TYPE', $type);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_TYPE');
			if ($stats = Tools::getValue('stats'))
				Configuration::updateValue('PRESTASPEED_STATS', $stats);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_STATS');

			if ($stats == 1)
			{
				Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'smush` ;');
				Configuration::updateValue('PRESTASPEED_TOTCOMP', 0);
				Configuration::updateValue('PRESTASPEED_TOTCOMPF', 0);
				$output .= $this->displayConfirmation($this->l('Image optimization stats deleted').'<br/>');

			}
			if ($smush == 1)
				$output .= $this->displayConfirmation($this->l('New product image optimization enabled').'<br/>');
			else
				$output .= $this->displayConfirmation($this->l('New product image optimization disabled').'<br/>');
			if ($smush2 == 1 && $cusi == null)
				/*	if (extension_loaded('gd'))
						$this->smushallgd($type, $output);
					elseif (extension_loaded('imagick'))
						$this->smushallimagick($type, $output);
					else*/
				$this->smushall($type, $output, $batcht);

			if ($smush2 == 1 && $cusi == null)
			{

				//if (!extension_loaded('imagick'))
				//		$output .= $this->displayError($this->l('imagick library not loaded'));
				//	else
				//		$output .= $this->displayConfirmation($this->l('imagick library loaded'));
				//	if (!extension_loaded('gd'))
				//		$output .= $this->displayError($this->l('GD library not loaded'));
				//	else
				//	$output .= $this->displayConfirmation($this->l('GD library loaded'));


				$output .= $this->displayConfirmation($this->l('Total proceced images:').Configuration::get('PRESTASPEED_TOTSMUSH').'<br/>');
				$output .= $this->displayConfirmation($this->l('Total images:').Configuration::get('PRESTASPEED_TOTFIL').'<br/>');
				$bytes = 'SELECT sum(saved) as total FROM '._DB_PREFIX_.'smush';
				$bytt  = Db::getInstance()->ExecuteS($bytes);
				Configuration::updateValue('PRESTASPEED_TOTCOMP', ($bytt[0]['total'] * 1) / 1024);
				$output .= $this->displayConfirmation($this->l('Total KB saved:').round(Configuration::get('PRESTASPEED_TOTCOMP'), 2).'<br/>');
			}
			if ($cusi != null)
			{
				$this->smushcustom($cusi);
				$output .= $this->displayConfirmation($this->l('Total proceced images:').Configuration::get('PRESTASPEED_TOTSMUSH').'<br/>');
				$bytes = 'SELECT sum(saved) as total FROM '._DB_PREFIX_.'smush';
				$bytt  = Db::getInstance()->ExecuteS($bytes);
				Configuration::updateValue('PRESTASPEED_TOTCOMP', ($bytt[0]['total'] * 1) / 1024);
				$output .= $this->displayConfirmation($this->l('Total KB saved:').round(Configuration::get('PRESTASPEED_TOTCOMP'), 2).'<br/>');
			}
			if ($cleani == 1)
			{
				$this->clearTmpDir();
				$output .= $this->displayConfirmation($this->l('TMP images deleted:').Configuration::get('PRESTASPEED_TMPI').'<br/>');

			}
			Configuration::updateValue('PRESTASPEED_TMPI', 0);
			return $output;
		}
		if (Tools::isSubmit('submitDelete'))
		{
			($GLOBALS['___mysqli_ston'] = mysqli_connect(_DB_SERVER_, _DB_USER_, _DB_PASSWD_));
			mysqli_query($GLOBALS['___mysqli_ston'], 'SET NAMES UTF8');//this is needed for UTF 8 characters - multilanguage
			((bool)mysqli_query($GLOBALS['___mysqli_ston'], 'USE '.constant('_DB_NAME_')));
			/*delete all*/
			$output .= '<div class="spinner">
  <div class="rect1"></div>
  <div class="rect2"></div>
  <div class="rect3"></div>
  <div class="rect4"></div>
  <div class="rect5"></div>
</div>';
			$total   = '';
			$total2  = '';
			$total3  = '';
			$total4  = '';
			$total5  = '';
			$total6  = '';
			$total7  = '';
			$total8  = '';
			$total9  = '';
			$total10 = '';
			$total11 = '';
			$total4c = '';
			$tmp     = '';
			if ($conn = Tools::getValue('conn'))
				Configuration::updateValue('PRESTASPEED_CO', $conn);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_CO');

			if ($gues = Tools::getValue('gues'))
				Configuration::updateValue('PRESTASPEED_GU', $gues);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_GU');

			if ($func = Tools::getValue('func'))
				Configuration::updateValue('PRESTASPEED_FUNC', $func);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_FUNC');

			if ($pag = Tools::getValue('pag'))
				Configuration::updateValue('PRESTASPEED_PA', $pag);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_PA');

			if ($dis = Tools::getValue('dis'))
				Configuration::updateValue('PRESTASPEED_DI', $dis);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_DI');

			if ($cav = Tools::getValue('cav'))
				Configuration::updateValue('PRESTASPEED_CAV', $cav);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_CAV');

			if ($car = Tools::getValue('car'))
				Configuration::updateValue('PRESTASPEED_CA', $car);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_CA');
			if ($speed = Tools::getValue('speed'))
				Configuration::updateValue('PRESTASPEED_SPEED', $speed);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_SPEED');

			if ($dat = Tools::getValue('dat'))
				Configuration::updateValue('PRESTASPEED_DA', $dat);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_DA');

			if ($cac = Tools::getValue('cac'))
				Configuration::updateValue('PRESTASPEED_CAC', $cac);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_CAC');

			if ($sp_from2 = Tools::getValue('sp_from2'))
				Configuration::updateValue('PRESTASPEED_FR2', $sp_from2);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_FR2');

			if ($sp_to2 = Tools::getValue('sp_to2'))
				Configuration::updateValue('PRESTASPEED_TO2', $sp_to2);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_TO2');

			if ($ord = Tools::getValue('ord'))
				Configuration::updateValue('PRESTASPEED_ORD', $ord);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_ORD');

			if ($tem = Tools::getValue('tem'))
				Configuration::updateValue('PRESTASPEED_TE', $tem);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_TE');

			if ($gzip = Tools::getValue('gzip'))
				Configuration::updateValue('PRESTASPEED_GZ', $gzip);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_GZ');

			if ($sp_from = Tools::getValue('sp_from'))
				Configuration::updateValue('PRESTASPEED_FR', $sp_from);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_FR');

			if ($java = Tools::getValue('java'))
				Configuration::updateValue('PRESTASPEED_JA', $java);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_JA');

			if ($sp_to = Tools::getValue('sp_to'))
				Configuration::updateValue('PRESTASPEED_TO', $sp_to);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_TO');

			if ($hta = Tools::getValue('hta'))
				Configuration::updateValue('PRESTASPEED_HTA', $hta);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_HTA');

			if ($bo = Tools::getValue('bo'))
				Configuration::updateValue('PRESTASPEED_BO', $bo);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('PRESTASPEED_BO');
			$cav = Configuration::get('PRESTASPEED_CAV');
			/*if ($compress = Tools::getValue('compress'))
			Configuration::updateValue('PRESTASPEED_COMPRESS', $compress);
		elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
			Configuration::deleteFromContext('PRESTASPEED_COMPRESS');*/
			if ($ord == 1 && $sp_from2 != null && $sp_to2 != null)
			{

				$ord = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('SELECT * FROM '._DB_PREFIX_.'orders WHERE date_add > \''.pSQL($sp_from2).'\' AND date_add < \''.pSQL($sp_to2).'\' AND valid = 1');

				$h = 0;
				foreach ($ord as $order)
				{
					$this->deleteorderbyid($ord[$h]['id_order']);
					//var_dump($ord[$h]['id_order']);
					$h++;
				}
				$output .= $this->displayConfirmation($this->l('Total orders deleted').': '.$h.'<br/>');

			}


//        if ($compress == 1)
//{
//$mask = '../themes/'._THEME_NAME_.'/cache/*.css';
//$cssFiles = glob($mask);
//$buffer = "";
//foreach ($cssFiles as $cssFile) {
//$buffer = file_get_contents($cssFile);
//$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
//$buffer = str_replace(': ', ':', $buffer);
//    $buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
//$my_file = $cssFile;
//$fh = fopen($my_file, 'w');
//fwrite($fh, $buffer);
//fclose($fh);
//}
///*js*/
//$mask2 = '../themes/'._THEME_NAME_.'/cache/*.js';
//$jsFiles = glob($mask2);
//$buffer = "";
//foreach ($jsFiles as $jsFile) {
//$buffer = file_get_contents($jsFile);
//$buffer = str_replace(': ', ':', $buffer);
//$buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
//$my_file = $jsFile;
//$fh = fopen($my_file, 'w');
//fwrite($fh, $buffer);
//fclose($fh);
//}
//}
			if ($bo == 1 && _PS_VERSION_ > '1.5.0.0')
			{
				$mask = '../config/xml/*.xml';
				$fil  = glob($mask);
				foreach ($fil as $fi)
					chmod($fi, 0777);
				$mask2 = '../config/xml/themes/*.xml';
				$fil2  = glob($mask2);
				foreach ($fil2 as $fi2)
					chmod($fi2, 0777);
				//exec('find ../config/xml -type f -exec chmod 0777 {} +');
				//exec('find ../config/xml/.htaccess -type f -exec chmod 0644 {} +');
				//exec('find ../config/xml/themes -type f -exec chmod 0777 {} +');
				//exec('find ../config/xml/themes/.htaccess -type f -exec chmod 0644 {} +');
				if (file_exists('../.htaccess'))
					copy('../modules/prestaspeed/htaccess.txt', '.htaccess');
			}
			if ($bo == 0 && _PS_VERSION_ > '1.5.0.0')
			{
				$mask = '../config/xml/*.xml';
				$fil  = glob($mask);
				foreach ($fil as $fi)
					chmod($fi, 0644);
				$mask2 = '../config/xml/themes/*.xml';
				$fil2  = glob($mask2);
				foreach ($fil2 as $fi2)
					chmod($fi2, 0644);
				//exec('find ../config/xml -type f -exec chmod 0644 {} +');
				//exec('find ../config/xml/themes -type f -exec chmod 0644 {} +');
				if (file_exists('.htaccess'))
					unlink('.htaccess');
			}
			//db 
			if ($func == 1)
			{

				$this->checkAndFix();
				$output .= $this->displayConfirmation($this->l('DB integrity fixed'));

			}
			//rewrite htaccess
			if ($hta == 1)
			{
				$this->removeHtaccessSection();
				if (file_exists('../.htaccess') && is_writeable('../.htaccess'))
				{
					if (file_exists('../.htaccessps'))
					{
						//copy('../.htaccess', '../.htaccessps');
						//copy('../.htaccess', '../.htaccessps');
					}
					else
						copy('../.htaccess', '../.htaccessps');
					$file = Tools::file_get_contents('../.htaccess');
					if (!preg_match('/Prestaspeed addon start/', $file))
					{
						$my_file     = '../.htaccess';
						$fh          = fopen($my_file, 'a');
						$string_data = '

#Prestaspeed addon start
<IfModule mod_mime.c>
    AddType text/css .css
    AddType application/x-javascript .js
    AddType text/x-component .htc
    AddType text/html .html .htm
    AddType text/richtext .rtf .rtx
    AddType image/svg+xml .svg .svgz
    AddType text/plain .txt
    AddType text/xsd .xsd
    AddType text/xsl .xsl
    AddType text/xml .xml
    AddType video/asf .asf .asx .wax .wmv .wmx
    AddType video/avi .avi
    AddType image/bmp .bmp
    AddType application/java .class
    AddType video/divx .divx
    AddType application/msword .doc .docx
    AddType application/vnd.ms-fontobject .eot
    AddType application/x-msdownload .exe
    AddType image/gif .gif
    AddType application/x-gzip .gz .gzip
    AddType image/x-icon .ico
    AddType image/jpeg .jpg .jpeg .jpe
    AddType application/vnd.ms-access .mdb
    AddType audio/midi .mid .midi
    AddType video/quicktime .mov .qt
    AddType audio/mpeg .mp3 .m4a
    AddType video/mp4 .mp4 .m4v
    AddType video/mpeg .mpeg .mpg .mpe
    AddType application/vnd.ms-project .mpp
    AddType application/x-font-otf .otf
    AddType application/vnd.oasis.opendocument.database .odb
    AddType application/vnd.oasis.opendocument.chart .odc
    AddType application/vnd.oasis.opendocument.formula .odf
    AddType application/vnd.oasis.opendocument.graphics .odg
    AddType application/vnd.oasis.opendocument.presentation .odp
    AddType application/vnd.oasis.opendocument.spreadsheet .ods
    AddType application/vnd.oasis.opendocument.text .odt
    AddType audio/ogg .ogg
    AddType application/pdf .pdf
    AddType image/png .png
    AddType application/vnd.ms-powerpoint .pot .pps .ppt .pptx
    AddType audio/x-realaudio .ra .ram
    AddType application/x-shockwave-flash .swf
    AddType application/x-tar .tar
    AddType image/tiff .tif .tiff
    AddType application/x-font-ttf .ttf .ttc
    AddType audio/wav .wav
    AddType audio/wma .wma
    AddType application/vnd.ms-write .wri
    AddType application/vnd.ms-excel .xla .xls .xlsx .xlt .xlw
    AddType application/zip .zip
</IfModule>

<IfModule mod_expires.c>
# Default directive

    ExpiresActive On
    ExpiresByType text/css A31536000
	ExpiresByType text/html "access plus 0 minutes"
    ExpiresByType application/x-javascript A31536000
    ExpiresByType text/x-component A31536000
    ExpiresByType text/richtext A3600
    ExpiresByType image/svg+xml A3600
    ExpiresByType text/plain A3600
    ExpiresByType text/xsd A3600
    ExpiresByType text/xsl A3600
    ExpiresByType video/asf A31536000
    ExpiresByType video/avi A31536000
    ExpiresByType image/bmp A31536000
    ExpiresByType application/java A31536000
    ExpiresByType video/divx A31536000
    ExpiresByType application/msword A31536000
    ExpiresByType application/vnd.ms-fontobject A31536000
    ExpiresByType application/x-msdownload A31536000
    ExpiresByType image/gif A31536000
	ExpiresByType image/jpg "access plus 1 month"
	ExpiresByType image/jpeg "access plus 1 month"
    ExpiresByType application/x-gzip A31536000
    ExpiresByType image/x-icon A31536000
    ExpiresByType application/vnd.ms-access A31536000
    ExpiresByType audio/midi A31536000
    ExpiresByType video/quicktime A31536000
    ExpiresByType audio/mpeg A31536000
    ExpiresByType video/mp4 A31536000
    ExpiresByType video/mpeg A31536000
    ExpiresByType application/vnd.ms-project A31536000
    ExpiresByType application/x-font-otf A31536000
    ExpiresByType application/vnd.oasis.opendocument.database A31536000
    ExpiresByType application/vnd.oasis.opendocument.chart A31536000
    ExpiresByType application/vnd.oasis.opendocument.formula A31536000
    ExpiresByType application/vnd.oasis.opendocument.graphics A31536000
    ExpiresByType application/vnd.oasis.opendocument.presentation A31536000
    ExpiresByType application/vnd.oasis.opendocument.spreadsheet A31536000
    ExpiresByType application/vnd.oasis.opendocument.text A31536000
    ExpiresByType audio/ogg A31536000
    ExpiresByType application/pdf A31536000
    ExpiresByType image/png A31536000
    ExpiresByType application/vnd.ms-powerpoint A31536000
    ExpiresByType audio/x-realaudio A31536000
    ExpiresByType image/svg+xml A31536000
    ExpiresByType application/x-shockwave-flash A31536000
    ExpiresByType application/x-tar A31536000
    ExpiresByType image/tiff A31536000
    ExpiresByType application/x-font-ttf A31536000
    ExpiresByType audio/wav A31536000
    ExpiresByType audio/wma A31536000
    ExpiresByType application/vnd.ms-write A31536000
    ExpiresByType application/vnd.ms-excel A31536000
    ExpiresByType application/zip A31536000
</IfModule>
<FilesMatch "\.(ico|jpg|jpeg|png|gif|js|css|swf)$">

Header unset ETag
FileETag None
</FilesMatch>
<IfModule mod_deflate.c>
    #The following line is enough for .js and .css
    AddOutputFilter DEFLATE js css

    #The following line also enables compression by file content type, for the following list of Content-Type:s
    AddOutputFilterByType DEFLATE text/html text/plain text/xml application/xml

    #The following lines are to avoid bugs with some browsers
    BrowserMatch ^Mozilla/4 gzip-only-text/html
    BrowserMatch ^Mozilla/4\.0[678] no-gzip
    BrowserMatch \bMSIE !no-gzip !gzip-only-text/html
    <IfModule mod_setenvif.c>
        BrowserMatch ^Mozilla/4 gzip-only-text/html
        BrowserMatch ^Mozilla/4\.0[678] no-gzip
        BrowserMatch \bMSIE !no-gzip !gzip-only-text/html
        BrowserMatch \bMSI[E] !no-gzip !gzip-only-text/html
    </IfModule>
    <IfModule mod_headers.c>
        Header append Vary User-Agent env=!dont-vary
    </IfModule>
    <IfModule mod_filter.c>
        AddOutputFilterByType DEFLATE text/css application/x-javascript text/x-component text/html text/richtext image/svg+xml text/plain text/xsd text/xsl text/xml image/x-icon
    </IfModule>
</IfModule>
<IfModule mod_headers.c>
  <FilesMatch "\.(js|css|xml|gz)$">
    Header append Vary: Accept-Encoding
  </FilesMatch>
</IfModule>
<FilesMatch "\.(css|js|htc|CSS|JS|HTC)$">
    <IfModule mod_headers.c>
        Header set Pragma "public"
        Header append Cache-Control "public, must-revalidate, proxy-revalidate"
    </IfModule>
    FileETag MTime Size
    <IfModule mod_headers.c>
         Header set X-Powered-By "prestaspeed"
    </IfModule>
</FilesMatch>
<FilesMatch "\.(html|htm|rtf|rtx|svg|svgz|txt|xsd|xsl|xml|HTML|HTM|RTF|RTX|SVG|SVGZ|TXT|XSD|XSL|XML)$">
    <IfModule mod_headers.c>
        Header set Pragma "public"
        Header append Cache-Control "public, must-revalidate, proxy-revalidate"
    </IfModule>
    FileETag MTime Size
    <IfModule mod_headers.c>
         Header set X-Powered-By "prestaspeed"
    </IfModule>
</FilesMatch>
<FilesMatch "\.(asf|asx|wax|wmv|wmx|avi|bmp|class|divx|doc|docx|eot|exe|gif|gz|gzip|ico|jpg|jpeg|jpe|mdb|mid|midi|mov|qt|mp3|m4a|mp4|m4v|mpeg|mpg|mpe|mpp|otf|odb|odc|odf|odg|odp|ods|odt|ogg|pdf|png|pot|pps|ppt|pptx|ra|ram|svg|svgz|swf|tar|tif|tiff|ttf|ttc|wav|wma|wri|xla|xls|xlsx|xlt|xlw|zip|ASF|ASX|WAX|WMV|WMX|AVI|BMP|CLASS|DIVX|DOC|DOCX|EOT|EXE|GIF|GZ|GZIP|ICO|JPG|JPEG|JPE|MDB|MID|MIDI|MOV|QT|MP3|M4A|MP4|M4V|MPEG|MPG|MPE|MPP|OTF|ODB|ODC|ODF|ODG|ODP|ODS|ODT|OGG|PDF|PNG|POT|PPS|PPT|PPTX|RA|RAM|SVG|SVGZ|SWF|TAR|TIF|TIFF|TTF|TTC|WAV|WMA|WRI|XLA|XLS|XLSX|XLT|XLW|ZIP)$">
    <IfModule mod_headers.c>
        Header set Pragma "public"
        Header append Cache-Control "public, must-revalidate, proxy-revalidate"
    </IfModule>
    FileETag MTime Size
    <IfModule mod_headers.c>
         Header set X-Powered-By "prestaspeed"
    </IfModule>
</FilesMatch>
<IfModule mod_setenvif.c>
 <IfModule mod_headers.c>
    # mod_headers, y u no match by Content-Type?!
    <FilesMatch ".(gif|png|jpe?g|svg|svgz|ico|webp)$">
      SetEnvIf Origin ":" IS_CORS
      Header set Access-Control-Allow-Origin "*" env=IS_CORS
    </FilesMatch>
 </IfModule>
</IfModule>

<IfModule mod_headers.c>
  <FilesMatch ".(ttf|ttc|otf|eot|woff|font.css)$">
    Header set Access-Control-Allow-Origin "*"
  </FilesMatch>
</IfModule>
<IfModule mod_headers.c>
Header set Connection keep-alive
</IfModule>


# compress text, html, javascript, css, xml:
AddOutputFilterByType DEFLATE text/plain
AddOutputFilterByType DEFLATE text/html
AddOutputFilterByType DEFLATE text/xml
AddOutputFilterByType DEFLATE text/css
AddOutputFilterByType DEFLATE application/xml
AddOutputFilterByType DEFLATE application/xhtml+xml
AddOutputFilterByType DEFLATE application/rss+xml
AddOutputFilterByType DEFLATE application/javascript
AddOutputFilterByType DEFLATE application/x-javascript
# Common Fonts
AddOutputFilterByType DEFLATE image/svg+xml
AddOutputFilterByType DEFLATE application/x-font-ttf
AddOutputFilterByType DEFLATE application/font-woff
AddOutputFilterByType DEFLATE application/vnd.ms-fontobject
AddOutputFilterByType DEFLATE application/x-font-otf
#Prestaspeed addon end';
						fwrite($fh, $string_data);
						fclose($fh);
					}
					$output .= $this->displayConfirmation($this->l('file .htaccess optimized').'<br/>');

				}
				else
					$output .= $this->displayError($this->l('file .htaccess no exists (or check permissons)').'<br/>');

			}
			if ($hta == 0)
			{
				$this->removeHtaccessSection();
				$output .= $this->displayConfirmation($this->l('file .htaccess optimization disabled').'<br/>');
			}
			/*smarty*/
			if ($tem == 1)
			{
				Configuration::updateValue('PS_SMARTY_CACHE', 1);
				if (_PS_VERSION_ > '1.5.0.0')
					Configuration::updateValue('PS_SMARTY_FORCE_COMPILE', 0);
				else
					Configuration::updateValue('PS_SMARTY_FORCE_COMPILE', 0);
				Configuration::updateValue('PS_CSS_THEME_CACHE', 1);
				Configuration::updateValue('PS_JS_THEME_CACHE', 1);
				Configuration::updateValue('PS_HTML_THEME_COMPRESSION', 1);

$folder = '../themes/'._THEME_NAME_.'/cache/';

if (false !== ($path = file_exists ($folder)))

$output .= $this->displayConfirmation($this->l('Cache theme folder exists').'');

else
{
mkdir($folder);
$output .= $this->displayConfirmation($this->l('Cache theme folder created').'');

}
// Continue do stuff
			}
			if ($gzip == 1)
			{
				if (_PS_VERSION_ > '1.5.0.0')
					Configuration::updateValue('PS_HTACCESS_CACHE_CONTROL', 1);
			}
			else
			{
				if (_PS_VERSION_ > '1.5.0.0')
					Configuration::updateValue('PS_HTACCESS_CACHE_CONTROL', 0);
			}
			/*java*/
			if ($gzip == 1)
			{
				if (_PS_VERSION_ > '1.6.0.0')
					Configuration::updateValue('PS_JS_DEFER', 1);
				if (_PS_VERSION_ > '1.4.0.0')
					Configuration::updateValue('PS_JS_HTML_THEME_COMPRESSION', 1);
			}
			else
			{
				if (_PS_VERSION_ > '1.6.0.0')
					Configuration::updateValue('PS_JS_DEFER', 0);
				if (_PS_VERSION_ > '1.4.0.0')
					Configuration::updateValue('PS_JS_HTML_THEME_COMPRESSION', 0);
			}
			/**/
			if ($tem == 0)
			{
				Configuration::updateValue('PS_SMARTY_CACHE', 0);
				if (_PS_VERSION_ > '1.5.0.0')
					Configuration::updateValue('PS_SMARTY_FORCE_COMPILE', 1);
				else
					Configuration::updateValue('PS_SMARTY_FORCE_COMPILE', 0);
				Configuration::updateValue('PS_CSS_THEME_CACHE', 0);
				Configuration::updateValue('PS_JS_THEME_CACHE', 0);
				Configuration::updateValue('PS_HTML_THEME_COMPRESSION', 0);

			}
			if ($cac == 1)
			{
				if (_PS_VERSION_ < '1.4.0.0')
				{
					$mask = '../tools/smarty/compile/*tpl.php';
					array_map('unlink', glob($mask));
				}
				if (_PS_VERSION_ > '1.4.0.0' && _PS_VERSION_ < '1.5.0.0')
					Tools::clearCache($this->context->smarty);
				if (_PS_VERSION_ > '1.5.0.0')
				{
					if (_PS_VERSION_ > '1.5.5.0')
						Tools::clearSmartyCache();
					Tools::clearCache($this->context->smarty);
					if (_PS_VERSION_ > '1.6.0.0')
						Media::clearCache();
				}

			}
			if ($conn == 1)
			{
				$sorgudc = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'connections`
         '.((_PS_VERSION_ > '1.5.0.0') ? ' WHERE `id_shop` = '.(int)$this->context->shop->id : '').'
         ');
				if ($sorgudc === false)
					$tmp = '';
				else
				{
					$veridc = mysqli_fetch_assoc($sorgudc);
					$total  = mysqli_num_rows($sorgudc);
				}
				$sorgudc2 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'connections_page`');
				if ($sorgudc2 === false)
					$tmp = '';
				else
				{
					$veridc2 = mysqli_fetch_assoc($sorgudc2);
					$total2  = mysqli_num_rows($sorgudc2);
				}

				$sorgudc3 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'connections_source`');
				if ($sorgudc3 === false)
					$tmp = '';
				else
				{
					$veridc3 = mysqli_fetch_assoc($sorgudc3);
					$total3  = mysqli_num_rows($sorgudc3);
				}
			}
			if ($gues == 1)
			{
				$sorgudc7 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'guest`');
				if ($sorgudc7 === false)
					$tmp = '';
				else
				{
					$veridc7 = mysqli_fetch_assoc($sorgudc7);
					$total7  = mysqli_num_rows($sorgudc7);
				}
			}
			if ($pag == 1)
			{
				$sorgudc8 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'page_viewed`
         '.((_PS_VERSION_ > '1.5.0.0') ? ' WHERE `id_shop` = '.(int)$this->context->shop->id : '').'
        ');
				if ($sorgudc8 === false)
					$tmp = '';
				else
				{
					$veridc8 = mysqli_fetch_assoc($sorgudc8);
					$total8  = mysqli_num_rows($sorgudc8);
				}
			}
			/*cart*/
			if ($car == 1)
			{
				if ($sp_from == 0 && $sp_to == 0)
				{
					$sorgudc4 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart` WHERE `id_customer` = 0'.($cav == 1 ? ' OR `id_customer` != 0' : ''));
					if ($sorgudc4 === false)
						$tmp = '';
					else
					{
						$veridc4 = mysqli_fetch_assoc($sorgudc4);
						$total4  = mysqli_num_rows($sorgudc4);
					}
				}
				if ($sp_from != 0 && $sp_to != 0)
				{
					$sorgudc4 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart`
                        WHERE `id_customer` = 0'.($cav == 1 ? ' OR `id_customer` != 0' : '').'
                        '.((_PS_VERSION_ > '1.5.0.0') ? ' AND `id_shop` = '.(int)$this->context->shop->id : '').'
                        AND `date_upd` BETWEEN \''.pSQL($sp_from).'\' AND \''.pSQL($sp_to).'\';');

					if ($sorgudc4 === false)
						$tmp = '';
					else
					{
						$veridc4 = mysqli_fetch_assoc($sorgudc4);
						$total4  = mysqli_num_rows($sorgudc4);
					}
				}
				/**/
				if ($sp_from == 0 && $sp_to == 0)
				{
					$sorgudc4c = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart` WHERE `id_customer` = 0'.($cav == 1 ? ' OR `id_customer` != 0' : ''));
					if ($sorgudc4c === false)
						$tmp = '';
					else
					{
						$veridc4c = mysqli_fetch_assoc($sorgudc4c);
						$total4c  = mysqli_num_rows($sorgudc4c);
					}
				}
				if ($sp_from != 0 && $sp_to != 0)
				{
					$sorgudc4c = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart`
        WHERE `id_customer` = 0'.($cav == 1 ? ' OR `id_customer` != 0' : '').'
        '.((_PS_VERSION_ > '1.5.0.0') ? ' AND `id_shop` = '.(int)$this->context->shop->id : '').'
        AND `date_upd` BETWEEN \''.pSQL($sp_from).'\' AND \''.pSQL($sp_to).'\'');
					if ($sorgudc4c === false)
						$tmp = '';
					else
					{
						$veridc4c = mysqli_fetch_assoc($sorgudc4c);
						$total4c  = mysqli_num_rows($sorgudc4c);
					}
				}
				/**/
			}
			if ($dis == 1)
			{
				$sorgudc6 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'discount`');
				if ($sorgudc6 === false)
					$tmp = '';
				else
				{
					$veridc6 = mysqli_fetch_assoc($sorgudc6);
					$total6  = mysqli_num_rows($sorgudc6);
				}
				$current_date = date('Y-m-d H:i:s');
				$sorgudc5     = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'discount` WHERE `date_to` < \''.pSQL($current_date).'\'');
				if ($sorgudc5 === false)
					$tmp = '';
				else
				{
					$veridc5 = mysqli_fetch_assoc($sorgudc5);
					$total5  = mysqli_num_rows($sorgudc5);
				}
				$current_date = date('Y-m-d H:i:s');
				$sorgudc9     = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart_rule` WHERE `date_to` < \''.pSQL($current_date).'\' AND `date_to` != \'0000-00-00 00:00:00\'');
				if ($sorgudc9 === false)
					$tmp = '';
				else
				{
					$veridc9 = mysqli_fetch_assoc($sorgudc9);
					$total9  = mysqli_num_rows($sorgudc9);
				}
				/**/
				$current_date = date('Y-m-d H:i:s');
				$sorgudc10    = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'specific_price_rule` WHERE `to` < \''.pSQL($current_date).'\' AND `to` != \'0000-00-00 00:00:00\'');
				if ($sorgudc10 === false)
					$tmp = '';
				else
				{
					$veridc10 = mysqli_fetch_assoc($sorgudc10);
					$total10  = mysqli_num_rows($sorgudc10);
				}
				$current_date = date('Y-m-d H:i:s');
				$sorgudc11    = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'specific_price` WHERE `to` < \''.pSQL($current_date).'\' AND `to` != \'0000-00-00 00:00:00\'');
				if ($sorgudc11 === false)
					$tmp = '';
				else
				{
					$veridc11 = mysqli_fetch_assoc($sorgudc11);
					$total11  = mysqli_num_rows($sorgudc11);
				}
			}

			if ($conn == 1)
			{
				Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'connections` ;');
				Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'connections_page` ;');
				Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'connections_source` ;');
			}
			if ($gues == 1)
				Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'guest`;');
			if ($pag == 1)
				Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'page_viewed`;');

			if ($total11 != null)
			{
				do
				{
					$idc11 = $veridc11['id_specific_price'];
					if ($dis == 1)
						$this->delete11($idc11);
				} while ($veridc11 = mysqli_fetch_assoc($sorgudc11));
			}
			if ($total10 != null)
			{
				do
				{
					$idc10 = $veridc10['id_specific_price_rule'];
					if ($dis == 1)
						$this->delete10($idc10);
				} while ($veridc10 = mysqli_fetch_assoc($sorgudc10));
			}
			/**/
			if ($total9 != null)
			{
				do
				{
					$idc9 = $veridc9['id_cart_rule'];
					if ($dis == 1)
						$this->delete9($idc9);
				} while ($veridc9 = mysqli_fetch_assoc($sorgudc9));
			}
			/**/
			if ($total5 != null)
			{
				do
				{
					$idc5 = $veridc5['id_discount'];
					if ($dis == 1)
						$this->delete5($idc5);
				} while ($veridc5 = mysqli_fetch_assoc($sorgudc5));
			}
			if ($total4 != null)
			{
				do
				{
					if ($car == 1)
						$idc = $veridc4['id_cart'];
					$this->delete($idc);
				} while ($veridc4 = mysqli_fetch_assoc($sorgudc4));
			}
			if ($total4c != null)
			{
				do
				{
					$idcc = $veridc4c['id_cart'];
					if ($car == 1)
						$this->deletec($idcc);
				} while ($veridc4c = mysqli_fetch_assoc($sorgudc4c));
			}
			if ($conn == 1 && $total != null)
				$output .= $this->displayConfirmation($this->l('Rows before optimization on connection table (now is zero)').': '.$total.'<br/>');
			if ($conn == 1 && $total2 != null)
				$output .= $this->displayConfirmation($this->l('Rows before optimization on connection_page table (now is zero)').': '.$total2.'<br/>');
			if ($conn == 1 && $total3 != null)
				$output .= $this->displayConfirmation($this->l('Rows before optimization on connection_page_source table (now is zero)').': '.$total3.'<br/>');
			if ($gues == 1 && $total7 != null)
				$output .= $this->displayConfirmation($this->l('Rows before optimization on guest table (now is zero)').': '.$total7.'<br/>');
			if ($pag == 1 && $total8 != null)
				$output .= $this->displayConfirmation($this->l('Rows before optimization on page_viewed table (now is zero)').': '.$total8.'<br/>');
			if ($car == 1 && $total4 != null)
				$output .= $this->displayConfirmation($this->l('Rows before optimization on cart table (only abandoned carts for guests or users)').': '.$total4.'<br/>');
			if ($dis == 1 && $total6 != null || $dis == 1 && $total9 != null || $dis == 1 && $total10 != null || $dis == 1 && $total11 != null)
			{
				$tottot = $total6 + $total9 + $total11 + $total10;
				$output .= $this->displayConfirmation($this->l('Rows before optimization on vouchers/discounts/product discounts tables').': '.$tottot.'<br/>');
			}
			if ($cac == 1)
				$output .= $this->displayConfirmation($this->l('Cache cleared').'<br/>');
			if ($tem == 1)
				$output .= $this->displayConfirmation($this->l('Template optimized').'<br/>');
			if ($java == 1)
				$output .= $this->displayConfirmation($this->l('Javascript optimized').'<br/>');
			if ($gzip == 1)
				$output .= $this->displayConfirmation($this->l('Gzip enabled').'<br/>');
			$server  = _DB_SERVER_;
			$user    = _DB_USER_;
			$pwd     = _DB_PASSWD_;
			$db_name = _DB_NAME_;
			if ($dat == 1)
			{
				$alltables = mysqli_query($GLOBALS['___mysqli_ston'], 'SHOW TABLES');
				$v         = '';
				while ($table = mysqli_fetch_assoc($alltables))
				{
					foreach ($table as $db => $tablename)
					{
						mysqli_query($GLOBALS['___mysqli_ston'], 'OPTIMIZE TABLE `'._DB_PREFIX_.$tablename.'`');
						mysqli_query($GLOBALS['___mysqli_ston'], 'REPAIR TABLE `'._DB_PREFIX_.$tablename.'`');
						$v .= $tablename.'</br>';
					}
				}
				$output .= $this->displayConfirmation($v);
// Alert that operation was successful
				$output .= $this->displayConfirmation($this->l('Above tables successfully optimized.').'<br/>');
			}
			$output .= '
<style type="text/css">
.spinner {
display:none
}
</style>';
			// Reset the module properties
			//    $this->initialize();
			$this->_clearCache('prestaspeed-dash.tpl');
			$sql   = "SELECT table_schema '"._DB_NAME_."', SUM( data_length + index_length) / 1024 / 1024 'db_size_in_mb' FROM information_schema.TABLES WHERE table_schema='"._DB_NAME_."' GROUP BY table_schema ;";
			$query = mysqli_query($GLOBALS['___mysqli_ston'], $sql);
			$data  = mysqli_fetch_array($query);
			Configuration::updateValue('PRESTASPEED_DBS', $data['db_size_in_mb']);
			//if (!$errors)
			return $output;
			//Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&conf=6');
			//echo $this->displayError($errors);
		}
	}

	public function getConfigFieldsValues()
	{
		$fields_values = array(
			'sp_to2'   => Tools::getValue('sp_to2', Configuration::get('PRESTASPEED_TO2')),
			'sp_from2' => Tools::getValue('sp_from2', Configuration::get('PRESTASPEED_FR2')),
			'sp_to'    => Tools::getValue('sp_to', Configuration::get('PRESTASPEED_TO')),
			'java'     => Tools::getValue('java', Configuration::get('PRESTASPEED_JA')),
			'ord'      => Tools::getValue('ord', Configuration::get('PRESTASPEED_ORD')),
			'sp_from'  => Tools::getValue('sp_from', Configuration::get('PRESTASPEED_FR')),
			'gzip'     => Tools::getValue('gzip', Configuration::get('PRESTASPEED_GZ')),
			//'compress'    => Tools::getValue('compress', Configuration::get('PRESTASPEED_COMPRESS')),
			'tem'      => Tools::getValue('tem', Configuration::get('PRESTASPEED_TE')),
			'speed'    => Tools::getValue('speed', Configuration::get('PRESTASPEED_SPEED')),
			'func'     => Tools::getValue('func', Configuration::get('PRESTASPEED_FUNC')),
			'cac'      => Tools::getValue('cac', Configuration::get('PRESTASPEED_CAC')),
			'cav'      => Tools::getValue('cav', Configuration::get('PRESTASPEED_CAV')),
			'car'      => Tools::getValue('car', Configuration::get('PRESTASPEED_CA')),
			'dat'      => Tools::getValue('dat', Configuration::get('PRESTASPEED_DA')),
			'dis'      => Tools::getValue('dis', Configuration::get('PRESTASPEED_DI')),
			'pag'      => Tools::getValue('pag', Configuration::get('PRESTASPEED_PA')),
			'batcht'   => Tools::getValue('batcht', Configuration::get('PRESTASPEED_BATCHT')),
			'gues'     => Tools::getValue('gues', Configuration::get('PRESTASPEED_GU')),
			'conn'     => Tools::getValue('conn', Configuration::get('PRESTASPEED_CO')),
			'hta'      => Tools::getValue('hta', Configuration::get('PRESTASPEED_HTA')),
			'bo'       => Tools::getValue('bo', Configuration::get('PRESTASPEED_BO')),
			'smush'    => Tools::getValue('smush', Configuration::get('PRESTASPEED_SMUSH')),
			'cleani'   => Tools::getValue('cleani', Configuration::get('PRESTASPEED_CLEANI')),
			'smush2'   => Tools::getValue('smush2', Configuration::get('PRESTASPEED_SMUSH2')),
			'type'     => Tools::getValue('type', Configuration::get('PRESTASPEED_TYPE')),
			'stats'    => Tools::getValue('stats', Configuration::get('PRESTASPEED_STATS')),
			'cusi'     => Tools::getValue('cusi', Configuration::get('PRESTASPEED_CUSI')),
		);





		return $fields_values;
	}

	public function renderForm()
	{
		$idbs = '';

		$dbs = Configuration::get('PRESTASPEED_DBS');
		if (Configuration::get('PRESTASPEED_IDB') == '')
		{
			$sql    = 'SELECT table_schema \''._DB_NAME_.'\', SUM( data_length + index_length) / 1024 / 1024 \'db_size_in_mb\' FROM information_schema.TABLES WHERE table_schema=\''._DB_NAME_.'\' GROUP BY table_schema ;';
			$query2 = mysqli_query($GLOBALS['___mysqli_ston'], $sql);
			$data   = mysqli_fetch_array($query2);
			Configuration::updateValue('PRESTASPEED_IDB', $data['db_size_in_mb']);
		}
		else
			$idbs = Configuration::get('PRESTASPEED_IDB');
		if ($idbs == 0)
			$idbs = $dbs;

		$fields_form                      = array(
			'form' => array(
				'legend'      => array(
					'title' => $this->l('Configuration'),
					'icon'  => 'icon-cogs'
				),
				'description' => $this->l('Database Size: ').round($dbs, 2).'MB'.' - '.$this->l('Initial database size before module install: ').round($idbs, 2).'MB',
				'input'       => array(
					array(
						'type'    => 'switch',
						'label'   => $this->l('Clean connections:'),
						'name'    => 'conn',
						'is_bool' => true,
						'desc'    => $this->l('Delete all fields on connection tables. These tables store information about how long a user keeps on the site, where user come from, etc'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type'    => 'switch',
						'label'   => $this->l('Clean guests data:'),
						'name'    => 'gues',
						'is_bool' => true,
						'desc'    => $this->l('Delete all fields on guest table. This table have data from visitors, like OS, browser, etc'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type'    => 'switch',
						'label'   => $this->l('Clean viewed page:'),
						'name'    => 'pag',
						'is_bool' => true,
						'desc'    => $this->l('Delete all fields on page table. This table store the viewed pages from users.'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type'    => 'switch',
						'label'   => $this->l('Clean expired discounts:'),
						'name'    => 'dis',
						'is_bool' => true,
						'desc'    => $this->l('This option cleans all the data on expired vouchers, discounts, and product discounts tables'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type'    => 'switch',
						'label'   => $this->l('Clean abandoned carts:'),
						'name'    => 'car',
						'is_bool' => true,
						'desc'    => $this->l('Delete all abandoned carts (non registered users). The users with products in cart, need to redo the orders.'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type'    => 'switch',
						'label'   => $this->l('Delete abandoned carts from users too?'),
						'name'    => 'cav',
						'is_bool' => true,
						'desc'    => $this->l('Delete all abandoned carts for register users in selected date.'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type'  => 'date',
						'label' => $this->l('From'),
						'name'  => 'sp_from',
					),
					array(
						'type'  => 'date',
						'label' => $this->l('To'),
						'name'  => 'sp_to',

					),
					/*delete valid carts*/
					array(
						'type'    => 'switch',
						'label'   => $this->l('Delete valid orders:'),
						'name'    => 'ord',
						'is_bool' => true,
						'desc'    => $this->l('Delete validated carts. This action cant be undone and you lost the orders between the date selected. This option is great to delete really old orders.'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type'  => 'date',
						'label' => $this->l('From'),
						'name'  => 'sp_from2',
					),
					array(
						'type'  => 'date',
						'label' => $this->l('To'),
						'name'  => 'sp_to2',

					),
					/*end deletion*/
					array(
						'type'    => 'switch',
						'label'   => $this->l('Optimize database:'),
						'name'    => 'dat',
						'is_bool' => true,
						'desc'    => $this->l('Optimize all database tables and repair errors.'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type'    => 'switch',
						'label'   => $this->l('Check database integrity:'),
						'name'    => 'func',
						'is_bool' => true,
						'desc'    => $this->l('Improve queries and solve problems'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type'    => 'switch',
						'label'   => $this->l('Clear cache:'),
						'name'    => 'cac',
						'is_bool' => true,
						'desc'    => $this->l('Clear all smarty cache files'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type'    => 'switch',
						'label'   => $this->l('Optimize template:'),
						'name'    => 'tem',
						'is_bool' => true,
						'desc'    => $this->l('Enable the best settings to speed up the template loading'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type'    => 'switch',
						'label'   => $this->l('Optimize javascript:'),
						'name'    => 'java',
						'is_bool' => true,
						'desc'    => $this->l('Enable best javascript settings to load JS files more efficient'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type'    => 'switch',
						'label'   => $this->l('Enable Gzip:'),
						'name'    => 'gzip',
						'is_bool' => true,
						'desc'    => $this->l('Activate the gzip compression for faster execution. This option maybe not work in all servers (only PS > 1.5)'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					/*array(
						'type'    => 'switch',
						'label'   => $this->l('Compression of css'),
						'name'    => 'compress',
						'is_bool' => true,
						'desc'    => $this->l('Compress all css cache files of your theme'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),*/
					/*	array(
							'type'    => 'switch',
							'label'   => $this->l('Boost performance'),
							'name'    => 'speed',
							'is_bool' => true,
							'desc'    => $this->l('The module try to compress all to increase load speed'),
							'values'  => array(
								array(
									'id'    => 'active_on',
									'value' => 1,
									'label' => $this->l('Enabled')
								),
								array(
									'id'    => 'active_off',
									'value' => 0,
									'label' => $this->l('Disabled')
								)
							),
						),*/
					array(
						'type'    => 'switch',
						'label'   => $this->l('Htaccess optimization:'),
						'name'    => 'hta',
						'is_bool' => true,
						'desc'    => $this->l('Enable the apache htaccess optimization. This option only works if you server have a .htaccess file created. If you enable this option and the site dont work (some servers dont support all optimizations), simply delete your .htaccess file in your root directory and rename the .htaccessps to .htaccess to restore your original file'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type'    => 'switch',
						'label'   => $this->l('Back office optimization'),
						'name'    => 'bo',
						'is_bool' => true,
						'desc'    => $this->l('Try to optimize the back office performance with a htaccess file in the admin folder (delete thsi file if you get an error). The expiration of the cache is for 1 day, if you modify admin css/js files or images, you can turn off this option to see the changes'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
				),
				'submit'      => array(
					'title' => $this->l('Save'),
				)

			),
		);
		$helper                           = new HelperForm();
		$helper->show_toolbar             = true;
		$helper->table                    = $this->table;
		$lang                             = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language    = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form                = array();
		$helper->identifier               = $this->identifier;
		$helper->submit_action            = 'submitDelete';
		$helper->currentIndex             = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token                    = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars                 = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages'    => $this->context->controller->getLanguages(),
			'id_language'  => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}

	public function renderForm2()
	{
		$datatot = Configuration::get('PRESTASPEED_TOTCOMP') + Configuration::get('PRESTASPEED_TOTCOMPF');
		$options                          = array(

			array(
				'id_option' => '../img/p/',
				'name'      => $this->l('Product images'),
			),
			array(
				'id_option' => '../img/c/',
				'name'      => $this->l('Category images'),
			),
			array(
				'id_option' => '../img/m/',
				'name'      => $this->l('Manufacturer images'),
			),
			array(
				'id_option' => '../img/admin/',
				'name'      => $this->l('Admin images'),
			),
			array(
				'id_option' => '../img/cms/',
				'name'      => $this->l('CMS images'),
			),
			array(
				'id_option' => '../img/l/',
				'name'      => $this->l('Language images'),
			),
			array(
				'id_option' => '../img/su/',
				'name'      => $this->l('Supplier images'),
			),
			array(
				'id_option' => '../img/scenes/',
				'name'      => $this->l('Scenes images'),
			),
			array(
				'id_option' => '../img/st/',
				'name'      => $this->l('Stores images'),
			),
			array(
				'id_option' => '../themes/',
				// The value of the 'value' attribute of the <option> tag.
				'name'      => $this->l('Template images'),
				// The value of the text content of the  <option> tag.
			),
			array(
				'id_option' => '../modules/',
				// The value of the 'value' attribute of the <option> tag.
				'name'      => $this->l('Modules images'),
				// The value of the text content of the  <option> tag.
			),
			array(
				'id_option' => '../upload/',
				// The value of the 'value' attribute of the <option> tag.
				'name'      => $this->l('Upload folder images'),
				// The value of the text content of the  <option> tag.
			),
			array(
				'id_option' => '../img/logo',
				// The value of the 'value' attribute of the <option> tag.
				'name'      => $this->l('Logos'),
				// The value of the text content of the  <option> tag.
			),

		);
		$fields_form                      = array(
			'form' => array(
				'legend'      => array(
					'title' => $this->l('Optimize images'),
					'icon'  => 'icon-image'
				),
				'description' => $this->l('Total KB optimized in images: ').round($datatot, 2).'<br/>'.$this->l('Total images to process: ').Configuration::get('PRESTASPEED_TOTFIL').'<br/>'.$this->l('Total images processed: ').Configuration::get('PRESTASPEED_TOTSMUSH').'<br/>'.(Configuration::get('PRESTASPEED_TOTFIL') != Configuration::get('PRESTASPEED_TOTSMUSH') ? $this->l('WARNING: The module does not process all the images. Please optimize the images again to complete the process') : $this->l('All images were processed')),
				'input'       => array(
					array(
						'type'    => 'switch',
						'label'   => $this->l('Clean all stats:'),
						'name'    => 'stats',
						'is_bool' => true,
						'desc'    => $this->l('This option clean all the stats for the images in the database. If you compress the images again, you need to wait more time to process all again. Clean all if you only have regenerated your images.'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type'    => 'switch',
						'label'   => $this->l('Optimize new images:'),
						'name'    => 'smush',
						'is_bool' => true,
						'desc'    => $this->l('This option optimize new images for products when you upload it, but your uploads take more time. Watermark module must be disabled. This option dont work always, but you can regenerate the images of products'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type'    => 'switch',
						'label'   => $this->l('Optimize all images:'),
						'name'    => 'smush2',
						'is_bool' => true,
						'desc'    => $this->l('If you enable this option, the module compress all selected images with Smushit service. This can take time depending of your amount of images. All proceced diles must be the same as total files. If you dont get the same number, repeat the process until you get it.'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type'    => 'select',
						'label'   => $this->l('Type of images'),
						'name'    => 'type',
						'desc'    => $this->l('Select the type of images to optimize. The modules/themes option optimize all themes and modules images to get a better performance'),
						'options' => array(
							'query' => $options,
							'id'    => 'id_option',
							'name'  => 'name'
						)
					),
					array(
						'type'  => 'text',
						'label' => $this->l('Total images to optimize in a batch'),
						'name'  => 'batcht',
						'desc'  => $this->l('Prestaspeed try to process x amount of files at same time  intil process all files. You can try with less quantity is your server is slow.)'),

					),
					array(
						'type'  => 'text',
						'label' => $this->l('Optimize only one image'),
						'name'  => 'cusi',
						'desc'  => $this->l('Set the url of the image to optimize (like http://www.site.com/img/test.jpg, set the url to ../img/test.jpg)'),

					),
					array(
						'type'    => 'switch',
						'label'   => $this->l('Clean all temp images:'),
						'name'    => 'cleani',
						'is_bool' => true,
						'desc'    => $this->l('Delete all images on TMP img folder to save space in the server hard disk'),
						'values'  => array(
							array(
								'id'    => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id'    => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),

				),
				'submit'      => array(
					'title' => $this->l('Optimize'),
				)

			),
		);
		$helper                           = new HelperForm();
		$helper->show_toolbar             = true;
		$helper->table                    = $this->table;
		$lang                             = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language    = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form2               = array();
		$helper->identifier               = $this->identifier;
		$helper->submit_action            = 'submitDelete2';
		$helper->currentIndex             = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token                    = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars                 = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages'    => $this->context->controller->getLanguages(),
			'id_language'  => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}

	private function _displayInfo()
	{
		$this->context->smarty->assign(
			array(
				'prestaspeed_cron' => _PS_BASE_URL_._MODULE_DIR_.'prestaspeed/prestaspeed-cron.php?token='.Tools::substr(Tools::encrypt('prestaspeed/cron'), 0, 10).'&id_shop='.$this->context->shop->id,
			)
		);
		return $this->display(__FILE__, 'views/templates/hook/infos.tpl');
	}

	private function _displayInfo2()
	{
		return $this->display(__FILE__, 'views/templates/hook/infos2.tpl');
	}

	public function hookActionAdminControllerSetMedia()
	{
		if (get_class($this->context->controller) == 'AdminDashboardController')
			$this->context->controller->addCSS($this->_path.'views/css/style.css', 'all');
	}

	public function hookDashboardZoneOne($params)
	{
		($GLOBALS['___mysqli_ston'] = mysqli_connect(_DB_SERVER_, _DB_USER_, _DB_PASSWD_));
		mysqli_query($GLOBALS['___mysqli_ston'], 'SET NAMES UTF8');//this is needed for UTF 8 characters - multilanguage
		((bool)mysqli_query($GLOBALS['___mysqli_ston'], 'USE '.constant('_DB_NAME_')));
		$total   = '';
		$total2  = '';
		$total3  = '';
		$total4  = '';
		$total5  = '';
		$total6  = '';
		$total7  = '';
		$total8  = '';
		$total9  = '';
		$total10 = '';
		$total11 = '';
		$total4c = '';
		$tottot  = '';
		$tmp     = '';
		$sql     = 'SELECT table_schema \''._DB_NAME_.'\', SUM( data_length + index_length) / 1024 / 1024 \'db_size_in_mb\' FROM information_schema.TABLES WHERE table_schema=\''._DB_NAME_.'\' GROUP BY table_schema ;';
		$query   = mysqli_query($GLOBALS['___mysqli_ston'], $sql);
		$data    = mysqli_fetch_array($query);
		$data['db_size_in_mb'];

		$sorgudc = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'connections`
         '.((_PS_VERSION_ > '1.5.0.0') ? ' WHERE `id_shop` = '.(int)$this->context->shop->id : '').'
         ');
		if ($sorgudc === false)
			$tmp = '';
		else
		{
			$veridc = mysqli_fetch_assoc($sorgudc);
			$total  = mysqli_num_rows($sorgudc);
		}
		$sorgudc2 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'connections_page`');
		if ($sorgudc2 === false)
			$tmp = '';
		else
		{
			$veridc2 = mysqli_fetch_assoc($sorgudc2);
			$total2  = mysqli_num_rows($sorgudc2);
		}
		$sorgudc3 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'connections_source`');
		if ($sorgudc3 === false)
			$tmp = '';
		else
		{
			$veridc3 = mysqli_fetch_assoc($sorgudc3);
			$total3  = mysqli_num_rows($sorgudc3);
		}
		$sorgudc7 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'guest`');
		if ($sorgudc7 === false)
			$tmp = '';
		else
		{
			$veridc7 = mysqli_fetch_assoc($sorgudc7);
			$total7  = mysqli_num_rows($sorgudc7);
		}
		$sorgudc8 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'page_viewed`
         '.((_PS_VERSION_ > '1.5.0.0') ? ' WHERE `id_shop` = '.(int)$this->context->shop->id : '').'
        ');
		if ($sorgudc8 === false)
			$tmp = '';
		else
		{
			$veridc8 = mysqli_fetch_assoc($sorgudc8);
			$total8  = mysqli_num_rows($sorgudc8);
		}
		/*cart*/
		$sorgudc4 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart` WHERE `id_customer` = 0');
		if ($sorgudc4 === false)
			$tmp = '';
		else
		{
			$veridc4 = mysqli_fetch_assoc($sorgudc4);
			$total4  = mysqli_num_rows($sorgudc4);
		}
		$sorgudc4c = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart` WHERE `id_customer` = 0');
		if ($sorgudc4c === false)
			$tmp = '';
		else
		{
			$veridc4c = mysqli_fetch_assoc($sorgudc4c);
			$total4c  = mysqli_num_rows($sorgudc4c);
		}
		$sorgudc6 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'discount`');
		if ($sorgudc6 === false)
			$tmp = '';
		else
		{
			$veridc6 = mysqli_fetch_assoc($sorgudc6);
			$total6  = mysqli_num_rows($sorgudc6);

		}
		$current_date = date('Y-m-d H:i:s');
		$sorgudc5     = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'discount` WHERE `date_to` < \''.pSQL($current_date).'\'');
		if ($sorgudc5 === false)
			$tmp = '';
		else
		{
			$veridc5 = mysqli_fetch_assoc($sorgudc5);
			$total5  = mysqli_num_rows($sorgudc5);
		}
		$current_date = date('Y-m-d H:i:s');
		$sorgudc9     = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart_rule` WHERE `date_to` < \''.pSQL($current_date).'\' AND `date_to` != \'0000-00-00 00:00:00\'');
		if ($sorgudc9 === false)
			$tmp = '';
		else
		{
			$veridc9 = mysqli_fetch_assoc($sorgudc9);
			$total9  = mysqli_num_rows($sorgudc9);
		}
		$current_date = date('Y-m-d H:i:s');
		$sorgudc10    = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'specific_price_rule` WHERE `to` < \''.pSQL($current_date).'\' AND `to` != \'0000-00-00 00:00:00\'');
		if ($sorgudc10 === false)
			$tmp = '';
		else
		{
			$veridc10 = mysqli_fetch_assoc($sorgudc10);
			$total10  = mysqli_num_rows($sorgudc10);
		}
		$current_date = date('Y-m-d H:i:s');
		$sorgudc11    = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'specific_price` WHERE `to` < \''.pSQL($current_date).'\' AND `to` != \'0000-00-00 00:00:00\'');
		if ($sorgudc11 === false)
			$tmp = '';
		else
		{
			$veridc11 = mysqli_fetch_assoc($sorgudc11);
			$total11  = mysqli_num_rows($sorgudc11);
		}
		$totc   = $total + $total2 + $total3;
		$tottot = $total11 + $total9 + $total10 + $total5;

		$linkpmo = Context::getContext()->link->getAdminLink('AdminModules', true).'&configure=prestaspeed&module_name=prestaspeed&tab_module=administration';
		$this->context->smarty->assign(array(
			'psversion' => _PS_VERSION_,
			'conn'      => $totc,
			'totsav'    => round(Configuration::get('PRESTASPEED_TOTCOMP'), 2),
			'guest'     => $total7,
			'pages'     => $total8,
			'cartsa'    => $total4,
			'disc'      => $tottot,
			'linkpmo'   => $linkpmo,
			'data'      => round($data['db_size_in_mb'], 2).' MB',
		));
		return $this->display(__FILE__, 'views/templates/front/prestaspeed-dash.tpl');
	}

	public function hookdisplayAdminHomeQuickLinks($params)
	{
		$linkpmo = Context::getContext()->link->getAdminLink('AdminModules', true).'&configure=prestaspeed&module_name=prestaspeed&tab_module=administration';
		$this->context->smarty->assign('linkpmo', $linkpmo);
		return $this->display(__FILE__, 'views/templates/front/prestaspeed.tpl');
	}

	public function backOfficeHome($params)
	{
		$linkpmo = Context::getContext()->link->getAdminLink('AdminModules', true).'&configure=prestaspeed&module_name=prestaspeed&tab_module=administration';
		$this->context->smarty->assign('linkpmo', $linkpmo);
		return $this->display(__FILE__, 'views/templates/front/prestaspeed.tpl');
	}

	public function displayForm()
	{
		$datatot = Configuration::get('PRESTASPEED_TOTCOMP') + Configuration::get('PRESTASPEED_TOTCOMPF');
		$datas   = Configuration::get('PRESTASPEED_DBS');
		if (Configuration::get('PRESTASPEED_IDB') == '')
		{
			$sql    = 'SELECT table_schema \''._DB_NAME_.'\', SUM( data_length + index_length) / 1024 / 1024 \'db_size_in_mb\' FROM information_schema.TABLES WHERE table_schema=\''._DB_NAME_.'\' GROUP BY table_schema ;';
			$query2 = mysqli_query($GLOBALS['___mysqli_ston'], $sql);
			$data   = mysqli_fetch_array($query2);
			Configuration::updateValue('PRESTASPEED_IDB', $data['db_size_in_mb']);
		}
		else
			$idbs = Configuration::get('PRESTASPEED_IDB');
		if ($idbs == 0)
			$idbs = $datas;
		$output = '

    <link rel="stylesheet" href="../modules/prestaspeed/views/css/ui-lightness/jquery-ui-1.9.1.custom.css" type="text/css" />
<link rel="stylesheet" href="../modules/prestaspeed/views/css/jquery-ui-timepicker-addon.css" type="text/css" />

    <script type="text/javascript" src="../modules/prestaspeed/views/js/jquery-1.8.2.js"></script>
        <script type="text/javascript" src="../modules/prestaspeed/views/js/jquery-ui-1.9.1.custom.js"></script>
            <script type="text/javascript" src="../modules/prestaspeed/views/js/jquery-ui-timepicker-addon.js"></script>

            <style type="text/css">
            .datepicker{
    position: relative;
    z-index:20;
}
            </style>

        <form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
     <fieldset>
            <legend><img src="'.$this->_path.'views/img/help.png" alt="" title="" />'.$this->l('Documentation').'</legend>

<p><span style="color:#093; font-weight:bolder">Tip: </span>'.$this->l('If enable back office optimization, sometimes you dont see the changes in cms. Simply, disable BO optimization.').'</p>
<p><span style="color:#093; font-weight:bolder">Tip: </span>'.$this->l('If the optimization process of the images don`t finish, and you see a white screen or an internal server error, just press F5 until you back to the module configuration. Remember, image optimization uses smushit service, if the service is down, Prestaspeed cant optimize the images.').'</p><br/>
                	<center><img src="../modules/prestaspeed/views/img/readme.png" style="  width: 31px;margin: 5px; vertical-align:middle" /><a href="../modules/prestaspeed/moduleinstall.pdf">README</a>
				<img src="../modules/prestaspeed/views/img/terms.png" style="  width: 31px;margin: 5px;" vertical-align:middle/><a href="../modules/prestaspeed/termsandconditions.pdf">TERMS</a><center>
            </fieldset>
            </form>
         <br/>
        <form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
     <fieldset>
            <legend><img src="'.$this->_path.'views/img/prefs.gif" alt="" title="" />'.$this->l('Configuration').'</legend>

     <label>'.$this->l('Clean connections:').'</label>
        <div class="margin-form"  >

        <select name="conn" id="conn">
        <option value="0"'.((Configuration::get('PRESTASPEED_CO') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_CO') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
                            <p class="clear">'.$this->l('Delete all fields on connection tables. These tables store information about how long a user keeps on the site, where user come from, etc').'</p>

        </div>
    <label>'.$this->l('Clean guests data:').'</label>
        <div class="margin-form"  >

        <select name="gues" id="gues">
        <option value="0"'.((Configuration::get('PRESTASPEED_GU') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_GU') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
                                    <p class="clear">'.$this->l('Delete all fields on guest table. This table have data from visitors, like OS, browser, etc').'</p>

        </div>
    <label>'.$this->l('Clean viewed page:').'</label>
        <div class="margin-form"  >

        <select name="pag" id="pag">
        <option value="0"'.((Configuration::get('PRESTASPEED_PA') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_PA') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
        <p class="clear">'.$this->l('Delete all fields on page table. This table store the viewed pages from users.').'</p>

        </div>

    <label>'.$this->l('Clean expired discounts:').'</label>
        <div class="margin-form"  >

        <select name="dis" id="dis">
        <option value="0"'.((Configuration::get('PRESTASPEED_DI') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_DI') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
    <p class="clear">'.$this->l('This option cleans all the data on expired vouchers, discounts, and product discounts tables').'</p>

        </div>
        <label>'.$this->l('Clean abandoned carts:').'</label>
        <div class="margin-form"  >

        <select name="car" id="car">
        <option value="0"'.((Configuration::get('PRESTASPEED_CA') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_CA') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
            <p class="clear">'.$this->l('Delete all abandoned carts. The users with products in cart, need to redo the orders.').'</p>

        </div>
           <label>'.$this->l('Delete abandoned carts from').'</label>
        <div class="margin-form">
            <input class="datepicker" type="text" name="sp_from" value="'.Tools::getValue('sp_from', Configuration::get('PRESTASPEED_FR')).'" style="text-align: center" id="sp_from" />
            <span style="font-weight:normal; color:#000000; font-size:12px"> '.$this->l('to').'</span>
            <input class="datepicker" type="text" name="sp_to" value="'.Tools::getValue('sp_to', Configuration::get('PRESTASPEED_TO')).'" style="text-align: center" id="sp_to" />
        </div>
		 <label>'.$this->l('Delete abandoned carts from users too?').'</label>
        <div class="margin-form"  >

        <select name="cav" id="cav">
        <option value="0"'.((Configuration::get('PRESTASPEED_CAV') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_CAV') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
            <p class="clear">'.$this->l('Delete all abandoned carts for register users in selected date').'</p>

        </div>
        <!-- delete orders -->
         <label>'.$this->l('Delete valid orders:').'</label>
        <div class="margin-form"  >

        <select name="ord" id="ord">
        <option value="0"'.((Configuration::get('PRESTASPEED_ORD') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_ORD') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
            <p class="clear">'.$this->l('Delete validated carts. This action cant be undone and you lost the orders between the date selected. This option is great to delete really old orders.').'</p>

        </div>
           <label>'.$this->l('Delete valid orders from').'</label>
        <div class="margin-form">
            <input class="datepicker" type="text" name="sp_from2" value="'.Tools::getValue('sp_from2', Configuration::get('PRESTASPEED_FR2')).'" style="text-align: center" id="sp_from2" />
            <span style="font-weight:normal; color:#000000; font-size:12px"> '.$this->l('to').'</span>
            <input class="datepicker" type="text" name="sp_to2" value="'.Tools::getValue('sp_to2', Configuration::get('PRESTASPEED_TO2')).'" style="text-align: center" id="sp_to2" />
        </div>

        <label>'.$this->l('Optimize database:').'</label>
        <div class="margin-form"  >

        <select name="dat" id="dat">
        <option value="0"'.((Configuration::get('PRESTASPEED_DA') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_DA') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
                    <p class="clear">'.$this->l('Optimize all database tables and repair errors.').'</p>

        </div>
		 <label>'.$this->l('Check database integrity:').'</label>
        <div class="margin-form"  >

        <select name="func" id="func">
        <option value="0"'.((Configuration::get('PRESTASPEED_FUNC') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_FUNC') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
        <p class="clear">'.$this->l('Improve queries and solve problems').'</p>

        </div>
         <!--<label>'.$this->l('Optimize images:').'</label>
        <div class="margin-form"  >

        <select name="smush" id="smush">
        <option value="0"'.((Configuration::get('PRESTASPEED_SMUSH') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_SMUSH') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
                    <p class="clear">'.$this->l('Optimize product images with Smush service when you upload new images. Requires PSP 5.3 and Curl extension.').'</p>

        </div>  -->
        <label>'.$this->l('Clear cache:').'</label>
        <div class="margin-form"  >

        <select name="cac" id="cac">
        <option value="0"'.((Configuration::get('PRESTASPEED_CAC') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_CAC') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
                    <p class="clear">'.$this->l('Clear all smarty cache files').'</p>

        </div>
        <label>'.$this->l('Optimize template:').'</label>
        <div class="margin-form"  >

        <select name="tem" id="tem">
        <option value="0"'.((Configuration::get('PRESTASPEED_TE') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_TE') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
                    <p class="clear">'.$this->l('Enable the best settings to speed up the template loading').'</p>

        </div>
        <label>'.$this->l('Optimize javascript:').'</label>
        <div class="margin-form"  >

        <select name="java" id="java">
        <option value="0"'.((Configuration::get('PRESTASPEED_JA') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_JA') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
                            <p class="clear">'.$this->l('Enable best javascript settings to load JS files more efficient').'</p>

        </div>
        <label>'.$this->l('Boost performance:').'</label>
        <div class="margin-form"  >

        <select name="speed" id="speed">
        <option value="0"'.((Configuration::get('PRESTASPEED_SPEED') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_SPEED') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
                    <p class="clear">'.$this->l('he module try to compress all to increase load speed').'</p>

        </div>
        <label>'.$this->l('Enable Gzip:').'</label>
        <div class="margin-form"  >

        <select name="gzip" id="gzip">
        <option value="0"'.((Configuration::get('PRESTASPEED_GZ') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_GZ') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
        <p class="clear">'.$this->l('Activate the gzip compression for faster execution. This option maybe not work in all servers (only PS > 1.5)').'</p>

        </div>
         <label>'.$this->l('Htaccess optimization:').'</label>
        <div class="margin-form"  >
          <select name="hta" id="hta">
        <option value="0"'.((Configuration::get('PRESTASPEED_HTA') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_HTA') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
        <p class="clear">'.$this->l('Enable the apache htaccess optimization. This option only works if you server have a .htaccess file created. If you enable this option and the site dont work (some servers dont support all optimizations), simply delete your .htaccess file in your root directory and rename the .htaccessps to .htaccess to restore your original file').'</p>

        </div>
         <label>'.$this->l('Back office optimization:').'</label>
        <div class="margin-form"  >
          <select name="bo" id="bo">
        <option value="0"'.((Configuration::get('PRESTASPEED_BO') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_BO') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>

        </select>
         <p class="clear">'.$this->l('Try to optimize the back office performance with a htaccess file in the admin folder (delete thsi file if you get an error). The expiration of the cache is for 1 day, if you modify admin css/js files or images, you can turn off this option to see the changes').'</p>

        </div>
            <label>'.$this->l('Database size: ').'</label>'.round($datas, 2).' MB - '.$this->l('Initial database size before module install: ').round($idbs, 2).'</label>    <p class="clear"></p>

        <p><center><input type="submit" name="submitDelete" value="'.$this->l('Optimize now').'" class="button" /></p><br/>
       <p> <center>'.$this->l('Ask your hosting provider to setup a "Cron task" to load the following URL at the time you would like:').'<a href="'._PS_BASE_URL_._MODULE_DIR_.'prestaspeed/prestaspeed-cron.php?token='.Tools::substr(Tools::encrypt('prestaspeed/cron'), 0, 10).'&id_shop='.$this->context->shop->id.'">'._PS_BASE_URL_._MODULE_DIR_.'prestaspeed/prestaspeed-cron.php?token='.Tools::substr(Tools::encrypt('prestaspeed/cron'), 0, 10).'&id_shop='.$this->context->shop->id.'</a></center><br/><center>'.$this->l('You can use the free Prestashop module Cron tasks manager (cronjobs).Just install it and configure the url and time').'</center></p><br/><br/> <br/>




        </fieldset>
        <br/>
           <form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
     <fieldset>
      <legend><img src="'.$this->_path.'views/img/pictures.gif" alt="" title="" />'.$this->l('Optimize images').'</legend>
     <label>'.$this->l('Clean all stats:').'</label>
        <div class="margin-form"  >

        <select name="stats" id="stats">
        <option value="0"'.((Configuration::get('PRESTASPEED_STATS') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_STATS') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
                            <p class="clear">'.$this->l('This option clean all the stats for the images in the database. If you compress the images again, you need to wait more time to process all again. Clean all if you only have regenerated your images.').'</p>

        </div>
    <label>'.$this->l('Optimize new images:').'</label>
        <div class="margin-form"  >

        <select name="smush" id="smush">
        <option value="0"'.((Configuration::get('PRESTASPEED_SMUSH') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_SMUSH') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
                                    <p class="clear">'.$this->l('This option optimize new images for products when you upload it, but your uploads take more time. Watermark module must be disabled.This option dont work always, but you can regenerate the images of products').'</p>

        </div>
    <label>'.$this->l('Optimize all images:').'</label>
        <div class="margin-form"  >

        <select name="smush2" id="smush2">
        <option value="0"'.((Configuration::get('PRESTASPEED_SMUSH2') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_SMUSH2') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
        <p class="clear">'.$this->l('If you enable this option, the module compress all selected images with Smushit service. This can take time depending of your amount of images. All proceced diles must be the same as total files. If you dont get the same number, repeat the process until you get it.').'</p>

        </div>


         <label>'.$this->l('Type of images:').'</label>
        <div class="margin-form"  >
          <select name="type" id="type">
        <option value="../img/p/"'.((Configuration::get('PRESTASPEED_TYPE') == '../img/p/') ? 'selected="selected"' : '').'>'.$this->l('Product images').'</option>
        <option value="../img/c/"'.((Configuration::get('PRESTASPEED_TYPE') == '../img/c/') ? 'selected="selected"' : '').'>'.$this->l('Category images').'</option>
        <option value="../img/m/"'.((Configuration::get('PRESTASPEED_TYPE') == '../img/m/') ? 'selected="selected"' : '').'>'.$this->l('Manufacturer images').'</option>
        <option value="../img/admin/"'.((Configuration::get('PRESTASPEED_TYPE') == '../img/admin/') ? 'selected="selected"' : '').'>'.$this->l('Admin images').'</option>
        <option value="../img/cms/"'.((Configuration::get('PRESTASPEED_TYPE') == '../img/cms/') ? 'selected="selected"' : '').'>'.$this->l('CMS images').'</option>
        <option value="../img/l/"'.((Configuration::get('PRESTASPEED_TYPE') == '../img/l/') ? 'selected="selected"' : '').'>'.$this->l('Language images').'</option>
        <option value="../img/su/"'.((Configuration::get('PRESTASPEED_TYPE') == '../img/su/') ? 'selected="selected"' : '').'>'.$this->l('Supplier images').'</option>
        <option value="../img/scenes/"'.((Configuration::get('PRESTASPEED_TYPE') == '../img/scenes/') ? 'selected="selected"' : '').'>'.$this->l('Scenes images').'</option>
        <option value="../img/st/"'.((Configuration::get('PRESTASPEED_TYPE') == '../img/st/') ? 'selected="selected"' : '').'>'.$this->l('Stores images').'</option>
        <option value="../themes/"'.((Configuration::get('PRESTASPEED_TYPE') == '../themes/') ? 'selected="selected"' : '').'>'.$this->l('Template images').'</option>
        <option value="../modules/"'.((Configuration::get('PRESTASPEED_TYPE') == '../modules/') ? 'selected="selected"' : '').'>'.$this->l('Modules images').'</option>
		  <option value="../upload/"'.((Configuration::get('PRESTASPEED_TYPE') == '../upload/') ? 'selected="selected"' : '').'>'.$this->l('Upload folder images').'</option>
        <option value="../img/logo"'.((Configuration::get('PRESTASPEED_TYPE') == '../img/logo') ? 'selected="selected"' : '').'>'.$this->l('Logo images').'</option>


        </select>
         <p class="clear">'.$this->l('Select the type of images to optimize. The modules/themes option optimize all themes and modules images to get a better performance').'</p>

        </div>
		 <label>'.$this->l('Total images to optimize in a batch').'</label>
        <div class="margin-form"  >

      					<input type="text" size="40" name="batcht" value="'.Tools::getValue('batcht', Configuration::get('SPONSORSFLIP_BATCHT')).'" />

                                    <p class="clear">'.$this->l('Prestaspeed try to process x amount of files at same time  intil process all files. You can try with less quantity is your server is slow.').'</p>

        </div>
		
		  <label>'.$this->l('Optimize only one image:').'</label>
        <div class="margin-form"  >

      					<input type="text" size="40" name="cusi" value="'.Tools::getValue('cusi', Configuration::get('SPONSORSFLIP_CUSI')).'" />

                                    <p class="clear">'.$this->l('Set the url of the image to optimize (If the image is in http://www.site.com/img/test.jpg, set the url to ../img/test.jpg)').'</p>

        </div>
		  <label>'.$this->l('Clean all temp images:').'</label>
        <div class="margin-form"  >

        <select name="cleani" id="cleani">
        <option value="0"'.((Configuration::get('PRESTASPEED_CLEANI') == '0') ? 'selected="selected"' : '').'>'.$this->l('no').'</option>
        <option value="1"'.((Configuration::get('PRESTASPEED_CLEANI') == '1') ? 'selected="selected"' : '').'>'.$this->l('yes').'</option>
        </select>
                                    <p class="clear">'.$this->l('Delete all images on TMP img folder to save space in the server hard disk').'</p>

        </div>
		
            <label>'.$this->l('Total KB optimized in images:').'</label>'.round($datatot, 2).'</label>    <p class="clear"></p>
                        <label>'.$this->l('Total images to process:').'</label>'.Configuration::get('PRESTASPEED_TOTFIL').' </label>    <p class="clear"></p>
                        <label>'.$this->l('Total images processed:').'</label>'.Configuration::get('PRESTASPEED_TOTSMUSH').'</label>    <p class="clear"></p>
                        <label>'.(Configuration::get('PRESTASPEED_TOTFIL') != Configuration::get('PRESTASPEED_TOTSMUSH') ? $this->l('WARNING: The module does not process all the images. Please optimize the images again to complete the process') : $this->l('All images were processed')).'</label><p class="clear"></p>





       <center><input type="submit" name="submitDelete2" value="'.$this->l('Optimize now').'" class="button" /></center>



        </fieldset>


<script type="text/javascript">
        $(function() {
        $( ".datepicker" ).datetimepicker({
                    prevText: \'\',
                    nextText: \'\',
                    dateFormat: \'yy-mm-dd\',

                    // Define a custom regional settings in order to use PrestaShop translation tools


                    ampm: false,
                    amNames: [\'AM\', \'A\'],
                    pmNames: [\'PM\', \'P\'],
                        timeFormat: \'hh:mm:ss\',
                    timeSuffix: \'\',


                });
    });
        </script>
        ';
		return $output;
	}

	public function uninstall()
	{
		if (!parent::uninstall() || !$this->uninstallDB())
			return false;
		return true;
	}

	private function uninstallDB()
	{
		return true;
	}

	public function getContent()
	{
		($GLOBALS['___mysqli_ston'] = mysqli_connect(_DB_SERVER_, _DB_USER_, _DB_PASSWD_));
		mysqli_query($GLOBALS['___mysqli_ston'], 'SET NAMES UTF8');//this is needed for UTF 8 characters - multilanguage
		((bool)mysqli_query($GLOBALS['___mysqli_ston'], 'USE '.constant('_DB_NAME_')));
		/*delete all*/
		if (_PS_VERSION_ < '1.6.0.0')
		{
			$output = '<h2>'.$this->displayName.'</h2>';
			if (Tools::isSubmit('submitDelete2'))
			{

				$smush  = Tools::getValue('smush');
				$smush2 = Tools::getValue('smush2');
				$cusi   = Tools::getValue('cusi');
				$stats  = Tools::getValue('stats');
				$cleani = Tools::getValue('cleani');
				$type   = Tools::getValue('type');
				$batcht = Tools::getValue('batcht');
				$tmp    = '';
				Configuration::updateValue('PRESTASPEED_SMUSH', $smush);
				Configuration::updateValue('PRESTASPEED_SMUSH2', $smush2);
				Configuration::updateValue('PRESTASPEED_STATS', $stats);
				Configuration::updateValue('PRESTASPEED_CUSI', $cusi);
				Configuration::updateValue('PRESTASPEED_CLEANI', $cleani);
				Configuration::updateValue('PRESTASPEED_TYPE', $type);
				Configuration::updateValue('PRESTASPEED_BATCHT', $batcht);
				if ($stats == 1)
				{
					Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'smush` ;');
					Configuration::updateValue('PRESTASPEED_TOTCOMP', 0);
					Configuration::updateValue('PRESTASPEED_TOTCOMPF', 0);
					$output .= $this->displayConfirmation($this->l('Image optimization stats deleted').'<br/>');

				}
				if ($smush == 1)
					$output .= $this->displayConfirmation($this->l('New product image optimization enabled').'<br/>');
				else
					$output .= $this->displayConfirmation($this->l('New product image optimization disabled').'<br/>');

				if ($smush2 == 1 && $cusi == null)
				{
					//if (extension_loaded('gd'))
					//	$this->smushallgd($type, $output);
					//elseif (extension_loaded('imagick'))
					//	$this->smushallimagick($type, $output);
					//else
					$this->smushall($type, $output, $batcht);

				}
				if ($smush2 == 1 && $cusi == null)
				{
					$output .= $this->displayConfirmation($this->l('Total proceced images:').Configuration::get('PRESTASPEED_TOTSMUSH').'<br/>');
					$output .= $this->displayConfirmation($this->l('Total images:').Configuration::get('PRESTASPEED_TOTFIL').'<br/>');
					$bytes = 'SELECT sum(saved) as total FROM '._DB_PREFIX_.'smush';
					$bytt  = Db::getInstance()->ExecuteS($bytes);
					Configuration::updateValue('PRESTASPEED_TOTCOMP', ($bytt[0]['total'] * 1) / 1024);
					$output .= $this->displayConfirmation($this->l('Total KB saved:').round(Configuration::get('PRESTASPEED_TOTCOMP'), 2).'<br/>');
				}
				if ($cusi != null)
				{
					$this->smushcustom($cusi);
					$output .= $this->displayConfirmation($this->l('Total proceced images:').Configuration::get('PRESTASPEED_TOTSMUSH').'<br/>');
					//	$output .= $this->displayConfirmation($this->l('Total images:').Configuration::get('PRESTASPEED_TOTFIL').'<br/>');
					$bytes = 'SELECT sum(saved) as total FROM '._DB_PREFIX_.'smush';
					$bytt  = Db::getInstance()->ExecuteS($bytes);
					Configuration::updateValue('PRESTASPEED_TOTCOMP', ($bytt[0]['total'] * 1) / 1024);
					$output .= $this->displayConfirmation($this->l('Total KB saved:').round(Configuration::get('PRESTASPEED_TOTCOMP'), 2).'<br/>');
				}
				if ($cleani == 1)
				{
					$this->clearTmpDir();
					$output .= $this->displayConfirmation($this->l('TMP images deleted:').Configuration::get('PRESTASPEED_TMPI').'<br/>');
				}
				Configuration::updateValue('PRESTASPEED_TMPI', 0);
				return $output.$this->displayForm();
			}
			if (Tools::isSubmit('submitDelete'))
			{
				$output .= '<div class="spinner">
  <div class="rect1"></div>
  <div class="rect2"></div>
  <div class="rect3"></div>
  <div class="rect4"></div>
  <div class="rect5"></div>
</div>';
				$total    = '';
				$total2   = '';
				$total3   = '';
				$total4   = '';
				$total5   = '';
				$total6   = '';
				$total7   = '';
				$total8   = '';
				$total9   = '';
				$total10  = '';
				$total11  = '';
				$total4c  = '';
				$conn     = Tools::getValue('conn');
				$gues     = Tools::getValue('gues');
				$pag      = Tools::getValue('pag');
				$dis      = Tools::getValue('dis');
				$car      = Tools::getValue('car');
				$dat      = Tools::getValue('dat');
				$func     = Tools::getValue('func');
				$cac      = Tools::getValue('cac');
				$cav      = Tools::getValue('cav');
				$tem      = Tools::getValue('tem');
				$java     = Tools::getValue('java');
				$gzip     = Tools::getValue('gzip');
				$sp_from  = Tools::getValue('sp_from');
				$sp_to    = Tools::getValue('sp_to');
				$sp_from2 = Tools::getValue('sp_from2');
				$sp_to2   = Tools::getValue('sp_to2');
				$ord      = Tools::getValue('ord');
				$speed    = Tools::getValue('speed');
				$hta      = Tools::getValue('hta');
				$bo       = Tools::getValue('bo');
				$tmp      = '';
				Configuration::updateValue('PRESTASPEED_CO', $conn);
				Configuration::updateValue('PRESTASPEED_FR', $sp_from);
				Configuration::updateValue('PRESTASPEED_TO', $sp_to);
				Configuration::updateValue('PRESTASPEED_FR2', $sp_from2);
				Configuration::updateValue('PRESTASPEED_FUNC', $func);
				Configuration::updateValue('PRESTASPEED_TO2', $sp_to2);
				Configuration::updateValue('PRESTASPEED_JA', $java);
				Configuration::updateValue('PRESTASPEED_GU', $gues);
				Configuration::updateValue('PRESTASPEED_PA', $pag);
				Configuration::updateValue('PRESTASPEED_DI', $dis);
				Configuration::updateValue('PRESTASPEED_CA', $car);
				Configuration::updateValue('PRESTASPEED_CAV', $cav);
				Configuration::updateValue('PRESTASPEED_CAC', $cac);
				Configuration::updateValue('PRESTASPEED_TE', $tem);
				Configuration::updateValue('PRESTASPEED_DA', $dat);
				Configuration::updateValue('PRESTASPEED_GZ', $gzip);
				Configuration::updateValue('PRESTASPEED_HTA', $hta);
				Configuration::updateValue('PRESTASPEED_SPEED', $speed);
				Configuration::updateValue('PRESTASPEED_BO', $bo);
				Configuration::updateValue('PRESTASPEED_ORD', $ord);
				if ($ord == 1 && $sp_from2 != null && $sp_to2 != null)
				{

					$ord2 = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('SELECT * FROM '._DB_PREFIX_.'orders WHERE date_add > \''.pSQL($sp_from2).'\' AND date_add < \''.pSQL($sp_to2).'\' AND valid = 1');

					$h = 0;
					foreach ($ord2 as $order)
					{
						$this->deleteorderbyid($ord2[$h]['id_order']);
						//var_dump($ord[$h]['id_order']);
						$h++;
					}
					$output .= $this->displayConfirmation($this->l('Total orders deleted').': '.$h.'<br/>');
				}

				//back office

				if ($bo == 1 && _PS_VERSION_ > '1.5.0.0')
				{
					$mask = '../config/xml/*.xml';
					$fil  = glob($mask);
					foreach ($fil as $fi)
						chmod($fi, 0777);
					$mask2 = '../config/xml/themes/*.xml';
					$fil2  = glob($mask2);
					foreach ($fil2 as $fi2)
						chmod($fi2, 0777);
					//exec('find ../config/xml -type f -exec chmod 0777 {} +');
					//exec('find ../config/xml/.htaccess -type f -exec chmod 0644 {} +');
					//exec('find ../config/xml/themes -type f -exec chmod 0777 {} +');
					//exec('find ../config/xml/themes/.htaccess -type f -exec chmod 0644 {} +');
					if (file_exists('../.htaccess'))
						copy('../modules/prestaspeed/htaccess.txt', '.htaccess');
				}
				if ($bo == 0 && _PS_VERSION_ > '1.5.0.0')
				{
					$mask = '../config/xml/*.xml';
					$fil  = glob($mask);
					foreach ($fil as $fi)
						chmod($fi, 0644);
					$mask2 = '../config/xml/themes/*.xml';
					$fil2  = glob($mask2);
					foreach ($fil2 as $fi2)
						chmod($fi2, 0644);
					//exec('find ../config/xml -type f -exec chmod 0644 {} +');
					//exec('find ../config/xml/themes -type f -exec chmod 0644 {} +');
					if (file_exists('.htaccess'))
						unlink('.htaccess');
				}
				//db integrity
				if ($func == 1)
				{
					$this->checkAndFix();
					$output .= $this->displayConfirmation($this->l('DB integrity fixed'));

				}
				//htaccess
				if ($hta == 1)
				{
					$this->removeHtaccessSection();
					if (file_exists('../.htaccess') && is_writeable('../.htaccess'))
					{
						if (file_exists('../.htaccessps'))
						{
							//copy('../.htaccess', '../.htaccessps');
							//copy('../.htaccess', '../.htaccessps');
						}
						else
							copy('../.htaccess', '../.htaccessps');
						$file = Tools::file_get_contents('../.htaccess');
						if (!preg_match('/Prestaspeed addon start/', $file))
						{
							$my_file     = '../.htaccess';
							$fh          = fopen($my_file, 'a');
							$string_data = '

#Prestaspeed addon start
<IfModule mod_mime.c>
    AddType text/css .css
    AddType application/x-javascript .js
    AddType text/x-component .htc
    AddType text/html .html .htm
    AddType text/richtext .rtf .rtx
    AddType image/svg+xml .svg .svgz
    AddType text/plain .txt
    AddType text/xsd .xsd
    AddType text/xsl .xsl
    AddType text/xml .xml
    AddType video/asf .asf .asx .wax .wmv .wmx
    AddType video/avi .avi
    AddType image/bmp .bmp
    AddType application/java .class
    AddType video/divx .divx
    AddType application/msword .doc .docx
    AddType application/vnd.ms-fontobject .eot
    AddType application/x-msdownload .exe
    AddType image/gif .gif
    AddType application/x-gzip .gz .gzip
    AddType image/x-icon .ico
    AddType image/jpeg .jpg .jpeg .jpe
    AddType application/vnd.ms-access .mdb
    AddType audio/midi .mid .midi
    AddType video/quicktime .mov .qt
    AddType audio/mpeg .mp3 .m4a
    AddType video/mp4 .mp4 .m4v
    AddType video/mpeg .mpeg .mpg .mpe
    AddType application/vnd.ms-project .mpp
    AddType application/x-font-otf .otf
    AddType application/vnd.oasis.opendocument.database .odb
    AddType application/vnd.oasis.opendocument.chart .odc
    AddType application/vnd.oasis.opendocument.formula .odf
    AddType application/vnd.oasis.opendocument.graphics .odg
    AddType application/vnd.oasis.opendocument.presentation .odp
    AddType application/vnd.oasis.opendocument.spreadsheet .ods
    AddType application/vnd.oasis.opendocument.text .odt
    AddType audio/ogg .ogg
    AddType application/pdf .pdf
    AddType image/png .png
    AddType application/vnd.ms-powerpoint .pot .pps .ppt .pptx
    AddType audio/x-realaudio .ra .ram
    AddType application/x-shockwave-flash .swf
    AddType application/x-tar .tar
    AddType image/tiff .tif .tiff
    AddType application/x-font-ttf .ttf .ttc
    AddType audio/wav .wav
    AddType audio/wma .wma
    AddType application/vnd.ms-write .wri
    AddType application/vnd.ms-excel .xla .xls .xlsx .xlt .xlw
    AddType application/zip .zip
</IfModule>
<IfModule mod_expires.c>
    ExpiresActive On
	# Default directive

	ExpiresByType text/html "access plus 0 minutes"
    ExpiresByType text/css A31536000
    ExpiresByType application/x-javascript A31536000
    ExpiresByType text/x-component A31536000
    ExpiresByType text/richtext A3600
    ExpiresByType image/svg+xml A3600
    ExpiresByType text/plain A3600
    ExpiresByType text/xsd A3600
    ExpiresByType text/xsl A3600
    ExpiresByType video/asf A31536000
    ExpiresByType video/avi A31536000
    ExpiresByType image/bmp A31536000
    ExpiresByType application/java A31536000
    ExpiresByType video/divx A31536000
    ExpiresByType application/msword A31536000
    ExpiresByType application/vnd.ms-fontobject A31536000
    ExpiresByType application/x-msdownload A31536000
    ExpiresByType image/gif A31536000
	ExpiresByType image/jpg "access plus 1 month"
	ExpiresByType image/jpeg "access plus 1 month"
    ExpiresByType application/x-gzip A31536000
    ExpiresByType image/x-icon A31536000
	ExpiresByType application/vnd.ms-access A31536000
    ExpiresByType audio/midi A31536000
    ExpiresByType video/quicktime A31536000
    ExpiresByType audio/mpeg A31536000
    ExpiresByType video/mp4 A31536000
    ExpiresByType video/mpeg A31536000
    ExpiresByType application/vnd.ms-project A31536000
    ExpiresByType application/x-font-otf A31536000
    ExpiresByType application/vnd.oasis.opendocument.database A31536000
    ExpiresByType application/vnd.oasis.opendocument.chart A31536000
    ExpiresByType application/vnd.oasis.opendocument.formula A31536000
    ExpiresByType application/vnd.oasis.opendocument.graphics A31536000
    ExpiresByType application/vnd.oasis.opendocument.presentation A31536000
    ExpiresByType application/vnd.oasis.opendocument.spreadsheet A31536000
    ExpiresByType application/vnd.oasis.opendocument.text A31536000
    ExpiresByType audio/ogg A31536000
    ExpiresByType application/pdf A31536000
    ExpiresByType image/png A31536000
    ExpiresByType application/vnd.ms-powerpoint A31536000
    ExpiresByType audio/x-realaudio A31536000
    ExpiresByType image/svg+xml A31536000
    ExpiresByType application/x-shockwave-flash A31536000
    ExpiresByType application/x-tar A31536000
    ExpiresByType image/tiff A31536000
    ExpiresByType application/x-font-ttf A31536000
    ExpiresByType audio/wav A31536000
    ExpiresByType audio/wma A31536000
    ExpiresByType application/vnd.ms-write A31536000
    ExpiresByType application/vnd.ms-excel A31536000
    ExpiresByType application/zip A31536000
</IfModule>
<FilesMatch "\.(ico|jpg|jpeg|png|gif|js|css|swf)$">

Header unset ETag
FileETag None
</FilesMatch>
<IfModule mod_deflate.c>
    #The following line is enough for .js and .css
    AddOutputFilter DEFLATE js css

    #The following line also enables compression by file content type, for the following list of Content-Type:s
    AddOutputFilterByType DEFLATE text/html text/plain text/xml application/xml

    #The following lines are to avoid bugs with some browsers
    BrowserMatch ^Mozilla/4 gzip-only-text/html
    BrowserMatch ^Mozilla/4\.0[678] no-gzip
    BrowserMatch \bMSIE !no-gzip !gzip-only-text/html
    <IfModule mod_setenvif.c>
        BrowserMatch ^Mozilla/4 gzip-only-text/html
        BrowserMatch ^Mozilla/4\.0[678] no-gzip
        BrowserMatch \bMSIE !no-gzip !gzip-only-text/html
        BrowserMatch \bMSI[E] !no-gzip !gzip-only-text/html
    </IfModule>
    <IfModule mod_headers.c>
        Header append Vary User-Agent env=!dont-vary
    </IfModule>
    <IfModule mod_filter.c>
        AddOutputFilterByType DEFLATE text/css application/x-javascript text/x-component text/html text/richtext image/svg+xml text/plain text/xsd text/xsl text/xml image/x-icon
    </IfModule>
</IfModule>
<IfModule mod_headers.c>
  <FilesMatch "\.(js|css|xml|gz)$">
    Header append Vary: Accept-Encoding
  </FilesMatch>
</IfModule>
<FilesMatch "\.(css|js|htc|CSS|JS|HTC)$">
    <IfModule mod_headers.c>
        Header set Pragma "public"
        Header append Cache-Control "public, must-revalidate, proxy-revalidate"
    </IfModule>
    FileETag MTime Size
    <IfModule mod_headers.c>
         Header set X-Powered-By "prestaspeed"
    </IfModule>
</FilesMatch>
<FilesMatch "\.(html|htm|rtf|rtx|svg|svgz|txt|xsd|xsl|xml|HTML|HTM|RTF|RTX|SVG|SVGZ|TXT|XSD|XSL|XML)$">
    <IfModule mod_headers.c>
        Header set Pragma "public"
        Header append Cache-Control "public, must-revalidate, proxy-revalidate"
    </IfModule>
    FileETag MTime Size
    <IfModule mod_headers.c>
         Header set X-Powered-By "prestaspeed"
    </IfModule>
</FilesMatch>
<FilesMatch "\.(asf|asx|wax|wmv|wmx|avi|bmp|class|divx|doc|docx|eot|exe|gif|gz|gzip|ico|jpg|jpeg|jpe|mdb|mid|midi|mov|qt|mp3|m4a|mp4|m4v|mpeg|mpg|mpe|mpp|otf|odb|odc|odf|odg|odp|ods|odt|ogg|pdf|png|pot|pps|ppt|pptx|ra|ram|svg|svgz|swf|tar|tif|tiff|ttf|ttc|wav|wma|wri|xla|xls|xlsx|xlt|xlw|zip|ASF|ASX|WAX|WMV|WMX|AVI|BMP|CLASS|DIVX|DOC|DOCX|EOT|EXE|GIF|GZ|GZIP|ICO|JPG|JPEG|JPE|MDB|MID|MIDI|MOV|QT|MP3|M4A|MP4|M4V|MPEG|MPG|MPE|MPP|OTF|ODB|ODC|ODF|ODG|ODP|ODS|ODT|OGG|PDF|PNG|POT|PPS|PPT|PPTX|RA|RAM|SVG|SVGZ|SWF|TAR|TIF|TIFF|TTF|TTC|WAV|WMA|WRI|XLA|XLS|XLSX|XLT|XLW|ZIP)$">
    <IfModule mod_headers.c>
        Header set Pragma "public"
        Header append Cache-Control "public, must-revalidate, proxy-revalidate"
    </IfModule>
    FileETag MTime Size
    <IfModule mod_headers.c>
         Header set X-Powered-By "prestaspeed"
    </IfModule>
</FilesMatch>
<IfModule mod_setenvif.c>
 <IfModule mod_headers.c>
    # mod_headers, y u no match by Content-Type?!
    <FilesMatch ".(gif|png|jpe?g|svg|svgz|ico|webp)$">
      SetEnvIf Origin ":" IS_CORS
      Header set Access-Control-Allow-Origin "*" env=IS_CORS
    </FilesMatch>
 </IfModule>
</IfModule>

<IfModule mod_headers.c>
  <FilesMatch ".(ttf|ttc|otf|eot|woff|font.css)$">
    Header set Access-Control-Allow-Origin "*"
  </FilesMatch>
</IfModule>
<IfModule mod_headers.c>
Header set Connection keep-alive
</IfModule>


# compress text, html, javascript, css, xml:
AddOutputFilterByType DEFLATE text/plain
AddOutputFilterByType DEFLATE text/html
AddOutputFilterByType DEFLATE text/xml
AddOutputFilterByType DEFLATE text/css
AddOutputFilterByType DEFLATE application/xml
AddOutputFilterByType DEFLATE application/xhtml+xml
AddOutputFilterByType DEFLATE application/rss+xml
AddOutputFilterByType DEFLATE application/javascript
AddOutputFilterByType DEFLATE application/x-javascript
# Common Fonts
AddOutputFilterByType DEFLATE image/svg+xml
AddOutputFilterByType DEFLATE application/x-font-ttf
AddOutputFilterByType DEFLATE application/font-woff
AddOutputFilterByType DEFLATE application/vnd.ms-fontobject
AddOutputFilterByType DEFLATE application/x-font-otf
#Prestaspeed addon end';
							fwrite($fh, $string_data);
							fclose($fh);
						}
						$output .= $this->displayConfirmation($this->l('file .htaccess optimized').'<br/>');

					}
					else
						$output .= $this->displayConfirmation($this->l('file .htaccess no exists or is not writable').'<br/>');
				}
				if ($hta == 0)
				{
					$this->removeHtaccessSection();
					$output .= $this->displayConfirmation($this->l('file .htaccess optimization disabled').'<br/>');

				}
				/*smarty*/
				if ($tem == 1)
				{
					Configuration::updateValue('PS_SMARTY_CACHE', 1);
					if (_PS_VERSION_ > '1.5.0.0')
						Configuration::updateValue('PS_SMARTY_FORCE_COMPILE', 0);
					else
						Configuration::updateValue('PS_SMARTY_FORCE_COMPILE', 0);
					Configuration::updateValue('PS_CSS_THEME_CACHE', 1);
					Configuration::updateValue('PS_JS_THEME_CACHE', 1);
					Configuration::updateValue('PS_HTML_THEME_COMPRESSION', 1);
$folder = '../themes/'._THEME_NAME_.'/cache/';

if (false !== ($path = file_exists ($folder)))

$output .= $this->displayConfirmation($this->l('Cache theme folder exists').'');

else
{
mkdir($folder);
$output .= $this->displayConfirmation($this->l('Cache theme folder created').'');

}
				}
				if ($gzip == 1)
				{
					if (_PS_VERSION_ > '1.5.0.0')
						Configuration::updateValue('PS_HTACCESS_CACHE_CONTROL', 1);
				}
				else
				{
					if (_PS_VERSION_ > '1.5.0.0')
						Configuration::updateValue('PS_HTACCESS_CACHE_CONTROL', 0);
				}
				/*java*/
				if ($gzip == 1)
				{
					if (_PS_VERSION_ > '1.6.0.0')
						Configuration::updateValue('PS_JS_DEFER', 1);
					if (_PS_VERSION_ > '1.4.0.0')
						Configuration::updateValue('PS_JS_HTML_THEME_COMPRESSION', 1);
				}
				else
				{
					if (_PS_VERSION_ > '1.6.0.0')
						Configuration::updateValue('PS_JS_DEFER', 0);
					if (_PS_VERSION_ > '1.4.0.0')
						Configuration::updateValue('PS_JS_HTML_THEME_COMPRESSION', 0);
				}
				/**/
				if ($tem == 0)
				{
					Configuration::updateValue('PS_SMARTY_CACHE', 0);
					if (_PS_VERSION_ > '1.5.0.0')
						Configuration::updateValue('PS_SMARTY_FORCE_COMPILE', 1);
					else
						Configuration::updateValue('PS_SMARTY_FORCE_COMPILE', 0);
					Configuration::updateValue('PS_CSS_THEME_CACHE', 0);
					Configuration::updateValue('PS_JS_THEME_CACHE', 0);
					Configuration::updateValue('PS_HTML_THEME_COMPRESSION', 0);
				}
				if ($cac == 1)
				{
					if (_PS_VERSION_ < '1.4.0.0')
					{
						$mask = '../tools/smarty/compile/*tpl.php';
						array_map('unlink', glob($mask));
					}
					if (_PS_VERSION_ > '1.4.0.0' && _PS_VERSION_ < '1.5.0.0')
						Tools::clearCache($this->context->smarty);
					if (_PS_VERSION_ > '1.5.0.0')
					{
						if (_PS_VERSION_ > '1.5.5.0')
							Tools::clearSmartyCache();
						Tools::clearCache($this->context->smarty);
						if (_PS_VERSION_ > '1.6.0.0')
							Media::clearCache();
					}
				}
				if ($conn == 1)
				{
					$sorgudc = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'connections`
         '.((_PS_VERSION_ > '1.5.0.0') ? ' WHERE `id_shop` = '.$this->context->shop->id : '').'
         ');
					if ($sorgudc === false)
						$tmp = '';
					else
					{
						$veridc = mysqli_fetch_assoc($sorgudc);
						$total  = mysqli_num_rows($sorgudc);
					}
					$sorgudc2 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'connections_page`');
					if ($sorgudc2 === false)
						$tmp = '';
					else
					{
						$veridc2 = mysqli_fetch_assoc($sorgudc2);
						$total2  = mysqli_num_rows($sorgudc2);
					}

					$sorgudc3 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'connections_source`');
					if ($sorgudc3 === false)
						$tmp = '';
					else
					{
						$veridc3 = mysqli_fetch_assoc($sorgudc3);
						$total3  = mysqli_num_rows($sorgudc3);
					}
				}
				if ($gues == 1)
				{
					$sorgudc7 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'guest`');
					if ($sorgudc7 === false)
						$tmp = '';
					else
					{
						$veridc7 = mysqli_fetch_assoc($sorgudc7);
						$total7  = mysqli_num_rows($sorgudc7);
					}

				}
				if ($pag == 1)
				{
					$sorgudc8 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'page_viewed`
         '.((_PS_VERSION_ > '1.5.0.0') ? ' WHERE `id_shop` = '.$this->context->shop->id : '').'
        ');
					if ($sorgudc8 === false)
						$tmp = '';
					else
					{
						$veridc8 = mysqli_fetch_assoc($sorgudc8);
						$total8  = mysqli_num_rows($sorgudc8);
					}

				}
				/*cart*/
				if ($car == 1)
				{
					if ($sp_from == 0 && $sp_to == 0)
					{
						$sorgudc4 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart` WHERE `id_customer` = 0'.($cav == 1 ? ' OR `id_customer` != 0' : ''));
						if ($sorgudc4 === false)
							$tmp = '';
						else
						{
							$veridc4 = mysqli_fetch_assoc($sorgudc4);
							$total4  = mysqli_num_rows($sorgudc4);
						}
					}
					if ($sp_from != 0 && $sp_to != 0)
					{
						$sorgudc4 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart`
                        WHERE `id_customer` = 0'.($cav == 1 ? ' OR `id_customer` != 0' : '').'
                        '.((_PS_VERSION_ > '1.5.0.0') ? ' AND `id_shop` = '.(int)$this->context->shop->id : '').'
                        AND `date_upd` BETWEEN \''.pSQL($sp_from).'\' AND \''.pSQL($sp_to).'\';');
						if ($sorgudc4 === false)
							$tmp = '';
						else
						{
							$veridc4 = mysqli_fetch_assoc($sorgudc4);
							$total4  = mysqli_num_rows($sorgudc4);
						}
					}
					if ($sp_from == 0 && $sp_to == 0)
					{
						$sorgudc4c = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart` WHERE `id_customer` = 0'.($cav == 1 ? ' OR `id_customer` != 0' : ''));
						if ($sorgudc4c === false)
							$tmp = '';
						else
						{
							$veridc4c = mysqli_fetch_assoc($sorgudc4c);
							$total4c  = mysqli_num_rows($sorgudc4c);
						}
					}
					if ($sp_from != 0 && $sp_to != 0)
					{
						$sorgudc4c = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart`
        WHERE `id_customer` = 0'.($cav == 1 ? ' OR `id_customer` != 0' : '').'
        '.((_PS_VERSION_ > '1.5.0.0') ? ' AND `id_shop` = '.(int)$this->context->shop->id : '').'
        AND `date_upd` BETWEEN \''.pSQL($sp_from).'\' AND \''.pSQL($sp_to).'\'');
						if ($sorgudc4c === false)
							$tmp = '';
						else
						{
							$veridc4c = mysqli_fetch_assoc($sorgudc4c);
							$total4c  = mysqli_num_rows($sorgudc4c);
						}
					}
				}
				if ($dis == 1)
				{
					$sorgudc6 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'discount`');
					if ($sorgudc6 === false)
						$tmp = '';
					else
					{
						$veridc6 = mysqli_fetch_assoc($sorgudc6);
						$total6  = mysqli_num_rows($sorgudc6);
					}
					$current_date = date('Y-m-d H:i:s');
					$sorgudc5     = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'discount` WHERE `date_to` < \''.pSQL($current_date).'\'');
					if ($sorgudc5 === false)
						$tmp = '';
					else
					{
						$veridc5 = mysqli_fetch_assoc($sorgudc5);
						$total5  = mysqli_num_rows($sorgudc5);
					}
					$current_date = date('Y-m-d H:i:s');
					$sorgudc9     = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart_rule` WHERE `date_to` < \''.pSQL($current_date).'\' AND `date_to` != \'0000-00-00 00:00:00\'');
					if ($sorgudc9 === false)
						$tmp = '';
					else
					{
						$veridc9 = mysqli_fetch_assoc($sorgudc9);
						$total9  = mysqli_num_rows($sorgudc9);
					}
					/**/
					$current_date = date('Y-m-d H:i:s');
					$sorgudc10    = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'specific_price_rule` WHERE `to` < \''.pSQL($current_date).'\' AND `to` != \'0000-00-00 00:00:00\'');
					if ($sorgudc10 === false)
						$tmp = '';
					else
					{
						$veridc10 = mysqli_fetch_assoc($sorgudc10);
						$total10  = mysqli_num_rows($sorgudc10);
					}
					$current_date = date('Y-m-d H:i:s');
					$sorgudc11    = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'specific_price` WHERE `to` < \''.pSQL($current_date).'\' AND `to` != \'0000-00-00 00:00:00\'');
					if ($sorgudc11 === false)
						$tmp = '';
					else
					{
						$veridc11 = mysqli_fetch_assoc($sorgudc11);
						$total11  = mysqli_num_rows($sorgudc11);
					}
				}
				if ($conn == 1)
				{
					Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'connections` ;');
					Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'connections_page` ;');
					Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'connections_source` ;');
				}
				if ($gues == 1)
					Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'guest`;');
				if ($pag == 1)
					Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'page_viewed`;');

				if ($total11 != null)
				{
					do
					{
						$idc11 = $veridc11['id_specific_price'];
						if ($dis == 1)
							$this->delete11($idc11);
					} while ($veridc11 = mysqli_fetch_assoc($sorgudc11));
				}
				if ($total10 != null)
				{
					do
					{
						$idc10 = $veridc10['id_specific_price_rule'];
						if ($dis == 1)
							$this->delete10($idc10);
					} while ($veridc10 = mysqli_fetch_assoc($sorgudc10));
				}
				/**/
				if ($total9 != null)
				{
					do
					{
						$idc9 = $veridc9['id_cart_rule'];
						if ($dis == 1)
							$this->delete9($idc9);
					} while ($veridc9 = mysqli_fetch_assoc($sorgudc9));
				}
				/**/
				if ($total5 != null)
				{
					do
					{
						$idc5 = $veridc5['id_discount'];
						if ($dis == 1)
							$this->delete5($idc5);
					} while ($veridc5 = mysqli_fetch_assoc($sorgudc5));
				}
				if ($total4 != null)
				{
					do
					{
						if ($car == 1)
							$idc = $veridc4['id_cart'];
						$this->delete($idc);

					} while ($veridc4 = mysqli_fetch_assoc($sorgudc4));
				}
				if ($total4c != null)
				{
					do
					{
						$idcc = $veridc4c['id_cart'];
						if ($car == 1)
							$this->deletec($idcc);

					} while ($veridc4c = mysqli_fetch_assoc($sorgudc4c));
				}
				if ($conn == 1 && $total != null)
					$output .= $this->displayConfirmation($this->l('Rows before optimization on connection table (now is zero)').': '.$total.'<br/>');
				if ($conn == 1 && $total2 != null)
					$output .= $this->displayConfirmation($this->l('Rows before optimization on connection_page table (now is zero)').': '.$total2.'<br/>');
				if ($conn == 1 && $total3 != null)
					$output .= $this->displayConfirmation($this->l('Rows before optimization on connection_page_source table (now is zero)').': '.$total3.'<br/>');
				if ($gues == 1 && $total7 != null)
					$output .= $this->displayConfirmation($this->l('Rows before optimization on guest table (now is zero)').': '.$total7.'<br/>');
				if ($pag == 1 && $total8 != null)
					$output .= $this->displayConfirmation($this->l('Rows before optimization on page_viewed table (now is zero)').': '.$total8.'<br/>');
				if ($car == 1 && $total4 != null)
					$output .= $this->displayConfirmation($this->l('Rows before optimization on cart table (only abandoned carts for guests or users)').': '.$total4.'<br/>');
				if ($dis == 1 && $total6 != null || $dis == 1 && $total9 != null || $dis == 1 && $total10 != null || $dis == 1 && $total11 != null)
				{
					$tottot = $total6 + $total9 + $total11 + $total10;
					$output .= $this->displayConfirmation($this->l('Rows before optimization on vouchers/discounts/product discounts tables').': '.$tottot.'<br/>');
				}
				if ($cac == 1)
					$output .= $this->displayConfirmation($this->l('Cache cleared').'<br/>');
				if ($tem == 1)
					$output .= $this->displayConfirmation($this->l('Template optimized').'<br/>');
				if ($java == 1)
					$output .= $this->displayConfirmation($this->l('Javascript optimized').'<br/>');
				if ($gzip == 1)
					$output .= $this->displayConfirmation($this->l('Gzip enabled').'<br/>');
				$server  = _DB_SERVER_;
				$user    = _DB_USER_;
				$pwd     = _DB_PASSWD_;
				$db_name = _DB_NAME_;
				if ($dat == 1)
				{
					$alltables = mysqli_query($GLOBALS['___mysqli_ston'], 'SHOW TABLES');
					$v         = '';
					while ($table = mysqli_fetch_assoc($alltables))
					{
						foreach ($table as $db => $tablename)
						{
							mysqli_query($GLOBALS['___mysqli_ston'], 'OPTIMIZE TABLE `'._DB_PREFIX_.$tablename.'`');
							mysqli_query($GLOBALS['___mysqli_ston'], 'REPAIR TABLE `'._DB_PREFIX_.$tablename.'`');
							$v .= $tablename.'</br>';
						}
					}
					$output .= $this->displayConfirmation($v);
// Alert that operation was successful
					$output .= $this->displayConfirmation($this->l('Above tables successfully optimized.').'<br/>');
				}
				$output .= '
<style type="text/css">
.spinner {
display:none
}
</style>';
			}
			$sql   = "SELECT table_schema '"._DB_NAME_."', SUM( data_length + index_length) / 1024 / 1024 'db_size_in_mb' FROM information_schema.TABLES WHERE table_schema='"._DB_NAME_."' GROUP BY table_schema ;";
			$query = mysqli_query($GLOBALS['___mysqli_ston'], $sql);
			$data  = mysqli_fetch_array($query);
			Configuration::updateValue('PRESTASPEED_DBS', $data['db_size_in_mb']);
			return $output.$this->displayForm();
		}
		else

			return $this->postProcess().$this->_displayInfo2().$this->renderForm().$this->renderForm2().$this->_displayInfo();

	}

	/*cron purposes*/
	public function cron()
	{
		($GLOBALS['___mysqli_ston'] = mysqli_connect(_DB_SERVER_, _DB_USER_, _DB_PASSWD_));
		mysqli_query($GLOBALS['___mysqli_ston'], 'SET NAMES UTF8');//this is needed for UTF 8 characters - multilanguage
		((bool)mysqli_query($GLOBALS['___mysqli_ston'], 'USE '.constant('_DB_NAME_')));
		/*delete all*/
		$total   = '';
		$total2  = '';
		$total3  = '';
		$total4  = '';
		$total5  = '';
		$total6  = '';
		$total7  = '';
		$total8  = '';
		$total9  = '';
		$total10 = '';
		$total11 = '';
		$total4c = '';
		$conn    = Configuration::get('PRESTASPEED_CO');
		$func    = Configuration::get('PRESTASPEED_FUNC');
		
		$gues    = Configuration::get('PRESTASPEED_GU');
		$pag     = Configuration::get('PRESTASPEED_PA');
		$dis     = Configuration::get('PRESTASPEED_DI');
		$car     = Configuration::get('PRESTASPEED_CA');
		$dat     = Configuration::get('PRESTASPEED_DA');
		$cav     = Configuration::get('PRESTASPEED_CAV');
		$cac     = Configuration::get('PRESTASPEED_CAC');
		$tem     = Configuration::get('PRESTASPEED_TE');
		$java    = Configuration::get('PRESTASPEED_JA');
		$gzip    = Configuration::get('PRESTASPEED_GZ');
		$sp_from = Configuration::get('PRESTASPEED_FR');
		$sp_to   = Configuration::get('PRESTASPEED_TO');
		$tmp     = '';
//    $this->backup_tables(_DB_SERVER_,_DB_USER_,_DB_PASSWD_,_DB_NAME_);
		/*smarty*/

		if ($conn == 1)
		{
			$sorgudc = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'connections`
         '.((_PS_VERSION_ > '1.5.0.0') ? ' WHERE `id_shop` = '.(int)$this->context->shop->id : '').'
         ');
			if ($sorgudc === false)
				$tmp = '';
			else
			{
				$veridc = mysqli_fetch_assoc($sorgudc);
				$total  = mysqli_num_rows($sorgudc);
			}
			$sorgudc2 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'connections_page`');
			if ($sorgudc2 === false)
				$tmp = '';
			else
			{
				$veridc2 = mysqli_fetch_assoc($sorgudc2);
				$total2  = mysqli_num_rows($sorgudc2);
			}

			$sorgudc3 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'connections_source`');
			if ($sorgudc3 === false)
				$tmp = '';
			else
			{
				$veridc3 = mysqli_fetch_assoc($sorgudc3);
				$total3  = mysqli_num_rows($sorgudc3);
			}
		}
		if ($gues == 1)
		{
			$sorgudc7 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'guest`');
			if ($sorgudc7 === false)
				$tmp = '';
			else
			{
				$veridc7 = mysqli_fetch_assoc($sorgudc7);
				$total7  = mysqli_num_rows($sorgudc7);
			}

		}
		if ($pag == 1)
		{
			$sorgudc8 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'page_viewed`
         '.((_PS_VERSION_ > '1.5.0.0') ? ' WHERE `id_shop` = '.$this->context->shop->id : '').'
        ');
			if ($sorgudc8 === false)
				$tmp = '';
			else
			{
				$veridc8 = mysqli_fetch_assoc($sorgudc8);
				$total8  = mysqli_num_rows($sorgudc8);
			}

		}
		/*cart*/
		if ($car == 1)
		{
			if ($sp_from == 0 && $sp_to == 0)
			{
				$sorgudc4 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart` WHERE `id_customer` = 0'.($cav == 1 ? ' OR `id_customer` != 0' : ''));
				if ($sorgudc4 === false)
					$tmp = '';
				else
				{
					$veridc4 = mysqli_fetch_assoc($sorgudc4);
					$total4  = mysqli_num_rows($sorgudc4);
				}
			}
			if ($sp_from != 0 && $sp_to != 0)
			{
				$sorgudc4 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart`
                        WHERE `id_customer` = 0'.($cav == 1 ? ' OR `id_customer` != 0' : '').'
                        '.((_PS_VERSION_ > '1.5.0.0') ? ' AND `id_shop` = '.(int)$this->context->shop->id : '').'
                        AND `date_upd` BETWEEN \''.pSQL($sp_from).'\' AND \''.pSQL($sp_to).'\';');
				if ($sorgudc4 === false)
					$tmp = '';
				else
				{
					$veridc4 = mysqli_fetch_assoc($sorgudc4);
					$total4  = mysqli_num_rows($sorgudc4);
				}
			}
			if ($sp_from == 0 && $sp_to == 0)
			{
				$sorgudc4c = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart` WHERE `id_customer` = 0'.($cav == 1 ? ' OR `id_customer` != 0' : ''));
				if ($sorgudc4c === false)
					$tmp = '';
				else
				{
					$veridc4c = mysqli_fetch_assoc($sorgudc4c);
					$total4c  = mysqli_num_rows($sorgudc4c);
				}
			}
			if ($sp_from != 0 && $sp_to != 0)
			{
				$sorgudc4c = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart`
        WHERE `id_customer` = 0'.($cav == 1 ? ' OR `id_customer` != 0' : '').'
        '.((_PS_VERSION_ > '1.5.0.0') ? ' AND `id_shop` = '.(int)$this->context->shop->id : '').'
        AND `date_upd` BETWEEN \''.pSQL($sp_from).'\' AND \''.pSQL($sp_to).'\'');
				if ($sorgudc4c === false)
					$tmp = '';
				else
				{
					$veridc4c = mysqli_fetch_assoc($sorgudc4c);
					$total4c  = mysqli_num_rows($sorgudc4c);
				}
			}
		}
		if ($func == 1)
		{
			$this->checkAndFix();

		}
		if ($dis == 1)
		{
			$sorgudc6 = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'discount`');
			if ($sorgudc6 === false)
				$tmp = '';
			else
			{
				$veridc6 = mysqli_fetch_assoc($sorgudc6);
				$total6  = mysqli_num_rows($sorgudc6);
			}
			$current_date = date('Y-m-d H:i:s');
			$sorgudc5     = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'discount` WHERE `date_to` < \''.pSQL($current_date).'\'');
			if ($sorgudc5 === false)
				$tmp = '';
			else
			{
				$veridc5 = mysqli_fetch_assoc($sorgudc5);
				$total5  = mysqli_num_rows($sorgudc5);
			}
			$current_date = date('Y-m-d H:i:s');
			$sorgudc9     = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'cart_rule` WHERE `date_to` < \''.pSQL($current_date).'\' AND `date_to` != \'0000-00-00 00:00:00\'');
			if ($sorgudc9 === false)
				$tmp = '';
			else
			{
				$veridc9 = mysqli_fetch_assoc($sorgudc9);
				$total9  = mysqli_num_rows($sorgudc9);
			}
			/**/
			$current_date = date('Y-m-d H:i:s');
			$sorgudc10    = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'specific_price_rule` WHERE `to` < \''.pSQL($current_date).'\' AND `to` != \'0000-00-00 00:00:00\'');
			if ($sorgudc10 === false)
				$tmp = '';
			else
			{
				$veridc10 = mysqli_fetch_assoc($sorgudc10);
				$total10  = mysqli_num_rows($sorgudc10);
			}
			$current_date = date('Y-m-d H:i:s');
			$sorgudc11    = mysqli_query($GLOBALS['___mysqli_ston'], 'SELECT * FROM `'._DB_PREFIX_.'specific_price` WHERE `to` < \''.pSQL($current_date).'\' AND `to` != \'0000-00-00 00:00:00\'');
			if ($sorgudc11 === false)
				$tmp = '';
			else
			{
				$veridc11 = mysqli_fetch_assoc($sorgudc11);
				$total11  = mysqli_num_rows($sorgudc11);
			}
		}
		if ($conn == 1)
		{
			Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'connections` ;');
			Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'connections_page` ;');
			Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'connections_source` ;');
		}
		if ($gues == 1)
			Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'guest`;');
		if ($pag == 1)
			Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'page_viewed`;');

		if ($total11 != null)
		{
			do
			{
				$idc11 = $veridc11['id_specific_price'];
				if ($dis == 1)
					$this->delete11($idc11);
			} while ($veridc11 = mysqli_fetch_assoc($sorgudc11));
		}
		if ($total10 != null)
		{
			do
			{
				$idc10 = $veridc10['id_specific_price_rule'];
				if ($dis == 1)
					$this->delete10($idc10);
			} while ($veridc10 = mysqli_fetch_assoc($sorgudc10));
		}
		/**/
		if ($total9 != null)
		{
			do
			{
				$idc9 = $veridc9['id_cart_rule'];
				if ($dis == 1)
					$this->delete9($idc9);
			} while ($veridc9 = mysqli_fetch_assoc($sorgudc9));
		}
		/**/
		if ($total5 != null)
		{

			do
			{
				$idc5 = $veridc5['id_discount'];
				if ($dis == 1)
					$this->delete5($idc5);
			} while ($veridc5 = mysqli_fetch_assoc($sorgudc5));
		}
		if ($total4 != null)
		{
			do
			{
				if ($car == 1)
					$idc = $veridc4['id_cart'];
				$this->delete($idc);

			} while ($veridc4 = mysqli_fetch_assoc($sorgudc4));
		}
		if ($total4c != null)
		{
			do
			{
				$idcc = $veridc4c['id_cart'];
				if ($car == 1)
					$this->deletec($idcc);

			} while ($veridc4c = mysqli_fetch_assoc($sorgudc4c));
		}

		$server  = _DB_SERVER_;
		$user    = _DB_USER_;
		$pwd     = _DB_PASSWD_;
		$db_name = _DB_NAME_;
		if ($dat == 1)
		{
			$alltables = mysqli_query($GLOBALS['___mysqli_ston'], 'SHOW TABLES');
			$v         = '';
			while ($table = mysqli_fetch_assoc($alltables))
			{
				foreach ($table as $db => $tablename)
				{
					mysqli_query($GLOBALS['___mysqli_ston'], 'OPTIMIZE TABLE `'._DB_PREFIX_.$tablename.'`');
					mysqli_query($GLOBALS['___mysqli_ston'], 'REPAIR TABLE `'._DB_PREFIX_.$tablename.'`');
					$v .= $tablename.'</br>';
				}
			}
// Alert that operation was successful
		}
		$sql   = "SELECT table_schema '"._DB_NAME_."', SUM( data_length + index_length) / 1024 / 1024 'db_size_in_mb' FROM information_schema.TABLES WHERE table_schema='"._DB_NAME_."' GROUP BY table_schema ;";
		$query = mysqli_query($GLOBALS['___mysqli_ston'], $sql);
		$data  = mysqli_fetch_array($query);
		Configuration::updateValue('PRESTASPEED_DBS', $data['db_size_in_mb']);
		if ($this->cron)
			die();
		if (_PS_VERSION_ > '1.5.0.0')
			Tools::redirect('location: ./index.php?tab=AdminModules&configure=prestaspeed&token='.Tools::getAdminTokenLite('AdminModules').'&tab_module='.$this->tab.'&module_name=prestaspeed&validation');
		else
			Tools::redirect($_SERVER['HTTP_REFERER'].'');
		die();

	}

	/*end cron*/
	public function delete11($idc11)
	{
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'specific_price` WHERE `id_specific_price` = '.(int)$idc11);
	}

	public function delete10($idc10)
	{
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'specific_price_rule` WHERE `id_specific_price_rule` = '.(int)$idc10);
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'specific_price_rule_condition_group` WHERE `id_specific_price_rule` = '.(int)$idc10);
	}

	public function delete9($idc9)
	{
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'cart_rule` WHERE `id_cart_rule` = '.(int)$idc9);
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'cart_rule_carrier` WHERE `id_cart_rule` = '.(int)$idc9);
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'cart_rule_country` WHERE `id_cart_rule` = '.(int)$idc9);
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'cart_rule_group` WHERE `id_cart_rule` = '.(int)$idc9);
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'cart_rule_lang` WHERE `id_cart_rule` = '.(int)$idc9);
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'cart_rule_shop` WHERE `id_cart_rule` = '.(int)$idc9);
	}

	public function delete5($idc5)
	{
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'discount_category` WHERE `id_discount` = '.(int)$idc5);
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'discount_lang` WHERE `id_discount` = '.(int)$idc5);
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'discount` WHERE `id_discount` = '.(int)$idc5);
	}

	public function deletec($idcc)
	{
		if ($this->OrderExists($idcc))
			return false;
		if (_PS_VERSION_ < '1.5.0.0')
			Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'cart_discount` WHERE `id_cart` = '.(int)$idcc);
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'cart_product` WHERE `id_cart` = '.(int)$idcc);
	}

	public function delete($idc)
	{
		if ($this->OrderExists($idc))
			return false;
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'cart` WHERE `id_cart` = '.(int)$idc);
		// --------------  must NOT delete a cart which is associated with an order!
		// --------------  unlink uploaded files associated with any customized product in the cart (similar to deletePictureToProduct() method )
		$uploadedFiles = Db::getInstance()->ExecuteS('
        SELECT cd.`value`
        FROM `'._DB_PREFIX_.'customized_data` cd
        INNER JOIN `'._DB_PREFIX_.'customization` c ON (cd.`id_customization`= c.`id_customization`)
        WHERE cd.`type`= 0 AND c.`id_cart`='.(int)$idc);
		foreach ($uploadedFiles as $mustUnlink)
		{
			unlink(_PS_UPLOAD_DIR_.$mustUnlink['value'].'_small');
			unlink(_PS_UPLOAD_DIR_.$mustUnlink['value']);
		}
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'customized_data` WHERE `id_customization` IN (
         SELECT `id_customization` FROM `'._DB_PREFIX_.'customization` WHERE `id_cart`='.(int)$idc.')');
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'customization` WHERE `id_cart`='.(int)$idc);
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'message_readed` WHERE `id_message` IN (
         SELECT `id_message` FROM `'._DB_PREFIX_.'message` WHERE `id_cart`='.(int)$idc.')');
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'message` WHERE `id_cart`='.(int)$idc);
		if (_PS_VERSION_ < '1.5.0.0')
			Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'cart_discount` WHERE `id_cart` = '.(int)$idc);
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'cart_product` WHERE `id_cart` = '.(int)$idc);
		//    ignore    TABLES  fianet_fraud  and prestafraud_carts
		// ------- FINALLY  (no multilingual aspect, so avoid calling parent -- just delete it from here)
	}

	public function table_exist($table)
	{
		$con = '';
		$sql = 'show tables like \''.$table.'\'';
		$res = $con->query($sql);
		return ($res->num_rows > 0);
	}

	/* backup the db or just a table */
	public function backup_tables($host, $user, $pass, $name, $tables = '*')
	{
		$db_name = $name;
		$dir     = __PS_BASE_URI__.'modules/prestaspeed/backup/';
		$bu_time = time();
		$dbc     = ($GLOBALS['___mysqli_ston'] = mysqli_connect(_DB_SERVER_, _DB_USER_, _DB_PASSWD_));
		$q       = 'SHOW TABLES';
		$r       = mysqli_query($GLOBALS['___mysqli_ston'], 'SHOW TABLES FROM '.$db_name);
		if (mysqli_num_rows($r) > 0)
		{
			echo '<p>Backing up database \'$db_name\'.</p>\n';
			while (list($table) = mysqli_fetch_array($r, MYSQLI_NUM))
			{
				$q2 = 'SELECT * FROM $table';
				$r2 = mysqli_query($q2, $dbc);
				if (mysqli_num_rows($r2) > 0)
				{
					if ($fp = gzopen('$dir/{$db_name}_{$table}_{$bu_time}.sql.gz', 'w9'))
					{
						while ($row = mysqli_fetch_array($r2, MYSQLI_NUM))
						{
							foreach ($row as $value)
								gzwrite($fp, '\'$value\', ');
							gzwrite($fp, '\n');
						}
						// Close the file:
						gzclose($fp);
					}
					else
					{ // Could not create the file!
						echo '<p>The file--$dir/{$table}_{$bu_time}.sql.gz--could not be opened for writing.</p>\n';
						break;
					}
				}
			}
		}
		else
			echo '<p>The submitted database--$db_name--contains no tables.</p>\n';
		//$output .= $this->displayConfirmation($this->l('executed backup').'<br/>');
	}

	public function dbSize()
	{
		$sql   = 'SELECT table_schema \''._DB_NAME_.'\', SUM( data_length + index_length) / 1024 / 1024 \'db_size_in_mb\' FROM information_schema.TABLES WHERE table_schema=\''._DB_NAME_.'\' GROUP BY table_schema ;';
		$query = mysqli_query($GLOBALS['___mysqli_ston'], $sql);
		$data  = mysqli_fetch_array($query);
		$data['db_size_in_mb'];
	}

	public function OrderExists($idc)
	{
		return (bool)Db::getInstance()->getValue('SELECT `id_cart` FROM `'._DB_PREFIX_.'orders` WHERE `id_cart` = '.(int)$idc);
	}

	public function psversion()
	{
		$version = _PS_VERSION_;
		$exp     = $explode = explode('.', $version);
		return $exp[1];
	}

	/*delete orders*/
	public function deleteorderbyid($id, $return = 0)
	{
		$psversion = $this->psversion();

		if ($psversion == 5 || $psversion == 6)
		{
			$thisorder = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('SELECT id_cart FROM '._DB_PREFIX_.'orders WHERE id_order = '.(int)$id);

			if (isset($thisorder[0]))
			{
				//deleting order_return
				$q = 'DELETE a,b FROM '._DB_PREFIX_.'order_return AS a LEFT JOIN '._DB_PREFIX_.'order_return_detail AS b ON a.id_order_return = b.id_order_return WHERE id_order="'.(int)$id.'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				//deleting order_slip
				$q = 'DELETE a,b FROM '._DB_PREFIX_.'order_slip AS a LEFT JOIN '._DB_PREFIX_.'order_slip_detail AS b ON a.id_order_slip = b.id_order_slip WHERE id_order="'.(int)$id.'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE FROM '._DB_PREFIX_.'cart_product WHERE id_cart="'.(int)$thisorder[0]['id_cart'].'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE FROM '._DB_PREFIX_.'order_history WHERE id_order="'.(int)$id.'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE FROM '._DB_PREFIX_.'order_detail WHERE id_order="'.(int)$id.'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE FROM '._DB_PREFIX_.'orders WHERE id_order="'.(int)$id.'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

			}
		}

		if ($psversion == 3)
		{
			$thisorder = Db::getInstance()->ExecuteS('SELECT id_cart FROM '._DB_PREFIX_.'orders WHERE id_order = '.(int)$id);

			if (isset($thisorder[0]))
			{
				$q = 'DELETE a,b FROM '._DB_PREFIX_.'order_return AS a LEFT JOIN '._DB_PREFIX_.'order_return_detail AS b ON a.id_order_return = b.id_order_return WHERE id_order="'.(int)$id.'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE a,b FROM '._DB_PREFIX_.'order_slip AS a LEFT JOIN '._DB_PREFIX_.'order_slip_detail AS b ON a.id_order_slip = b.id_order_slip WHERE id_order="'.(int)$id.'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE FROM '._DB_PREFIX_.'cart_discount WHERE id_cart="'.(int)$thisorder[0]['id_cart'].'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE FROM '._DB_PREFIX_.'cart_product WHERE id_cart="'.(int)$thisorder[0]['id_cart'].'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE FROM '._DB_PREFIX_.'order_history WHERE id_order="'.(int)$id.'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE FROM '._DB_PREFIX_.'order_discount WHERE id_order="'.(int)$id.'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE FROM '._DB_PREFIX_.'order_detail WHERE id_order="'.(int)$id.'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE FROM '._DB_PREFIX_.'orders WHERE id_order="'.(int)$id.'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

			}
		}

		if ($psversion == 4)
		{
			$thisorder = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('SELECT id_cart FROM '._DB_PREFIX_.'orders WHERE id_order = '.(int)$id);

			if (isset($thisorder[0]))
			{
				$q = 'DELETE a,b FROM '._DB_PREFIX_.'order_return AS a LEFT JOIN '._DB_PREFIX_.'order_return_detail AS b ON a.id_order_return = b.id_order_return WHERE id_order="'.(int)$id.'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE a,b FROM '._DB_PREFIX_.'order_slip AS a LEFT JOIN '._DB_PREFIX_.'order_slip_detail AS b ON a.id_order_slip = b.id_order_slip WHERE id_order="'.(int)$id.'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE FROM '._DB_PREFIX_.'cart_discount WHERE id_cart="'.(int)$thisorder[0]['id_cart'].'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE FROM '._DB_PREFIX_.'cart_product WHERE id_cart="'.(int)$thisorder[0]['id_cart'].'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE FROM '._DB_PREFIX_.'order_history WHERE id_order="'.(int)$id.'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE FROM '._DB_PREFIX_.'order_discount WHERE id_order="'.(int)$id.'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE FROM '._DB_PREFIX_.'order_detail WHERE id_order="'.(int)$id.'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

				$q = 'DELETE FROM '._DB_PREFIX_.'orders WHERE id_order="'.(int)$id.'"';
				if (!Db::getInstance()->Execute($q))

					$this->errorlog[] = $this->l('ERROR');

			}
		}

	}

	/* Retrocompatibility image*/
	public function hookwatermark($params)
	{
		$this->hookActionWatermark($params);
	}

	public function hookActionWatermark($params)
	{
		if (Configuration::get('PRESTASPEED_SMUSH') == 1)
		{
			/*defined('SMUSHIT_VERSION') or define('SMUSHIT_VERSION', '0.3');
			defined('SMUSHIT_USER_AGENT') or define('SMUSHIT_USER_AGENT', 'SMUSHIT '.SMUSHIT_VERSION);
			defined('SMUSHIT_URL') or define('SMUSHIT_URL', 'http://www.smushit.com/ysmush.it/ws.php');
			defined('SMUSHIT_WINDOW') or define('SMUSHIT_WINDOW', 5);*/
			//require_once _PS_MODULE_DIR_.$this->name.'/libs/RollingCurl/RollingCurl.php';

			$image             = new Image($params['id_image']);
			$image->id_product = $params['id_product'];

			if (_PS_VERSION_ > '1.4.5.0')
				$images_list = glob(_PS_PROD_IMG_DIR_.$image->getExistingImgPath().'*.{jpg,jpeg,png,gif,JPG}', GLOB_BRACE);
			else
				$images_list = glob(_PS_PROD_IMG_DIR_.$params['id_product'].'-'.$params['id_image'].'*.{jpg,jpeg,png,gif,JPG}', GLOB_BRACE);

			//$output = print_r($images_list, true);
			//$this->savetolog($output);
			require_once _PS_MODULE_DIR_.$this->name.'/smushit.inc.php';

			$query = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'smush (`id_smush` int(2) NOT NULL, `url` varchar(255) NULL, `smushed` TINYINT(1) NOT NULL,`saved` varchar(255) NULL, PRIMARY KEY(`url`)) ENGINE=MyISAM default CHARSET=utf8';
			Db::getInstance()->Execute($query);
			$total   = '';
			$smushit = new SmushIt();
			$url     = 'http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__;
			//    $files2 = $this->rglob('{*.jpg,*.png,*.gif}', $type, GLOB_BRACE);
//var_dump($files2);
			//Configuration::updateValue('PRESTASPEED_TOTFIL', count($images_list));
			//$fileschunk = array_chunk($files2, 40, false);
// Make batches of 3 images
//$files = array_chunk($files, 3);
			$start_time         = 0;
			$max_execution_time = 3337200;
			/* $process =
						array(
							array('type' => 'categories', 'dir' => _PS_CAT_IMG_DIR_),
							array('type' => 'manufacturers', 'dir' => _PS_MANU_IMG_DIR_),
							array('type' => 'suppliers', 'dir' => _PS_SUPP_IMG_DIR_),
							array('type' => 'scenes', 'dir' => _PS_SCENE_IMG_DIR_),
							array('type' => 'products', 'dir' => _PS_PROD_IMG_DIR_),
							array('type' => 'stores', 'dir' => _PS_STORE_IMG_DIR_)
						);*/
// Take a batch of three files
			$start_time = time();
			ini_set('max_execution_time', $max_execution_time); // ini_set may be disabled, we need the real value
			$max_execution_time = (int)ini_get('max_execution_time');
			@set_time_limit(0);
			$i = 0;

			/**/
			//$cdir = scandir('img/');
			//print_r( $cdir);

			/**/
			foreach ($images_list as $fil)
			{


				$i++;

				$query = 'INSERT IGNORE INTO '._DB_PREFIX_.'smush (`id_smush`, `url`, `smushed`, `saved`) VALUES (\''.(int)$i.'\', \''.pSQL((string)$fil).'\', \'0\', \'0\')';
				if (!Db::getInstance()->Execute($query));

				$query2 = 'SELECT * FROM '._DB_PREFIX_.'smush WHERE `url` = \''.pSQL((string)$fil).'\' AND `smushed` = 0';
				$quer   = Db::getInstance()->ExecuteS($query2);
				//    $output = print_r($remote_result, true);
				//$this->savetolog($output);
				if ($quer != null)
				{

					$remote_result = $smushit->compress($fil);

					$a = 0;
					if (!empty($remote_result->error) || !empty($remote_result->dest_size) == -1)
					{
						$upd = 'UPDATE '._DB_PREFIX_.'smush SET `smushed` = \'-1\' WHERE  '._DB_PREFIX_.'smush.url =  \''.pSQL((string)$fil).'\'';
						Db::getInstance()->Execute($upd);
					}
					if (!empty($remote_result->dest))
					{
						if ($remote_result->dest != null && $remote_result->dest_size != -1)
						{

							$file    = $remote_result->dest;
							$newfile = $fil;

							if (copy($file, $newfile))
							{

								$total = $remote_result->src_size - $remote_result->dest_size;
								$upd   = 'UPDATE '._DB_PREFIX_.'smush SET `smushed` = \'1\', `saved` = \''.(float)$total.'\' WHERE  '._DB_PREFIX_.'smush.url =  \''.str_replace('../../', '', pSQL((string)$fil)).'\'';
								Db::getInstance()->Execute($upd);

							}

							$a++;

						}
					}
				}
				$bytes = 'SELECT sum(saved) as total FROM '._DB_PREFIX_.'smush';
				$bytt  = Db::getInstance()->ExecuteS($bytes);
				Configuration::updateValue('PRESTASPEED_TOTCOMP', ($bytt[0]['total'] * 1) / 1024);
				//Configuration::updateValue('PRESTASPEED_TOTSMUSH', $i);
				if (time() - $start_time > $max_execution_time - 4) // stop 4 seconds before the timeout, just enough time to process the end of the page on a slow server
					return 'timeout';

			}
			/*
			if (!isset( $images_list) || !sizeof( $images_list)>0)
				return false;

			set_time_limit(9000);
			ini_set("max_execution_time", "9000");

			$this->_images = array();

			$rc = new RollingCurl(array(&$this, 'request_callback'));
			$rc->window_size = SMUSHIT_WINDOW;


			foreach ($images_list as $key=>$imageFile) {
				$this->_images[$key] = array('src' => $imageFile);
				$request = new RollingCurlRequest(SMUSHIT_URL, $method = "POST", array('files[]' => '@'.$imageFile));
				$request->userdata = $key;
				$rc->add($request);
			}
			$rc->execute();
			$rc->__destruct();

			$rc = new RollingCurl(array(&$this, 'save_callback'));
			$rc->window_size = SMUSHIT_WINDOW;
			foreach ($this->_images as $key=>$image) {
				if (isset($image['dst']) && isset($image['saving']) && $image['saving'] > 0) {
					$request = new RollingCurlRequest($image['dst']);
					$request->userdata = $key;
					$rc->add($request);
				}
			}

			if (sizeof($rc->requests) >= 1) {
				$rc->execute();
				$rc->__destruct();
			}

			return true;
			*/

		}

	}
	/*old code to generate smush*/
	/*
	public function save_callback($response, $info, $request)
	{
		$output = print_r($response, true);
		$this->savetolog($output);
		$handle = @fopen($this->_images[$request->userdata]['src'], 'wb');
		if (!$handle) {
			return array(
				'error' => 'Save error'
			);
			return false;
		}

		$this->_images[$request->userdata]['saved'] = true;

		fwrite($handle, $response);
		fclose($handle);

		Configuration::updateValue('PRESTASPEED_TOTCOMPF', Configuration::get('PRESTASPEED_TOTCOMPF') + $this->_images[$request->userdata]['saving']);

		return true;
	}

	public function request_callback($response, $info, $request)
	{
		if ($info['http_code'] <> '200')
			return false;


		$response = json_decode($response);

		$output = print_r($response, true);
		$this->savetolog($output);

		if ( -1 === intval($response->dest_size)) {
			return array(
				'error' => print_r($response, true)
			);
			return false;
		}

		if (!$response->dest) {
			return array(
				'error' => print_r($response, true)
			);
			return false;
		}

		$this->_images[$request->userdata]['dst'] = $response->dest;
		$this->_images[$request->userdata]['saving'] = $response->src_size - $response->dest_size;
		return true;
	}
	*/
	public function savetolog($data)
	{
		file_put_contents(dirname(__FILE__).'/log.txt', $data, FILE_APPEND);
	}

	public function removeHtaccessSection()
	{
		$key1 = '#Prestaspeed addon start';
		$key2 = '#Prestaspeed addon end';
		$path = _PS_ROOT_DIR_.'/.htaccess';
		if (file_exists($path) && is_writable($path))
		{
			$s  = Tools::file_get_contents($path);
			$p1 = strpos($s, $key1);
			$p2 = strpos($s, $key2, $p1);
			if ($p1 === false || $p2 === false)
				return false;
			$s = Tools::substr($s, 0, $p1).Tools::substr($s, $p2 + Tools::strlen($key2));
			file_put_contents($path, $s);
		}

		return true;
	}

	public function getExistingImgPath()
	{
		if (!$this->id)
			return false;

		if (!$this->existing_path)
		{
			if (Configuration::get('PS_LEGACY_IMAGES') && file_exists(_PS_PROD_IMG_DIR_.$this->id_product.'-'.$this->id.'.'.$this->image_format))
				$this->existing_path = $this->id_product.'-'.$this->id;
			else
				$this->existing_path = $this->getImgPath();
		}

		return $this->existing_path;
	}

	public static function getImgFolderStatic($id_image)
	{
		if (!is_numeric($id_image))
			return false;
		$folders = str_split((string)$id_image);
		return implode('/', $folders).'/';
	}

	public function getImgPath()
	{
		if (!$this->id)
			return false;

		$path = $this->getImgFolder().$this->id;
		return $path;
	}

	public function getImgFolder()
	{
		if (!$this->id)
			return false;

		if (!$this->folder)
			$this->folder = self::getImgFolderStatic($this->id);

		return $this->folder;
	}

	public static function checkAndFix()
	{
		$db   = Db::getInstance();
		$logs = array();

		// Remove doubles in the configuration
		$filtered_configuration = array();
		$result                 = $db->ExecuteS('SELECT * FROM '._DB_PREFIX_.'configuration');
		foreach ($result as $row)
		{
			$key = $row['id_shop_group'].'-|-'.$row['id_shop'].'-|-'.$row['name'];
			if (in_array($key, $filtered_configuration))
			{
				$query = 'DELETE FROM '._DB_PREFIX_.'configuration WHERE id_configuration = '.(int)$row['id_configuration'];
				$db->Execute($query);
				$logs[$query] = 1;
			}
			else
				$filtered_configuration[] = $key;
		}
		unset($filtered_configuration);

		// Remove inexisting or monolanguage configuration value from configuration_lang
		$query = 'DELETE FROM `'._DB_PREFIX_.'configuration_lang`
		WHERE `id_configuration` NOT IN (SELECT `id_configuration` FROM `'._DB_PREFIX_.'configuration`)
		OR `id_configuration` IN (SELECT `id_configuration` FROM `'._DB_PREFIX_.'configuration` WHERE name IS NULL OR name = "")';
		if ($db->Execute($query))
			if ($affected_rows = $db->Affected_Rows())
				$logs[$query] = $affected_rows;

		// Simple Cascade Delete
		$queries = array(
			// 0 => DELETE FROM __table__, 1 => WHERE __id__ NOT IN, 2 => NOT IN __table__, 3 => __id__ used in the "NOT IN" table, 4 => module_name
			array('access', 'id_profile', 'profile', 'id_profile'),
			array('access', 'id_tab', 'tab', 'id_tab'),
			array('accessory', 'id_product_1', 'product', 'id_product'),
			array('accessory', 'id_product_2', 'product', 'id_product'),
			array('address_format', 'id_country', 'country', 'id_country'),
			array('attribute', 'id_attribute_group', 'attribute_group', 'id_attribute_group'),
			array('carrier_group', 'id_carrier', 'carrier', 'id_carrier'),
			array('carrier_group', 'id_group', 'group', 'id_group'),
			array('carrier_zone', 'id_carrier', 'carrier', 'id_carrier'),
			array('carrier_zone', 'id_zone', 'zone', 'id_zone'),
			array('cart_cart_rule', 'id_cart', 'cart', 'id_cart'),
			array('cart_product', 'id_cart', 'cart', 'id_cart'),
			array('cart_rule_carrier', 'id_cart_rule', 'cart_rule', 'id_cart_rule'),
			array('cart_rule_carrier', 'id_carrier', 'carrier', 'id_carrier'),
			array('cart_rule_combination', 'id_cart_rule_1', 'cart_rule', 'id_cart_rule'),
			array('cart_rule_combination', 'id_cart_rule_2', 'cart_rule', 'id_cart_rule'),
			array('cart_rule_country', 'id_cart_rule', 'cart_rule', 'id_cart_rule'),
			array('cart_rule_country', 'id_country', 'country', 'id_country'),
			array('cart_rule_group', 'id_cart_rule', 'cart_rule', 'id_cart_rule'),
			array('cart_rule_group', 'id_group', 'group', 'id_group'),
			array('cart_rule_product_rule_group', 'id_cart_rule', 'cart_rule', 'id_cart_rule'),
			array(
				'cart_rule_product_rule',
				'id_product_rule_group',
				'cart_rule_product_rule_group',
				'id_product_rule_group'
			),
			array(
				'cart_rule_product_rule_value',
				'id_product_rule',
				'cart_rule_product_rule',
				'id_product_rule'
			),
			array('category_group', 'id_category', 'category', 'id_category'),
			array('category_group', 'id_group', 'group', 'id_group'),
			array('category_product', 'id_category', 'category', 'id_category'),
			array('category_product', 'id_product', 'product', 'id_product'),
			array('cms', 'id_cms_category', 'cms_category', 'id_cms_category'),
			array('cms_block', 'id_cms_category', 'cms_category', 'id_cms_category', 'blockcms'),
			array('cms_block_page', 'id_cms', 'cms', 'id_cms', 'blockcms'),
			array('cms_block_page', 'id_cms_block', 'cms_block', 'id_cms_block', 'blockcms'),
			array('compare', 'id_customer', 'customer', 'id_customer'),
			array('compare_product', 'id_compare', 'compare', 'id_compare'),
			array('compare_product', 'id_product', 'product', 'id_product'),
			array('connections', 'id_shop_group', 'shop_group', 'id_shop_group'),
			array('connections', 'id_shop', 'shop', 'id_shop'),
			array('connections_page', 'id_connections', 'connections', 'id_connections'),
			array('connections_page', 'id_page', 'page', 'id_page'),
			array('connections_source', 'id_connections', 'connections', 'id_connections'),
			array('customer', 'id_shop_group', 'shop_group', 'id_shop_group'),
			array('customer', 'id_shop', 'shop', 'id_shop'),
			array('customer_group', 'id_group', 'group', 'id_group'),
			array('customer_group', 'id_customer', 'customer', 'id_customer'),
			array(
				'customer_message',
				'id_customer_thread',
				'customer_thread',
				'id_customer_thread'
			),
			array('customer_thread', 'id_shop', 'shop', 'id_shop'),
			array('customization', 'id_cart', 'cart', 'id_cart'),
			array('customization_field', 'id_product', 'product', 'id_product'),
			array('customized_data', 'id_customization', 'customization', 'id_customization'),
			array('delivery', 'id_shop', 'shop', 'id_shop'),
			array('delivery', 'id_shop_group', 'shop_group', 'id_shop_group'),
			array('delivery', 'id_carrier', 'carrier', 'id_carrier'),
			array('delivery', 'id_zone', 'zone', 'id_zone'),
			array('editorial', 'id_shop', 'shop', 'id_shop', 'editorial'),
			array('favorite_product', 'id_product', 'product', 'id_product', 'favoriteproducts'),
			array('favorite_product', 'id_customer', 'customer', 'id_customer', 'favoriteproducts'),
			array('favorite_product', 'id_shop', 'shop', 'id_shop', 'favoriteproducts'),
			array('feature_product', 'id_feature', 'feature', 'id_feature'),
			array('feature_product', 'id_product', 'product', 'id_product'),
			array('feature_value', 'id_feature', 'feature', 'id_feature'),
			array('group_reduction', 'id_group', 'group', 'id_group'),
			array('group_reduction', 'id_category', 'category', 'id_category'),
			array('homeslider', 'id_shop', 'shop', 'id_shop', 'homeslider'),
			array(
				'homeslider',
				'id_homeslider_slides',
				'homeslider_slides',
				'id_homeslider_slides',
				'homeslider'
			),
			array('hook_module', 'id_hook', 'hook', 'id_hook'),
			array('hook_module', 'id_module', 'module', 'id_module'),
			array('hook_module_exceptions', 'id_hook', 'hook', 'id_hook'),
			array('hook_module_exceptions', 'id_module', 'module', 'id_module'),
			array('hook_module_exceptions', 'id_shop', 'shop', 'id_shop'),
			array('image', 'id_product', 'product', 'id_product'),
			array('message', 'id_cart', 'cart', 'id_cart'),
			array('message_readed', 'id_message', 'message', 'id_message'),
			array('message_readed', 'id_employee', 'employee', 'id_employee'),
			array('orders', 'id_shop', 'shop', 'id_shop'),
			array('orders', 'id_shop_group', 'group_shop', 'id_shop_group'),
			array('order_carrier', 'id_order', 'orders', 'id_order'),
			array('order_cart_rule', 'id_order', 'orders', 'id_order'),
			array('order_detail', 'id_order', 'orders', 'id_order'),
			array('order_detail_tax', 'id_order_detail', 'order_detail', 'id_order_detail'),
			array('order_history', 'id_order', 'orders', 'id_order'),
			array('order_invoice', 'id_order', 'orders', 'id_order'),
			array('order_invoice_payment', 'id_order', 'orders', 'id_order'),
			array('order_invoice_tax', 'id_order_invoice', 'order_invoice', 'id_order_invoice'),
			array('order_return', 'id_order', 'orders', 'id_order'),
			array('order_return_detail', 'id_order_return', 'order_return', 'id_order_return'),
			array('order_slip', 'id_order', 'orders', 'id_order'),
			array('order_slip_detail', 'id_order_slip', 'order_slip', 'id_order_slip'),
			array('pack', 'id_product_pack', 'product', 'id_product'),
			array('pack', 'id_product_item', 'product', 'id_product'),
			array('page', 'id_page_type', 'page_type', 'id_page_type'),
			array('page_viewed', 'id_shop', 'shop', 'id_shop'),
			array('page_viewed', 'id_shop_group', 'shop_group', 'id_shop_group'),
			array('page_viewed', 'id_date_range', 'date_range', 'id_date_range'),
			array('product_attachment', 'id_attachment', 'attachment', 'id_attachment'),
			array('product_attachment', 'id_product', 'product', 'id_product'),
			array('product_attribute', 'id_product', 'product', 'id_product'),
			array(
				'product_attribute_combination',
				'id_product_attribute',
				'product_attribute',
				'id_product_attribute'
			),
			array('product_attribute_combination', 'id_attribute', 'attribute', 'id_attribute'),
			array('product_attribute_image', 'id_image', 'image', 'id_image'),
			array(
				'product_attribute_image',
				'id_product_attribute',
				'product_attribute',
				'id_product_attribute'
			),
			array('product_carrier', 'id_product', 'product', 'id_product'),
			array('product_carrier', 'id_shop', 'shop', 'id_shop'),
			array('product_carrier', 'id_carrier_reference', 'carrier', 'id_reference'),
			array('product_country_tax', 'id_product', 'product', 'id_product'),
			array('product_country_tax', 'id_country', 'country', 'id_country'),
			array('product_country_tax', 'id_tax', 'tax', 'id_tax'),
			array('product_download', 'id_product', 'product', 'id_product'),
			array('product_group_reduction_cache', 'id_product', 'product', 'id_product'),
			array('product_group_reduction_cache', 'id_group', 'group', 'id_group'),
			array('product_sale', 'id_product', 'product', 'id_product'),
			array('product_supplier', 'id_product', 'product', 'id_product'),
			array('product_supplier', 'id_supplier', 'supplier', 'id_supplier'),
			array('product_tag', 'id_product', 'product', 'id_product'),
			array('product_tag', 'id_tag', 'tag', 'id_tag'),
			array('range_price', 'id_carrier', 'carrier', 'id_carrier'),
			array('range_weight', 'id_carrier', 'carrier', 'id_carrier'),
			array('referrer_cache', 'id_referrer', 'referrer', 'id_referrer'),
			array(
				'referrer_cache',
				'id_connections_source',
				'connections_source',
				'id_connections_source'
			),
			array('scene_category', 'id_scene', 'scene', 'id_scene'),
			array('scene_category', 'id_category', 'category', 'id_category'),
			array('scene_products', 'id_scene', 'scene', 'id_scene'),
			array('scene_products', 'id_product', 'product', 'id_product'),
			array('search_index', 'id_product', 'product', 'id_product'),
			array('search_word', 'id_lang', 'lang', 'id_lang'),
			array('search_word', 'id_shop', 'shop', 'id_shop'),
			array('shop_url', 'id_shop', 'shop', 'id_shop'),
			array('specific_price_priority', 'id_product', 'product', 'id_product'),
			array('stock', 'id_warehouse', 'warehouse', 'id_warehouse'),
			array('stock', 'id_product', 'product', 'id_product'),
			array('stock_available', 'id_product', 'product', 'id_product'),
			array('stock_mvt', 'id_stock', 'stock', 'id_stock'),
			array('tab_module_preference', 'id_employee', 'employee', 'id_employee'),
			array('tab_module_preference', 'id_tab', 'tab', 'id_tab'),
			array('tax_rule', 'id_country', 'country', 'id_country'),
			array('theme_specific', 'id_theme', 'theme', 'id_theme'),
			array('theme_specific', 'id_shop', 'shop', 'id_shop'),
			array('warehouse_carrier', 'id_warehouse', 'warehouse', 'id_warehouse'),
			array('warehouse_carrier', 'id_carrier', 'carrier', 'id_carrier'),
			array('warehouse_product_location', 'id_product', 'product', 'id_product'),
			array('warehouse_product_location', 'id_warehouse', 'warehouse', 'id_warehouse'),
		);

		$queries = self::bulle($queries);
		foreach ($queries as $query_array)
		{
			// If this is a module and the module is not installed, we continue
			if (isset($query_array[4]) && !Module::isInstalled($query_array[4]))
				continue;

			$query = 'DELETE FROM `'._DB_PREFIX_.$query_array[0].'` WHERE `'.$query_array[1].'` NOT IN (SELECT `'.$query_array[3].'` FROM `'._DB_PREFIX_.$query_array[2].'`)';
			if ($db->Execute($query))
				if ($affected_rows = $db->Affected_Rows())
					$logs[$query] = $affected_rows;
		}

		// _lang table cleaning
		$tables = Db::getInstance()->executeS('SHOW TABLES LIKE "'.preg_replace('/([%_])/', '\\$1', _DB_PREFIX_).'%_\\_lang"');
		foreach ($tables as $table)
		{
			$table_lang = current($table);
			$table      = str_replace('_lang', '', $table_lang);
			$id_table   = 'id_'.preg_replace('/^'._DB_PREFIX_.'/', '', $table);

			$query = 'DELETE FROM `'.bqSQL($table_lang).'` WHERE `'.bqSQL($id_table).'` NOT IN (SELECT `'.bqSQL($id_table).'` FROM `'.bqSQL($table).'`)';
			if ($db->Execute($query))
				if ($affected_rows = $db->Affected_Rows())
					$logs[$query] = $affected_rows;

			$query = 'DELETE FROM `'.bqSQL($table_lang).'` WHERE `id_lang` NOT IN (SELECT `id_lang` FROM `'._DB_PREFIX_.'lang`)';
			if ($db->Execute($query))
				if ($affected_rows = $db->Affected_Rows())
					$logs[$query] = $affected_rows;
		}

		// _shop table cleaning
		$tables = Db::getInstance()->executeS('SHOW TABLES LIKE "'.preg_replace('/([%_])/', '\\$1', _DB_PREFIX_).'%_\\_shop"');
		foreach ($tables as $table)
		{
			$table_shop = current($table);
			$table      = str_replace('_shop', '', $table_shop);
			$id_table   = 'id_'.preg_replace('/^'._DB_PREFIX_.'/', '', $table);

			if (in_array($table_shop, array(_DB_PREFIX_.'carrier_tax_rules_group_shop')))
				continue;

			$query = 'DELETE FROM `'.bqSQL($table_shop).'` WHERE `'.bqSQL($id_table).'` NOT IN (SELECT `'.bqSQL($id_table).'` FROM `'.bqSQL($table).'`)';
			if ($db->Execute($query))
				if ($affected_rows = $db->Affected_Rows())
					$logs[$query] = $affected_rows;

			$query = 'DELETE FROM `'.bqSQL($table_shop).'` WHERE `id_shop` NOT IN (SELECT `id_shop` FROM `'._DB_PREFIX_.'shop`)';
			if ($db->Execute($query))
				if ($affected_rows = $db->Affected_Rows())
					$logs[$query] = $affected_rows;
		}

		// stock_available
		$query = 'DELETE FROM `'._DB_PREFIX_.'stock_available` WHERE `id_shop` NOT IN (SELECT `id_shop` FROM `'._DB_PREFIX_.'shop`) AND `id_shop_group` NOT IN (SELECT `id_shop_group` FROM `'._DB_PREFIX_.'shop_group`)';
		if ($db->Execute($query))
			if ($affected_rows = $db->Affected_Rows())
				$logs[$query] = $affected_rows;

		Category::regenerateEntireNtree();



		return $logs;
	}

	protected static function bulle($array)
	{
		$sorted = false;
		$size   = count($array);
		while (!$sorted)
		{
			$sorted = true;
			for ($i = 0; $i < $size - 1; ++$i)
				for ($j = $i + 1; $j < $size; ++$j)
					if ($array[$i][2] == $array[$j][0])
					{
						$tmp       = $array[$i];
						$array[$i] = $array[$j];
						$array[$j] = $tmp;
						$sorted    = false;
					}
		}
		return $array;
	}
}

?>