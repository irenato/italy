{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* No redistribute in other sites, or copy.
*
*  @author RSI <rsi_2004>
*  @copyright  2007-2015 RSI
*} 



<div class="alert alert-message"> 
<p><span style="color:#093; font-weight:bolder">Tip: </span>{l s='If enable back office optimization, sometimes you dont see the changes in cms. Simply, disable BO optimization.' mod='prestaspeed'}</p>
<p><span style="color:#093; font-weight:bolder">Tip: </span>{l s='If the optimization process of the images don`t finish, and you see a white screen or an internal server error, just press F5 until you back to the module configuration.Remember, image optimization uses smushit service, if the service is down, Prestaspeed can`t optimize the images.' mod='prestaspeed'}</p><br/>
<center><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/readme.png" style="  width: 31px;margin: 5px;" /><a href="{$module_dir|escape:'htmlall':'UTF-8'}moduleinstall.pdf" target="_blank">README</a> / 
		<img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/terms.png" style="  width: 31px;margin: 5px;" /><a href="{$module_dir|escape:'htmlall':'UTF-8'}termsandconditions.pdf" target="_blank">TERMS</a></center><br/>

</div>
