<?php
/*
* 2009-2016 Singsys Pte. Ltd.
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* It is available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* DISCLAIMER
* This code is provided as is without any warranty.
* No promise of being safe or secure
*
*  @author      Singsys Pte. Ltd. <info@singsys.com>
*  @copyright   2009-2016 Singsys Pte. Ltd.
*  @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

if (!defined('_PS_VERSION_'))
	exit;

class SocialConnectLinkModuleFrontController extends ModuleFrontController
{
	public $display_column_left = false;
	public $ssl = true;

	/**
	* @see FrontController::initContent()
	*/
	public function initContent()
	{
		parent::initContent();

		if (!$this->context->customer->isLogged())
		{
			$back = $this->context->link->getModuleLink('socialconnect', 'link', array(), TRUE, $this->context->language->id);
			Tools::redirect('index.php?controller=authentication&back='.urlencode($back));

		}

		$social_connect_fbappid = (Configuration::get('SOCIAL_CONNECT_FBAPPID'));
		$social_connect_fbappkey = (Configuration::get('SOCIAL_CONNECT_FBAPPKEY'));

		require_once(_PS_ROOT_DIR_.'/modules/socialconnect/fb_sdk/facebook.php');

		$facebook = new Facebook(array(
			'appId'  => $social_connect_fbappid,
			'secret' => $social_connect_fbappkey,
			));

		// Get User ID
		$user = $facebook->getUser();

		if ($user)
		{
			try {
				// Proceed knowing you have a logged in user who's authenticated.
				$fb_user_profile = $facebook->api('/me?fields=name,email,id,gender,first_name,last_name');
			} catch (FacebookApiException $e) {
				error_log($e);
				$user = null;
			}
		}
		else
		{
			// Get new Access tokens
			Tools::redirect($facebook->getLoginUrl(array('scope' => 'email')));
		}

		// current user state Logged In with FB
		if (!$user || !$fb_user_profile['id'])
		{
			// Get new Access tokens
			Tools::redirect($facebook->getLoginUrl(array('scope' => 'email')));
		}

		$sql = 'SELECT `id_customer`
		FROM `'._DB_PREFIX_.'customer_profile_connect`
		WHERE `facebook_id` = \''.(int)$fb_user_profile['id'].'\''
		. Shop::addSqlRestriction(Shop::SHARE_CUSTOMER);

		$customer_id = Db::getInstance()->getValue($sql);

		if ($customer_id > 0 && $customer_id != $this->context->customer->id)
		{
			if($this->context->language->id == 1){
				$socialconnect_fb_massage = 'The Facebook account is already linked to another account.';
			}
			if($this->context->language->id == 2){

				$socialconnect_fb_massage = 'L\'account di Facebook è già collegato a un altro account.';
			}
			if($this->context->language->id != 1 && $this->context->language->id != 2){
				$socialconnect_fb_massage = 'The translation of this message is not';
			}

			$this->context->smarty->assign(array(
				'socialconnect_fb_status' => 'error',
				'socialconnect_fb_massage' => $socialconnect_fb_massage,
				'socialconnect_fb_picture' => 'https://graph.facebook.com/'.$fb_user_profile['id'].'/picture',
				'socialconnect_fb_name' => $fb_user_profile['name'],
				'fb_user_profile' => $fb_user_profile
				));
		}
		else if ($customer_id == $this->context->customer->id)
		{

			if($this->context->language->id == 1){
				$socialconnect_fb_massage = 'The Facebook account is already linked to your account.';
			}
			if($this->context->language->id == 2){
				$socialconnect_fb_massage = 'L\'account di Facebook è già collegato al tuo account.';
			}
			if($this->context->language->id != 1 && $this->context->language->id != 2){
				$socialconnect_fb_massage = 'The translation of this message is not';
			}

			$this->context->smarty->assign(array(
				'socialconnect_fb_status' => 'linked',
				'socialconnect_fb_massage' => $socialconnect_fb_massage,
				'socialconnect_fb_picture' => 'https://graph.facebook.com/'.$fb_user_profile['id'].'/picture',
				'socialconnect_fb_name' => $fb_user_profile['name'],
				'fb_user_profile' => $fb_user_profile
				));
		}
		else
		{
			if($fb_user_profile['email'] != $this->context->customer->email)
			{
					// The message
				$message = 'Email address on files was not the same as the Facebook account.';
				$message .= 'customer ID: '. print_r($this->context->customer->id, true);
				$message .= "\n\n";
				$message .= 'user info:'. print_r($fb_user_profile, true);
				$message .= "\n\n";

					// In case any of our lines are larger than 70 characters, we should use wordwrap()
				$message = wordwrap($message, 70, "\r\n");
				@mail(Configuration::get('PS_SHOP_EMAIL'), 'socialconnect: error #1 log', $message);
			}

			$sql = 'SELECT `facebook_id`
			FROM `'._DB_PREFIX_.'customer_profile_connect`
			WHERE `id_customer` = \''.(int)$this->context->customer->id.'\' AND `id_shop` = '
			. (int)$this->context->getContext()->shop->id;

			$facebook_id = Db::getInstance()->getValue($sql);

			if(!$facebook_id)
			{
				Db::getInstance()->insert('customer_profile_connect',array( 'id_customer' => (int)$this->context->customer->id, 'facebook_id' => (int)$fb_user_profile['id']));

				$this->context->smarty->assign(array(
					'socialconnect_fb_status' => 'conform',
					'socialconnect_fb_massage' => 'Your Facebook account has been linked to account.',
					'socialconnect_fb_picture' => 'https://graph.facebook.com/'.$fb_user_profile['id'].'/picture',
					'socialconnect_fb_name' => $fb_user_profile['name'],
					'fb_user_profile' => $fb_user_profile
					));
			}
			else
			{
// This could happen if the user logged off from FB but not the prestashop
// And 2nd user logs in to facebook than opens this page.
				$this->context->smarty->assign(array(
					'socialconnect_fb_status' => 'error',
					'socialconnect_fb_massage' => 'Sorry, there was a error when we tried to link your account with Facebook. Our Site admin has been notified of error, once it\'s resolved you will be sent a email notice.',
					));

					// The message
				$message = 'customer ID: '. print_r($this->context->customer->id, true);
				$message .= "\n\n";
				$message .= 'user info:'. print_r($fb_user_profile, true);
				$message .= "\n\n";

					// In case any of our lines are larger than 70 characters, we should use wordwrap()
				$message = wordwrap($message, 70, "\r\n");
				@mail(Configuration::get('PS_SHOP_EMAIL'), 'socialconnect: error #2 log', $message);
			}
		}

		$this->setTemplate('link_fb.tpl');
	}
}