{*
* 2009-2016 Singsys Pte. Ltd.
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* It is available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* DISCLAIMER
* This code is provided as is without any warranty.
* No promise of being safe or secure
*
*  @author      Singsys Pte. Ltd. <info@singsys.com>
*  @copyright   2009-2016 Singsys Pte. Ltd.
*  @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}

{include file="$tpl_dir./errors.tpl"}

<br/>
<!-- socialconnect login_fb -->