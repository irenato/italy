{*
* 2009-2016 Singsys Pte. Ltd.
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* It is available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* DISCLAIMER
* This code is provided as is without any warranty.
* No promise of being safe or secure
*
*  @author      Singsys Pte. Ltd. <info@singsys.com>
*  @copyright   2009-2016 Singsys Pte. Ltd.
*  @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
{capture name=path}{l s='Authentication'}{/capture}
{include file="$tpl_dir./errors.tpl"}
<div class="row">
<div class="col-xs-12 col-sm-6">
    <form action="{$link->getModuleLink('socialconnect', 'registration')|escape:'html':'UTF-8'}" method="post" id="create-account_form" class="box">
        <h3 class="page-subheading">{l s='Create an account'}</h3>
        <div class="form_content clearfix">
            <p>{l s='Please enter your email address to create an account.'}</p>
            <div class="alert alert-danger" id="create_account_error" style="display:none"></div>
            <div class="form-group">
                <label for="email_create">{l s='Email address'}</label>
                <input type="email" class="is_required validate account_input form-control" data-validate="isEmail" id="email_create" name="email_create" value="{if isset($smarty.post.email_create)}{$smarty.post.email_create|stripslashes}{/if}" />
            </div>
            <div class="submit">
                {if isset($back)}<input type="hidden" class="hidden" name="back" value="{$back|escape:'html':'UTF-8'}" />{/if}
                <button class="btn btn-default button button-medium exclusive" type="submit" id="SubmitCreate" name="SubmitCreate">
                    <span>
                        <i class="icon-user left"></i>
                        {l s='Create an account'}
                    </span>
                </button>
                <input type="hidden" class="hidden" name="SubmitCreate" value="{l s='Create an account'}" />
            </div>
            <input type="hidden" name="code" id="code" value="{$social_connect_code}"/>
            <input type="hidden" name="state" id="state" value="{$social_connect_state}"/>
        </div>
    </form>
</div>
</div>
<div id="fb-root"></div><script src="{$protocol_content}connect.facebook.net/en_US/all.js#appId={$social_connect_fbappid}&xfbml=1"></script>
<div class="clear">
{literal}
<fb:registration 
	fields='[
		{"name":"name"},
		{"name":"first_name"},
		{"name":"last_name"},
		{"name":"email"},
		{"name":"password"},
		{"name":"birthday"},
		{"name":"gender"}]'
		redirect_uri="{/literal}{$redirect_uri}{literal}" width="530"></fb:registration>
{/literal}
</div>