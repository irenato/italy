{*
* 2009-2016 Singsys Pte. Ltd.
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* It is available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* DISCLAIMER
* This code is provided as is without any warranty.
* No promise of being safe or secure
*
*  @author      Singsys Pte. Ltd. <info@singsys.com>
*  @copyright   2009-2016 Singsys Pte. Ltd.
*  @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
{capture name=path}<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='My account' mod='socialconnect'}">{l s='My account' mod='socialconnect'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='Linked to Facebook' mod='socialconnect'}{/capture}
{include file="$tpl_dir./errors.tpl"}

<br/>
{if $socialconnect_fb_status == 'error'}
	<div class="error">
		<p>{$socialconnect_fb_massage}{if isset($socialconnect_fb_picture)}<br/><img src="{$socialconnect_fb_picture}">{$socialconnect_fb_name}{/if}</p>
	</div>
{else if $socialconnect_fb_status == 'linked' || $socialconnect_fb_status == 'conform'}
	<div class="success">
		<h3 class="page-heading">{$socialconnect_fb_massage}</h3>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-3">
					<img src="{$socialconnect_fb_picture}?width=140&height=140">
				</div>
				<div class="col-md-9">
					<p>{l s='Name' mod='socialconnect'} : <a href="{$fb_user_profile.link}" target="_blank">{$socialconnect_fb_name}</a></p>
					{if $fb_user_profile.email}<p>{l s='Email' mod='socialconnect'} : {$fb_user_profile.email}</p>{/if}
					<p>{l s='Gender' mod='socialconnect'} : {$fb_user_profile.gender|ucfirst}</p>
				</div>
			</div>
		</div>
	</div>
{else if $socialconnect_fb_status == 'login'}
	<div class="error">
		<p>{$socialconnect_fb_massage}<br/><a href="{$socialconnect_fb_loginURL}">{l s='Log in to Facebook'}</a></p>
	</div>
{else}
	<div class="error">
		<p>{l s='Sorry, there was error with Facebook Profile Connect.' mod='socialconnect'}</p>
	</div>
{/if}
<br/>