<?php

class Customer extends CustomerCore
{
	/**
	 * Logout
	 *
	 * @since 1.0.1
	 */
	public function logout()
	{
		if (file_exists(_PS_ROOT_DIR_.'/modules/socialconnect/fb_sdk/facebook.php'))
		{
			include(_PS_ROOT_DIR_.'/modules/socialconnect/fb_sdk/facebook.php');
			$facebook = new Facebook(array(
				'appId'  => Configuration::get('SOCIAL_CONNECT_FBAPPID'),
				'secret' => Configuration::get('SOCIAL_CONNECT_FBAPPKEY'),
			));
			$facebook->destroySession();
		}

		parent::logout();
	}

	/**
	 * Soft logout, delete everything links to the customer
	 *
	 */
	public function mylogout()
	{
		if (file_exists(_PS_ROOT_DIR_.'/modules/socialconnect/fb_sdk/facebook.php'))
		{
			include(_PS_ROOT_DIR_.'/modules/socialconnect/fb_sdk/facebook.php');
			$facebook = new Facebook(array(
				'appId'  => Configuration::get('SOCIAL_CONNECT_FBAPPID'),
				'secret' => Configuration::get('SOCIAL_CONNECT_FBAPPKEY'),
			));
			$facebook->destroySession();
		}

		parent::mylogout();
	}
}
