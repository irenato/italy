<?php
/*
* 2009-2016 Singsys Pte. Ltd.
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* It is available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* DISCLAIMER
* This code is provided as is without any warranty.
* No promise of being safe or secure
*
*  @author      Singsys Pte. Ltd. <info@singsys.com>
*  @copyright   2009-2016 Singsys Pte. Ltd.
*  @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

if (!defined('_PS_VERSION_'))
  exit;

class Socialconnect extends Module
{
	public function __construct()
	{
		$this->name = 'socialconnect';
		$this->tab = 'social_networks';
		$this->author = 'Singsys Pte. Ltd.';
		$this->version = '1.0.6';
		$this->bootstrap=true;

		$this->need_instance = 0;
		$this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.6');

		parent::__construct();

		$this->displayName = $this->l('Social Connect');
		$this->description = $this->l('This modules allows Customers to Register & Login with Facebook.');

		$this->dbUpdate_output = '';
	}

	public function install()
	{
		// Removed this because we have to use a override
		// $this->registerHook('DisplayTop') == false ||

		if (parent::install() == false ||
			$this->registerHook('displayCustomerAccountFormTop') == false ||
			$this->registerHook('displayCustomerAccount') == false ||
			$this->registerHook('displaySocialconnect') == false)
				return false;

		return Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'customer_profile_connect` (
			`id_customer` int(10) unsigned NOT NULL,
			`id_shop` int(11) NOT NULL DEFAULT \'1\',
			`facebook_id` varchar(50) NOT NULL,
			UNIQUE KEY `id_customer` (`id_customer`,`id_shop`))
			ENGINE=InnoDB DEFAULT CHARSET=latin1');
	}

	public function uninstall()
	{
		//TODO: see if you have to delete the Hook from install function
		
		Configuration::deleteByName('SOCIAL_CONNECT_FBAPPKEY');
		Configuration::deleteByName('SOCIAL_CONNECT_FBAPPID');
		if (!parent::uninstall())
			return false;

		// TODO: Should the table be deleted?
		//return Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'customer_profile_connect`');
		return true;
	}

	public function getContent()
	{
		$errors = array();$FBAPPIDUpdated=false;$FBAPPKEYUpdated=false;$confirmation='';

		if( $_SERVER["SERVER_NAME"] == "localhost" || $_SERVER["SERVER_NAME"] == "127.0.0.1" )
		{
			$errors[] = $this->l('NOTE: Social Connect will not work properly in localhost.');
		}
		//if( !Configuration::get('PS_SSL_ENABLED') )
		//{
		//	$errors[] = $this->l('NOTE: Social Connect will not work with out SSL.');
		//}
		if (!function_exists('curl_init'))
		{
			$errors[] = $this->l('Error: Social Connect needs the CURL PHP extension.');
		}
		if (!function_exists('json_decode'))
		{
			$errors[] = $this->l('Error: Social Connect needs the JSON PHP extension.');
		}
		if (!function_exists('hash_hmac'))
		{
			$errors[] = $this->l('Error: Social Connect needs the HMAC Hash (hash_hmac) PHP extension.');
		}

		$output = '<h2>'.$this->displayName.'</h2>';
		if (Tools::isSubmit('submitsocialKey'))
		{
			$social_connect_fbappid = (Tools::getValue('SOCIAL_CONNECT_FBAPPID'));
			if (!$social_connect_fbappid){
				$errors[] = $this->l('Facebook AppID is required.');
				$FBAPPIDUpdated = false;
			}
			else{
				Configuration::updateValue('SOCIAL_CONNECT_FBAPPID', $social_connect_fbappid);
				$FBAPPIDUpdated = true;
			}
			$social_connect_fbappkey = (Tools::getValue('SOCIAL_CONNECT_FBAPPKEY'));
			if (!$social_connect_fbappkey){
				$errors[] = $this->l('Facebook App Key is required.');
				$FBAPPKEYUpdated = false;
			}
			else{
				Configuration::updateValue('SOCIAL_CONNECT_FBAPPKEY', $social_connect_fbappkey);
				$FBAPPKEYUpdated = true;
			}
			
			if($FBAPPKEYUpdated==true && $FBAPPIDUpdated==true){
				$confirmation = $this->displayConfirmation($this->l('Settings updated'));
			}
		}
		
		if (isset($errors) AND sizeof($errors))
				$output .= $this->displayError(implode('<br />', $errors));

		return $output.$confirmation.$this->renderForm();
	}
	
	public function renderForm()
	{
		$info = '';
		if (Shop::getContext() === Shop::CONTEXT_SHOP)
			$info = 'Edit authentication.tpl place the link to Facebook login<br />You can put the login links anywhere.<br /><br />
			&lt;a title="Login with your Facebook Account" class="button_large" href="{$link-&gt;getModuleLink(\'socialconnect\', \'login\', array(), true)}"&gt;Facebook Login&lt/a&gt;</br>
			or you can use {hook h="displaySocialconnect"}</br></br>';

		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Facebook App details'),
					'icon' => 'icon-facebook-sign',
				),
				'description' => $info,
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Facebook AppID'),
						'name' => 'SOCIAL_CONNECT_FBAPPID',
						'required' => true,
					),
					array(
						'type' => 'text',
						'label' => $this->l('Facebook App Key'),
						'name' => 'SOCIAL_CONNECT_FBAPPKEY',
						'required' => true,
					),
					
				),
				'submit' => array(
					'title' => $this->l('Save'),
					'class' => 'btn btn-default pull-right'
				)
			)
		);
		
		
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->override_folder = '/';
		$helper->module = $this;
		$helper->submit_action = 'submitsocialKey';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array(
			$fields_form
		));
	}
	

	public function hookDisplayCustomerAccount($params)
	{
		$this->context->smarty->assign(array(
			'socialconnect_fb_link' => $this->context->link->getModuleLink('socialconnect', 'link', array(), false, $this->context->language->id)
		));

		return $this->display(__FILE__, 'customer-account.tpl');
	}
	
	public function hookDisplaySocialconnect($params)
	{
		return $this->hookDisplayCustomerAccountFormTop($params);
	}

	public function hookDisplayCustomerAccountFormTop($params)
	{
		$this->context->smarty->assign(array(
			'socialconnect_fb_reg_link' => $this->context->link->getModuleLink('socialconnect', 'registration', array(), true, $this->context->language->id)
		));

		return $this->display(__FILE__, 'customer-account-form-top.tpl');
	}


	function _dbUpdate()
	{
		$output = '';
	
		if(Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'customer_profile_connect` CHANGE `id_customer` `id_customer` INT( 10 ) UNSIGNED NOT NULL'))
			$output .= 'Removed AUTO INCREMENT</br>';
		else
			$output .= 'Error: could not remove AUTO INCREMENT</br>';

		if(Db::getInstance()->ExecuteS('SHOW INDEX FROM `'._DB_PREFIX_.'customer_profile_connect` WHERE Key_name = \'PRIMARY\''))
		{
			if(Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'customer_profile_connect` DROP PRIMARY KEY'))
				$output .= 'Droped the primary key</br>';
			else
				$output .= 'Error: Could not drop primary key</br>';
		}
		else
		{
			$output .= 'Note: Did not drop primary key because could not find any.</br>';
		}

		if(!Db::getInstance()->ExecuteS('SHOW INDEX FROM `'._DB_PREFIX_.'customer_profile_connect` WHERE Key_name = \'id_customer\''))
		{
			if(Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'customer_profile_connect` ADD UNIQUE (`id_customer` , `id_shop`)'))
				$output .= 'Added a new UNIQUE id_customer & id_shop</br>';
			else
				$output .= 'Error: Did not add UNIQUE id_customer & id_shop</br>';
		}
		else
		{
			$output .= 'Note: Index was already set to id_customer</br>';
		}
		
		return $output;
	}
	
	public function getConfigFieldsValues()
	{
		return array(
			'SOCIAL_CONNECT_FBAPPKEY' => Tools::getValue('SOCIAL_CONNECT_FBAPPKEY', Configuration::get('SOCIAL_CONNECT_FBAPPKEY')),
			'SOCIAL_CONNECT_FBAPPID' => Tools::getValue('SOCIAL_CONNECT_FBAPPID', Configuration::get('SOCIAL_CONNECT_FBAPPID'))
		);
	}
}