<?php

/*
 * 2007-2014 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2014 PrestaShop SA
 *  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class AdminJsComposerSettingController extends AdminController {

    public function __construct() {
        $this->bootstrap = true;
        $this->className = 'Configuration';
        $this->table = 'configuration';

        parent::__construct();

        $arr = array();
        $arr[] = array('id' => 'no', 'name' => 'No');
        $arr[] = array('id' => 'yes', 'name' => 'Yes');
        $tab_arr[] = array('id' => 'general', 'name' => 'General Style');
        $tab_arr[] = array('id' => 'classic', 'name' => 'Classic Style');


        $this->fields_options = array(
            'email' => array(
                'title' => $this->l('General Setting for Visual Composer'),
                'icon' => 'icon-cogs',
                'fields' => array(
                    'vc_load_flex_js' => array(
                        'title' => $this->l('Load Flexslider JS:'),
                        'desc' => $this->l('if you want to load Flexslider JS from your theme or module.'),
                        'validation' => 'isGenericName',
                        'type' => 'select',
                        'identifier' => 'id',
                        'list' => $arr
                    ),
                    'vc_load_flex_css' => array(
                        'title' => $this->l('Load Flexslider CSS:'),
                        'desc' => $this->l('if you want to load Flexslider CSS from your theme or module.'),
                        'validation' => 'isGenericName',
                        'type' => 'select',
                        'identifier' => 'id',
                        'list' => $arr
                    ),
                    'vc_load_nivo_js' => array(
                        'title' => $this->l('Load NivoSlider JS:'),
                        'desc' => $this->l('if you want to load NivoSlider JS from your theme or module.'),
                        'validation' => 'isGenericName',
                        'type' => 'select',
                        'identifier' => 'id',
                        'list' => $arr
                    ),
                    'vc_load_nivo_css' => array(
                        'title' => $this->l('Load NivoSlider CSS:'),
                        'desc' => $this->l('if you want to load NivoSlider CSS from your theme or module.'),
                        'validation' => 'isGenericName',
                        'type' => 'select',
                        'identifier' => 'id',
                        'list' => $arr
                    ),
                    'vc_product_tab_style' => array(
                        'title' => $this->l('Product Tab Style'),
                        'desc' => $this->l('you Can Change Product Tab Style.'),
                        'validation' => 'isGenericName',
                        'type' => 'select',
                        'identifier' => 'id',
                        'list' => $tab_arr
                    ),
                    'vc_include_modules' => array(
                        'title' => $this->l('Include Modules'),
                        'desc' => $this->l('You can include modules by putting modules name here(e.g. blockcms) to be appear in visual composer shortcodes list. Put a module name per line.'),
                        'type' => 'textarea',
                        'identifier' => 'id',
                        'rows' => 7,
                        'cols' => 7,
                    ),
                    'vc_exclude_modules' => array(
                        'title' => $this->l('Exclude Modules'),
                        'desc' => $this->l('You can exclude modules by putting modules name here(e.g. blockcms) to be removed from visual composer shortcodes list. Put a module name per line.'),
                        'type' => 'textarea',
                        'identifier' => 'id',
                        'rows' => 7,
                        'cols' => 7,
                    ),
                ),
                'submit' => array('title' => $this->l('Save'))
            ),
        );
//    ksort($this->fields_options['email']['fields']);
    }
    public function initPageHeaderToolbar(){
        parent::initPageHeaderToolbar();

        $this->page_header_toolbar_btn['import_tinymcejs'] = array(
             'href' => self::$currentIndex.'&action=importtinymce&token='.$this->token,
            'desc' => $this->l('Import Tinymce', null, null, false),
            'icon' => 'process-icon-import'
        );

    }
    public function initHeader()
    { 
        if(Tools::getValue('action') == 'importtinymce'){
            JsComposer::installTinymce(false);
            $redirect = $this->context->link->getAdminLink('AdminJsComposerSetting').'&importsuccess=true';
            Tools::redirectAdmin($redirect);
        }
        parent::initHeader();
    }
}
