<div class="manufacturer_slider">
    {$id = rand(000000,999999)}
  <ul id="man_slider_{$id}">
        {foreach from=$suppliers item=manufacturer name=manufacturers}
        <li>
            <a href="{$link->getmanufacturerLink($manufacturer.id_supplier, $manufacturer.link_rewrite)}"> 
                {$imgname=$manufacturer.id_supplier}
                {if !empty($man_img_size)}
                    {$imgname=$imgname|cat:'-'|cat:$man_img_size}
                {/if}
                {$imgname=$imgname|cat:'.jpg'}
                <img src="{$img_sup_dir}{$imgname}" alt="{$manufacturer.name}" title="{$manufacturer.name}"  />
            </a>
        </li>
          {/foreach}
   </ul>
</div>
<script type="text/javascript">
        
    jQuery(function($) { 
        $(window).load(function() {             
            var elem = $('#man_slider_{$id}');
            var wdi = 0;
            elem.children('li').each(function() { 
                if($(this).find('img').width() > wdi)
                    wdi = $(this).find('img').innerWidth()+10;            
             } );
            //var maxSlide = Math.floor(elem.parents('.manufacturer_slider').outerWidth(true)/wdi);
            var sliderType = "{$slider_type}";
           
           
            if(typeof $.fn.bxSlider != 'undefined' && sliderType == 'bxslider') { 
                elem.bxSlider({                    
                    slideMargin : 10,
                    controls : true,
                    infiniteLoop : true,
                    responsive : true,
                    speed : parseInt({$speed}),
                    slideWidth : wdi,
                    minSlides : 1,
                    moveSlides : 2,
                    maxSlides : parseInt({$maxslide}),
                    pager : false,                        
                    adaptiveHeight: true,
                    useCSS : false,
                    auto: true,
                    onSliderLoad: function () {
			            $('.manufacturer_slider .bx-controls-direction').hide();
			            $('.manufacturer_slider .bx-wrapper').hover(
			            function () { $('.manufacturer_slider .bx-controls-direction').fadeIn(300); },
			            function () { $('.manufacturer_slider .bx-controls-direction').fadeOut(300); }
			            );
		            }
                });
                //$('.manufacturer_slider .bx-wrapper').css('max-width','100% !important');
            }
        });            
    });
</script>