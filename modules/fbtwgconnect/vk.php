<?php
/**
 * StorePrestaModules SPM LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    StorePrestaModules SPM
 * @category social_networks
 * @package fbtwgconnect
 * @copyright Copyright StorePrestaModules SPM
 * @license   StorePrestaModules SPM
 */

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');


require_once(dirname(__FILE__).'/backward_compatibility/backward.php');


$http_referer = isset($_REQUEST['http_referer'])?$_REQUEST['http_referer']:'';



$name_module = 'fbtwgconnect';

$cookie = new Cookie('ref');
$cookie->http_referer_custom = $http_referer;


include(dirname(__FILE__).'/classes/vkhelp.class.php');



$VK_APP_ID = Configuration::get($name_module.'vci');
$VK_APP_ID = trim($VK_APP_ID);
$VK_SECRET_CODE = Configuration::get($name_module.'vcs');
$VK_SECRET_CODE = trim($VK_SECRET_CODE);
$redirect_url = Configuration::get($name_module.'vru');
$redirect_url = trim($redirect_url);

if(Tools::strlen($VK_APP_ID)==0 || Tools::strlen($VK_SECRET_CODE)==0 || Tools::strlen($redirect_url)==0){
	echo "Error: Please fill Vkontakte ID, Vkontakte Secret Key, Vkontakte Callback URL in the module settings!";
	exit;
}

 
//$VK_APP_ID = "5008967";
//$VK_SECRET_CODE = "T4wkBmNYTjIfzpf8M9cS"; // секретно, изменено
$scope = "id,first_name,last_name,email"; 


$code = Tools::getValue('code');

if($code) {
 
  $vk_grand_url = "https://api.vk.com/oauth/access_token?client_id=".$VK_APP_ID."&client_secret=".$VK_SECRET_CODE."&code=".$code."&redirect_uri=".$redirect_url;
 
  // отправляем запрос на получения access token
  $resp = Tools::file_get_contents($vk_grand_url);
  $data = Tools::jsonDecode($resp, true);
  
  
  $vk_access_token = $data['access_token'];
  $vk_uid =  $data['user_id'];
  $email_address  = $data['email'];
  
  if(!$email_address){
  	echo 'You don\'t have email in your Vkontakte Account. Please add your Email!';exit;
  }
  
  // 	обращаемся к ВК Api, получаем имя, фамилию и ID пользователя вконтакте
  // 	метод users.get
  $url_data = "https://api.vk.com/method/users.get?uids=".$vk_uid."&access_token=".$vk_access_token."&fields=".$scope;
  $res = Tools::file_get_contents($url_data); 
  $data = Tools::jsonDecode($res, true);

  $user_info = $data['response'][0];
  
  
  $first_name = $user_info['first_name'];
  $last_name = $user_info['last_name'];
  
  $data_profile = array(
  		'first_name'=>$first_name,
  		'last_name'=>$last_name,
  		'email'=>$email_address,
  
  );
  
  
  $vkhelp = new vkhelp();
  $vkhelp->userLog(
  		array(
  				'data'=>$data_profile,
  				'http_referer_custom'=>$http_referer
  		)
  );
 
  
  
  exit;
 
} else {
	
	$red_url = "https://oauth.vk.com/authorize?client_id=".$VK_APP_ID."&scope=".$scope."&redirect_uri=".$redirect_url."&response_type=code";
	Tools::redirect($red_url);
	exit;
}



?>
