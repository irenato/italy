{*
/**
 * StorePrestaModules SPM LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    StorePrestaModules SPM
 * @category social_networks
 * @package fbtwgconnect
 * @copyright Copyright StorePrestaModules SPM
 * @license   StorePrestaModules SPM
 */
*}

{if $fbtwgconnect_leftcolumnf == "leftcolumnf" || $fbtwgconnect_leftcolumng == "leftcolumng" 
    || $fbtwgconnect_leftcolumnp == "leftcolumnp" || $fbtwgconnect_leftcolumnt == "leftcolumnt" 
	|| $fbtwgconnect_leftcolumny == "leftcolumny" || $fbtwgconnect_leftcolumnl == "leftcolumnl"
	|| $fbtwgconnect_leftcolumnm == "leftcolumnm" || $fbtwgconnect_leftcolumni == "leftcolumni" 
	|| $fbtwgconnect_leftcolumnfs == "leftcolumnfs" || $fbtwgconnect_leftcolumngi == "leftcolumngi"
	|| $fbtwgconnect_leftcolumnd == "leftcolumnd" || $fbtwgconnect_leftcolumnv == "leftcolumnv"
	|| $fbtwgconnect_leftcolumna == "leftcolumna"}

{if !$fbtwgconnectislogged}

<div id="fbtwgconnect_block_left" class="block">
		<h4 class="title_block" style="text-align:left;{if $fbtwgconnectis16 == 1}margin-bottom:0px{/if}">{l s='Your account' mod='fbtwgconnect'}</h4>
		<div class="block_content">
<form action="{if $fbtwgconnectis_rewrite == 1}{$link->getPageLink('authentication', true)|escape:'html':'UTF-8'}{else}{$base_dir_ssl|escape:'htmlall':'UTF-8'}index.php?controller=authentication{/if}" method="post">
		<fieldset style="border-bottom:1px none #D0D3D8;border-top:none;background-color:#F1F2F4">
		<div class="form_content clearfix">
			<p class="text" style="padding-bottom:10px">
				<br/>
				<label for="email" style="margin-left:10px"><b>{l s='E-mail:' mod='fbtwgconnect'}</b></label>
				<br/>
				<span  style="margin-left:10px"><input type="text" id="email" name="email" 
							 value="{if isset($smarty.post.email)}{$smarty.post.email|escape:'htmlall':'UTF-8'|stripslashes}{/if}" 
							 class="account_input" style="width:14em"/>
				</span>
			</p>
			<p class="text" style="padding-bottom:10px">
				<br/>
				<label for="passwd"  style="margin-left:10px"><b>{l s='Password:' mod='fbtwgconnect'}</b></label>
				<br/>
				<span  style="margin-left:10px"><input type="password" id="passwd" name="passwd" 
							 value="{if isset($smarty.post.passwd)}{$smarty.post.passwd|escape:'htmlall':'UTF-8'|stripslashes}{/if}" 
							 class="account_input" style="width:14em"/>
				</span>
			</p>
				{if isset($back)}
					<input type="hidden" class="hidden" name="back" value="{$back|escape:'htmlall':'UTF-8'}"  style="margin-left:10px" />
				{/if}
				
				<div class="fbtwgblock-columns15">
				<input type="submit" id="SubmitLogin" name="SubmitLogin" class="button" 
						value="{l s='Log in' mod='fbtwgconnect'}" />
				<div style="clear:both"></div>
				
				
				{if $fbtwgconnect_leftcolumnf == "leftcolumnf" && $fbtwgconnectf_on == 1}
				<a href="javascript:void(0)" onclick="return fblogin();" 
				   title="Facebook" >
	   				<img src="{$fbtwgconnectfleftimg|escape:'htmlall':'UTF-8'}" alt="Facebook"  />
	 			</a>
	 			{/if}
	 			{if $fbtwgconnect_leftcolumnt == "leftcolumnt" && $fbtwgconnectt_on == 1}
	 			<a href="javascript:void(0)" title="Twitter" 
				{if $fbtwgconnecttconf == 1}
		   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/twitter.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'login', 'location,width=600,height=600,top=0'); popupWin.focus();"
		   		{else}
					onclick="alert('{$terror|escape:'htmlall':'UTF-8'}')"
				{/if}  
		   		>
						<img src="{$fbtwgconnecttleftimg|escape:'htmlall':'UTF-8'}" alt="Twitter" />
				</a>
				{/if}
				
				{if $fbtwgconnect_leftcolumna == "leftcolumna" && $fbtwgconnecta_on == 1}
				<a href="javascript:void(0)" onclick="return amazonlogin();" 
				   title="Amazon" >
	   				<img src="{$fbtwgconnectaleftimg|escape:'htmlall':'UTF-8'}" alt="Amazon"  />
	 			</a>
	 			{/if}
				
				
	 			{if $fbtwgconnect_leftcolumng == "leftcolumng" && $fbtwgconnectg_on == 1}
	 			<a href="javascript:void(0)" title="Google" 
	 			{if $fbtwgconnectgconf == 1}
		   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/login.php?p=google{if $fbtwgconnectorder_page == 1}&http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();"
			   	{else}
					onclick="alert('{$gerror|escape:'htmlall':'UTF-8'}')"
				{/if} 
		   		   >
						<img src="{$fbtwgconnectgleftimg|escape:'htmlall':'UTF-8'}" alt="Google" />
				</a>
				{/if}
				 {if $fbtwgconnect_leftcolumny == "leftcolumny" && $fbtwgconnecty_on == 1}
				<a href="javascript:void(0)" title="Yahoo"
		   			onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/login.php?p=yahoo{if $fbtwgconnectorder_page == 1}&http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=400,height=300,top=0');popupWin.focus();">
					<img src="{$fbtwgconnectyleftimg|escape:'htmlall':'UTF-8'}" alt="Yahoo"  />
				</a>
	 			{/if}
	 			
	 			{if $fbtwgconnect_leftcolumnp == "leftcolumnp" && $fbtwgconnectp_on == 1}
	 			<a href="javascript:void(0)" title="Paypal" 
			   		{if $fbtwgconnectpconf == 1} 
			   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/paypalconnect.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|escape:'htmlall':'UTF-8'}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();">
			   		{else}
						onclick="alert('{$perror|escape:'htmlall':'UTF-8'}')">
					{/if}
		 				<img src="{$fbtwgconnectpleftimg|escape:'htmlall':'UTF-8'}" alt="Paypal" />
				</a>
				{/if}
				
				{if $fbtwgconnect_leftcolumnl == "leftcolumnl" && $fbtwgconnectl_on == 1}
	 			<a href="javascript:void(0)" title="LinkedIn" 
		   		    {if $fbtwgconnectlconf == 1}  
		   		   		onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/linkedin.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();">
		   			{else}
				   		onclick="alert('{$lerror|escape:'htmlall':'UTF-8'}')">
					{/if}
		 			<img src="{$fbtwgconnectlleftimg|escape:'htmlall':'UTF-8'}" alt="LinkedIn" />
				</a>
				{/if}
				
				{if $fbtwgconnect_leftcolumnm == "leftcolumnm" && $fbtwgconnectm_on == 1}
	 			<a href="javascript:void(0)" title="Microsoft Live" 
		   		{if $fbtwgconnectmconf == 1}
		   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/microsoft.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();">
		   		{else}
		       		onclick="alert('{$merror|escape:'htmlall':'UTF-8'}')">
				 {/if}
		        
		        	<img src="{$fbtwgconnectmleftimg|escape:'htmlall':'UTF-8'}" alt="Microsoft Live" />
				</a>
				{/if}
				
				{if $fbtwgconnect_leftcolumni == "leftcolumni" && $fbtwgconnecti_on == 1}
	 			<a href="javascript:void(0)" title="Instagram"
		   		{if $fbtwgconnecticonf == 1}
		   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/instagram.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();">
		   		{else}
		       		onclick="alert('{$ierror|escape:'htmlall':'UTF-8'}')">
				 {/if}
		        
		        	<img src="{$fbtwgconnectileftimg|escape:'htmlall':'UTF-8'}" alt="Instagram" />
				</a>
				{/if}
				
				{if $fbtwgconnect_leftcolumnfs == "leftcolumnfs" && $fbtwgconnectfs_on == 1}
	 			<a href="javascript:void(0)" title="Foursquare" 
		   		{if $fbtwgconnectfsconf == 1}
		   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/foursquare.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();">
		   		{else}
		       		onclick="alert('{$fserror|escape:'htmlall':'UTF-8'}')">
				 {/if}
		        
		        	<img src="{$fbtwgconnectfsleftimg|escape:'htmlall':'UTF-8'}" alt="Foursquare" />
				</a>
				{/if}
				
				
				{if $fbtwgconnect_leftcolumngi == "leftcolumngi" && $fbtwgconnectgi_on == 1}
	 			<a href="javascript:void(0)" title="Github" 
		   		{if $fbtwgconnectgiconf == 1}
		   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/github.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();">
		   		{else}
		       		onclick="alert('{$gierror|escape:'htmlall':'UTF-8'}')">
				 {/if}
		        
		        	<img src="{$fbtwgconnectgileftimg|escape:'htmlall':'UTF-8'}" alt="Github" />
				</a>
				{/if}
				
				{if $fbtwgconnect_leftcolumnd == "leftcolumnd" && $fbtwgconnectd_on == 1}
	 			<a href="javascript:void(0)" title="Disqus"
		   		{if $fbtwgconnectdconf == 1}
		   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/disqus.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();">
		   		{else}
		       		onclick="alert('{$derror|escape:'htmlall':'UTF-8'}')">
				 {/if}
		        
		        	<img src="{$fbtwgconnectdleftimg|escape:'htmlall':'UTF-8'}" alt="Disqus" />
				</a>
				{/if}
				
				{if $fbtwgconnect_leftcolumnv == "leftcolumnv" && $fbtwgconnectv_on == 1}
	 			<a href="javascript:void(0)" title="Vkontakte" class="fbtwgconnect-last"
		   		{if $fbtwgconnectvconf == 1}
		   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/vk.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();">
		   		{else}
		       		onclick="alert('{$verror|escape:'htmlall':'UTF-8'}')">
				 {/if}
		        
		        	<img src="{$fbtwgconnectvleftimg|escape:'htmlall':'UTF-8'}" alt="Vkontakte" />
				</a>
				{/if}
				
				<div style="clear:both"></div>
				</div>
			<p class="lost_password" style="margin-top:10px;">
				<a style="margin-left:10px" href="{if $fbtwgconnectis_rewrite == 1}{$link->getPageLink('password')|escape:'html':'UTF-8'}{else}{$base_dir_ssl|escape:'htmlall':'UTF-8'}index.php?controller=password{/if}">{l s='Forgot your password?' mod='fbtwgconnect'}</a>
			</p>
			</div>
		</fieldset>
</form>
</div>
</div>

{else}
<div id="fbtwgconnect_block_left" class="block">
		<h4 class="title_block" style="text-align:left;{if $fbtwgconnectis16 == 1}margin-bottom:0px{/if}">{l s='Your account' mod='fbtwgconnect'}</h4>
		<div class="block_content" style="background-color:#F1F2F4">
		<br/>
		<p style="padding-left:10px">
			{l s='Welcome' mod='fbtwgconnect'},<br/> <b>{$customerName|escape:'htmlall':'UTF-8'}</b> (<a href="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{if $fbtwgconnectis_rewrite == 1}{$fbtwgconnectiso_lang|escape:'htmlall':'UTF-8'}{else}index.php{/if}?mylogout" 
											 title="{l s='Log out' mod='fbtwgconnect'}"
											 style="text-decoration:underline">{l s='Log out' mod='fbtwgconnect'}</a>)
		</p>
		<br/>
		
		<div style="padding-left:10px">
				<img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/views/img/icon/my-account.gif" alt="{l s='Your Account' mod='fbtwgconnect'}"/>
				<a href="{if $fbtwgconnectis_rewrite == 1}{$link->getPageLink('my-account', true)|escape:'htmlall':'UTF-8'}{else}{$base_dir_ssl|escape:'htmlall':'UTF-8'}index.php?controller=my-account{/if}" 
				   title="{l s='Your Account' mod='fbtwgconnect'}"><b>{l s='Your Account' mod='fbtwgconnect'}</b></a>
		</div>  
		 <br/> 
		<div style="padding-left:10px">
			<img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/views/img/icon/cart.gif" alt="{l s='Your Shopping Cart' mod='fbtwgconnect'}"/>
			<a href="{if $fbtwgconnectis_rewrite == 1}{$link->getPageLink('order', true)|escape:'htmlall':'UTF-8'}{else}{$base_dir_ssl|escape:'htmlall':'UTF-8'}index.php?controller=order{/if}" title="{l s='Your Shopping Cart' mod='fbtwgconnect'}"><b>{l s='Cart:' mod='fbtwgconnect'}</b></a>
			<span class="ajax_cart_quantity{if $cart_qties == 0} hidden{/if}">{$cart_qties|escape:'htmlall':'UTF-8'}</span>
			<span class="ajax_cart_product_txt{if $cart_qties != 1} hidden{/if}">{l s='product' mod='fbtwgconnect'}</span>
			<span class="ajax_cart_product_txt_s{if $cart_qties < 2} hidden{/if}">{l s='products' mod='fbtwgconnect'}</span>
			
				<span class="ajax_cart_total{if $cart_qties == 0} hidden{/if}">
					{if $priceDisplay == 1}
						{convertPrice price=$cart->getOrderTotal(false, 4)}
					{else}
						{convertPrice price=$cart->getOrderTotal(true, 4)}
					{/if}
				</span>
			<span class="ajax_cart_no_product{if $cart_qties > 0} hidden{/if}">{l s='(empty)' mod='fbtwgconnect'}</span> 
		</div>
		 <br/>
		</div>
</div>

{/if}

{/if}