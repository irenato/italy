{*
/**
 * StorePrestaModules SPM LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    StorePrestaModules SPM
 * @category social_networks
 * @package fbtwgconnect
 * @copyright Copyright StorePrestaModules SPM
 * @license   StorePrestaModules SPM
 */
*}

{if !$fbtwgconnectislogged}

{literal}
<style type="text/css">
.padding-left-logins{padding-left:5px!important;margin:0px!important}
.header_user_info_ps16{float:right;padding:9px;border-left: 1px solid #515151;}

.auth-page-txt-before-logins{font-weight:bold;color:#555454}
.padding-top-10{padding-top:10px}
.auth-page-txt-info-block{text-align:center;margin-top:20px;font-weight:bold;color:#555454}


.wrap a{text-decoration:none;opacity:1}
.wrap a:hover{text-decoration:none;opacity:0.5}
.width_fbtwgconnect{margin-top:12px}

.wrap1 a{text-decoration:none;opacity:1}
.wrap1 a:hover{text-decoration:none;opacity:0.5}
.width_fbtwgconnect1{width:40px}

.fbtwgblock-columns15{margin-top:10px;margin-left:10px}
.fbtwgblock-columns{margin-top:10px}

.fbtwgblock-columns15 a{float:left;margin-top:10px;margin-right:5px;opacity:1}
.fbtwgblock-columns15 a:hover{float:left;margin-top:10px;margin-right:5px;opacity:0.5}
.fbtwgblock-columns15 a.fbtwgconnect-last{margin-right:0px!important;}

.fbtwgblock-columns a{float:left;margin-top:10px;margin-right:5px;opacity:1}
.fbtwgblock-columns a:hover{float:left;margin-top:10px;margin-right:5px;opacity:0.5}
.fbtwgblock-columns a.fbtwgconnect-last{margin-right:0px!important;}

a.fbtwgconnect-log-in:hover{opacity:0.5}
a.fbtwgconnect-log-in{opacity:1} 
{/literal}
{if $fbtwgconnect_topf == "topf" || $fbtwgconnect_topg == "topg" || $fbtwgconnect_topp == "topp"
|| $fbtwgconnect_topt == "topt" || $fbtwgconnect_topy == "topy" || $fbtwgconnect_topl == "topl"
|| $fbtwgconnect_topm == "topm" || $fbtwgconnect_topi == "topi"
|| $fbtwgconnect_topfs == "topfs" || $fbtwgconnect_topgi == "topgi" || $fbtwgconnect_topd == "topd" || $fbtwgconnect_topv == "topv"
|| $fbtwgconnect_topa == "topa"}
{literal}
#follow-teaser  {
	background-color:#F3F3F3;
	border-bottom:none;
}
#follow-teaser .wrap {
    margin: auto;
    position: relative;
    width: auto;
	text-align:center;
	padding-bottom:10px;
}

{/literal}
{/if}

{if $fbtwgconnect_footerf == "footerf" || $fbtwgconnect_footerg == "footerg" 
    || $fbtwgconnect_footerp == "footerp" || $fbtwgconnect_footert == "footert" 
    || $fbtwgconnect_footery == "footery" || $fbtwgconnect_footerl == "footerl"
	|| $fbtwgconnect_footerm == "footerm" || $fbtwgconnect_footeri == "footeri"
	|| $fbtwgconnect_footerfs == "footerfs" || $fbtwgconnect_footergi == "footergi" || $fbtwgconnect_footerd == "footerd" || $fbtwgconnect_footerv == "footerv"
	|| $fbtwgconnect_footera == "footera"}
{literal}
#follow-teaser-footer  {
	background-color:#F3F3F3;
	border-bottom:none;
	font-weight:bold;
	padding:10px 0;
	font-size:1.05em;
	width:100%;
	margin-top:0px;
}
#follow-teaser-footer .wrap {
    margin: auto;
    position: relative;
    text-align:center
}

{/literal}
{/if}
{literal}
</style>
{/literal}
{/if}



{if !$fbtwgconnectislogged}
{literal}

<script type="text/javascript">{/literal}
{if $fbtwgconnectis16 != 1}	
{literal}
<!--
//<![CDATA[
{/literal}
{/if}

{if $fbtwgconnect_footerf == "footerf" || $fbtwgconnect_footerg == "footerg" 
    || $fbtwgconnect_footerp == "footerp" || $fbtwgconnect_footert == "footert" 
    || $fbtwgconnect_footery == "footery"  || $fbtwgconnect_footerl == "footerl"
    || $fbtwgconnect_footerm == "footerm" || $fbtwgconnect_footeri == "footeri"
    || $fbtwgconnect_footerfs == "footerfs" || $fbtwgconnect_footergi == "footergi" || $fbtwgconnect_footerd == "footerd"
    || $fbtwgconnect_footerv == "footerv" || $fbtwgconnect_footera == "footera"}
{literal}


$(document).ready(function() {
	var bottom_teaser = '<div id="follow-teaser-footer">'+
	'<div class="wrap">'+
	
	{/literal}
		{if $fbtwgconnect_footertxt == "footertxt"}
	{literal}
	 '<div class="auth-page-txt-before-logins padding-top-10">{/literal}{$fbtwgconnectauthp|escape:'quotes':'UTF-8'}{literal}</div>'+
	 {/literal}
		{/if}
	{literal}
	
	{/literal}
	{if $fbtwgconnect_footerf == "footerf" && $fbtwgconnectf_on == 1}{literal}
	'<a href="javascript:void(0)" onclick="return fblogin();" title="Facebook">'+
		'<img src="{/literal}{$fbtwgconnectffooterimg|escape:'htmlall':'UTF-8'}{literal}" class="width_fbtwgconnect" alt="Facebook"  />'+
	'</a>&nbsp;'+
	{/literal}
	{/if}
	{if $fbtwgconnect_footert == "footert" && $fbtwgconnectt_on == 1}{literal}
		'<a href="javascript:void(0)"'+
		{/literal}{if $fbtwgconnecttconf == 1}{literal} 
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/twitter.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'login\', \'location,width=600,height=600,top=0\'); popupWin.focus();"'+
		 {/literal}{else}{literal}
			  'onclick="alert(\'{/literal}{$terror|escape:'htmlall':'UTF-8'}{literal}\')"'+
		 {/literal}{/if}{literal}
			'title="Twitter">'+
			'<img src="{/literal}{$fbtwgconnecttfooterimg|escape:'htmlall':'UTF-8'}{literal}" style="margin-top:12px" alt="Twitter" />'+
		'</a>&nbsp;'+
	{/literal}
	{/if}
	
	{if $fbtwgconnect_footera == "footera" && $fbtwgconnecta_on == 1}{literal}
	'<a href="javascript:void(0)" onclick="return amazonlogin();" title="Amazon">'+
		'<img src="{/literal}{$fbtwgconnectafooterimg|escape:'htmlall':'UTF-8'}{literal}" class="width_fbtwgconnect" alt="Amazon"  />'+
	'</a>&nbsp;'+
	{/literal}
	{/if}
	
	
	{if $fbtwgconnect_footerg == "footerg" && $fbtwgconnectg_on == 1}{literal}
	'<a href="javascript:void(0)" title="Google"'+
	{/literal}{if $fbtwgconnectgconf == 1}{literal} 
	   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/login.php?p=google{/literal}{if $fbtwgconnectorder_page == 1}{literal}&http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();"'+
	  {/literal}{else}{literal}
			  'onclick="alert(\'{/literal}{$gerror|escape:'htmlall':'UTF-8'}{literal}\')"'+
	{/literal}{/if}{literal}
	   '>'+
			'<img src="{/literal}{$fbtwgconnectgfooterimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="Google" />'+
		'</a>&nbsp;'+
	{/literal}{/if}
	{if $fbtwgconnect_footery == "footery" && $fbtwgconnecty_on == 1}{literal}
	'<a href="javascript:void(0)" title="Yahoo"'+
	   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/login.php?p=yahoo{/literal}{if $fbtwgconnectorder_page == 1}{literal}&http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=400,height=300,top=0\');popupWin.focus();"'+
		'>'+
		'<img src="{/literal}{$fbtwgconnectyfooterimg|escape:'htmlall':'UTF-8'}{literal}" style="margin-top:12px" alt="Yahoo"  />'+
	'</a>&nbsp;'+
	{/literal}{/if}
	{if $fbtwgconnect_footerp == "footerp" && $fbtwgconnectp_on == 1}{literal}
	'<a href="javascript:void(0)" title="Paypal"'+
	{/literal}{if $fbtwgconnectpconf == 1}{literal} 
	   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/paypalconnect.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
    {/literal}{else}{literal}
			  'onclick="alert(\'{/literal}{$perror|escape:'htmlall':'UTF-8'}{literal}\')">'+
	 {/literal}{/if}{literal}
			'<img src="{/literal}{$fbtwgconnectpfooterimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="Paypal" />'+
		'</a>&nbsp;'+
	{/literal}{/if}
	{if $fbtwgconnect_footerl == "footerl" && $fbtwgconnectl_on == 1}{literal}
	'<a href="javascript:void(0)" title="LinkedIn"'+
	{/literal}{if $fbtwgconnectlconf == 1}{literal} 
	   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/linkedin.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
	{/literal}{else}{literal}
			  'onclick="alert(\'{/literal}{$lerror|escape:'htmlall':'UTF-8'}{literal}\')">'+
	{/literal}{/if}{literal}   
			'<img src="{/literal}{$fbtwgconnectlfooterimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="LinkedIn" />'+
		'</a>&nbsp;'+
	{/literal}{/if}
	{if $fbtwgconnect_footerm == "footerm" && $fbtwgconnectm_on == 1}{literal}
	'<a href="javascript:void(0)" title="Microsoft Live"'+
	{/literal}{if $fbtwgconnectmconf == 1}{literal} 
	   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/microsoft.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
	{/literal}{else}{literal}
			  'onclick="alert(\'{/literal}{$merror|escape:'htmlall':'UTF-8'}{literal}\')">'+
	{/literal}{/if}{literal}   
		
			'<img src="{/literal}{$fbtwgconnectmfooterimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="Microsoft Live" />'+
		'</a>&nbsp;'+
	{/literal}{/if}
	
	{if $fbtwgconnect_footeri == "footeri" && $fbtwgconnecti_on == 1}{literal}
	'<a href="javascript:void(0)" title="Instagram"'+
	{/literal}{if $fbtwgconnecticonf == 1}{literal} 
	   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/instagram.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
	{/literal}{else}{literal}
			  'onclick="alert(\'{/literal}{$ierror|escape:'htmlall':'UTF-8'}{literal}\')">'+
	{/literal}{/if}{literal}   
		
			'<img src="{/literal}{$fbtwgconnectifooterimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="Instagram" />'+
		'</a>&nbsp;'+
	{/literal}{/if}
	
	
	{if $fbtwgconnect_footerfs == "footerfs" && $fbtwgconnectfs_on == 1}{literal}
	'<a href="javascript:void(0)" title="Foursquare"'+
	{/literal}{if $fbtwgconnectfsconf == 1}{literal} 
	   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/foursquare.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
	{/literal}{else}{literal}
			  'onclick="alert(\'{/literal}{$fserror|escape:'htmlall':'UTF-8'}{literal}\')">'+
	{/literal}{/if}{literal}   
		
			'<img src="{/literal}{$fbtwgconnectfsfooterimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="Foursquare" />'+
		'</a>&nbsp;'+
	{/literal}{/if}
	
	{if $fbtwgconnect_footergi == "footergi" && $fbtwgconnectgi_on == 1}{literal}
	'<a href="javascript:void(0)" title="Github"'+
	{/literal}{if $fbtwgconnectgiconf == 1}{literal} 
	   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/github.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
	{/literal}{else}{literal}
			  'onclick="alert(\'{/literal}{$gierror|escape:'htmlall':'UTF-8'}{literal}\')">'+
	{/literal}{/if}{literal}   
		
			'<img src="{/literal}{$fbtwgconnectgifooterimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="Github" />'+
		'</a>&nbsp;'+
	{/literal}{/if}
	
	
	{if $fbtwgconnect_footerd == "footerd" && $fbtwgconnectd_on == 1}{literal}
	'<a href="javascript:void(0)" title="Disqus"'+
	{/literal}{if $fbtwgconnectdconf == 1}{literal} 
	   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/disqus.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
	{/literal}{else}{literal}
			  'onclick="alert(\'{/literal}{$derror|escape:'htmlall':'UTF-8'}{literal}\')">'+
	{/literal}{/if}{literal}   
		
			'<img src="{/literal}{$fbtwgconnectdfooterimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="Disqus" />'+
		'</a>&nbsp;'+
	{/literal}{/if}
	
	
	{if $fbtwgconnect_footerv == "footerv" && $fbtwgconnectv_on == 1}{literal}
	'<a href="javascript:void(0)" title="Vkontakte"'+
	{/literal}{if $fbtwgconnectvconf == 1}{literal} 
	   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/vk.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
	{/literal}{else}{literal}
			  'onclick="alert(\'{/literal}{$verror|escape:'htmlall':'UTF-8'}{literal}\')">'+
	{/literal}{/if}{literal}   
		
			'<img src="{/literal}{$fbtwgconnectvfooterimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="Vkontakte" />'+
		'</a>&nbsp;'+
	{/literal}{/if}
	
	{literal}
	
	
	
	'<\/div>'+ 
'<\/div>';

$('body').append(bottom_teaser);

    });
   
    
{/literal}
{/if}


{if $fbtwgconnect_topf == "topf" || $fbtwgconnect_topg == "topg" || $fbtwgconnect_topp == "topp"
	|| $fbtwgconnect_topt == "topt" || $fbtwgconnect_topy == "topy" || $fbtwgconnect_topl == "topl"
	|| $fbtwgconnect_topm == "topm" || $fbtwgconnect_topi == "topi"
	|| $fbtwgconnect_topfs == "topfs" || $fbtwgconnect_topgi == "topgi" || $fbtwgconnect_topd == "topd" || $fbtwgconnect_topv == "topv"}
{literal}


$(document).ready(function() {
	

	var top_teaser = '<div id="follow-teaser">'+
	'<div class="wrap">'+
	
	{/literal}
		{if $fbtwgconnect_toptxt == "toptxt"}
	{literal}
	 '<div class="auth-page-txt-before-logins padding-top-10">{/literal}{$fbtwgconnectauthp|escape:'quotes':'UTF-8'}{literal}</div>'+
	 {/literal}
		{/if}
	{literal}
	
	
		{/literal}{if $fbtwgconnect_topf == "topf" && $fbtwgconnectf_on == 1}{literal}
		'<a href="javascript:void(0)" onclick="return fblogin();" title="Facebook">'+
			'<img src="{/literal}{$fbtwgconnectftopimg|escape:'htmlall':'UTF-8'}{literal}" class="width_fbtwgconnect"  alt="Facebook" />'+
		'</a>&nbsp;'+
		{/literal}
		{/if}
		{if $fbtwgconnect_topt == "topt" && $fbtwgconnectt_on == 1}{literal}
			'<a href="javascript:void(0)"'+ 
			{/literal}{if $fbtwgconnecttconf == 1}{literal} 
				   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/twitter.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'login\', \'location,width=600,height=600,top=0\'); popupWin.focus();"'+
			 {/literal}{else}{literal}
						  'onclick="alert(\'{/literal}{$terror|escape:'htmlall':'UTF-8'}{literal}\')"'+
			 {/literal}{/if}{literal}
				'title="Twitter">'+
				'<img src="{/literal}{$fbtwgconnectttopimg|escape:'htmlall':'UTF-8'}{literal}" style="margin-top:12px" alt="Twitter" />'+
			'</a>&nbsp;'+
		{/literal}
		{/if}
	
		{if $fbtwgconnect_topa == "topa" && $fbtwgconnecta_on == 1}{literal}
		'<a href="javascript:void(0)" onclick="return amazonlogin();" title="Amazon">'+
			'<img src="{/literal}{$fbtwgconnectatopimg|escape:'htmlall':'UTF-8'}{literal}" class="width_fbtwgconnect"  alt="Amazon" />'+
		'</a>&nbsp;'+
		{/literal}
		{/if}
		
		
		{if $fbtwgconnect_topg == "topg" && $fbtwgconnectg_on == 1}{literal}
		'<a href="javascript:void(0)" title="Google"'+
		{/literal}{if $fbtwgconnectgconf == 1}{literal} 
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/login.php?p=google{/literal}{if $fbtwgconnectorder_page == 1}{literal}&http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();"'+
		 {/literal}{else}{literal}
				  'onclick="alert(\'{/literal}{$gerror|escape:'htmlall':'UTF-8'}{literal}\')"'+
	 	 {/literal}{/if}{literal}  
		   '>'+
				'<img src="{/literal}{$fbtwgconnectgtopimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="Google" />'+
			'</a>&nbsp;'+
		{/literal}{/if}
		{if $fbtwgconnect_topy == "topy" && $fbtwgconnecty_on == 1}{literal}
		'<a href="javascript:void(0)" title="Yahoo"'+
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/login.php?p=yahoo{/literal}{if $fbtwgconnectorder_page == 1}{literal}&http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=400,height=300,top=0\');popupWin.focus();"'+
			'>'+
			'<img src="{/literal}{$fbtwgconnectytopimg|escape:'htmlall':'UTF-8'}{literal}" style="margin-top:12px" alt="Yahoo"  />'+
		'</a>&nbsp;'+
		{/literal}{/if}
		{if $fbtwgconnect_topp == "topp" && $fbtwgconnectp_on == 1}{literal}
		'<a href="javascript:void(0)" title="Paypal"'+
			{/literal}{if $fbtwgconnectpconf == 1}{literal} 
			   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/paypalconnect.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
		    {/literal}{else}{literal}
					  'onclick="alert(\'{/literal}{$perror|escape:'htmlall':'UTF-8'}{literal}\')">'+
			 {/literal}{/if}{literal}
		'<img src="{/literal}{$fbtwgconnectptopimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="Paypal" />'+
			'</a>&nbsp;'+
		{/literal}{/if}
		{if $fbtwgconnect_topl == "topl" && $fbtwgconnectl_on == 1}{literal}
		'<a href="javascript:void(0)" title="LinkedIn"'+
		{/literal}{if $fbtwgconnectlconf == 1}{literal} 
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/linkedin.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
		{/literal}{else}{literal}
				  'onclick="alert(\'{/literal}{$lerror|escape:'htmlall':'UTF-8'}{literal}\')">'+
		{/literal}{/if}{literal}   
		  	'<img src="{/literal}{$fbtwgconnectltopimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="LinkedIn" />'+
			'</a>&nbsp;'+
		{/literal}{/if}
		{if $fbtwgconnect_topm == "topm" && $fbtwgconnectm_on == 1}{literal}
		'<a href="javascript:void(0)" title="Microsoft Live"'+

		{/literal}{if $fbtwgconnectmconf == 1}{literal} 
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/microsoft.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
		{/literal}{else}{literal}
				  'onclick="alert(\'{/literal}{$merror|escape:'htmlall':'UTF-8'}{literal}\')">'+
		{/literal}{/if}{literal}	

		'<img src="{/literal}{$fbtwgconnectmtopimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="Microsoft Live" />'+
			'</a>&nbsp;'+
		{/literal}{/if}
		
		{if $fbtwgconnect_topi == "topi" && $fbtwgconnecti_on == 1}{literal}
		'<a href="javascript:void(0)" title="Instagram"'+

		{/literal}{if $fbtwgconnecticonf == 1}{literal} 
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/instagram.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
		{/literal}{else}{literal}
				  'onclick="alert(\'{/literal}{$ierror|escape:'htmlall':'UTF-8'}{literal}\')">'+
		{/literal}{/if}{literal}	

		'<img src="{/literal}{$fbtwgconnectitopimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="Instagram" />'+
			'</a>&nbsp;'+
		{/literal}{/if}
		
		
		{if $fbtwgconnect_topfs == "topfs" && $fbtwgconnectfs_on == 1}{literal}
		'<a href="javascript:void(0)" title="Foursquare"'+

		{/literal}{if $fbtwgconnectfsconf == 1}{literal} 
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/foursquare.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
		{/literal}{else}{literal}
				  'onclick="alert(\'{/literal}{$fserror|escape:'htmlall':'UTF-8'}{literal}\')">'+
		{/literal}{/if}{literal}	

		'<img src="{/literal}{$fbtwgconnectfstopimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="Foursquare" />'+
			'</a>&nbsp;'+
		{/literal}{/if}
		
		
		{if $fbtwgconnect_topgi == "topgi" && $fbtwgconnectgi_on == 1}{literal}
		'<a href="javascript:void(0)" title="Github"'+

		{/literal}{if $fbtwgconnectgiconf == 1}{literal} 
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/github.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
		{/literal}{else}{literal}
				  'onclick="alert(\'{/literal}{$gierror|escape:'htmlall':'UTF-8'}{literal}\')">'+
		{/literal}{/if}{literal}	

		'<img src="{/literal}{$fbtwgconnectgitopimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="Github" />'+
			'</a>&nbsp;'+
		{/literal}{/if}
		
		
		{if $fbtwgconnect_topd == "topd" && $fbtwgconnectd_on == 1}{literal}
		'<a href="javascript:void(0)" title="Disqus"'+

		{/literal}{if $fbtwgconnectdconf == 1}{literal} 
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/disqus.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
		{/literal}{else}{literal}
				  'onclick="alert(\'{/literal}{$derror|escape:'htmlall':'UTF-8'}{literal}\')">'+
		{/literal}{/if}{literal}	

		'<img src="{/literal}{$fbtwgconnectdtopimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="Disqus" />'+
			'</a>&nbsp;'+
		{/literal}{/if}
		
		
		{if $fbtwgconnect_topv == "topv" && $fbtwgconnectv_on == 1}{literal}
		'<a href="javascript:void(0)" title="Vkontakte"'+

		{/literal}{if $fbtwgconnectvconf == 1}{literal} 
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/vk.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
		{/literal}{else}{literal}
				  'onclick="alert(\'{/literal}{$verror|escape:'htmlall':'UTF-8'}{literal}\')">'+
		{/literal}{/if}{literal}	

		'<img src="{/literal}{$fbtwgconnectvtopimg|escape:'htmlall':'UTF-8'}{literal}"  style="margin-top:12px" alt="Vkontakte" />'+
			'</a>&nbsp;'+
		{/literal}{/if}
		
		
		{literal}
	
	'<\/div>'+ 
'<\/div>';

$('body').prepend(top_teaser);

    });
   
    
{/literal}
{/if}

{if $blockfacebookappid != '' && $blockfacebooksecret != ''}
{literal}

$(document).ready(function(){

	//add div fb-root
	if ($('div#fb-root').length == 0)
	{
	    FBRootDom = $('<div>', {'id':'fb-root'});
	    $('body').prepend(FBRootDom);
	}

	(function(d){
        var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
        js = d.createElement('script'); js.id = id; js.async = true;
        js.src = "//connect.facebook.net/{/literal}{$fbtwgconnectlang|escape:'htmlall':'UTF-8'}{literal}/all.js";
        d.getElementsByTagName('head')[0].appendChild(js);
      }(document));
});	

	function login(){
		$.post(baseDir+'modules/fbtwgconnect/ajax.php', 
					{action:'login',
					 secret:'{/literal}{$blockfacebooksecret|escape:'htmlall':'UTF-8'}{literal}',
					 appid:'{/literal}{$blockfacebookappid|escape:'htmlall':'UTF-8'}{literal}'
					 }, 
		function (data) {
			if (data.status == 'success') {
						
				{/literal}{if $fbtwgconnectorder_page == 1}{literal}
					var url = "{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$fbtwgconnecturi|escape:'htmlall':'UTF-8'}{literal}";
				{/literal}{else}{literal}		
					var url = "{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$fbtwgconnecturi|escape:'htmlall':'UTF-8'}{literal}";
				{/literal}{/if}{literal}		
				window.location.href= url;
				
				
						
			} else {
				alert(data.message);
			}
		}, 'json');
	}
	/*function logout(){
				var url = "{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}index.php?mylogout{literal}";
				$('#fb-log-out').html('');
				$('#fb-log-out').html('Log in');
				$('#fb-fname-lname').remove();
				window.location.href= url;
	}*/
	function greet(){
	   FB.api('/me', function(response) {
		   
		var src = 'https://graph.facebook.com/'+response.id+'/picture';
		$('#header_user_info span').append('<img style="margin-left:5px" height="20" src="'+src+'"/>');
			
		{/literal}{if !$fbtwgconnectislogged}{literal}
			login();
		{/literal}{/if}{literal}
		 });
	}


	   function fblogin(){
		   
			FB.init({appId: '{/literal}{$blockfacebookappid|escape:'htmlall':'UTF-8'}{literal}', 
					status: true, 
					cookie: true, 
					xfbml: true,
		         	oauth: true});
         	
				FB.login(function(response) {
		            if (response.status == 'connected') {
			            login();
		            } else {
		                // user is not logged in
		                logout();
		            }
		        }, {scope:'email'});
		       
		        return false;
			}
	   {/literal}
{else}
{literal}
function fblogin(){
	  alert("{/literal}{$ferror|escape:'htmlall':'UTF-8'}{literal}");
	return;	
}
{/literal}
{/if}
		 
		 
		 
		 
{if $fbtwgconnectamazonci != '' && $fbtwgconnectis_ssl == 1}		   
{literal}

// amazon connect

$(document).ready(function(){

	//add div amazon-root
	if ($('div#amazon-root').length == 0)
	{
	    FBRootDomAmazon = $('<div>', {'id':'amazon-root'});
	    $('body').prepend(FBRootDomAmazon);
	}

	window.onAmazonLoginReady = function() {
   	 amazon.Login.setClientId('{/literal}{$fbtwgconnectamazonci|escape:'htmlall':'UTF-8'}{literal}');
  	};
  (function(d) {
    var a = d.createElement('script'); a.type = 'text/javascript';
    a.async = true; a.id = 'amazon-login-sdk';
    a.src = 'https://api-cdn.amazon.com/sdk/login1.js';
    d.getElementById('amazon-root').appendChild(a);
  })(document);
  
   
});

 function amazonlogin(){
 	    options = { scope : 'profile' };
	    amazon.Login.authorize(options, '{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/amazon.php');
	    return false;
 }


// amazon connect	

{/literal}
{else}
{literal}

function amazonlogin(){
		  
		  {/literal}{if $fbtwgconnectis_ssl == 0}{literal}
		  alert("{/literal}{$fbtwgconnectssltxt|escape:'htmlall':'UTF-8'}{literal}");
		  {/literal}{else}{literal}
		  	alert("{/literal}{$aerror|escape:'htmlall':'UTF-8'}{literal}");
		  {/literal}{/if}{literal}	
		 	return;
		 	
 }
 
{/literal}
{/if}


{literal}
$(document).ready(function() {

{/literal}
	{if $fbtwgconnect_authpagef == "authpagef" || $fbtwgconnect_authpaget == "authpaget"
		|| $fbtwgconnect_authpageg == "authpageg" || $fbtwgconnect_authpagey == "authpagey" 
		|| $fbtwgconnect_authpagel == "authpagel" || $fbtwgconnect_authpagem == "authpagem"
		|| $fbtwgconnect_authpagei == "authpagei" || $fbtwgconnect_authpagep == "authpagep" 
		|| $fbtwgconnect_authpagefs == "authpagefs"
		|| $fbtwgconnect_authpagegi == "authpagegi" || $fbtwgconnect_authpaged == "authpaged"
		|| $fbtwgconnect_authpagev == "authpagev" || $fbtwgconnect_authpagea == "authpagea"}
{literal}

	 var ph = '<div style="clear:both"></div><div class="wrap" style="text-align:center">'+
	 
	 {/literal}
		{if $fbtwgconnect_authpagetxt == "authpagetxt"}
	{literal}
	 '<div class="auth-page-txt-before-logins">{/literal}{$fbtwgconnectauthp|escape:'quotes':'UTF-8'}{literal}</div>'+
	 {/literal}
		{/if}
	{literal}
	 
	{/literal}
		{if $fbtwgconnect_authpagef == "authpagef" && $fbtwgconnectf_on == 1}
	{literal}
	 '<a href="javascript:void(0)" onclick="return fblogin();" title="Facebook">'+
	   '<img src="{/literal}{$fbtwgconnectfauthpageimg|escape:'htmlall':'UTF-8'}{literal}" alt="Facebook" style="margin-top:12px"  />'+
	 '<\/a>&nbsp;'+
	 {/literal}
	 	{/if}
	 {if $fbtwgconnect_authpaget == "authpaget" && $fbtwgconnectt_on == 1}{literal}
		'<a href="javascript:void(0)"'+ 
		{/literal}{if $fbtwgconnecttconf == 1}{literal} 
			   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/twitter.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'login\', \'location,width=600,height=600,top=0\'); popupWin.focus();"'+
		 {/literal}{else}{literal}
					  'onclick="alert(\'{/literal}{$terror|escape:'htmlall':'UTF-8'}{literal}\')"'+
		 {/literal}{/if}{literal}
		 'title="Twitter">'+
			'<img src="{/literal}{$fbtwgconnecttauthpageimg|escape:'htmlall':'UTF-8'}{literal}" style="margin-top:12px" alt="Twitter" />'+
		'</a>&nbsp;'+
	 {/literal}
	 {/if}
	 
	 
	 {if $fbtwgconnect_authpagea == "authpagea" && $fbtwgconnecta_on == 1}
	{literal}
	 '<a href="javascript:void(0)" onclick="return amazonlogin();" title="Amazon">'+
	   '<img src="{/literal}{$fbtwgconnectaauthpageimg|escape:'htmlall':'UTF-8'}{literal}" alt="Amazon" style="margin-top:12px"  />'+
	 '<\/a>&nbsp;'+
	 {/literal}
	 	{/if}
		 
	 {if $fbtwgconnect_authpageg == "authpageg" && $fbtwgconnectg_on == 1}
	 {literal}
	 '<a href="javascript:void(0)" title="Google"'+
	 {/literal}{if $fbtwgconnectgconf == 1}{literal} 
	   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/login.php?p=google{/literal}{if $fbtwgconnectorder_page == 1}{literal}&http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();"'+
	 {/literal}{else}{literal}
			  'onclick="alert(\'{/literal}{$gerror|escape:'htmlall':'UTF-8'}{literal}\')"'+
	{/literal}{/if}{literal} 
	   '>'+
			'<img src="{/literal}{$fbtwgconnectgauthpageimg|escape:'htmlall':'UTF-8'}{literal}" alt="Google" style="margin-top:12px" />'+
		'</a>&nbsp;'+
	 {/literal}
	 {/if}
	 {if $fbtwgconnect_authpagey == "authpagey" && $fbtwgconnecty_on == 1}{literal}
		'<a href="javascript:void(0)" title="Yahoo"'+
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/login.php?p=yahoo{/literal}{if $fbtwgconnectorder_page == 1}{literal}&http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=400,height=300,top=0\');popupWin.focus();"'+
			'>'+
			'<img src="{/literal}{$fbtwgconnectyauthpageimg|escape:'htmlall':'UTF-8'}{literal}" style="margin-top:12px" alt="Yahoo"  />'+
		'</a>&nbsp;'+
	 {/literal}{/if}
	 
	 
	 {if $fbtwgconnect_authpagep == "authpagep" && $fbtwgconnectp_on == 1}
	 {literal}
	 '<a href="javascript:void(0)" title="Paypal"'+
	 {/literal}{if $fbtwgconnectpconf == 1}{literal}
	   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/paypalconnect.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|escape:'htmlall':'UTF-8'}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
     {/literal}{else}{literal}
			  'onclick="alert(\'{/literal}{$perror|escape:'htmlall':'UTF-8'}{literal}\')">'+
	 {/literal}{/if}{literal}
  
			'<img src="{/literal}{$fbtwgconnectpauthpageimg|escape:'htmlall':'UTF-8'}{literal}" alt="Paypal" style="margin-top:12px" \/>'+
	 '<\/a>&nbsp;'+
	 {/literal}
		 {/if}
	
	 {if $fbtwgconnect_authpagel == "authpagel" && $fbtwgconnectl_on == 1}
	 {literal}
		 '<a href="javascript:void(0)" title="LinkedIn"'+
		 {/literal}{if $fbtwgconnectlconf == 1}{literal} 
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/linkedin.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
		 {/literal}{else}{literal}
				  'onclick="alert(\'{/literal}{$lerror|escape:'htmlall':'UTF-8'}{literal}\')">'+
		{/literal}{/if}{literal}  
				'<img src="{/literal}{$fbtwgconnectlauthpageimg|escape:'htmlall':'UTF-8'}{literal}" alt="LinkedIn" style="margin-top:12px" />'+
		 '</a>&nbsp;'+
	 {/literal}
	 {/if}
	 {if $fbtwgconnect_authpagem == "authpagem" && $fbtwgconnectm_on == 1}
		 {literal}
			 '<a href="javascript:void(0)" title="Microsoft Live"'+
			 
			    {/literal}{if $fbtwgconnectmconf == 1}{literal} 
				   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/microsoft.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
				{/literal}{else}{literal}
						  'onclick="alert(\'{/literal}{$merror|escape:'htmlall':'UTF-8'}{literal}\')">'+
				{/literal}{/if}{literal}

				'<img src="{/literal}{$fbtwgconnectmauthpageimg|escape:'htmlall':'UTF-8'}{literal}" alt="Microsoft Live" style="margin-top:12px" />'+
			 '</a>&nbsp;'+
		 {/literal}
	 {/if}
	 
	 {if $fbtwgconnect_authpagei == "authpagei" && $fbtwgconnecti_on == 1}
		 {literal}
			 '<a href="javascript:void(0)" title="Instagram"'+
			 
			    {/literal}{if $fbtwgconnecticonf == 1}{literal} 
				   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/instagram.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
				{/literal}{else}{literal}
						  'onclick="alert(\'{/literal}{$ierror|escape:'htmlall':'UTF-8'}{literal}\')">'+
				{/literal}{/if}{literal}

				'<img src="{/literal}{$fbtwgconnectiauthpageimg|escape:'htmlall':'UTF-8'}{literal}" alt="Instagram" style="margin-top:12px" />'+
			 '</a>&nbsp;'+
		 {/literal}
	 {/if}
	 
	 
	  {if $fbtwgconnect_authpagefs == "authpagefs" && $fbtwgconnectfs_on == 1}
		 {literal}
			 '<a href="javascript:void(0)" title="Foursquare"'+
			 
			    {/literal}{if $fbtwgconnectfsconf == 1}{literal} 
				   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/foursquare.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
				{/literal}{else}{literal}
						  'onclick="alert(\'{/literal}{$fserror|escape:'htmlall':'UTF-8'}{literal}\')">'+
				{/literal}{/if}{literal}

				'<img src="{/literal}{$fbtwgconnectfsauthpageimg|escape:'htmlall':'UTF-8'}{literal}" alt="Foursquare" style="margin-top:12px" />'+
			 '</a>&nbsp;'+
		 {/literal}
	 {/if}
	 
	  {if $fbtwgconnect_authpagegi == "authpagegi" && $fbtwgconnectgi_on == 1}
		 {literal}
			 '<a href="javascript:void(0)" title="Github"'+
			 
			    {/literal}{if $fbtwgconnectgiconf == 1}{literal} 
				   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/github.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
				{/literal}{else}{literal}
						  'onclick="alert(\'{/literal}{$gierror|escape:'htmlall':'UTF-8'}{literal}\')">'+
				{/literal}{/if}{literal}

				'<img src="{/literal}{$fbtwgconnectgiauthpageimg|escape:'htmlall':'UTF-8'}{literal}" alt="Github" style="margin-top:12px" />'+
			 '</a>&nbsp;'+
		 {/literal}
	 {/if}
	 
	  {if $fbtwgconnect_authpaged == "authpaged" && $fbtwgconnectd_on == 1}
		 {literal}
			 '<a href="javascript:void(0)" title="Disqus"'+
			 
			    {/literal}{if $fbtwgconnectdconf == 1}{literal} 
				   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/disqus.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
				{/literal}{else}{literal}
						  'onclick="alert(\'{/literal}{$derror|escape:'htmlall':'UTF-8'}{literal}\')">'+
				{/literal}{/if}{literal}

				'<img src="{/literal}{$fbtwgconnectdauthpageimg|escape:'htmlall':'UTF-8'}{literal}" alt="Disqus" style="margin-top:12px" />'+
			 '</a>&nbsp;'+
		 {/literal}
	 {/if}
	 
	 
	  {if $fbtwgconnect_authpagev == "authpagev" && $fbtwgconnectv_on == 1}
		 {literal}
			 '<a href="javascript:void(0)" title="Vkontakte"'+
			 
			    {/literal}{if $fbtwgconnectvconf == 1}{literal} 
				   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/vk.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
				{/literal}{else}{literal}
						  'onclick="alert(\'{/literal}{$verror|escape:'htmlall':'UTF-8'}{literal}\')">'+
				{/literal}{/if}{literal}

				'<img src="{/literal}{$fbtwgconnectvauthpageimg|escape:'htmlall':'UTF-8'}{literal}" alt="Vkontakte" style="margin-top:12px" />'+
			 '</a>'+
		 {/literal}
	 {/if}
	 
	 
	 {if $fbtwgconnectiauth == 1}
	 {literal}
	 '<div class="auth-page-txt-info-block">{/literal}{$fbtwgconnecttxtauthp|escape:'quotes':'UTF-8'}{literal}</div>'+
	 {/literal}
	 {/if}
	 
	 {literal}
	'<\/div>';
	
    {/literal}{if $fbtwgconnectis16 == 1}{literal}
    	$('#login_form').parent('div').after(ph);
    {/literal}{else}{literal}
    	$('#login_form').after(ph);
	{/literal}{/if}{literal}
	
	
{/literal}{/if}{literal}


{/literal}
	{if $fbtwgconnect_welcomef == "welcomef" || $fbtwgconnect_welcomet == "welcomet"
		|| $fbtwgconnect_welcomeg == "welcomeg" || $fbtwgconnect_welcomey == "welcomey" 
		|| $fbtwgconnect_welcomel == "welcomel" || $fbtwgconnect_welcomem == "welcomem"
		|| $fbtwgconnect_welcomei == "welcomei" || $fbtwgconnect_welcomep == "welcomep" || $fbtwgconnect_welcomefs == "welcomefs"
		|| $fbtwgconnect_welcomegi == "welcomegi" || $fbtwgconnect_welcomed == "welcomed" || $fbtwgconnect_welcomev == "welcomev"
		|| $fbtwgconnect_welcomea == "welcomea"}
{literal}	

    var ph_top = '&nbsp;'+
    {/literal}
		{if $fbtwgconnect_welcomef == "welcomef" && $fbtwgconnectf_on == 1}
	{literal}
       '<a class="fbtwgconnect-log-in {/literal}{if $fbtwgconnectis_ps5 == 1 && $fbtwgconnectis16 == 0}padding-left-logins{/if}{literal}" href="javascript:void(0)"  onclick="return fblogin();" title="Facebook">'+
	   '<img src="{/literal}{$fbtwgconnectfwelcomeimg|escape:'htmlall':'UTF-8'}{literal}" alt="Facebook"  />'+
	 '<\/a>&nbsp;'+
	 {/literal}
	 	{/if}
	 {if $fbtwgconnect_welcomet == "welcomet" && $fbtwgconnectt_on == 1}{literal}
		'<a href="javascript:void(0)" title="Twitter" '+ 
		{/literal}{if $fbtwgconnecttconf == 1}{literal} 
			   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/twitter.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'login\', \'location,width=600,height=600,top=0\'); popupWin.focus();"'+
		 {/literal}{else}{literal}
					  'onclick="alert(\'{/literal}{$terror|escape:'htmlall':'UTF-8'}{literal}\')"'+
		 {/literal}{/if}{literal}
		'title="Twitter" class="fbtwgconnect-log-in {/literal}{if $fbtwgconnectis_ps5 == 1}padding-left-logins{/if}{literal}">'+
	 			'<img src="{/literal}{$fbtwgconnecttwelcomeimg|escape:'htmlall':'UTF-8'}{literal}"  alt="Twitter"/>'+
		'</a>&nbsp;'+
	 {/literal}
	 {/if}
	 
	 {if $fbtwgconnect_welcomea == "welcomea" && $fbtwgconnecta_on == 1}
	{literal}
       '<a class="fbtwgconnect-log-in {/literal}{if $fbtwgconnectis_ps5 == 1}padding-left-logins{/if}{literal}" href="javascript:void(0)"  onclick="return amazonlogin();" title="Amazon">'+
	   '<img src="{/literal}{$fbtwgconnectawelcomeimg|escape:'htmlall':'UTF-8'}{literal}" alt="Amazon"  />'+
	 '<\/a>&nbsp;'+
	 {/literal}
	 	{/if}
	 
	 
	 {if $fbtwgconnect_welcomeg == "welcomeg" && $fbtwgconnectg_on == 1}
	 {literal}
	 '<a class="fbtwgconnect-log-in {/literal}{if $fbtwgconnectis_ps5 == 1}padding-left-logins{/if}{literal}" href="javascript:void(0)" title="Google" '+
	 {/literal}{if $fbtwgconnectgconf == 1}{literal} 
	   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/login.php?p=google{/literal}{if $fbtwgconnectorder_page == 1}{literal}&http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();"'+
	  {/literal}{else}{literal}
			  'onclick="alert(\'{/literal}{$gerror|escape:'htmlall':'UTF-8'}{literal}\')"'+
	  {/literal}{/if}{literal}  
	   '>'+
			'<img src="{/literal}{$fbtwgconnectgwelcomeimg|escape:'htmlall':'UTF-8'}{literal}" alt="Google"  />'+
		'</a>&nbsp;'+
	 {/literal}
	 {/if}

	 {if $fbtwgconnect_welcomey == "welcomey" && $fbtwgconnecty_on == 1}{literal}
		'<a class="fbtwgconnect-log-in {/literal}{if $fbtwgconnectis_ps5 == 1}padding-left-logins{/if}{literal}" href="javascript:void(0)" title="Yahoo" '+
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/login.php?p=yahoo{/literal}{if $fbtwgconnectorder_page == 1}{literal}&http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=400,height=300,top=0\');popupWin.focus();"'+
			'>'+
			'<img src="{/literal}{$fbtwgconnectywelcomeimg|escape:'htmlall':'UTF-8'}{literal}" alt="Yahoo"  />'+
		'</a>&nbsp;'+
	 {/literal}{/if}
		 
		 
	 {if $fbtwgconnect_welcomep == "welcomep" && $fbtwgconnectp_on == 1}
	 {literal}
	 '<a class="fbtwgconnect-log-in {/literal}{if $fbtwgconnectis_ps5 == 1}padding-left-logins{/if}{literal}" href="javascript:void(0)" title="Paypal" '+
	    {/literal}{if $fbtwgconnectpconf == 1}{literal}
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/paypalconnect.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|escape:'htmlall':'UTF-8'}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
	     {/literal}{else}{literal}
		   	  'onclick="alert(\'{/literal}{$perror|escape:'htmlall':'UTF-8'}{literal}\')">'+
		 {/literal}{/if}{literal}
	  
	  	'<img src="{/literal}{$fbtwgconnectpwelcomeimg|escape:'htmlall':'UTF-8'}{literal}" alt="Paypal" \/>'+
	 '<\/a>&nbsp;'+
	 {/literal}
		 {/if}		 
	
	{if $fbtwgconnect_welcomel == "welcomel" && $fbtwgconnectl_on == 1}
	 {literal}
	 '<a class="fbtwgconnect-log-in {/literal}{if $fbtwgconnectis_ps5 == 1}padding-left-logins{/if}{literal}" href="javascript:void(0)" title="LinkedIn" '+
		 {/literal}{if $fbtwgconnectlconf == 1}{literal} 
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/linkedin.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
		{/literal}{else}{literal}
			  'onclick="alert(\'{/literal}{$lerror|escape:'htmlall':'UTF-8'}{literal}\')">'+
		{/literal}{/if}{literal}  
				'<img src="{/literal}{$fbtwgconnectlwelcomeimg|escape:'htmlall':'UTF-8'}{literal}" alt="LinkedIn" />'+
	 '</a>&nbsp;'+
	 {/literal}
		 {/if}

	{if $fbtwgconnect_welcomem == "welcomem" && $fbtwgconnectm_on == 1}
	 {literal}
	 '<a class="fbtwgconnect-log-in {/literal}{if $fbtwgconnectis_ps5 == 1}padding-left-logins{/if}{literal}" href="javascript:void(0)" title="Microsoft Live" '+

	 	{/literal}{if $fbtwgconnectmconf == 1}{literal} 
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/microsoft.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
		{/literal}{else}{literal}
				  'onclick="alert(\'{/literal}{$merror|escape:'htmlall':'UTF-8'}{literal}\')">'+
		{/literal}{/if}{literal}
		
	   	'<img src="{/literal}{$fbtwgconnectmwelcomeimg|escape:'htmlall':'UTF-8'}{literal}" alt="Microsoft Live" />'+
	 '</a>&nbsp;'+
	 {/literal}
	 {/if}
	 
	 
	 {if $fbtwgconnect_welcomei == "welcomei" && $fbtwgconnecti_on == 1}
	 {literal}
	 '<a class="fbtwgconnect-log-in {/literal}{if $fbtwgconnectis_ps5 == 1}padding-left-logins{/if}{literal}" href="javascript:void(0)" title="Instagram" '+

	 	{/literal}{if $fbtwgconnecticonf == 1}{literal} 
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/instagram.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
		{/literal}{else}{literal}
				  'onclick="alert(\'{/literal}{$ierror|escape:'htmlall':'UTF-8'}{literal}\')">'+
		{/literal}{/if}{literal}
		
	   	'<img src="{/literal}{$fbtwgconnectiwelcomeimg|escape:'htmlall':'UTF-8'}{literal}" alt="Instagram" />'+
	 '</a>&nbsp;'+
	 {/literal}
	 {/if}
	 
	 
	 {if $fbtwgconnect_welcomefs == "welcomefs" && $fbtwgconnectfs_on == 1}
	 {literal}
	 '<a class="fbtwgconnect-log-in {/literal}{if $fbtwgconnectis_ps5 == 1}padding-left-logins{/if}{literal}" href="javascript:void(0)" title="Foursquare" '+

	 	{/literal}{if $fbtwgconnectfsconf == 1}{literal} 
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/foursquare.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
		{/literal}{else}{literal}
				  'onclick="alert(\'{/literal}{$fserror|escape:'htmlall':'UTF-8'}{literal}\')">'+
		{/literal}{/if}{literal}
		
	   	'<img src="{/literal}{$fbtwgconnectfswelcomeimg|escape:'htmlall':'UTF-8'}{literal}" alt="Foursquare" />'+
	 '</a>&nbsp;'+
	 {/literal}
	 {/if}
	 
	 
	  {if $fbtwgconnect_welcomegi == "welcomegi" && $fbtwgconnectgi_on == 1}
	 {literal}
	 '<a class="fbtwgconnect-log-in {/literal}{if $fbtwgconnectis_ps5 == 1}padding-left-logins{/if}{literal}" href="javascript:void(0)" title="Github" '+

	 	{/literal}{if $fbtwgconnectgiconf == 1}{literal} 
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/github.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
		{/literal}{else}{literal}
				  'onclick="alert(\'{/literal}{$gierror|escape:'htmlall':'UTF-8'}{literal}\')">'+
		{/literal}{/if}{literal}
		
	   	'<img src="{/literal}{$fbtwgconnectgiwelcomeimg|escape:'htmlall':'UTF-8'}{literal}" alt="Github" />'+
	 '</a>&nbsp;'+
	 {/literal}
	 {/if}
	 
	 
	  {if $fbtwgconnect_welcomed == "welcomed" && $fbtwgconnectd_on == 1}
	 {literal}
	 '<a class="fbtwgconnect-log-in {/literal}{if $fbtwgconnectis_ps5 == 1}padding-left-logins{/if}{literal}" href="javascript:void(0)" title="Disqus" '+

	 	{/literal}{if $fbtwgconnectdconf == 1}{literal} 
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/disqus.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
		{/literal}{else}{literal}
				  'onclick="alert(\'{/literal}{$derror|escape:'htmlall':'UTF-8'}{literal}\')">'+
		{/literal}{/if}{literal}
		
	   	'<img src="{/literal}{$fbtwgconnectdwelcomeimg|escape:'htmlall':'UTF-8'}{literal}" alt="Disqus" />'+
	 '</a>&nbsp;'+
	 {/literal}
	 {/if}
	 
	 
	 {if $fbtwgconnect_welcomev == "welcomev" && $fbtwgconnectv_on == 1}
	 {literal}
	 '<a class="fbtwgconnect-log-in {/literal}{if $fbtwgconnectis_ps5 == 1}padding-left-logins{/if}{literal}" href="javascript:void(0)" title="Vkontakte" '+

	 	{/literal}{if $fbtwgconnectvconf == 1}{literal} 
		   'onclick="javascript:popupWin = window.open(\'{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/vk.php{/literal}{if $fbtwgconnectorder_page == 1}{literal}?http_referer={/literal}{$fbtwgconnecthttp_referer|urlencode}{/if}{literal}\', \'openId\', \'location,width=512,height=512,top=0\');popupWin.focus();">'+
		{/literal}{else}{literal}
				  'onclick="alert(\'{/literal}{$verror|escape:'htmlall':'UTF-8'}{literal}\')">'+
		{/literal}{/if}{literal}
		
	   	'<img src="{/literal}{$fbtwgconnectvwelcomeimg|escape:'htmlall':'UTF-8'}{literal}" alt="Vkontakte" />'+
	 '</a>&nbsp;'+
	 {/literal}
	 {/if}
	 
	 {literal}
	 '';

	 if($('#header_user_info a'))
	    	$('#header_user_info a:last').after(ph_top);

    // for PS 1.6 >
    if($('.header_user_info'))
		$('.header_user_info').after('<div class="header_user_info_ps16">'+ph_top+'<\/div>');
		
{/literal}{/if}{literal}	
    });
{/literal}


{if $fbtwgconnectis16 != 1}
{literal}
	// ]]>
-->
{/literal}
{/if}
{literal}	
</script>
{/literal}


{else}


{if $fbtwgconnecttwpopup == 1  || $fbtwgconnectinpopup == 1}

<!--  show popup for twitter customer which not changed email address  -->

{literal}
<style type="text/css">
div#fb-con-wrapper {
	width: 500px;
	padding: 20px 25px;
	position: fixed;
	bottom: 50%;
	left: 50%;
	margin-left: -250px;
	z-index: 9999;
	background-color: #EEE;
	color: #444;
	border-radius: 5px;
	font-size: 14px;
	font-weight: bold;
	display: none;
	box-shadow: 0 0 27px 0 #111;
	text-align: center;
	line-height: 1em;
}

div#fb-con {
	filter: alpha(opacity=70);
	-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=70)";	
	opacity: 0.7;
	background-color: #444;	
	width: 100%;
	height: 100%;	
	cursor: pointer;
	z-index: 9998;
	position: fixed;
	bottom: 0;
	top: 0;
	left:0;
	display: none;	
}

#button-close-twitter{
    background: url("{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/views/img/fancybox.png") repeat scroll -40px 0 transparent;
    cursor: pointer;
    height: 30px;
    position: absolute;
    right: -15px;
    top: -15px;
    width: 30px;
    z-index: 1103;
}
</style>
{/literal}


{literal}
<script type="text/javascript"><!--
{/literal}
{if $fbtwgconnectis16 != 1}	
{literal}

//<![CDATA[
{/literal}
{/if}


{literal}
$(document).ready(function() {

	{/literal}{if $fbtwgconnecttwpopup == 1}{literal}

	var data = '<h4><img src="{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}modules/fbtwgconnect/views/img/settings_t.png"/>&nbsp;{/literal}{$fbtwgconnecttw_one|escape:'htmlall':'UTF-8'}{literal}</h4>'+
			   '<br/>'+
			   '<p>{/literal}{$fbtwgconnecttw_two|escape:'htmlall':'UTF-8'}{literal} </p>'+
			   '<br/>'+
			   '<label for="twitter-email">{/literal}{l s='Your e-mail' mod='fbtwgconnect'}{literal}:</label>&nbsp;<input type="text" value="" id="twitter-email" name="twitter-email">'+
			   '<br/>'+
			   '<br/>'+
			   '<a class="button" onclick="update_twitter_email();return false;" value="{/literal}{l s='Send' mod='fbtwgconnect'}{literal}"><b>{/literal}{l s='Send' mod='fbtwgconnect'}{literal}</b></a>'+
			   '';
	{/literal}{else}{literal}
	var data = '<h4><img src="{/literal}{$base_dir_ssl|escape:'UTF-8'}{literal}modules/fbtwgconnect/views/img/settings_i.png"/>&nbsp;{/literal}{$fbtwgconnectin_one|escape:'htmlall':'UTF-8'}{literal}</h4>'+
			   '<br/>'+
			   '<p>{/literal}{$fbtwgconnectin_two|escape:'htmlall':'UTF-8'}{literal} </p>'+
			   '<br/>'+
			   '<label for="instagram-email">{/literal}{l s='Your e-mail' mod='fbtwgconnect'}{literal}:</label>&nbsp;<input type="text" value="" id="instagram-email" name="instagram-email">'+
			   '<br/>'+
			   '<br/>'+
			   '<a class="button" style="margin:0 auto" onclick="update_instagram_email();return false;" value="{/literal}{l s='Send' mod='fbtwgconnect'}{literal}"><b>{/literal}{l s='Send' mod='fbtwgconnect'}{literal}</b></a>'+
			   '';
			   
	{/literal}{/if}{literal}
			   
			   
    if ($('div#fb-con-wrapper').length == 0)				
	{					
		conwrapper = '<div id="fb-con-wrapper"><\/div>';		
		$('body').append(conwrapper);				
	}
	
	if ($('div#fb-con').length == 0)				
	{					
		condom = '<div id="fb-con"><\/div>';					
		$('body').append(condom);				
	}				

	$('div#fb-con').fadeIn(function(){	
				
		$(this).css('filter', 'alpha(opacity=70)');					
		$(this).bind('click dblclick', function(){						
		$('div#fb-con-wrapper').hide();						
		$(this).fadeOut();	
		});				
	});				

		
	$('div#fb-con-wrapper').html('<a id="button-close-twitter" style="display: inline;"><\/a>'+data).fadeIn();

	$("a#button-close-twitter").click(function() {
		$('div#fb-con-wrapper').hide();
		$('div#fb-con').fadeOut();	
	});

});
	function update_twitter_email(){
   	 $('#fb-con-wrapper').css('opacity',0.8);

	var twemail = $('#twitter-email').val();
    $.post(baseDir+'modules/fbtwgconnect/twupdate.php', 
    			{cid:'{/literal}{$fbtwgconnectcid|escape:'htmlall':'UTF-8'}{literal}',
				 email:twemail 
    			 }, 
    function (data) {
    	if (data.status == 'success') {

    		$('#fb-con-wrapper').html('');
    		$('#fb-con-wrapper').html('<br/><p>'+data.params.content+'</p><br/>');
    		$('#fb-con-wrapper').css('opacity',1);
    	} else {
    		$('#fb-con-wrapper').css('opacity',1);
    		alert(data.message);
    		
    	}
    }, 'json');
    
  	 

    }
    
    
    function update_instagram_email(){
   	 $('#fb-con-wrapper').css('opacity',0.8);

	var inemail = $('#instagram-email').val();
    $.post(baseDir+'modules/fbtwgconnect/inupdate.php', 
    			{cid:'{/literal}{$fbtwgconnectcid|escape:'htmlall':'UTF-8'}{literal}',
				 email:inemail 
    			 }, 
    function (data) {
    	if (data.status == 'success') {

    		$('#fb-con-wrapper').html('');
    		$('#fb-con-wrapper').html('<br/><p>'+data.params.content+'</p><br/>');
    		$('#fb-con-wrapper').css('opacity',1);
    	} else {
    		$('#fb-con-wrapper').css('opacity',1);
    		alert(data.message);
    		
    	}
    }, 'json');
    
    }
    
	{/literal}
	
{if $fbtwgconnectis16 != 1}
{literal}
	// ]]>

{/literal}
{/if}
{literal}
--></script>
{/literal}

<!--  show popup for twitter customer which not changed email address  -->
{/if}

{/if}
