{*
/**
 * StorePrestaModules SPM LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    StorePrestaModules SPM
 * @category social_networks
 * @package fbtwgconnect
 * @copyright Copyright StorePrestaModules SPM
 * @license   StorePrestaModules SPM
 */
*}

{if $fbtwgconnect_rightcolumnf == "rightcolumnf" || $fbtwgconnect_rightcolumng == "rightcolumng" 
	|| $fbtwgconnect_rightcolumnp == "rightcolumnp" || $fbtwgconnect_rightcolumnt == "rightcolumnt" 
	|| $fbtwgconnect_rightcolumny == "rightcolumny" || $fbtwgconnect_rightcolumnl == "rightcolumnl"
	|| $fbtwgconnect_rightcolumnm == "rightcolumnm" || $fbtwgconnect_rightcolumni == "rightcolumni"
	|| $fbtwgconnect_rightcolumnfs == "rightcolumnfs" || $fbtwgconnect_rightcolumngi == "rightcolumngi"
	|| $fbtwgconnect_rightcolumnd == "rightcolumnd" || $fbtwgconnect_rightcolumnv == "rightcolumnv"
	|| $fbtwgconnect_rightcolumna == "rightcolumna"}

{if !$cookie->isLogged()}

<div class="block">
		<h4 style="text-align:left;">{l s='Your account' mod='fbtwgconnect'}</h4>
<form action="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{if $fbtwgconnectis_rewrite == 1}{$fbtwgconnectiso_lang|escape:'htmlall':'UTF-8'}{if $fbtwgconnectis15 == 1}login{else}authentication{/if}{else}authentication.php{/if}" method="post">
		<fieldset class="block_content" style="border-bottom:1px none #D0D3D8;border-top:none">
			<p class="text">
				<br/>
				<label for="email"><b>{l s='E-mail:' mod='fbtwgconnect'}</b></label>
				<span><input type="text" id="email" name="email" 
							 value="{if isset($smarty.post.email)}{$smarty.post.email|escape:'htmlall':'UTF-8'|stripslashes}{/if}" 
							 class="account_input" style="width:14em"/>
				</span>
			</p>
			<p class="text">
				<br/>
				<label for="passwd"><b>{l s='Password:' mod='fbtwgconnect'}</b></label>
				<span><input type="password" id="passwd" name="passwd" 
							 value="{if isset($smarty.post.passwd)}{$smarty.post.passwd|escape:'htmlall':'UTF-8'|stripslashes}{/if}" 
							 class="account_input" style="width:14em"/>
				</span>
			</p>
			<p class="submit">
				{if isset($back)}
					<input type="hidden" class="hidden" name="back" value="{$back|escape:'htmlall':'UTF-8'}" />
				{/if}
				<div class="fbtwgblock-columns">
				<input type="submit" id="SubmitLogin" name="SubmitLogin" class="button" 
						value="{l s='Log in' mod='fbtwgconnect'}" style="margin-left:0px;float:left"/>
				<div style="clear:both"></div>
				{if $fbtwgconnect_rightcolumnf == "rightcolumnf" && $fbtwgconnectf_on == 1}
				<a href="javascript:void(0)" onclick="return fblogin();" 
				   title="Facebook" >
	   				<img src="{$fbtwgconnectfrightimg|escape:'htmlall':'UTF-8'}" alt="Facebook"  />
	 			</a>
	 			{/if}
	 			{if $fbtwgconnect_rightcolumnt == "rightcolumnt" && $fbtwgconnectt_on == 1}
	 			<a href="javascript:void(0)" title="Twitter" 
		   		{if $fbtwgconnecttconf == 1}
		   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/twitter.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'login', 'location,width=600,height=600,top=0'); popupWin.focus();"
		   		 {else}
		   		    onclick="alert('{$terror|escape:'htmlall':'UTF-8'}')"
		   		{/if}
		   		>
						<img src="{$fbtwgconnecttrightimg|escape:'htmlall':'UTF-8'}" alt="Twitter" />
				</a>
				{/if}
				
				
				{if $fbtwgconnect_rightcolumna == "rightcolumna" && $fbtwgconnecta_on == 1}
				<a href="javascript:void(0)" onclick="return amazonlogin();" 
				   title="Amazon" >
	   				<img src="{$fbtwgconnectarightimg|escape:'htmlall':'UTF-8'}" alt="Amazon"  />
	 			</a>
	 			{/if}
				
				
	 			{if $fbtwgconnect_rightcolumng == "rightcolumng" && $fbtwgconnectg_on == 1}
	 			<a href="javascript:void(0)" title="Google"
	 			{if $fbtwgconnectgconf == 1} 
		   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/login.php?p=google{if $fbtwgconnectorder_page == 1}&http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();"
			   	{else}
		   		    onclick="alert('{$gerror|escape:'htmlall':'UTF-8'}')"
		   		{/if}  
		   		   >
						<img src="{$fbtwgconnectgrightimg|escape:'htmlall':'UTF-8'}"  alt="Google" />
				</a>
				{/if}
				{if $fbtwgconnect_rightcolumny == "rightcolumny" && $fbtwgconnecty_on == 1}
				<a href="javascript:void(0)" title="Yahoo" 
		   			onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/login.php?p=yahoo{if $fbtwgconnectorder_page == 1}&http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=400,height=300,top=0');popupWin.focus();">
					<img src="{$fbtwgconnectyrightimg|escape:'htmlall':'UTF-8'}" alt="Yahoo"  />
				</a>
	 			{/if}
	 			
	 			{if $fbtwgconnect_rightcolumnp == "rightcolumnp" && $fbtwgconnectp_on == 1}
	 			<a href="javascript:void(0)" title="Paypal" 
		   		   {if $fbtwgconnectpconf == 1} 
			   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/paypalconnect.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|escape:'htmlall':'UTF-8'}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();">
			   		{else}
			   			onclick="alert('{$perror|escape:'htmlall':'UTF-8'}')">
					{/if}
			 		<img src="{$fbtwgconnectprightimg|escape:'htmlall':'UTF-8'}" alt="Paypal" />
				</a>
				{/if}
				
				{if $fbtwgconnect_rightcolumnl == "rightcolumnl" && $fbtwgconnectl_on == 1}
	 			<a href="javascript:void(0)" title="LinkedIn" 
		   		   	{if $fbtwgconnectlconf == 1}
		   		   		onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/linkedin.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();">
		   			{else}
						onclick="alert('{$lerror|escape:'htmlall':'UTF-8'}')">
					{/if}
		 			<img src="{$fbtwgconnectlrightimg|escape:'htmlall':'UTF-8'}"  alt="LinkedIn" />
				</a>
				{/if}
				{if $fbtwgconnect_rightcolumnm == "rightcolumnm" && $fbtwgconnectm_on == 1}
	 			<a href="javascript:void(0)" title="Microsoft Live"
		   		
		   		{if $fbtwgconnectmconf == 1}
		   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/microsoft.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();">
		   		{else}
		        	onclick="alert('{$merror|escape:'htmlall':'UTF-8'}')">
				{/if}
		        
		        		<img src="{$fbtwgconnectmrightimg|escape:'htmlall':'UTF-8'}"  alt="Microsoft Live" />
				</a>
				{/if}
				
				{if $fbtwgconnect_rightcolumni == "rightcolumni" && $fbtwgconnecti_on == 1}
	 			<a href="javascript:void(0)" title="Instagram" 
		   		{if $fbtwgconnecticonf == 1}
		   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/instagram.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();">
		   		{else}
		        	onclick="alert('{$ierror|escape:'htmlall':'UTF-8'}')">
				{/if}
		        		<img src="{$fbtwgconnectirightimg|escape:'htmlall':'UTF-8'}"  alt="Instagram" />
				</a>
				{/if}
				
				
				{if $fbtwgconnect_rightcolumnfs == "rightcolumnfs" && $fbtwgconnectfs_on == 1}
	 			<a href="javascript:void(0)" title="Foursquare"
		   		{if $fbtwgconnectfsconf == 1}
		   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/foursquare.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();">
		   		{else}
		        	onclick="alert('{$fserror|escape:'htmlall':'UTF-8'}')">
				{/if}
		        		<img src="{$fbtwgconnectfsrightimg|escape:'htmlall':'UTF-8'}"  alt="Foursquare" />
				</a>
				{/if}
				
				{if $fbtwgconnect_rightcolumngi == "rightcolumngi" && $fbtwgconnectgi_on == 1}
	 			<a href="javascript:void(0)" title="Github" 
		   		{if $fbtwgconnectgiconf == 1}
		   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/github.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();">
		   		{else}
		        	onclick="alert('{$gierror|escape:'htmlall':'UTF-8'}')">
				{/if}
		        		<img src="{$fbtwgconnectgirightimg|escape:'htmlall':'UTF-8'}"  alt="Github" />
				</a>
				{/if}
				
				{if $fbtwgconnect_rightcolumnd == "rightcolumnd" && $fbtwgconnectd_on == 1}
	 			<a href="javascript:void(0)" title="Disqus" 
		   		{if $fbtwgconnectdconf == 1}
		   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/disqus.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();">
		   		{else}
		        	onclick="alert('{$derror|escape:'htmlall':'UTF-8'}')">
				{/if}
		        		<img src="{$fbtwgconnectdrightimg|escape:'htmlall':'UTF-8'}"  alt="Disqus" />
				</a>
				{/if}
				
				{if $fbtwgconnect_rightcolumnv == "rightcolumnv" && $fbtwgconnectv_on == 1}
	 			<a href="javascript:void(0)" title="Vkontakte" class="fbtwgconnect-last"
		   		{if $fbtwgconnectvconf == 1}
		   		   onclick="javascript:popupWin = window.open('{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/fbtwgconnect/vk.php{if $fbtwgconnectorder_page == 1}?http_referer={$fbtwgconnecthttp_referer|urlencode}{/if}', 'openId', 'location,width=512,height=512,top=0');popupWin.focus();">
		   		{else}
		        	onclick="alert('{$verror|escape:'htmlall':'UTF-8'}')">
				{/if}
		        		<img src="{$fbtwgconnectvrightimg|escape:'htmlall':'UTF-8'}"  alt="Vkontakte" />
				</a>
				{/if}
				
				<div style="clear:both"></div>
				</div>
			</p>
			<p class="lost_password" style="margin-top:10px;padding-left:0px">
				<a href="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{if $fbtwgconnectis_rewrite == 1}{$fbtwgconnectiso_lang|escape:'htmlall':'UTF-8'}password-recovery{else}password.php{/if}">{l s='Forgot your password?' mod='fbtwgconnect'}</a>
			</p>
		</fieldset>
</form>
</div>

{else}
<div class="block">
		<h4 style="text-align:left;">{l s='Your account' mod='fbtwgconnect'}</h4>
		<div class="block_content">
		<br/>
		<p>
			{l s='Welcome' mod='fbtwgconnect'},<br/> <b>{$customerName|escape:'htmlall':'UTF-8'}</b> (<a href="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{if $fbtwgconnectis_rewrite == 1}{$fbtwgconnectiso_lang|escape:'htmlall':'UTF-8'}{else}index.php{/if}?mylogout" 
											 title="{l s='Log out' mod='fbtwgconnect'}"
											 style="text-decoration:underline">{l s='Log out' mod='fbtwgconnect'}</a>)
		</p>
		<br/>
		
		<div>
				<img src="{$img_dir|escape:'htmlall':'UTF-8'}icon/my-account.gif" alt="{l s='Your Account' mod='fbtwgconnect'}"/>
				<a href="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{if $fbtwgconnectis_rewrite == 1}{$fbtwgconnectiso_lang|escape:'htmlall':'UTF-8'}my-account{else}my-account.php{/if}" 
				   title="{l s='Your Account' mod='fbtwgconnect'}"><b>{l s='Your Account' mod='fbtwgconnect'}</b></a>
		</div>  
		 <br/> 
		<div>
			<img src="{$img_dir|escape:'htmlall':'UTF-8'}icon/cart.gif" alt="{l s='Cart' mod='fbtwgconnect'}"/>
			<a href="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{if $fbtwgconnectis_rewrite == 1}{$fbtwgconnectiso_lang|escape:'htmlall':'UTF-8'}order{else}order.php{/if}" title="{l s='Cart' mod='fbtwgconnect'}"><b>{l s='Cart:' mod='fbtwgconnect'}</b></a>
			<span class="ajax_cart_quantity{if $cart_qties == 0} hidden{/if}">{$cart_qties|escape:'htmlall':'UTF-8'}</span>
			<span class="ajax_cart_product_txt{if $cart_qties != 1} hidden{/if}">{l s='product' mod='fbtwgconnect'}</span>
			<span class="ajax_cart_product_txt_s{if $cart_qties < 2} hidden{/if}">{l s='products' mod='fbtwgconnect'}</span>
			
				<span class="ajax_cart_total{if $cart_qties == 0} hidden{/if}">
					{if $priceDisplay == 1}
						{convertPrice price=$cart->getOrderTotal(false, 4)}
					{else}
						{convertPrice price=$cart->getOrderTotal(true, 4)}
					{/if}
				</span>
			<span class="ajax_cart_no_product{if $cart_qties > 0} hidden{/if}">{l s='(empty)' mod='fbtwgconnect'}</span> 
		</div>
		 <br/>
		</div>
</div>

{/if}

{/if}