<?php
  
class AboutCore extends ObjectModel
{
     /** @var string Name */
    public $id;

    /** @var string Name */
    public $title;

    /** @var bool Status for display */
    public $active = 'Y';

    /** @var  int category position */
    public $position;

    /** @var string Description */
    public $description;

    /** @var string Image name */
    public $image;
  
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'admin_about',
        'primary' => 'id_admin_about',
        'multilang' => false,
        'multilang_shop' => false,
        'fields' => array(
            'title' =>       array('type' => self::TYPE_STRING, 'required' => true, 'size' => 50),
            'image' =>       array('type' => self::TYPE_STRING, 'required' => false, 'size' => 255),
            'active' =>      array('type' => self::TYPE_STRING, 'required' => false, 'size' => 1),
            'position' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
            'description' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 255)
        )
    );

    public function getAboutContent()
    {
        $sql = "SELECT id_admin_about, image, title, description, position FROM admin_about WHERE active = 1";
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
    }
}