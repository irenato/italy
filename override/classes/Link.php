<?php
class Link extends LinkCore
{
      
    /*
    * module: purls
    * date: 2016-05-30 08:23:00
    * version: 1.8.2
    */
    public function psversion($part = 1) {
		$version = _PS_VERSION_;
		$exp = $explode = explode(".", $version);
		if($part == 1)
			return $exp[1];
		if($part == 2)
			return $exp[2];
		if($part == 3)
			return $exp[3];
	}
    
    
	
    /*
    * module: vipadvancedurl
    * date: 2016-07-26 08:34:17
    * version: 1.3.3
    */
    public function getCategoryLink(
        $category,
        $alias = null,
        $id_lang = null,
        $selected_filters = null,
        $id_shop = null,
        $relative_protocol = false
    ) {
        $dispatcher = Dispatcher::getInstance();
        if (!$id_lang) {
            $id_lang = Context::getContext()->language->id;
        }
        $url = $this->getBaseLink($id_shop, null, $relative_protocol).$this->getLangLink($id_lang, null, $id_shop);
        if (!is_object($category)) {
            $category = new Category($category, $id_lang);
        }
        $params = array();
        $params['id'] = $category->id;
        $params['rewrite'] = (!$alias) ? $category->link_rewrite : $alias;
        $params['meta_keywords'] =  Tools::str2url($category->getFieldByLang('meta_keywords'));
        $params['meta_title'] = Tools::str2url($category->getFieldByLang('meta_title'));
        if ($dispatcher->hasKeyword('category_rule', $id_lang, 'categories', $id_shop)) {
            $p = array();
            foreach ($category->getParentsCategories($id_lang) as $c) {
                if (!in_array($c['id_category'], Link::$category_disable_rewrite)
                    && $c['id_category'] != $category->id) {
                    $p[$c['level_depth']] = $c['link_rewrite'];
                }
            }
            $params['categories'] = implode('/', array_reverse($p));
        }
        $selected_filters = is_null($selected_filters) ? '' : $selected_filters;
        if (empty($selected_filters)) {
            $rule = 'category_rule';
        } else {
            $rule = 'layered_rule';
            $params['selected_filters'] = $selected_filters;
        }
        return $url.$dispatcher->createUrl($rule, $id_lang, $params, $this->allow, '', $id_shop);
    }
}
