<?php
class Dispatcher extends DispatcherCore
{	
    
	
	
    
	
	
    /*
    * module: vipadvancedurl
    * date: 2016-07-26 08:34:17
    * version: 1.3.3
    */
    protected $backup = array();
    /*
    * module: vipadvancedurl
    * date: 2016-07-26 08:34:17
    * version: 1.3.3
    */
    protected $advanced_dispatcher = 0;
    
    
    /*
    * module: vipadvancedurl
    * date: 2016-07-26 08:34:17
    * version: 1.3.3
    */
    public function setController($controller = '')
    {
        $this->advanced_dispatcher = $this->controller != $controller ? 1 : 0;
        $this->controller = $controller;
        $_GET['controller'] = $this->controller;
    }
    
    /*
    * module: vipadvancedurl
    * date: 2016-07-26 08:34:17
    * version: 1.3.3
    */
    public function removeRoute($route_id, $id_lang = null, $id_shop = null)
    {
        if ($id_lang === null && isset(Context::getContext()->language)) {
            $id_lang = (int)Context::getContext()->language->id;
        }
        if ($id_shop === null && isset(Context::getContext()->shop)) {
            $id_shop = (int)Context::getContext()->shop->id;
        }
        if (isset($this->routes[$id_shop][$id_lang][$route_id.'_rule'])) {
            $this->backup[$route_id.'_rule'] = $this->routes[$id_shop][$id_lang][$route_id.'_rule'];
            unset($this->routes[$id_shop][$id_lang][$route_id.'_rule']);
        } elseif (isset($this->routes[$id_lang][$route_id.'_rule'])) {
            $this->backup[$route_id.'_rule'] = $this->routes[$id_lang][$route_id.'_rule'];
            unset($this->routes[$id_lang][$route_id.'_rule']);
        }
    }
    /*
    * module: vipadvancedurl
    * date: 2016-07-26 08:34:17
    * version: 1.3.3
    */
    public function restoreRoute()
    {
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        if (isset($this->routes[$id_shop][$id_lang])) {
            $this->routes[$id_shop][$id_lang] = array_merge($this->routes[$id_shop][$id_lang], $this->backup);
        } else {
            $this->routes[$id_lang] = array_merge($this->routes[$id_lang], $this->backup);
        }
        $this->backup = array();
    }
    
    /*
    * module: vipadvancedurl
    * date: 2016-07-26 08:34:17
    * version: 1.3.3
    */
    public function dispatch()
    {
        $o = Configuration::get('VIP_ADVANCED_URL_DISPATCHER');
        if (!$o) {
            parent::dispatch();
            return;
        }
        $controller_class = '';
        $this->getController();
        if (!$this->controller) {
            $this->controller = method_exists($this, 'useDefaultController') ? $this->useDefaultController() :
                $this->default_controller;
        }
        switch ($this->front_controller) {
            case self::FC_FRONT:
                $controllers = Dispatcher::getControllers(array(_PS_FRONT_CONTROLLER_DIR_,
                    _PS_OVERRIDE_DIR_.'controllers/front/'));
                $controllers['index'] = 'IndexController';
                if (isset($controllers['auth'])) {
                    $controllers['authentication'] = $controllers['auth'];
                }
                if (isset($controllers['compare'])) {
                    $controllers['productscomparison'] = $controllers['compare'];
                }
                if (isset($controllers['contact'])) {
                    $controllers['contactform'] = $controllers['contact'];
                }
                if (!isset($controllers[Tools::strtolower($this->controller)])) {
                    $this->controller = $this->controller_not_found;
                }
                $controller_class = $controllers[Tools::strtolower($this->controller)];
                $params_hook_action_dispatcher = array('controller_type' => self::FC_FRONT,
                    'controller_class' => $controller_class, 'is_module' => 0);
                break;
            case self::FC_MODULE:
                $module_name = Validate::isModuleName(Tools::getValue('module')) ? Tools::getValue('module') : '';
                $module = Module::getInstanceByName($module_name);
                $controller_class = 'PageNotFoundController';
                if (Validate::isLoadedObject($module) && $module->active) {
                    $controllers = Dispatcher::getControllers(_PS_MODULE_DIR_.$module_name.'/controllers/front/');
                    if (isset($controllers[Tools::strtolower($this->controller)])) {
                        include_once(_PS_MODULE_DIR_.$module_name.'/controllers/front/'.$this->controller.'.php');
                        $controller_class = $module_name.$this->controller.'ModuleFrontController';
                    }
                }
                $params_hook_action_dispatcher = array('controller_type' => self::FC_FRONT,
                    'controller_class' => $controller_class, 'is_module' => 1);
                break;
            case self::FC_ADMIN:
                if (isset($this->use_default_controller) && $this->use_default_controller && !Tools::getValue('token')
                    && Validate::isLoadedObject(Context::getContext()->employee)
                    && Context::getContext()->employee->isLoggedBack()) {
                    Tools::redirectAdmin(
                        'index.php?controller='.$this->controller.'&token='.Tools::getAdminTokenLite($this->controller)
                    );
                }
                $tab = Tab::getInstanceFromClassName($this->controller, Configuration::get('PS_LANG_DEFAULT'));
                $retrocompatibility_admin_tab = null;
                if ($tab->module) {
                    if (file_exists(_PS_MODULE_DIR_.$tab->module.'/'.$tab->class_name.'.php')) {
                        $retrocompatibility_admin_tab = _PS_MODULE_DIR_.$tab->module.'/'.$tab->class_name.'.php';
                    } else {
                        $controllers = Dispatcher::getControllers(_PS_MODULE_DIR_.$tab->module.'/controllers/admin/');
                        if (!isset($controllers[Tools::strtolower($this->controller)])) {
                            $this->controller = $this->controller_not_found;
                            $controller_class = 'AdminNotFoundController';
                        } else {
                            include_once(_PS_MODULE_DIR_.$tab->module.'/controllers/admin/'.
                                $controllers[Tools::strtolower($this->controller)].'.php');
                            $controller_class = $controllers[Tools::strtolower($this->controller)]
                                .(strpos($controllers[Tools::strtolower($this->controller)], 'Controller') ? '' :
                                'Controller');
                        }
                    }
                    $params_hook_action_dispatcher = array('controller_type' => self::FC_ADMIN,
                        'controller_class' => $controller_class, 'is_module' => 1);
                } else {
                    $controllers = Dispatcher::getControllers(array(_PS_ADMIN_DIR_.'/tabs/', _PS_ADMIN_CONTROLLER_DIR_,
                        _PS_OVERRIDE_DIR_.'controllers/admin/'));
                    if (!isset($controllers[Tools::strtolower($this->controller)])) {
                        if (version_compare(_PS_VERSION_, '1.6.0.1', '>=') && Validate::isLoadedObject($tab)
                            && $tab->id_parent == 0
                            && ($tabs = Tab::getTabs(Context::getContext()->language->id, $tab->id))
                            && isset($tabs[0])) {
                            Tools::redirectAdmin(Context::getContext()->link->getAdminLink($tabs[0]['class_name']));
                        }
                        $this->controller = $this->controller_not_found;
                    }
                    $controller_class = $controllers[Tools::strtolower($this->controller)];
                    $params_hook_action_dispatcher = array('controller_type' => self::FC_ADMIN,
                        'controller_class' => $controller_class, 'is_module' => 0);
                    if (file_exists(_PS_ADMIN_DIR_.'/tabs/'.$controller_class.'.php')) {
                        $retrocompatibility_admin_tab = _PS_ADMIN_DIR_.'/tabs/'.$controller_class.'.php';
                    }
                }
                if ($retrocompatibility_admin_tab) {
                    include_once($retrocompatibility_admin_tab);
                    include_once(_PS_ADMIN_DIR_.'/functions.php');
                    runAdminTab($this->controller, !empty($_REQUEST['ajaxMode']));
                    return;
                }
                break;
            default:
                throw new PrestaShopException('Bad front controller chosen');
        }
        try {
            if (isset($params_hook_action_dispatcher)) {
                Hook::exec('actionDispatcher', $params_hook_action_dispatcher);
            }
            if ($this->advanced_dispatcher) {
                if ($this->front_controller == self::FC_FRONT) {
                    if (!isset($controllers[Tools::strtolower($this->controller)])) {
                        $this->controller = $this->controller_not_found;
                    }
                    $controller_class = $controllers[Tools::strtolower($this->controller)];
                } elseif ($this->front_controller == self::FC_MODULE) {
                    $module_name = Validate::isModuleName(Tools::getValue('module')) ? Tools::getValue('module') : '';
                    $module = Module::getInstanceByName($module_name);
                    $controller_class = 'PageNotFoundController';
                    if (Validate::isLoadedObject($module) && $module->active) {
                        $controllers = Dispatcher::getControllers(_PS_MODULE_DIR_.$module_name.'/controllers/front/');
                        if (isset($controllers[Tools::strtolower($this->controller)])) {
                            include_once(_PS_MODULE_DIR_.$module_name.'/controllers/front/'.$this->controller.'.php');
                            $controller_class = $module_name.$this->controller.'ModuleFrontController';
                        }
                    }
                }
            }
            $controller = Controller::getController($controller_class);
            $controller->run();
        } catch (PrestaShopException $e) {
            $e->displayMessage();
        }
    }
}