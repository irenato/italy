<?php


class CmsController extends CmsControllerCore
{

    /*
    * module: jscomposer
    * date: 2016-06-03 09:53:11
    * version: 4.3.15
    */
    public function display()
    {
            if((bool)Module::isEnabled('jscomposer'))
            {
                   $this->cms->content = JsComposer::do_shortcode( $this->cms->content );
                           
                   if(vc_mode() === 'page_editable'){                               
                        $this->cms->content = call_user_func(JsComposer::$front_editor_actions['vc_content'],$this->cms->content);
                           }
            }
            if((bool)Module::isEnabled('smartshortcode'))
            {
                   $smartshortcode = Module::getInstanceByName('smartshortcode');
                   $this->cms->content = $smartshortcode->parse( $this->cms->content );
            }
                    
                    return parent::display();
                    
    }
}