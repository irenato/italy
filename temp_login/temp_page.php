<?php
session_start();
$errormsg = $login_name = ''; 
$error_div = "";
if(isset($_POST['Login']) && $_POST['Login']=='Login'){
	if(isset($_POST['login_name']) && !empty($_POST['login_name'])){
		$login_name = $_POST['login_name'];
	}
	if($_POST['login_name']=='' || $_POST['login_password']==''){
		$errormsg = 'Please enter user name and password.';
	}
	if($_POST['login_name']!='nh8to9' || $_POST['login_password']!='8goto9'){
		$errormsg = 'Please enter correct user name and password.';
	}
	$error_div =  "<span style='color:red'>" . $errormsg . "</span>";
	if($_POST['login_name']=='nh8to9' && $_POST['login_password']=='8goto9'){
		$_SESSION['temp_login'] = 1;
		header('Location: ../');
		exit;
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Temporary Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
</head>
<body>
<style type="text/css">
html {
	height:100%;
}
body, div, span, h1, h2, h3, h4, h5, h6, p, a, abbr, font, img, ins, kbd, table, caption {
	margin: 0;
	padding:0;
	border: 0;
	outline: 0;
	font-weight: inherit;
	font-style: inherit;
	font-size: 100%;
	font-family: inherit;
	vertical-align: baseline;

}
input {
  -moz-appearance:none;
  -webkit-appearance:none;
  appearance:none;
  border-radius:0;
}
body{	background: url(ComingSoon.jpg) center no-repeat; background-size: cover; height: 100%; width: 100%; max-width: 100%;}
a{ padding:0!important; margin:0!important;}
.block_content li {
    display: inline-block;
    margin-right: 5px;
}
.dt {
display: table;
width: 100%;
height: 100%;
}
.under-cons-table {
	max-width: 620px;
	width : 100%;
	display: table-cell;
	vertical-align: middle;
	text-align: center;;
	height: 100%;
	margin: 0 auto;
	padding: 0;
	padding:0 15px 5px;
	font-weight:normal;

}
.under-construction {

    color: #f49b00;
    font-family: arial;
    font-size: 66px;
    letter-spacing: 2px;
    line-height: 60px;
    margin: 0;
    text-align: center;
    text-shadow: 1px 1px 0 #CCCCCC;
    text-transform: uppercase;
}

.logo{ text-align:center; text-align: center; padding-top: 10px;}

input#login:hover {
    background-color: black;
}

.powered {
	color:#333333;
	font-size:12px;
	font-weight:bold;
	font-family:Arial, Helvetica, sans-serif;
	position:relative;
	line-height:11px;
	text-align:right;
	padding-top:12px;
	padding-right:22px;
	overflow:auto;
	margin-top:5px;

}
.powered a {
	height:20px;
	width:46px;
	display:inline-block;
	float:right;
	padding-bottom:6px;
  border-radius: 3%;
}
.powered span {
	position:relative;
	float:right;

}
.under-cons-table input[type="text"], .under-cons-table input[type="password"] {
    background-color: rgba(255,255,255,.5);
    border: 1px solid rgba(0,0,0,.2);
    color:#8D8D8D;
    font-size: 12px;
    line-height: 10px;
    margin: 2px 3px 2px 0;
    padding: 8px !important;
    max-width: 250px;
    width: 100%;
    border-radius: 3%; 
}
.login form{
  text-align: center;
}
.fa{ color: black;}
.under-cons-table input[type="submit"] {
    background:  #f62459;
    border: medium none;
    border-radius: 3%;
    color: #FFFFFF;
    display: block;
    font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
    font-size: 14px;
    margin: 0 5px 5px 0;
    padding:12px 60px;
    height: 44px;
    text-align: center;
    text-transform: uppercase;
    cursor:pointer;
}
.under-cons-table img{ max-width: 100%;}
.block_content ul {
    padding: 20px 0 10px 0;
    margin: 0;
}
.block_content ul li a {
    border: 1px solid #000;
    border-radius: 50%;
    color: #fff;
    cursor: pointer;
    display: inline-block;
    font-size: 16px;
    height: 35px;
    margin-right: 15px;
    padding: 0;
    position: relative;
    text-align: center;
    text-decoration: none;
    width: 35px;
}
.block_content ul li a .fa {
    color: #000;
    font-size: 20px;
    line-height: 37px;
}
.block_content ul li a:hover, 
.block_content ul li a:active,
.block_content ul li a:focus { border-color: #f62459;}
.block_content ul li a:hover .fa, 
.block_content ul li a:active .fa,
.block_content ul li a:focus .fa { color: #f62459;}
.bottom-text {    
  position: static;
    width: 100%;
    text-align: center;
    margin-top: 2%;
  }
.content{
    max-width: 410px;
    width: 100%;
    border: 1px solid #fff;
    padding: 33px 15px;
    padding-bottom: 20px;
    margin: 0 auto;
    background: white;
}
.wrapper
{
  width: 50%;
  float: right;
}
.ver-middle{
  display: table;
    height: 100%;
    width: 100%;
}
.content table{
  width: 100%;
}

/** media query **/

@media only screen and (max-width: 768px) {
  .wrapper{
    width: 100%;
    float: none;
  }
}
@media only screen and (max-width: 767px) {

  .wrapper{
    margin: 0 auto;
  }
}
@media only screen and (max-width: 568px) {
  body{
    background-size: inherit;
  }
}
@media only screen and (max-width: 480px) {
  .content{
    width: auto;
    max-width: 100%;
  }
  .under-cons-table{
    padding: 15px;
  }
}

@media only screen and (max-width: 480px)  {
  .under-cons-table input[type="text"], .under-cons-table input[type="password"] {
    max-width: 210px;
  }
}

</style>

<div class="dt">
  <div class="ver-middle">
    <div class="under-cons-table">
    <div class="wrapper">
    <div class="content">
      <div class="logo-style"> <img src="logo-comingsoon.png" /> </div>
      <table cellpadding="10" cellspacing="0" border="0" align="center">
        <tr>
          <td height="50" align="center" valign="top">
          <div class="login" >
          <?php echo $error_div; ?>
              <form method="post" id="login_form" name="login_form">
              <table cellpadding="0" cellspacing="0" border="0" align="center" style="font-family: Arial,Helvetica,sans-serif; font-size:12px;">
                  <tr>
                    <td>
                    <input type="text" name="login_name" id="login_name" value="<?php echo $login_name; ?>" placeholder="Username" onBlur="myBlur(this)" onFocus="myFocus(this)">
                    </td>
                  </tr>
                  <tr>
                    <td style="line-height:2px">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><input type="password" name="login_password" id="login_password" placeholder="Password" value=""  onBlur="myBlur(this)" onFocus="myFocus(this)"></td>
                  </tr>
                  <tr>
                    <td align="center" style="padding-top:10px;">
                    <input type="submit" value="Login" name="Login" id="login" class="input_button" >
                    <div class="block_content">
                    <ul>
                      <li class="facebook"> <a target="_blank" href="https://www.facebook.com/Charly-Calzature-1611150852484596"> <i class="fa fa-facebook fa-2x" aria-hidden="true"></i> </a>
                      </li>                    
                        <li class="instagram"> <a target="_blank" href="https://www.instagram.com/charlyselection"> <i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>
                        </li>
                      </ul>
                    </div>
                    </td>
                  </tr>
                </table>
              </form>
            </div></td>
        </tr>
      </table>
    </div>
      <div class="bottom-text">Copyright &copy;  Charly Selection  <?php echo date('Y'); ?></div>
    </div>
    </div>
  </div>
</div>
</body>
</html>