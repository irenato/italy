<?php
  
class AdminAboutControllerCore extends AdminController
{
    public function __construct()
    {
         $this->table = 'admin_about';
         $this->className = 'About';
  		 $this->bootstrap = true;
         $this->lang = false;

        $this->fieldImageSettings = array(
            'name' => 'image',
            'dir' => 'about-img'
        );
  
        // Building the list of records stored within the "test" table
        $this->fields_list = array(
            'id_admin_about' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'title' => array(
                'title' => $this->l('Title')
            ),
            'image' => array(
                'title' => $this->l('Image')
            ),
            'description' => array(
                'title' => $this->l('Description'),
                'type' => 'string',
                // 'callback' => 'getDescriptionClean',
                'orderby' => false
            ),
            'position' => array(
                'title' => $this->l('Position'),
                'position' => 'position',
                'align' => 'center'
            ),
            'active' => array(
                'title' => $this->l('Displayed'),
                'active' => 'status',
                'type' => 'string',
                'class' => 'fixed-width-xs',
                'align' => 'center',
                'ajax' => true,
                'orderby' => false
            )
        );
  
        // This adds a multiple deletion button
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?')
            )
        );
  
        parent::__construct();
    }
  
    // This method generates the list of results
    public function renderList()
    {
        // Adds an Edit button for each result
        $this->addRowAction('add');
        $this->addRowAction('edit');
        $this->addRowAction('delete');
  
        return parent::renderList();
    }
  
    // This method generates the Add/Edit form
    public function renderForm()
    {
         if (!($about = $this->loadObject(true))) {
            return;
        }

        $path = _PS_IMG_DIR_ . 'about-img/';
        
        if(!file_exists($path))
            mkdir($path);

        $image = $path.$about->id.'.jpg';
        $image_url = ImageManager::thumbnail($image, $this->table.'_'.(int)$about->id.'.'.$this->imageType, 350,
            $this->imageType, true, true);
        $image_size = file_exists($image) ? filesize($image) / 1000 : false;

        // Building the Add/Edit form
        $this->fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l('About Section'),
                'icon' => 'icon-tags'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Name'),
                    'name' => 'title',
                    'lang' => false,
                    'required' => true,
                    'class' => 'copy2friendlyUrl',
                    'hint' => $this->l('Invalid characters:').' <>;=#{}',
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Displayed'),
                    'name' => 'active',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Description'),
                    'name' => 'description',
                    'autoload_rte' => true,
                    'lang' => false,
                    'hint' => $this->l('Invalid characters:').' <>;=#{}'
                ),
                array(
                    'type' => 'file',
                    'label' => $this->l('image'),
                    'name' => 'image',
                    'image' => $image_url ? $image_url : false,
                    'size' => $image_size,
                    'display_image' => true,
                    'col' => 6,
                    'hint' => $this->l('Upload an image.')
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'name' => 'submitAdd'
            )
        );
  
        return parent::renderForm();
    }
    
    protected function postImage($id)
    {
        if(!empty($_FILES['image']['name']))
        {
            $this->imageType = explode('.', $_FILES['image']['name'])[1];
            $filename = $id . "." . $this->imageType;
            $status = false;
            if($uploaded = $this->uploadImage($id,
             $this->fieldImageSettings['name'],
             $this->fieldImageSettings['dir'].'/'))
            {
                $sql = "UPDATE admin_about SET image = '" . $filename . "' WHERE id_admin_about = $id";
                $status = Db::getInstance()->execute($sql);
            }
            
            return $status;
        }

        return true;
    }
}