<?php
/*
* 2007-2014 CloudSelect
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@cloudselect.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade CloudSelect to newer
* versions in the future. If you wish to customize CloudSelect for your
* needs please refer to http://www.cloudselect.com for more information.
*
*  @author CloudSelect SA <contact@cloudselect.com>
*  @copyright  2007-2014 CloudSelect SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of CloudSelect SA
*/

class ThismonthController extends FrontController
{
   public $ssl = true;
   public $php_self = 'thismonth';
   protected $category;

    public function __construct()
    {
      parent::__construct();
      $this->display_column_left  = false;
      $this->display_column_right = false;
    }

   /**
    * Initialize auth controller
    * @see FrontController::init()
    */
   public function init()
   {
       // Instantiate category
      $id_category = 16;
      $this->category = new Category($id_category, $this->context->language->id);
      parent::init();


   }
   
   /**
   * Assign template vars related to page content
   * @see FrontController::initContent()
   */
   public function initContent()
   {
       //parent::initContent();
       parent::initContent();
       $this->addJS(_THEME_JS_DIR_.'index.js');

      $this->cat_products = $this->category->getProducts($this->context->language->id, 0, 10);

       $this->context->smarty->assign(array(
            'products' => $this->cat_products
       ));
       
       $this->setTemplate(_PS_THEME_DIR_.'this-month.tpl');
   }

    public function assignProductList()
    {
        // Instantiate category
        $this->category = new Category(16, $this->context->language->id);
        $this->cat_products = $this->category->getProducts($this->context->language->id, 0, 10);

        $hook_executed = false;
        Hook::exec('actionProductListOverride', array(
            'nbProducts'   => &$this->nbProducts,
            'catProducts'  => &$this->cat_products,
            'hookExecuted' => &$hook_executed,
        ));

        // The hook was not executed, standard working
        if (!$hook_executed) {
            $this->context->smarty->assign('categoryNameComplement', '');
            $this->nbProducts = $this->category->getProducts(null, null, null, $this->orderBy, $this->orderWay, true);
            $this->pagination((int)$this->nbProducts); // Pagination must be call after "getProducts"
           $this->cat_products = $this->category->getProducts($this->context->language->id, (int)$this->p, (int)$this->n, $this->orderBy, $this->orderWay);
        }
        // Hook executed, use the override
        else {
            // Pagination must be call after "getProducts"
            $this->pagination($this->nbProducts);
        }
        
        $this->addColorsToProductList($this->cat_products);

        Hook::exec('actionProductListModifier', array(
            'nb_products'  => &$this->nbProducts,
            'cat_products' => &$this->cat_products,
        ));

        foreach ($this->cat_products as &$product) {
            if (isset($product['id_product_attribute']) && $product['id_product_attribute'] && isset($product['product_attribute_minimal_quantity'])) {
                $product['minimal_quantity'] = $product['product_attribute_minimal_quantity'];
            }
        }
        
        $this->context->smarty->assign('nb_products', $this->nbProducts);
    }
}

?>
