<?php
/*
* 2007-2014 CloudSelect
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@cloudselect.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade CloudSelect to newer
* versions in the future. If you wish to customize CloudSelect for your
* needs please refer to http://www.cloudselect.com for more information.
*
*  @author CloudSelect SA <contact@cloudselect.com>
*  @copyright  2007-2014 CloudSelect SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of CloudSelect SA
*/

class AboutUsController extends FrontController
{
   public $ssl = true;
   public $php_self = 'aboutus';

   public function __construct()
   {
      parent::__construct();
      $this->display_column_left  =  false;
   }

   /**
    * Initialize auth controller
    * @see FrontController::init()
    */
   public function init()
   {
       parent::init();
   }
   
   /**
   * Assign template vars related to page content
   * @see FrontController::initContent()
   */
   public function initContent()
   {
       //parent::initContent();
       parent::initContent();

       $this->addJS(_THEME_JS_DIR_.'index.js');

       $about = new About;
       
       $this->context->smarty->assign(array(
            'contents' => $about->getAboutContent()
       ));
       
       $this->setTemplate(_PS_THEME_DIR_.'about_us.tpl');
   }

}

?>